<?php defined('_VALID_MOS') or die('Direct Access to this location is not allowed.'); ?>
<?php
$iso = preg_split("/[=]/", _ISO);
echo '<?xml version="1.0" encoding="' . $iso[1] . '"?' . '>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; <?php echo _ISO; ?>" />
        <?php mosCommonHTML::loadOverlib(); ?>
        <?php mosShowHead(); ?>
        <?php
        if ($my->id) {
            initEditor();
        }
        ?>	
        <link rel="stylesheet" type="text/css" href="<?php echo $mosConfig_live_site; ?>/templates/newshore_intranet/css/template_css.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $mosConfig_live_site; ?>/templates/newshore_intranet/css/general.css" />
    </head>

    <body>
        <div class="contentGeneral">
            <div class="idioma"><jdoc:include type="modules" name="user3" /></div>
            <div class="header">
                <div class="top" id="top">
                    Intranet newshore
                </div>
                <!--                <div class="right_intranet" id="right_intranet">
                                    <jdoc:include type="modules" name="right_intranet" style="beezDivision" headerLevel="3" />
                                </div>-->
            </div>

            <div class="left_intranet" id="left_intranet">
                <!--                       <div class="menu" id="menu">-->
                <jdoc:include type="modules" name="left_intranet" style="beezDivision" headerLevel="3" />

                <div class="left" id="left">
                    <jdoc:include type="modules" name="left" style="beezDivision" headerLevel="3" />
                </div>
                <!--                      </div>-->
            </div>
            <div class="body" id="body">
                <div class="menuImput" style="float: right;">
                    <jdoc:include type="modules" name="right_intranet" style="beezDivision" headerLevel="3" />
                </div>
                <?php mosLoadModules('user1'); ?><?php mosMainBody(); ?>
            </div>
            <script type="text/javascript">
                var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
            </script>
            <script type="text/javascript">
                try {
                    var pageTracker = _gat._getTracker("UA-8101456-1");
                    pageTracker._trackPageview();
                } catch(err) {}</script>
        </div>
    </body>
</html>
