/**
 * ES lang variables
 * 
 * Authors : Alvaro Velasco,
 *           Adolfo Sanz De Diego (asanzdiego) <asanzdiego@yahoo.es>,
 *           Carlos C Soto (eclipxe) <csoto@sia-solutions.com>
 *           Eneko Castresana Vara
 * Last Updated : July 14, 2006
 * TinyMCE Version : 2.0.6.1
 */


tinyMCE.addToLang('',{
bold_desc : 'Negrita (Ctrl+B)',
italic_desc : 'Cursiva (Ctrl+I)',
underline_desc : 'Subrayado (Ctrl+U)',
striketrough_desc : 'Tachado',
justifyleft_desc : 'Alinear a la izquierda',
justifycenter_desc : 'Alinear al centro',
justifyright_desc : 'Alinear a la derecha',
justifyfull_desc : 'Alinear justificado',
bullist_desc : 'Lista sin ordenar',
numlist_desc : 'Lista ordenada',
outdent_desc : 'Disminuir sangr&iacute;a',
indent_desc : 'Aumentar sangr&iacute;a',
undo_desc : 'Deshacer',
redo_desc : 'Rehacer',
link_desc : 'Insertar enlace',
unlink_desc : 'Quitar enlace',
image_desc : 'Insertar imagen',
cleanup_desc : 'Limpiar codigo',
focus_alert : 'Una instanacia del editor debe ser enfocada antes de usar este comando.',
edit_confirm : '&#191;Quieres usar el modo WYSIWYG (What You See Is What You Get - "lo que ves es lo que obtienes") para esta area de texto?',
insert_link_title : 'Insertar/editar enlace',
insert : 'Insertar',
update : 'Actualizar',
cancel : 'Cancelar',
insert_link_url : 'Direcci&oacute;n del enlace',
insert_link_target : 'Destino',
insert_link_target_same : 'Abrir enlace en la misma ventana',
insert_link_target_blank : 'Abrir enlace en una ventana nueva',
insert_image_title : 'Insertar/editar imagen',
insert_image_src : 'Direcci&oacute;n de la imagen',
insert_image_alt : 'Descripci&oacute;n de la imagen',
help_desc : 'Ayuda',
bold_img : "bold_es.gif",
italic_img : "italic_es.gif",
underline_img : "underline_es.gif",
clipboard_msg : 'No se pueden utilizar los comandos Copiar / Cortar / Pegar en Mozilla y en Firefox.\r\nUse el teclado Copiar (Ctrl+C) / Cortar (Ctrl+X) / Pegar (Ctrl+V)\r\n&#191;Quieres obtener m&aacute;s informaci&oacute;n?',
popup_blocked : 'Lo siento, pero tu bloqueador de popups ha desactivado una ventana que ha lanzado la aplicaci&oacute;n. Debes desactivar tu bloqueador de popups para tener toda la funcionalad de la aplicaci&oacute;n.'
});


tinyMCE.addI18n({en:{
common:{
edit_confirm:"Do you want to use the WYSIWYG mode for this textarea?",
apply:"Apply",
insert:"Insert",
update:"Update",
cancel:"Cancel",
close:"Close",
browse:"Browse",
class_name:"Class",
not_set:"-- Not set --",
clipboard_msg:"Copy/Cut/Paste is not available in Mozilla and Firefox.\nDo you want more information about this issue?",
clipboard_no_support:"Currently not supported by your browser, use keyboard shortcuts instead.",
popup_blocked:"Sorry, but we have noticed that your popup-blocker has disabled a window that provides application functionality. You will need to disable popup blocking on this site in order to fully utilize this tool.",
invalid_data:"Error: Invalid values entered, these are marked in red.",
more_colors:"More colors"
},
contextmenu:{
align:"Alignment",
left:"Left",
center:"Center",
right:"Right",
full:"Full"
},
insertdatetime:{
date_fmt:"%Y-%m-%d",
time_fmt:"%H:%M:%S",
insertdate_desc:"Insert date",
inserttime_desc:"Insert time",
months_long:"January,February,March,April,May,June,July,August,September,October,November,December",
months_short:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec",
day_long:"Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday",
day_short:"Sun,Mon,Tue,Wed,Thu,Fri,Sat,Sun"
},
print:{
print_desc:"Print"
},
preview:{
preview_desc:"Preview"
},
directionality:{
ltr_desc:"Direction left to right",
rtl_desc:"Direction right to left"
},
layer:{
insertlayer_desc:"Insert new layer",
forward_desc:"Move forward",
backward_desc:"Move backward",
absolute_desc:"Toggle absolute positioning",
content:"New layer..."
},
save:{
save_desc:"Save",
cancel_desc:"Cancel all changes"
},
nonbreaking:{
nonbreaking_desc:"Insert non-breaking space character"
},
iespell:{
iespell_desc:"Run spell checking",
download:"ieSpell not detected. Do you want to install it now?"
},
advhr:{
advhr_desc:"Horizontal rule"
},
emotions:{
emotions_desc:"Emotions"
},
searchreplace:{
search_desc:"Find",
replace_desc:"Find/Replace"
},
advimage:{
image_desc:"Insert/edit image"
},
advlink:{
link_desc:"Insert/edit link"
},
xhtmlxtras:{
cite_desc:"Citation",
abbr_desc:"Abbreviation",
acronym_desc:"Acronym",
del_desc:"Deletion",
ins_desc:"Insertion",
attribs_desc:"Insert/Edit Attributes"
},
style:{
desc:"Edit CSS Style"
},
paste:{
paste_text_desc:"Paste as Plain Text",
paste_word_desc:"Paste from Word",
selectall_desc:"Select All"
},
paste_dlg:{
text_title:"Use CTRL+V on your keyboard to paste the text into the window.",
text_linebreaks:"Keep linebreaks",
word_title:"Use CTRL+V on your keyboard to paste the text into the window."
},
table:{
desc:"Inserts a new table",
row_before_desc:"Insert row before",
row_after_desc:"Insert row after",
delete_row_desc:"Delete row",
col_before_desc:"Insert column before",
col_after_desc:"Insert column after",
delete_col_desc:"Remove column",
split_cells_desc:"Split merged table cells",
merge_cells_desc:"Merge table cells",
row_desc:"Table row properties",
cell_desc:"Table cell properties",
props_desc:"Table properties",
paste_row_before_desc:"Paste table row before",
paste_row_after_desc:"Paste table row after",
cut_row_desc:"Cut table row",
copy_row_desc:"Copy table row",
del:"Delete table",
row:"Row",
col:"Column",
cell:"Cell"
},
autosave:{
unload_msg:"The changes you made will be lost if you navigate away from this page."
},
fullscreen:{
desc:"Toggle fullscreen mode"
},
media:{
desc:"Insert / edit embedded media",
edit:"Edit embedded media"
},
fullpage:{
desc:"Document properties"
},
template:{
desc:"Insert predefined template content"
},
visualchars:{
desc:"Visual control characters on/off."
},
spellchecker:{
desc:"Toggle spellchecker",
menu:"Spellchecker settings",
ignore_word:"Ignore word",
ignore_words:"Ignore all",
langs:"Languages",
wait:"Please wait...",
sug:"Suggestions",
no_sug:"No suggestions",
no_mpell:"No misspellings found."
},
pagebreak:{
desc:"Insert page break."
}}});