<?php
/**
 * AutoThumb for DOCman 1.4.x
 * @version $Id: CHANGELOG.php 802 2009-02-13 12:25:59Z mathias $
 * @package AutoThumb
 * @copyright (C) 2007-2009 Joomlatools
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomlatools.eu/ Official website
 **/
defined('_VALID_MOS') or die('Restricted access');
?>

Joomlatools AutoThumb for DOCman Changelog
------------------------------------------

Legend
    * -> Security Fix
    # -> Bug Fix
    + -> Addition
    ^ -> Change
    - -> Removed
    ! -> Note

----- 2009-02-14 Released v1.4.1 -----

2009-02-12 Mathias Verraes
 # Fixed issues with uppercase file extensions
 # Fixed bug when saving config
 # Upgraded to phpthumb 1.7.9
 ^ Rebranded to Joomlatools

2007-12-13 Mjaz
 ! Released v1.4.0 stable

2007-11-28 Mjaz
 + Added 'Clear thumbnails' feature

2007-11-19 Mjaz
 # Some fixes

2007-11-15 Mjaz
 ! First release, v1.4.0RC (based on the old AutoThumb component)