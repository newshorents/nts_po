<?php
/**
 * AutoThumb for DOCman 1.4.x
 * @version $Id: README.php 798 2009-02-12 14:42:04Z mathias $
 * @package AutoThumb
 * @copyright (C) 2007-2009 Joomlatools
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomlatools.eu/ Official website
 **/
defined('_VALID_MOS') or die('Restricted access');;
?>

Joomlatools AutoThumb Plugin for DOCman
----------------------------------------

Joomla! plugin to generate thumbnails for images in DOCMan.

Once installed and configured, there's nothing you need to do. Whenever
browsing the DOCman frontend, thumbnails will be added to the documents,
wherever possible.

http://www.joomlatools.eu

Always download from the official site, so you know they haven't been tampered with.

Installation
------------
Use Joomla's mambot/plugin installer. Don't forget to publish this mambot.

Go to Components -> DOCman -> Theme, select your theme and make sure thumbnails
are active. In the default theme, this is done by setting 'Document Image' to
Thumbnail.

Logging
-------
You can log all actions and errors by turning on Logging in the mambot/plugin
configuration. Please be aware that the log file will get very big, so only use
this when needed, eg to find out why some things aren't working. The log file is
located in mambots/docman/autothumb/autothumb.log (Joomla! 1.0.x) or
plugins/docman/autothumb/autothumb.log (Joomla! 1.5)

Formats
-------
AutoThumb can generate thumbnails for over a hundred formats, depending on the
libraries installed on your system. eg PDF's require you to have Imagick and
Ghostscript.
See http://imagemagick.org/script/formats.php
