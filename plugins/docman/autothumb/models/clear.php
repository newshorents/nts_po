<?php
/**
 * AutoThumb for DOCman 1.4.x
 * @version $Id: clear.php 793 2009-02-12 12:40:26Z mathias $
 * @package AutoThumb
 * @copyright (C) 2007-2009 Joomlatools
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomlatools.eu/ Official website
 **/
defined('_VALID_MOS') or die('Restricted access');

/**
 * @static
 */
class ATClear {
    function clear()
    {
        global $database;
        $query = "UPDATE #__docman SET dmthumbnail = ''";
        $database->setQuery($query);
        return $database->query();
    }
}

