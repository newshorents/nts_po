<?php
/**
 * AutoThumb for DOCman 1.4.x
 * @version $Id: config.php 797 2009-02-12 13:21:40Z mathias $
 * @package AutoThumb
 * @copyright (C) 2007-2009 Joomlatools
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomlatools.eu/ Official website
 **/
defined('_VALID_MOS') or die('Restricted access');

class ATConfig extends mosParameters {

    /**
     * @constructor
     */
    function ATConfig()
    {
        global $database;

        $plugin = new mosMambot( $database );
        $plugin->load( $this->_getMambotId() );

        if(defined('_DM_J15')) {
            parent::__construct( $plugin->params );
        } else {
            parent::mosParameters( $plugin->params );
        }

    }

    function & getInstance()
    {
        static $instance;

        if( !isset($instance)) {
            $instance = new ATConfig();
        }

        return $instance;
    }

    function _getMambotId()
    {
        global $database;

        static $result;
        if(!isset($result))
        {
        	if(defined('_DM_J15')) {
                $query = "SELECT id FROM #__plugins WHERE element = 'autothumb' AND folder = 'docman'";
            } else {
                $query = "SELECT id FROM #__mambots WHERE element = 'autothumb' AND folder = 'docman'";
            }
            $database->setQuery( $query );
            $result = $database->loadResult();
        }
        return $result;
    }

    function store()
    {
    	global $database;

        $tmp = array();
		$arr = (array) $this->toArray();    
        foreach($arr as $k=>$v)
        {
        	$tmp[] = "$k=$v";
        }
        $attribs = implode( "\n", $tmp );

        $id = $this->_getMambotId();
        $plugin = new mosMambot( $database );
        $plugin->load($id);

        if(defined('_DM_J15')) {
            $query = "UPDATE #__plugins SET params='$attribs' WHERE id = $id";
        } else {
            $query = "UPDATE #__mambots SET params='$attribs' WHERE id = $id";
        }
        $database->setQuery( $query );
        return $database->query();
    }

}