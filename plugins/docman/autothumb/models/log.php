<?php
/**
 * AutoThumb for DOCman 1.4.x
 * @version $Id: log.php 796 2009-02-12 12:58:14Z mathias $
 * @package AutoThumb
 * @copyright (C) 2007-2009 Joomlatools
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomlatools.eu/ Official website
 **/
defined('_VALID_MOS') or die('Restricted access');

/**
 * @static
 */
class ATLog {
    function _handle() {
        static $handle;
        if(!isset($handle)) {
            $handle = fopen(_AT_PATH_LOG, 'a');
        }
        return $handle;
    }

    /**
     * Write a string to the log, if logging is turned on in defines.php
     */
    function _($string){
        fwrite(ATLog::_handle(), $string."\n");
    }
}