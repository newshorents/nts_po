<?php
/**
 * AutoThumb for DOCman 1.4.x
 * @version $Id: defines.php 798 2009-02-12 14:42:04Z mathias $
 * @package AutoThumb
 * @copyright (C) 2007-2009 Joomlatools
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomlatools.eu/ Official website
 **/
defined('_VALID_MOS') or die('Restricted access');;

// Version
define('_AT_VERSION', '1.4.1');

// Globals
global $mosConfig_live_site, $mosConfig_absolute_path;

// Paths
if ( !defined('DS') ) define('DS', DIRECTORY_SEPARATOR);
define('_AT_PATH', dirname(__FILE__));
define('_AT_PATH_LANGUAGES', _AT_PATH.DS.'languages');
define('_AT_PATH_IMAGES', $mosConfig_absolute_path . '/images/stories');
define('_AT_PATH_LOG', _AT_PATH.DS.'autothumb.log');
define('_AT_PATH_MODELS', _AT_PATH.DS.'models');
define('_AT_PATH_LIBRARIES', _AT_PATH.DS.'libraries');

// Urls
define('_AT_URL_IMAGES', $mosConfig_live_site. '/images/stories');

// Filetypes
define('_AT_FILETYPE_LIST', 'art,avi,avs,bmp,cgm,cin,cmyk,cmyka,cr2,crw,cur,cut,'
    .'dcm,dcr,dcx,dib,djvu,dng,dpx,emf,epdf,epi,eps,epsf,epsi,ept,exr,fax,fig,'
    .'fits,fpx,gif,gplt,gray,hpgl,htm,html,ico,icon,jbig,bie,jbg,jng,jp2,jpc,'
    .'jpeg,jpg,man,mat,miff,mono,mng,mpeg,m2v,mpc,mrw,msl,mtv,mvg,nef,orf,otb,p7,'
    .'palm,pam,pbm,pcd,pcds,pcx,pdb,pdf,pef,pfa,pfb,pfm,pgm,picon,pict,pix,png,'
    .'pnm,ppm,ps,ps2,ps3,psd,ptif,pwp,rad,raf,rgb,rgba,rla,rle,sct,sfw,sgi,shtml,'
    .'sun,svg,tga,tiff,tif,tim,ttf,txt,uyvy,vicar,viff,wbmp,wmf,wpg,x,xbm,xcf,'
    .'xpm,pm,xwd,x3f,ycbcr,ycbcra,yuv');