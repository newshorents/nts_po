<?php
/**
 * AutoThumb for DOCman 1.4.x
 * @version $Id: autothumb.php 797 2009-02-12 13:21:40Z mathias $
 * @package AutoThumb
 * @copyright (C) 2007-2009 Joomlatools
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link http://www.joomlatools.eu/ Official website
 **/
defined('_VALID_MOS') or die('Restricted access');

// requires
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'autothumb'.DIRECTORY_SEPARATOR.'defines.php');
require_once(_AT_PATH_MODELS.DS.'config.php');

// register mambots
$_MAMBOTS->registerFunction( 'onFetchDocument', 'AT_onFetchDocument' );

function AT_onFetchDocument($params) {
    global $_DOCMAN, $mosConfig_absolute_path;

    // DOCman config
    $dmpath = $_DOCMAN->getCfg('dmpath', $mosConfig_absolute_path.DS.'dmdocuments');

    // config
    $config = & ATConfig::getInstance();
    if( $log = $config->get('log', 0)) {
    	require_once(_AT_PATH_MODELS.DS.'log.php');
    }

    // clear thumbs
    if($config->get('clear', 0))
    {
    	require_once(_AT_PATH_MODELS.DS.'clear.php');
        ATClear::clear();
        $config->set('clear', 0);
        $config->store();
        $log ? ATLog::_("\nCleared thumbnails.") : null;
    }

    // Parameters
    $id     = $params['id'];
    $type   = $params['type'];

    // start logging if is set in config
    $log ? ATLog::_("\nDocument ID: $id | Action: $type | Date: ".date("Y-m-d H:i:s")) : null;

    // get the document instance
    $doc = & DOCMAN_Document::getInstance($id);

    // check for existing thumbnail
    if($doc->objDBTable->dmthumbnail) {
        $log ? ATLog::_('Document already has a thumbnail') : null;
    	return;
    }

    // dmfilename exists?
    if(!file_exists($dmpath)) {
        $log ? ATLog::_("Path $dmpath doesn't exist.") : null;
        return;
    }
    if(!file_exists($dmpath.DS.$doc->objDBTable->dmfilename)) {
        $log ? ATLog::_("File {$doc->objDBTable->dmfilename} doesn't exist.") : null;
        return;
    }

    // check extension
    $extensions = $config->get('extensions', _AT_FILETYPE_LIST);
    if(!in_array(strtolower($doc->objFormatData->filetype), explode(',', $extensions))) {
    	$log ? ATLog::_("Extension {$doc->objFormatData->filetype} not in list ($extensions)") : null;
        return;
    }

    // Target path writable?
    if(!is_writable(_AT_PATH_IMAGES)) {
    	$log ? ATLog::_("Path "._AT_PATH_IMAGES." is not writable") : null;
        return;
    }

    // build target filename
    $fstruct        = $config->get('filename_structure', '~autothumb.{FILE}.{TIME}');
    $output_format  = $config->get('output_format', 'png');
    $search         = array('{FILE}', '{TIME}');
    $replace        = array($doc->objDBTable->dmfilename, date("U"));
    $target = _AT_PATH_IMAGES.DS.str_replace($search, $replace, $fstruct) .".$output_format";

    // phpThumb
    require_once(_AT_PATH_LIBRARIES.DS.'phpthumb'.DS.'phpthumb.class.php');
    $phpThumb  = new phpThumb();

    // parameters
    $phpThumb->setSourceFilename( $dmpath.DS.$doc->objDBTable->dmfilename );
    $phpThumb->setParameter('w',    $config->get('width', 64));
    $phpThumb->setParameter('h',    $config->get('height', 64));
    $phpThumb->setParameter('far',  'C');
    $phpThumb->setParameter('f',    $config->get('output_format', 'png'));
    $phpThumb->setParameter('q',    $config->get('jpeg_quality', 75));
    $phpThumb->setParameter('bg',   $config->get('background_color', 'FFFFFF'));
    if ($config->get('grayscale', 0)) {
        $phpThumb->setParameter('fltr', 'gray' );
    }

    // generate
    if ( !$phpThumb->GenerateThumbnail() ) {
        $log ? ATLog::_("Error generating thumbnail\n".implode("\n", $phpThumb->debugmessages)) : null;
        return;
    }

    // render
    if (!$phpThumb->RenderToFile($target)) {
        $log ? ATLog::_("Error rendering to file\n".implode("\n", $phpThumb->debugmessages)) : null;
        return;
    } else {
        $log ? ATLog::_("Generated $target") : null;
    }
    unset($phpThumb);

    // assign thumbnail
    $doc->objDBTable->dmthumbnail       = basename($target);
    $doc->objFormatData->dmthumbnail    = $doc->objDBTable->dmthumbnail;
    $doc->objFormatPath->thumb = DOCMAN_Utils::pathThumb($doc->objDBTable->dmthumbnail, 1);

    // store
    if(!$doc->objDBTable->store()) {
        $log ? ATLog::_('Problem storing changes') : null;
    }
    return;
}
