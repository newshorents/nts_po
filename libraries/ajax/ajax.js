// JavaScript Document

function sendRequest(url, args, customFunction) {
    req = false;

    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
        req.onreadystatechange = customFunction;
        if (args == "")
            req.open("GET", url, true);
        else
            req.open("GET", url + "?" + args, true);        
        req.send("");
    }
    else {
        //window.alert("This browser not is supported");
    }
    return req;
}

function newArg(key, value) {
    return key + "=" + encodeURIComponent(value);
}

function isValidResponse(obj) {
    if (obj.readyState == 4)
        if (obj.status == 200)
            return true;
        else
            //window.alert("There was a problem retrieving the data:\n" + obj.statusText);
    return false;
}