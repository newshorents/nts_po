<?php defined('_JEXEC') or die('Restricted access');
 $params = array( 'size' => array( 'x' => 800, 'y' => 500 ) , 'onClose'=>'\function() {location.reload();}' );
 JHTML::_('behavior.modal', 'a.modal', $params); ?>
<form action="index.php?option=com_projectopen&view=projectopen&task=viewImputeHours&Itemid=<?php echo JRequest::getVar("Itemid"); ?>" method="post" name="adminForm">

    <?php $link = 'index.php?option=com_projectopen&task=addImputeHour';
     $linkHelp = 'index.php?option=com_content&id=128&tmpl=component&task=preview';?>
<!--    <div id="imgGridImputacion"></div>-->
<h1 class="borderblue">Imputaci&oacute;n de horas</h1>
    <h3>
        <?php echo JRequest::getVar('msg'); ?>
    </h3>
    <div id="btnGridCost">
        <span class="btnBlog">
            <a class="modal" href="<?php echo $linkHelp; ?>"><?php echo JText::_('Help'); ?></a>
        </span>
        <span class="btnBlog" >
            <a href="<?php echo $link; ?>"><?php echo JText::_('New'); ?></a>
        </span>
    </div>
    <br/>
    <table class="adminlistCost" CELLSPACING=2>
        <thead>
            <tr>
                <th width="20" id="encabezadoImputeGridId" style="width:20px;">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoImputeGridId">
                    <?php echo JText::_('Start Date Week'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoImputeGridId">
                    <?php echo JText::_('Number Week'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoImputeGridId">
                    <?php echo JText::_('Year'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoImputeGridId">
                    <?php echo JText::_('Number Hour Impute'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoImputeGridId">
                    <?php echo JText::_('State'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="6">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php $i = 0;
                    $k = 0;
                    $st = 0;?>
            <?php foreach ($this->imputes as $impute) : ?>
            <?php $link2 = 'index.php?option=com_projectopen&task=editImputeHour&start_date='.$impute->start_date.'&week=' . $impute->num_week.'&year='.$impute->year.'&state='.$this->states[$st][0]; ?>

                        <tr class="<?php echo "row" . $k; ?>">
                            <td align="center" width="30" id="encabezadoImputeGridDatos">
                    <?php echo $this->pagination->limitstart + 1 + $i; ?>
                    </td>
                    <td align="center" id="encabezadoImputeGridDatos">
                        <span class="editlinktip hasTip">
                            <a href="<?php echo $link2; ?>"><?php
                            $start = preg_split("/[\/.-]/", $impute->start_date);
                            echo $start[2]."-".$start[1]."-".$start[0]; ?></a>
                        </span>
                    </td>
                    <td align="center" id="encabezadoImputeGridDatos">
                    <?php echo $impute->num_week; ?>
                    </td>
                    <td align="center" id="encabezadoImputeGridDatos">
                    <?php echo $impute->year; ?>
                    </td>
                    <td align="center" align="center" id="encabezadoImputeGridDatos">
                    <?php echo $impute->hour_week; ?>
                    </td>
                    <td align="center" id="encabezadoImputeGridDatos">
                    <?php
                    if($this->states[$st][0]==0){
                        $stateImage='media/system/images/save.png';
                    }else if($this->states[$st][0]==1){
                        $stateImage='media/system/images/pending.png';
                    }else if($this->states[$st][0]==2){
                        $stateImage='media/system/images/novalidate.png';
                    }else if($this->states[$st][0]==3){
                        $stateImage='media/system/images/validate.png';
                    }
                    $title=$this->states[$st][1];?>
                    <img id="state" width='15' heigth="20" name="state" alt="state" title="<?php echo $title;?>" src="<?php echo $stateImage; ?>" />
                    </td>
                </tr>
            <?php $i++;
                  $k = 1 - $k;
                  $st++;?>
            <?php endforeach; ?>
                    </tbody>
                </table>
                <input type="hidden" name="c" value="imputeHours" />
                <input type="hidden" name="option" value="com_projectopen" />
                <input type="hidden" name="task" value="viewImputeHours" />

    <?php echo JHTML::_('form.token'); ?>
</form>

<script type="text/javascript">

    function submitbutton(pressbutton){
        alert(pressbutton);
        if(pressbutton == 'newCosts'){
            href("index.php?option=com_projectopen&task=addImputeHour");
        }
    }

    function new_impute_hour(){
        href("index.php?option=com_projectopen&task=addImputeHour");
    }
</script>