<?php defined('_JEXEC') or die('Restricted access'); 
 $params = array( 'size' => array( 'x' => 800, 'y' => 500 ) , 'onClose'=>'\function() {location.reload();}' );
 JHTML::_('behavior.modal', 'a.modal', $params); ?>
<form action="index.php?option=com_projectopen&view=projectopen&task=viewCosts&Itemid=<?php echo JRequest::getVar("Itemid"); ?>" method="post" name="adminForm">

    <?php $link = 'index.php?option=com_projectopen&task=addCosts';
    $linkHelp = 'index.php?option=com_content&id=125&tmpl=component&task=preview';?>
<!--    <div id="imgGridCost"></div>-->
<h1 class="borderblue">Gastos</h1>
    <h3>
        <?php echo JRequest::getVar('msg'); ?>
    </h3>
    <div id="btnGridCost">
        <span class="btnBlog">
            <a class="modal" href="<?php echo $linkHelp; ?>"><?php echo JText::_('Help'); ?></a>
        </span>
        <span class="btnBlog">
            <a href="<?php echo $link; ?>"><?php echo JText::_('New'); ?></a>
        </span>           
    </div>
    <br/>
    <table class="adminlistCost" border="0" CELLSPACING=2>
        <thead>
            <tr>
                <th width="20" id="encabezadoCostosGridId">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoCostosGrid">
                    <?php echo JText::_('Type'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoCostosGrid">
                    <?php echo JText::_('Date'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoCostosGrid">
                    <?php echo JText::_('Project'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoCostosGrid">
                    <?php echo JText::_('Amount'); ?>
                </th>
                <th class="title" nowrap="nowrap" id="encabezadoCostosGrid">
                    <?php echo JText::_('State'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="6">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php
            $i = 0;
            $k = 0;
            foreach ($this->costs as $cost) :
                $link2 = 'index.php?option=com_projectopen&task=editCosts&amp;id=' . $cost->id;
            ?>

                <tr class="<?php echo "row" . $k; ?>">
                <td align="center" width="30" id="encabezadoCostosGridDatos">
                    <?php echo $this->pagination->limitstart + 1 + $i; ?>
                </td>
                <?php 
                if ($cost->state_cost == 0 || $cost->state_cost == 2) {
                ?>
                    <td id="encabezadoCostosGridDatos">
                        <span class="editlinktip hasTip">
                            <a href="<?php echo $link2; ?>"><?php echo $cost->name_type; ?></a>
                        </span>
                    </td>
                <?php } else { ?>
                        <td id="encabezadoCostosGridDatos">
                            <?php echo $cost->name_type; ?>
                        </td>
                <?php } ?>


                    <td id="encabezadoCostosGridDatos" align="center">
                        <?php
                        $date= preg_split("/[\/.-]/",$cost->date_cost);                        

                        echo $date[2]."-".$date[1]."-".$date[0]; ?>
                    </td>
                    <td id="encabezadoCostosGridDatos">
                        <?php echo $cost->project_name; ?>
                    </td>
                    <td id="encabezadoCostosGridDatos" align="right">
                        <?php 
						echo number_format($cost->amount, 2, ',', '.'); 
						?>
                    </td>
                    <td id="encabezadoCostosGridDatos" align="center">
                        <?php
                            if($cost->state_cost==0){
                                    $stateImage='media/system/images/save.png';
                            }else if($cost->state_cost==1){
                                    $stateImage='media/system/images/pending.png';
                            }else if($cost->state_cost==2){
                                    $stateImage='media/system/images/novalidate.png';
                            }else if($cost->state_cost==3){
                                    $stateImage='media/system/images/validate.png';
                            }
                            $title=$cost->name_state;
                            ?>
                            <img id="state" width='15' heigth="20" name="state" alt="state" title="<?php echo $title;?>" src="<?php echo $stateImage; ?>" />
                    </td>
                </tr>
            <?php
            $i++;
            $k = 1 - $k;
            endforeach;
            ?>
        </tbody>
    </table>
    <input type="hidden" name="c" value="costs" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="viewCosts" />
    <input type="hidden" name="boxchecked" value="0" />
    

    <?php echo JHTML::_('form.token'); ?>
</form>

<script type="text/javascript">

    function submitbutton(pressbutton){
        if(pressbutton == 'newCosts'){
            href("index.php?option=com_projectopen&task=addCosts");
        }            
    }

    function new_cost(){
        href("index.php?option=com_projectopen&task=addCosts");
    }
</script>