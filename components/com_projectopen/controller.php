<?php

/**
 * @version		1.0.0 projectopen $
 * @package		projectopen
 * @copyright	Copyright © 2010 - All rights reserved.
 * @license		GNU/GPL
 * @author		
 * @author mail	nobody@nobody.com
 *
 *
 * @MVC architecture generated by MVC generator tool at http://www.alphaplug.com
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class projectopenController extends JController {

    /**
     * Custom Constructor
     */
    function __construct() {
        $user = & JFactory::getUser();
        if ($user->get('id') > 0) {
            parent::__construct();
        } else {
            parent::__construct();
            $this->setRedirect(JRoute::_('index.php?option=com_user&view=login', false), 'login pleas');
        }
    }

    public function register() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->registerForm();
    }

    public function reports() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->reportsForm();
    }

    public function viewProject() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->registerForm();
    }

    public function viewTasks() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->getTasks();
    }

    public function registerHours() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $response = $view->setHours();
        $msg = "kjk";
        if ($response) {
            $this->setRedirect(JRoute::_('index.php?option=com_projectopen&task=register&date_from=' . $response, false), $msg);
        }
    }

    public function eeeevents() {
        //include(JPATH_COMPONENT.DS."json.class.php");
        $mes = date("m");
        $years = date("Y");
        $model = $this->getModel('projectopen');
        $rows = $model->getReportsData($mes, $years);
        $view = &$this->getView('projectopen');
        $view->events();
    }

    function display() {
        parent::display();
    }

    function events() {

        $ini = date('Y-m-d', JRequest::getVar('start')); //date("m");
        $fin = date("Y-m-d", JRequest::getVar('end'));

        $model = $this->getModel('projectopen');
        $rows = $model->getReportsData($ini, $fin);

        echo json_encode($rows);
    }

    //Costs
    public function viewCosts() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->gridCosts();
    }

    public function addCosts() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->editFormCost(false, null);
    }

    public function editCosts() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->editFormCost(true, null);
    }

    public function saveCost() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->savecost();
        $view->saveDocumentsCost();
        $msg = JText::_('Los cambios fueron guardados con éxito');
        $this->getGridCost($msg);
    }

    public function sendCost() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->sendcost();
        $view->saveDocumentsCost();
        $msg = JText::_('Los cambios fueron guardados con éxito');
        $this->getGridCost($msg);
    }

    function removeCost() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->removecost();
        $msg = JText::_('El registro fue eliminado correctamente');
        $this->getGridCost($msg);
    }

    public function deleteDocument() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        if ($view->deleteDocument()) {
            $msg = JText::_('El archivo ha sido borrado');
            $this->setRedirect('index.php?option=com_projectopen&task=editCosts&id=' . JRequest::getVar('id') . '&lang=es', $msg);
        } else {
            $msg = JText::_('El archivo no se puedo borrar');
            $this->setRedirect('index.php?option=com_projectopen&task=editCosts&id=' . JRequest::getVar('id') . '&lang=es', $msg, 'error');
        }
    }

    function cancelCost() {
        $this->getGridCost();
    }

    function cancel() {
        $this->getGridCost();
    }

    function getGridCost($msg) {
        $this->setRedirect('index.php?option=com_projectopen&c=costs&task=viewCosts&msg=' . $msg, $msg);
    }

    //Impute hours
    public function viewImputeHours() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->gridImputeHours();
    }

    //Vacaciones
    public function viewVacations() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->viewTableVacations();
    }

    public function addImputeHour() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->editFormImputeHour(false, null);
    }

    public function editImputeHour() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        $view->editFormImputeHour(true, null);
    }

    public function getProjectsUser() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        echo $view->getProjectsUser();
        die;
    }

    public function getImputeUser() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        echo $view->getCountImputeUser();
        die;
    }

    public function getUserActive() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        echo $view->getUserActive();
        die;
    }

    public function saveImpute() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        if ($view->saveImpute()) {
            $msg = JText::_('Los cambios han sido guardados con éxito');
            $this->getGridImpute($msg);
        } else {
            $msg = JText::_('Esta semana ya había sido imputada por lo tanto no se almacenó');
            $this->getGridImpute($msg);
        }
    }

    public function validateImpute() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        if ($view->saveImpute()) {
            $msg = JText::_('Los cambios han sido guardados con éxito y la imputación fue enviada al jefe de proyecto');
            $this->getGridImpute($msg);
        } else {
            $msg = JText::_('Esta semana ya había sido imputada por lo tanto no se almacenó');
            $this->getGridImpute($msg);
        }
    }

    function getGridImpute($msg) {
        $this->setRedirect('index.php?option=com_projectopen&view=projectopen&task=viewImputeHours&msg=' . $msg, $msg);
    }

    function cancelImpute() {
        $this->getGridImpute();
    }

    public function getHoliday() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        echo $view->getHoliday();
        die;
    }

    public function getFutureResourse() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        echo $view->getFutureResourse();
        die;
    }

    public function getMonthCloseScorecard() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        echo $view->getMonthCloseScorecard();
        die;
    }
    
    public function getLoadProjects() {
        $model = &$this->getModel('projectopen');
        $view = &$this->getView('projectopen');
        $view->setModel($model, true);
        echo $view->getLoadProjects();
        die;
    }
    

}

?>