<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class idoblogModelEntry extends JModel
{

//Возвращает номер категории в которую пишет юзверь, а если у него нет блога, то он создается автоматом
function getcatid($idblog) {

$user =& JFactory::getUser();
$db =& JFactory::getDBO();
$query="SELECT idcategory FROM #__idoblog_user_reffer WHERE iduser=".$user->id."";
$db->setQuery( $query );
$catid=$db->loadResult();

if (empty($catid)) $catid=createaccaunt();

return $catid;
}


//загружаем статью
function getarticle($id) {
if ($id != "") {
	$db =& JFactory::getDBO();
	$query="SELECT * FROM #__content WHERE id=".$id;
	$article=$this->_getList( $query );
	return $article[0];
}
}

//сохраняем запись в блог юзверя
function save() {
global $mainframe, $config;
$data = JRequest::get( 'post' );
$article  =& JTable::getInstance('content');
$user     =& JFactory::getUser();

		// Bind the form fields to the web link table
		if (!$article->bind($data, "published")) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// sanitise id field
		$article->id = (int) $article->id;

		$isNew = ($article->id < 1);
		if ($isNew)
		{
			$article->created 		= gmdate('Y-m-d H:i:s');
			$article->created_by 	= $user->get('id');
		}
		else
		{
			$article->modified 		= gmdate('Y-m-d H:i:s');
			$article->modified_by 	= $user->get('id');
		}



		// Append time if not added to publish date
		if (strlen(trim($article->publish_up)) <= 10) {
			$article->publish_up .= ' 00:00:00';
		}

		jimport( 'joomla.utilities.date' );
		$date = & JFactory::getDate($article->publish_up, $mainframe->getCfg('offset'));
		$article->publish_up = $date->toMySQL();

		// Handle never unpublish date
		if (trim($article->publish_down) == JText::_('Never') || trim( $article->publish_down ) == '')
		{
			$article->publish_down = $this->_db->getNullDate();;
		}
		else
		{
			if (strlen(trim( $article->publish_down )) <= 10) {
				$article->publish_down .= ' 00:00:00';
			}

			$date = & JFactory::getDate($article->publish_down, $mainframe->getCfg('offset'));
			$article->publish_down = $date->toMySQL();
		}

		$article->title = trim( JFilterOutput::ampReplace($article->title) );

		// Publishing state hardening for Authors
		if (!$user->authorize('com_content', 'publish', 'content', 'all'))
		{
			if ($isNew)
			{
				// For new items - author is not allowed to publish - prevent them from doing so
				$article->state = 1;
				//notification admin about new post
				$subject=JText::_( 'New Topic notification -"' ).$article->title.'"';
				$email=$config->adminemail;
				$SiteName 	= $mainframe->getCfg('sitename');
				if ($config->typename=='1') $username=$user->username; else $username=$user->name;
				$body="Hello administrator. User ".$username." send new post ".$article->title." on site ".$SiteName;
			// Clean the email data
				$subject = JMailHelper::cleanSubject($subject);
				$body	 = JMailHelper::cleanBody($body);
				$sender	 = JMailHelper::cleanAddress($sender);
						$MailFrom 	= $mainframe->getCfg('mailfrom');
						$FromName 	= $mainframe->getCfg('fromname');
				if ($config->adminnewentry==1) JUtility::sendMail($MailFrom, $FromName, $email, $subject, $body);
			}
			else
			{
				// For existing items keep existing state - author is not allowed to change status
				$query = 'SELECT state' .
						' FROM #__content' .
						' WHERE id = '.(int) $article->id;

				$this->_db->setQuery($query);
				$state = $this->_db->loadResult();

				if ($state) {
					$article->state = 1;
				}
				else {
					$article->state = 0;
				}
			}
		}

		// Search for the {readmore} tag and split the text up accordingly.
		$text = str_replace('<br>', '<br />', stripcslashes($_POST['text']));
		$text = str_replace('class="system-pagebreak"', '', $text);
$tagPos=JString::strpos($text, '<hr id="system-readmore"');
if ($tagPos === false) $tagPos=JString::strpos($text, '<HR id="system-readmore"');
if ($tagPos === false) $tagPos=JString::strpos($text, '<HR id=system-readmore');

		if ($tagPos === false)	{
			$article->introtext = $text;
		} else 	{
			$article->introtext = JString::substr($text, 0, $tagPos);
			$koncovka=JString::substr($text, $tagPos+1);
			$endtagpos=JString::strpos($koncovka, '>');
			$article->fulltext = JString::substr($koncovka, $endtagpos+1);
		}



		// Make sure the article table is valid
		if (!$article->check()) {
			$this->setError($article->getError());
			return false;
		}

		$article->version++;

		// Store the article table to the database
		if (!$article->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if ($isNew)
		{
			$this->_id = $article->_db->insertId();
			$article->id=$this->_id;
		}

		//обрабатываем тэги
		$tags = JRequest::getVar('tags',  '', 'post');
		$tags=explode(",", $tags);

		$query = "DELETE FROM #__idoblog_tags_reffer WHERE idarticle=".$article->id;
					$this->_db->setQuery($query);
					$this->_db->query();

		for ($i=0, $n=count( $tags ); $i < $n; $i++)
 	   {
	    $tag =trim($tags[$i]);
		if (!empty($tag)) {

		$query = "SELECT id FROM #__idoblog_tags WHERE tagname='".$tag."' LIMIT 1";
		$this->_db->setQuery($query);
		$tagid = $this->_db->loadResult();

		if (empty($tagid)) {
					$query = "INSERT INTO #__idoblog_tags ( `id` , `tagname` )
					VALUES (
					'', '".$tag."');";
					$this->_db->setQuery($query);
					$this->_db->query();
					$tagid=$this->_db->insertid();
					}

		$query = "SELECT * FROM #__idoblog_tags_reffer WHERE idarticle='".$article->id."' AND idtag='".$tagid."'";
		$this->_db->setQuery($query);
		$taginbase = $this->_db->loadResult();
		if (empty($taginbase)) {
					$query = "INSERT INTO `#__idoblog_tags_reffer` ( `idarticle` , `idtag` )
					VALUES ('".$article->id."', '".$tagid."');";
					$this->_db->setQuery($query);
					$this->_db->query();
					}

		}
		}


		$article->reorder("catid = " . (int) $data['catid']);

		$this->_article	=& $article;

		return true;

}

function delete() {
GLOBAL $config;
$user     =& JFactory::getUser();
$id = JRequest::getVar( 'id', 0, '', 'int');

$db =& JFactory::getDBO();

$query="SELECT created_by FROM #__content WHERE id=".$id;
$db->setQuery( $query );
$userid=$db->loadResult();

$acl	=& JFactory::getACL();
$gid	= $user->get( 'gid' );
if ($gid==0) $gid=29;
$gids 	= $acl->get_group_parents( $gid, 'users', 'RECURSE' );
$gids[]=$gid;

if (!in_array( $config->access_moderate, $gids ) && $userid<>$user->id) {
			JError::raiseError( 403, JText::_('ALERTNOTAUTH') );
			return;
		}

$query="DELETE FROM #__content WHERE id=".$id;
$db->setQuery( $query );
$db->query();
$query="DELETE FROM #__idoblog_comments WHERE idarticle=".$id;
$db->setQuery( $query );
$db->query();
$query="DELETE FROM #__idoblog_tags_reffer WHERE idarticle=".$id;
$db->setQuery( $query );
$db->query();
return true;
}


function getalltags()
{
$query="SELECT t.tagname, sum(c.hits) as hits FROM  #__idoblog_tags as t LEFT JOIN #__idoblog_tags_reffer as r ON t.id=r.idtag LEFT JOIN #__content as c ON r.idarticle=c.id GROUP BY t.id";
$tags=$this->_getList( $query );

$max=0;
	for ($i=0, $n=count( $tags ); $i < $n; $i++)
 	   {
	   $tag=$tags[$i];
	   if (!empty($tag->hits) && $tag->hits>$max) $max=$tag->hits;
	   }
if (empty($max) || $max==0) $max=1;
	for ($i=0, $n=count( $tags ); $i < $n; $i++)
 	   {
	    $tag=$tags[$i];
		$tags[$i]->div=round($tags[$i]->hits/$max*10)+1;
	   }

return $tags;
}

function gettags($id) {
$query="SELECT t.tagname FROM #__idoblog_tags as t, #__idoblog_tags_reffer as c WHERE c.idarticle=".$id." AND c.idtag=t.id";
$this->_db->setQuery($query);
$tags=$this->_db->loadResultArray();
$tags=implode(", ",$tags);
if (!empty($tags)) $tags.=", ";
//$tags=$this->_getList( $query );
return $tags;
}

function getyourfiles() {
GLOBAL $live_site;
jimport('joomla.filesystem.file');
$user =& JFactory::getUser();
$yourpath=JPATH_ROOT."/images/idoblog/upload/".JSTRING::strtolower($user->username);
$sitepath=$live_site."images/idoblog/upload/".JSTRING::strtolower($user->username);
$imagepath=$live_site."components/com_idoblog/assets/images";
$files=JFolder::files($yourpath);

$file=array();
for($z=0;$z<count($files);$z++) {
$file[$z]->path=$yourpath.DS.$files[$z];
$file[$z]->ext=strtolower(JFile::getExt($files[$z]));
$file[$z]->name=strtok($files[$z],".");
if ($file[$z]->ext=='jpg' || $file[$z]->ext=='gif' || $file[$z]->ext=='png') {
	$file[$z]->show="<span  id='f".$z."'><table width='100' style='float:left'><tr><td align='center'><small>".$file[$z]->name."<a href='#f' onClick='xajax_deletefile(\"".$files[$z]."\",$z)'><img src='".$imagepath."/delete.png'></a></small></td></tr><tr height='100'><td style='border:1px solid black' align='center'><img src='".$sitepath."/preview/small_".$files[$z]."' width='100' onclick='jInsertEditorText(\"<img src=".addslashes($sitepath."/".$files[$z]).">\",\"text\");' style='cursor:pointer'></td></tr></table></span>";
	}

if ($file[$z]->ext=='zip') {
	$file[$z]->show="<span  id='f".$z."'><table width='100' style='float:left'><tr><td align='center'><small>".$file[$z]->name."<a href='#f' onClick='xajax_deletefile(\"".$files[$z]."\",$z)'><img src='".$imagepath."/delete.png'></a></small></td></tr><tr height='100'><td style='border:1px solid black' align='center'><img src='".$imagepath."/archive_zip.gif"."' width='80' onclick='jInsertEditorText(\"<a href=".addslashes($sitepath."/".$files[$z]).">".$files[$z]."</a>\",\"text\");' style='cursor:pointer'></td></tr></table></span>";
	}

if ($file[$z]->ext=='rar') {
	$file[$z]->show="<span  id='f".$z."'><table width='100' style='float:left'><tr><td align='center'><small>".$file[$z]->name."<a href='#f' onClick='xajax_deletefile(\"".$files[$z]."\",$z)'><img src='".$imagepath."/delete.png'></a></small></td></tr><tr height='100'><td style='border:1px solid black' align='center'><img src='".$imagepath."/archive_rar.gif"."' width='80' onclick='jInsertEditorText(\"<a href=".addslashes($sitepath."/".$files[$z]).">".$files[$z]."</a>\",\"text\");' style='cursor:pointer'></td></tr></table></span>";
	}

}

return $file;
}


function upfile()
{
GLOBAL $config;
$userfile = JRequest::getVar('image1', null, 'files', 'array' );
$user =& JFactory::getUser();
$imagemaxwidth = JRequest::getVar('imagemaxwidth');
		if ($userfile['type']!='image/gif' && $userfile['type']!='image/jpeg' && $userfile['type']!='image/pjpeg' && $userfile['type']!='application/x-zip-compressed' && $userfile['type']!='application/x-rar-compressed'  && $userfile['type']!='image/png' && $userfile['type']!='application/zip'  && $userfile['type']!='application/rar')
		{
			JError::raiseWarning('SOME_ERROR_CODE', JText::_("File types don't support"));
			return false;
		}

		// Check if there was a problem uploading the file.
		if ( $userfile['error'] || $userfile['size'] < 1 )
		{
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('WARNINSTALLUPLOADERROR'));
			return false;
		}

$config->maxwidth=600;

$tmp_src	= $userfile['tmp_name'];
$dir_dest   = JPATH_BASE.DS."images".DS."idoblog".DS."upload".DS.strtolower($user->username.DS);
$tmp_dest   = JPATH_BASE.DS."images".DS."idoblog".DS."upload".DS.strtolower($user->username.DS.$userfile['tmp_name']);
$uploaded = JFile::upload($tmp_src, $tmp_dest);

$filename=strtolower(strtok($userfile['name'],"."));
$ext=JFILE::getExt($userfile['name']);

$size = getimagesize($tmp_dest);

$newwidth=$size[0];
  if (!empty($config->maxwidth))
  {
  if ($size[0]>$config->maxwidth) $newwidth = $config->maxwidth; else $newwidth =$size[0];
  if (!empty($imagemaxwidth) && $config->maxwidth>$imagemaxwidth) $newwidth=$imagemaxwidth;
  }

$userfile['name']=$filename.'_'.$newwidth.'px';
$filename=$userfile['name'];

$this->img_resize($tmp_dest, $dir_dest.$filename.'.jpg', $newwidth, $newwidth, $rgb=0xFFFFFF, $quality=90);

$this->img_resize($tmp_dest, $dir_dest.'preview/small_'.$filename.'.jpg', '100', '100', $rgb=0xFFFFFF, $quality=90);

if (JFILE::exists($tmp_dest)) JFILE::delete($tmp_dest);

  $script="<script language='javascript'> parent.xajax_addfile('".$filename.".jpg', parent.document.getElementById('number').value); </script>";

return $script;
}

function img_resize($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=100)
{
  if (!file_exists($src)) return false;

  $size = getimagesize($src);

  if ($size === false) return false;

  // Определяем исходный формат по MIME-информации, предоставленной
  // функцией getimagesize, и выбираем соответствующую формату
  // imagecreatefrom-функцию.
  $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
  $icfunc = "imagecreatefrom" . $format;
  if (!function_exists($icfunc)) return false;

  $x_ratio = $width / $size[0];
  $y_ratio = $height / $size[1];

  $ratio       = min($x_ratio, $y_ratio);
  $use_x_ratio = ($x_ratio == $ratio);

  $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
  $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
  $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
  $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

  $isrc = $icfunc($src);
  $idest = imagecreatetruecolor($new_width , $new_height);

  imagefill($idest, 0, 0, $rgb);
  imagecopyresampled($idest, $isrc, 0, 0, 0, 0,
    $new_width, $new_height, $size[0], $size[1]);

  imagejpeg($idest, $dest, $quality);

  imagedestroy($isrc);
  imagedestroy($idest);

  return true;

}

}

?>