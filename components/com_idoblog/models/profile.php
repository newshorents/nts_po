<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class idoblogModelProfile extends JModel
{
var $_online;

function getuser()
{
GLOBAL $mainframe, $config;
//проверяем юзверя
		$task = JRequest::getVar('task');
		$userid = JRequest::getVar('userid', 0, '', 'int');

		if ($task=='editprofile') {
		$user=& JFactory::getUser();
		if (empty($user->id)) {
		JError::raiseError( 403, JText::_('ALERTNOTAUTH') );
		return;
		}
		$userid=$user->id;

		}



		if (empty($userid)) {
		$user=& JFactory::getUser();
		$userid=$user->id;
		if (empty($user->id)) {
		JError::raiseError( 403, JText::_('ALERTNOTAUTH') );
		return;
		}
		}


$query="SELECT * FROM #__users as u, #__idoblog_users as ku WHERE u.id=".$userid." AND u.id=ku.iduser";
$userdata=$this->_getList( $query );

if (empty($userdata) && $task=='editprofile')
	{
	$query="INSERT INTO #__idoblog_users ( `iduser` , `birthday` , `avatar` , `icq` , `about`, `small_avatar` )
	VALUES (
	'".$user->id."', '0000-00-00', '', '', '',''
	);";
	$this->_db->setQuery( $query );
	$this->_db->query();

	$query="SELECT * FROM #__users as u, #__idoblog_users as ku WHERE u.id=".$userid." AND u.id=ku.iduser";
	$userdata=$this->_getList( $query );
	}

//get online users
if ($config->online_enabled=="1") {
$query="SELECT * FROM #__session as ss WHERE ss.client_id=0";
$onlines=$this->_getList( $query );
for ($z=0;$z<count($onlines);$z++) {
$online=$onlines[$z];
if ($online->userid>0) $this->_online[$online->userid]=1;
}
}

$userdata=$this->_obrabotka($userdata[0]);


$layout=JRequest::getVar('layout');
$breadcrumbs = & $mainframe->getPathWay();

if ($layout=='profile') {
$breadcrumbs->addItem( $userdata->username, $userdata->userlink );
$breadcrumbs->addItem( JText::_( 'profile' ), '' );
};

if ($layout=='editprofile') {
$breadcrumbs->addItem( $userdata->username, $userdata->userlink );
$breadcrumbs->addItem( JText::_( 'Edit profile' ), '' );
};

if ($layout=='editfriends') {
$breadcrumbs->addItem( $userdata->username, $userdata->userlink );
$breadcrumbs->addItem( JText::_( 'Edit friends' ), '' );
};

return $userdata;
}

function _obrabotka($data)
{
GLOBAL $mainframe, $config;
		$user=& JFactory::getUser();
$Itemid=JRequest::getVar('Itemid',  '');
$layout=JRequest::getVar('layout',  '');
        $row =$data;
		//приводим дату к русскому виду
		if ($row->birthday!=='0000-00-00')
		{
		$date = & JFactory::getDate($row->birthday, '');
		if ($layout=='editprofile') $row->birthday=$date->toFormat('%Y-%m-%d'); else $row->birthday=$date->toFormat($config->dateformat);
		$row->data=$date;
		}

$row->readmore=JRoute::_( 'index.php?option=com_idoblog&task=viewpost&id='. $row->id.'&Itemid='.$Itemid );
$row->real_username=$row->username;
$row->real_name=$row->name;
if ($config->typename=='1') $row->username=$row->username; else $row->username=$row->name;
		$row->userlink= JRoute::_( 'index.php?option=com_idoblog&task=userblog&userid='. $row->iduser.'&Itemid='.$Itemid );
$row->profile=JRoute::_( 'index.php?option=com_idoblog&task=profile&userid='. $row->iduser.'&Itemid='.$Itemid );
$row->myavatar=$this->getavatar($row->avatar);
$row->seximage=$this->_getsex($row->sex,$row->iduser);
$row->mysmallavatar=$this->getavatar($row->small_avatar);
//всплывающее окно
if ($layout=='profile') $row->tooltip=$this->_gettooltip($row);

if ($config->import_plugins==1)	$row->text=$row->email; else $row->text="<a href='mailto:".$row->email."'>".$row->email."</a>";

		if ($user->id==$row->id) {
		$row->editprofile="(<a href='".JRoute::_( 'index.php?option=com_idoblog&task=editprofile&Itemid='.$Itemid )."'>".JText::_( 'Edit profile' )."</a>)";
		$row->editfriends="(<a href='".JRoute::_( 'index.php?option=com_idoblog&task=editfriends&Itemid='.$Itemid )."'>".JText::_( 'Edit friends' )."</a>)";
		}
		$data=$row;

return $data;
}

function _gettooltip($row)
{
GLOBAL $config;
$user =& JFactory::getUser();
$check="";
$layout=JRequest::getVar('layout');

if ($layout=='editprofile' || $layout=='avatarform') $userid=$user->id;
else
		$userid = JRequest::getVar('userid', 0, '', 'int');
$text="<table width='150' border='0'><tr>
					<td><a href='".$row->userlink."'>".$row->username." ".JText::_( "Blog" )." </a></td></tr>
					<tr>
					<td><a href='".$row->profile."'>".JText::_( "Profile" )."</a></td>
					</tr>";

//check friends
if (!empty($userid)) {
$db =& JFactory::getDBO();
$query="SELECT * FROM #__idoblog_friends WHERE id=".$user->id." AND idfriend=".$userid;
$db->setQuery( $query );
$db->query();
$check=$db->getNumRows();
}

if ($user->id>0 && $user->id!=$userid && $check!=1 && !empty($userid)) $text.="<tr>
		<td><span id='friend".$userid."'><a href='javascript:void(0)' onclick='xajax_addfriend(".$userid.")'>".JText::_( "Add to friendlist" )."</a></span></td></tr>";

$text.="</table>";
return $text;
}

function getavatar($avatar) {
GLOBAL $config, $live_site;
jimport('joomla.filesystem.file');
if (empty($avatar) || !JFile::exists(JPATH_BASE.DS.'/images/idoblog/'.$avatar))
return '<img src="'.$live_site.'images/idoblog/gallery/default.gif" border="0"  width="'.$config->avatarwidth.'">';
else
return '<img src="'.$live_site.'images/idoblog/'.$avatar.'" border="0" width="'.$config->avatarwidth.'">';
}

function upavatar()
{
GLOBAL $config;
error_reporting (E_ALL & ~E_NOTICE);
$userfile = JRequest::getVar('image1', null, 'files', 'array' );
$user =& JFactory::getUser();

if ($userfile['type']!='image/gif' && $userfile['type']!='image/jpeg' && $userfile['type']!='image/pjpeg' && $userfile['type']!='image/png')
		{
			JError::raiseWarning('SOME_ERROR_CODE', JText::_("File types don't support"));
			return false;
		}

		// Check if there was a problem uploading the file.
		if ( $userfile['error'] || $userfile['size'] < 1 )
		{
			JError::raiseWarning('SOME_ERROR_CODE', JText::_('WARNINSTALLUPLOADERROR'));
			return false;
		}



jimport('joomla.filesystem.file');

$db =& JFactory::getDBO();

$query="SELECT * FROM #__idoblog_users WHERE iduser='".$user->id."'";
$db->setQuery( $query );
$avatar=$db->loadObjectList();
$lastavatar=$avatar[0]->avatar;
$lastsmallavatar=$avatar[0]->small_avatar;


if (JString::strpos( $lastavatar, "avatar" ) !== false) {
if (JFILE::exists(JPATH_BASE.DS."images".DS."idoblog".DS.$lastavatar)) JFILE::delete(JPATH_BASE.DS."images".DS."idoblog".DS.$lastavatar);
if (JFILE::exists(JPATH_BASE.DS."images".DS."idoblog".DS.$lastsmallavatar)) JFILE::delete(JPATH_BASE.DS."images".DS."idoblog".DS.$lastsmallavatar);
}

$prefix=@JString::substr($lastavatar,-5,1);
$prefix++;
$ext=JFILE::getExt($userfile['name']);

// resized to 100px wide
$pristavka=$user->id;
$imagename = $pristavka.'_'.$prefix.'.'.$ext;

$tmp_dest 	= JPATH_BASE.DS."images".DS."idoblog".DS."avatars".DS."tmp".DS.$imagename;
$tmp_src	= $userfile['tmp_name'];
$dir_dest   = JPATH_BASE.DS."images".DS."idoblog".DS."avatars".DS;

$uploaded = JFile::upload($tmp_src, $tmp_dest);


$this->img_resize($tmp_dest, $dir_dest.$pristavka.'_'.$prefix.'.jpg', $config->avatarwidth, $config->avatarwidth, $rgb=0xFFFFFF, $quality=90);

$this->img_resize($tmp_dest, $dir_dest.'small_'.$pristavka.'_'.$prefix.'.jpg', $config->smallavatarwidth, $config->smallavatarwidth, $rgb=0xFFFFFF, $quality=90);

$script="<script language='javascript'> parent.xajax_updateavatar('avatars/".$pristavka.'_'.$prefix.'.jpg'."','avatars/small_".$pristavka.'_'.$prefix.'.jpg'."'); </script>";

if (JFILE::exists($tmp_dest)) JFILE::delete($tmp_dest);

//update avatar in database

$query = 'UPDATE #__idoblog_users SET avatar = \'avatars/'.$pristavka.'_'.$prefix.'.jpg'.'\' WHERE iduser = \''.$user->id.'\'';
			$db->setQuery( $query );
			if (!$db->query()) {
				$this->setError( $db->getErrorMsg() );
				return false;
			}
$query = 'UPDATE #__idoblog_users SET small_avatar = \'avatars/small_'.$pristavka.'_'.$prefix.'.jpg'.'\' WHERE iduser = \''.$user->id.'\'';
			$db->setQuery( $query );
			if (!$db->query()) {
				$this->setError( $db->getErrorMsg() );
				return false;
			}



		return $script;
}

function img_resize($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=100)
{
  if (!file_exists($src)) return false;

  $size = getimagesize($src);

  if ($size === false) return false;

  // Определяем исходный формат по MIME-информации, предоставленной
  // функцией getimagesize, и выбираем соответствующую формату
  // imagecreatefrom-функцию.
  $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
  $icfunc = "imagecreatefrom" . $format;
  if (!function_exists($icfunc)) return false;

  $x_ratio = $width / $size[0];
  $y_ratio = $height / $size[1];

  $ratio       = min($x_ratio, $y_ratio);
  $use_x_ratio = ($x_ratio == $ratio);

  $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
  $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
  $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
  $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

  $isrc = $icfunc($src);
  $idest = imagecreatetruecolor($new_width , $new_height);

  imagefill($idest, 0, 0, $rgb);
  imagecopyresampled($idest, $isrc, 0, 0, 0, 0,
    $new_width, $new_height, $size[0], $size[1]);

  imagejpeg($idest, $dest, $quality);

  imagedestroy($isrc);
  imagedestroy($idest);

  return true;

}

	function saveprofile()
	{
	global $mainframe, $config;
$data = JRequest::get( 'post' );
		$user     =& JFactory::getUser();
		$nameis	= $user->get('username');

$user->email=$data['email'];

//update user
$query="UPDATE #__users SET `name` = '".$data['name']."',
`email` = '".$user->email."' WHERE `id` ='".$user->id."';";
$this->_db->setQuery( $query );
$this->_db->query();

//update password
if (!empty($data['password']) && $data['password']==$data['password2'])
{
		jimport('joomla.user.helper');
			$salt  = JUserHelper::genRandomPassword(32);
			$crypt = JUserHelper::getCryptedPassword($data['password'], $salt);
			$data['password'] = $crypt.':'.$salt;
$query="UPDATE #__users SET `password` = '".$data['password']."' WHERE `id` ='".$user->id."';";
$this->_db->setQuery( $query );
$this->_db->query();
}

//update other

		jimport( 'joomla.utilities.date' );
		$birthday = new JDate($data['birthday']." 01:01:01", '');
		$birthday = $birthday->toMySQL();
$query="UPDATE #__idoblog_users SET `birthday` = '".$birthday."', `avatar` = '".$data['myavatarname']."', `icq`='".$data['icq']."', `about`= '".$_POST['text']."', `small_avatar`='".$data['mysmallavatarname']."', `sex`= '".$data['sex']."' WHERE `iduser` ='".$user->id."';";
$this->_db->setQuery( $query );
$this->_db->query();

		$session =& JFactory::getSession();
		$session->set('user', $user);

	return true;
	}


function getyourfriends() {
JRequest::getVar('userid', 0, '', 'int');

		if (empty($userid)) {
		$user=& JFactory::getUser();
		$userid=$user->id;
		}

$query="SELECT u.*, kf.both, ku.* FROM #__idoblog_friends as kf, #__users as u, #__idoblog_users as ku WHERE kf.id=".$userid." AND kf.idfriend=u.id AND ku.iduser=u.id";
$friends=$this->_getList( $query );
$friends=$this->_obrabotkafriends($friends);
return $friends;
}

function getyourfriendsat() {
$userid = JRequest::getVar('userid', 0, '', 'int');

		if (empty($userid)) {
		$user=& JFactory::getUser();
		$userid=$user->id;
		}

$query="SELECT u.id,u.username, u.name, kf.both, ku.* FROM #__idoblog_friends as kf, #__users as u, #__idoblog_users as ku WHERE kf.idfriend=".$userid." AND kf.id=u.id  AND ku.iduser=u.id";
$friendsat=$this->_getList( $query );

$friendsat=$this->_obrabotkafriends($friendsat);
return $friendsat;
}

function _obrabotkafriends($friends) {
GLOBAL $config;
$Itemid=JRequest::getVar('Itemid',  '');
for ($i=0, $n=count( $friends ); $i < $n; $i++)
    {
	$friend=$friends[$i];
	$friend->real_name=$friend->name;
	$friend->real_username=$friend->username;
	if ($config->typename=='1') $friend->username=$friend->username; else $friend->username=$friend->name;
	$friend->link=JRoute::_( 'index.php?option=com_idoblog&task=userblog&userid='.$friend->id.'&Itemid='.$Itemid );

	$friends[$i]=$friend;
	}
return $friends;
}

function getselectsex($sex) {
$typesex = array(
			JHTML::_('select.option',  '', JText::_( 'Undefined' ) ),
			JHTML::_('select.option',  'm', JText::_( 'Male' ) ),
			JHTML::_('select.option',  'f', JText::_( 'Female' ) )
		);
return $typesex;
}

	function saveblog()
	{
	global $mainframe, $config;
$data = JRequest::get( 'post' );
		$user     =& JFactory::getUser();

//update user
$query="UPDATE #__idoblog_users SET `description` = '".stripcslashes($_POST['text'])."',
`template` = '".$data['templ']."', `adsense_pub` = '".$data['adsense_pub']."', `adsense_slot` = '".$data['adsense_slot']."', `adsense_type` = '".$data['typeadsense']."' WHERE `iduser` ='".$user->id."';";
$this->_db->setQuery( $query );
$this->_db->query();
	return true;
	}

function _getsex($sex,$userid) {
GLOBAL $config, $live_site;
//определяем в сети или нет, и выводим пол
$sexava="";
if ($sex=='f') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/female.gif' border='0'>";
if ($sex=='m') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/male.gif' border='0'>";

if ($config->online_enabled=="1") {
if (empty($this->_online[$userid])) {
if ($sex=='f') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/female_offline.gif' border='0'>";
if ($sex=='m') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/male_offline.gif' border='0'>";
}
}
return $sexava;
}


}

?>