<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class idoblogModelidoblog extends JModel
{
var $_data;
var $_total;
var $_tooltip;
var $_tooltiparray;
var $_online;
var $_totalposts;

//get entrys
function getdata($limitstart,$limit,$userid)
{
GLOBAL $mainframe,$config;

$task = JRequest::getVar('task');
$id = JRequest::getVar('id', 0, '', 'int');
$tagid = JRequest::getVar('tagid', 0, '', 'int');
$layout=JRequest::getVar('layout');

$select="c.id, c.hits, c.introtext, c.fulltext, c.created, c.title, u.username, u.name, u.id as userid, ku.avatar, ku.sex";
$from="#__content as c, #__users as u, #__idoblog_users as ku";
$where="c.sectionid='".$config->idblog."' AND c.state=1 AND c.created_by=u.id AND u.id=ku.iduser";
$order="created desc";

$day=JRequest::getVar('day', 0, '', 'int');
$month=JRequest::getVar('month', 0, '', 'int');
$year=JRequest::getVar('year', 0, '', 'int');

if (!empty($day)) $where.=" AND DAYOFMONTH(c.created)=".$day;
if (!empty($month)) $where.=" AND MONTH(c.created)='".$month."'";
if (!empty($year)) $where.=" AND YEAR(c.created)='".$year."'";

if ($task=='userblog' || $layout=='yourblog') {
$select.=", ku.description, ku.template, ku.adsense_pub, ku.adsense_slot, ku.adsense_type";
}

if ($task=='viewtag') {
$select.=", t.tagname";
$from.=",#__idoblog_tags as t, #__idoblog_tags_reffer as r";
$where.=" AND t.id=r.idtag AND c.id=r.idarticle AND r.idtag=".$tagid;
}

if ($layout=='friend') {
$from.=", #__idoblog_friends as kf ";
$where.=" AND kf.id=".$userid." AND kf.idfriend=u.id";
}

if ($task=='viewpost') {
$select.=", ku.template, ku.adsense_pub, ku.adsense_slot, ku.adsense_type";
$where.=" AND c.id=".$id;
}

if ($userid>0 && $layout!='friend') {$where.=" AND u.id=".$userid;}

//get online users
if ($config->online_enabled=="1") {
$query="SELECT * FROM #__session as ss WHERE ss.client_id=0";
$onlines=$this->_getList( $query );
for ($z=0;$z<count($onlines);$z++) {
$online=$onlines[$z];
if ($online->userid>0) $this->_online[$online->userid]=1;
}
}

//get entrys
$query="SELECT ".$select." FROM ".$from." WHERE ".$where." ORDER BY ".$order."  LIMIT ".$limitstart.", ".$limit;

$data = $this->_getList( $query );
$this->_data=$this->obrabotka($data);

//get count all entrys
$query="SELECT ".$select." FROM ".$from." WHERE ".$where;
$this->_db->setQuery($query);
$this->_totalposts=@count($this->_getList( $query ));

//create breadcrumbs
$breadcrumbs = & $mainframe->getPathWay();
if ($layout=='post') {
$breadcrumbs->addItem( $this->_data[0]->username, $this->_data[0]->userlink );
$breadcrumbs->addItem( $this->_data[0]->title, '' );
};

if ($layout=='yourblog' && !empty($this->_data[0]->username)) {
$breadcrumbs->addItem( $this->_data[0]->username, '' );
};

if ($layout=='tag') {
$breadcrumbs->addItem( JText::_( 'tag:' ).' '.$this->_data[0]->tagname, '' );
}

return $this->_data;
}



function obrabotka($data) {
GLOBAL $mainframe, $config;
$Itemid=JRequest::getVar('Itemid', 0, '', 'int');
$user =& JFactory::getUser();
$layout=JRequest::getVar('layout');
$acl	=& JFactory::getACL();
$gid	= $user->get( 'gid' );
if ($gid==0) $gid=29;
$gids 	= $acl->get_group_parents( $gid, 'users', 'RECURSE' );
$gids[]=$gid;

//грузим френдов
if (!empty($user->id)) {
$db =& JFactory::getDBO();
$query="SELECT idfriend FROM #__idoblog_friends WHERE id=".$user->id;
$db->setQuery( $query );
$friends=$db->loadResultArray();

//определяем тултип для себя
$user->userid=$user->id;
$user->userlink= getMyVar('userlink',$user);
$user->profile= getMyVar('profile',$user);

if (empty($this->_tooltip[$user->id])) $this->_tooltip[$user->id]=gettooltip($user,$friends);
}


for ($i=0, $n=count( $data ); $i < $n; $i++)
    {
    	$row =&$data[$i];
		if ($i==0) $row->adsense=getMyVar('adsense',$user,$row);
		$row->readmore=getMyVar('readmore',$user,$row);
		$row->username=getMyVar('username',$row);
		$row->userlink=getMyVar('userlink',$row);
		$row->profile=getMyVar('profile',$row);

		//check date
		$date = & JFactory::getDate($row->created, $mainframe->getCfg('offset'));
		$row->fulldate=$date->toFormat('%Y-%m-%d %H:%M:%S');
		$row->date=$date->toFormat($config->dateformat);
		$row->time=$date->toFormat('%H:%M:%S');

		$row->tags=$this->_gettags($row->id);
		myget($this->_online[$row->userid]);
		$row->seximage=getsex($row->sex,$row->userid,$this->_online[$row->userid]);
		$row->avatar=getavatar($row->avatar,false);

		if (empty($friends)) $friends="";
		if (empty($this->_tooltip[$row->userid])) $this->_tooltip[$row->userid]=gettooltip($row,$friends);

		if ($layout=="post")
		$row->text=$row->introtext."<br>".$row->fulltext;
		else
		$row->text=$row->introtext;

		$row->sumcomments=$this->_getsumcomments($row->id);

		if (in_array( $config->access_moderate, $gids) || ($user->id==$row->userid)) {
		$row->editlink=getMyVar('editlink',$user,$row);
		$row->deletelink=getMyVar('deletelink',$user,$row);
		}
    }

return $data;
}


//gettotalposts
//return count posts
function gettotalposts() {
return $this->_totalposts;
}


function getComments() {
GLOBAL $mainframe;
$id = JRequest::getVar('id', 0, '', 'int');
$query="SELECT c.*, ku.small_avatar, ku.sex FROM #__idoblog_comments as c  LEFT JOIN #__idoblog_users as ku ON  ku.iduser=c.created_by WHERE c.idarticle=".$id." ORDER BY c.parent, c.date";
//$query="SELECT c.*, u.username, u.name, u.id as userid, ku.small_avatar, ku.sex FROM #__idoblog_comments as c, #__users as u, #__idoblog_users as ku WHERE c.idarticle=".$id." AND c.created_by=u.id AND ku.iduser=u.id  ORDER BY c.parent, c.date";
 $rows = $this->_getList( $query );

$levellimit=50;
		// establish the hierarchy of the menu
		$children = array();
		// first pass - collect children
		foreach ($rows as $v )
		{
			$pt = $v->parent;
			$v->name="";
			$list = @$children[$pt] ? $children[$pt] : array();
			array_push( $list, $v );
			$children[$pt] = $list;
		}

		// second pass - get an indent list of the items
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, max( 0, $levellimit-1 ) );

 $comments=$this->obrabotkacomments($list);
 return $comments;
}

function obrabotkacomments($comments) {
GLOBAL $mainframe, $config;
$user =& JFactory::getUser();
$Itemid=JRequest::getVar('Itemid',  '');

$friends="";
//грузим френдов
if (!empty($user->id)) {
$db =& JFactory::getDBO();
$query="SELECT idfriend FROM #__idoblog_friends WHERE id=".$user->id;
$db->setQuery( $query );
$friends=$db->loadResultArray();
}

$obrcomments=array();
$i=0;
	foreach ($comments as $comment )
	{
		$comment->userid=$comment->created_by;
		$comment->indent=round(strlen($comment->treename)/2);

		$date = & JFactory::getDate($comment->date, $mainframe->getCfg('offset'));
		$comment->date=$date->toFormat($config->dateformat);
		$comment->time=$date->toFormat('%H:%M:%S');

		$comment->userlink=getMyVar('userlink',$comment);
		$comment->username=getMyVar('username',$comment);

		if ($comment->userid>0)
			$comment->itusername='<a href="'.$comment->userlink.'"><small>'.getMyVar('username',$comment).'</small></a>';
			else
			$comment->itusername='<small>'.getMyVar('username',$comment).'</small>';

		$comment->text=$this->replace_smile($comment->text);
		$comment->text=$this->replace_bbcode($comment->text);

		if ($config->show_avatars=='1')
			if ($comment->userid>0) $comment->avatar='<a href="'.$comment->userlink.'" class="supernote-hover-demo'.$comment->userid.'">'.getavatar($comment->small_avatar,true).'</a>';
			else $comment->avatar=getavatar($comment->small_avatar,true);

		myget($this->_online[$comment->userid]);
		$comment->seximage=getsex($comment->sex,$comment->userid,$this->_online[$comment->userid]);
		$comment->profile=getMyVar('profile',$comment);

		if (empty($this->_tooltip[$comment->userid]) && $comment->userid>0) $this->_tooltip[$comment->userid]=gettooltip($comment,$friends);

		$obrcomments[$i]=$comment;
		$i++;
		}

return $obrcomments;
}


function gettooltip() {
return $this->_tooltip;
}


function _gettags($id) {
$query="SELECT t.* FROM #__idoblog_tags as t, #__idoblog_tags_reffer as c WHERE c.idarticle=".$id." AND c.idtag=t.id";
$tags=$this->_getList( $query );
$string="";
   for ($i=0, $n=count( $tags ); $i < $n; $i++)
    {
	$tag=$tags[$i];
	$string.='<a href="'.JRoute::_( 'index.php?option=com_idoblog&task=viewtag&tagid='. $tag->id ).'">'.$tag->tagname.'</a>';
	if ($i<($n-1)) $string.=" | ";
	}
return $string;
}


function _getsumcomments($idarticle) {
$db =& JFactory::getDBO();
$query="SELECT * FROM #__idoblog_comments WHERE idarticle=".$idarticle." AND publish=1";
$db->setQuery( $query );
$db->query();
$sum=$db->getNumRows();
return $sum;
}


function getSmiley() {
GlOBAL $live_site;
$path=$live_site.'components/com_idoblog/assets/images/smiley/';
$text="
<div style='float:right'>
<img src='".$path."regular_smile.gif' onClick='smile(\":)\")' class='smile'>
<img src='".$path."teeth_smile.gif' onClick='smile(\":D\")' class='smile'>
<img src='".$path."sad_smile.gif' onClick='smile(\":(\")' class='smile'>
<img src='".$path."wink_smile.gif' onClick='smile(\";)\")' class='smile'>
<img src='".$path."whatchutalkingabout_smile.gif' onClick='smile(\":o\")' class='smile'>
<img src='".$path."tounge_smile.gif' onClick='smile(\":P\")' class='smile'>
<img src='".$path."shades_smile.gif' onClick='smile(\"8)\")' class='smile'>
<img src='".$path."thumbs_up.gif' onClick='smile(\":up:\")' class='smile'>
<img src='".$path."thumbs_down.gif' onClick='smile(\":down:\")' class='smile'>
<img src='".$path."cry_smile.gif' onClick='smile(\":-(\")' class='smile'>
<img src='".$path."angry_smile.gif' onClick='smile(\":devil:\")' class='smile'>
<img src='".$path."kiss.gif' onClick='smile(\":kiss:\")' class='smile'>
</div>
";
return $text;
}

function getBBCode() {
GlOBAL $live_site;
$path=$live_site.'components/com_idoblog/assets/images/';
$text="
<div style='float:left'>
<img src='".$path."bb_bold.gif' onClick='bbcode(\"B\")' class='smile'>
<img src='".$path."bb_italic.gif' onClick='bbcode(\"I\")' class='smile'>
<img src='".$path."bb_underline.gif' onClick='bbcode(\"U\")' class='smile'>
<img src='".$path."bb_strike.gif' onClick='bbcode(\"S\")' class='smile'>
<img src='".$path."bb_url.gif' onClick='bbcode(\"URL\")' class='smile'>
<img src='".$path."bb_img.gif' onClick='bbcode(\"IMG\")' class='smile'>
</div>
";

return $text;
}

function replace_bbcode($comment) {
$comment=str_replace("[B]","<b>",$comment);
$comment=str_replace("[/B]","</b>",$comment);
$comment=str_replace("[I]","<i>",$comment);
$comment=str_replace("[/I]","</i>",$comment);
$comment=str_replace("[U]","<u>",$comment);
$comment=str_replace("[/U]","</u>",$comment);
$comment=str_replace("[S]","<s>",$comment);
$comment=str_replace("[/S]","</s>",$comment);
$comment=str_replace("[IMG]",'<img src="',$comment);
$comment=str_replace("[/IMG]",'">',$comment);
$comment=preg_replace("#\[URL=((?:[^]]|](?=\.\w{2,4}))+)\]((?:(?!\[/URL]).)+)\[/URL]#",'<a href="\\1">\\2</a>',$comment);
return $comment;
}

function replace_smile($comment) {
GLOBAL $live_site;
//обработка смайлов
$path=$live_site.'components/com_idoblog/assets/images/smiley/';
$comment=str_replace(":)","<img src=".$path."regular_smile.gif>",$comment);
$comment=str_replace(":D","<img src=".$path."teeth_smile.gif>",$comment);
$comment=str_replace(":(","<img src=".$path."sad_smile.gif>",$comment);
$comment=str_replace(";)","<img src=".$path."wink_smile.gif>",$comment);
$comment=str_replace(":o","<img src=".$path."whatchutalkingabout_smile.gif>",$comment);
$comment=str_replace(":P","<img src=".$path."tounge_smile.gif>",$comment);
$comment=str_replace("8)","<img src=".$path."shades_smile.gif>",$comment);
$comment=str_replace(":up:","<img src=".$path."thumbs_up.gif>",$comment);
$comment=str_replace(":down:","<img src=".$path."thumbs_down.gif>",$comment);
$comment=str_replace(":-(","<img src=".$path."cry_smile.gif>",$comment);
$comment=str_replace(":devil:","<img src=".$path."angry_smile.gif>",$comment);
$comment=str_replace(":kiss:","<img src=".$path."kiss.gif>",$comment);
return $comment;
}

function getkeystring() {
$allowed_symbols = "23456789abcdeghkmnpqsuvxyz";
$length = mt_rand(5,6);
	while(true){
		$keystring='';
		for($i=0;$i<$length;$i++){
			$keystring.=$allowed_symbols{mt_rand(0,strlen($allowed_symbols)-1)};
			}
		if(!preg_match('/cp|cb|ck|c6|c9|rn|rm|mm|co|do|cl|db|qp|qb|dp|ww/', $keystring)) break;
		}
return $keystring;
}

}

?>