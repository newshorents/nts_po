<?php
defined( '_JEXEC' ) or die( 'Restricted access' );


function idoblogBuildRoute( &$query )
{
       $segments = array();



       if(isset($query['task']))
       {
                $segments[] = $query['task'];
                unset( $query['task'] );
       };
       if(isset($query['id']))
       {
                $segments[] = $query['id'];
                unset( $query['id'] );
       };
       if(isset($query['tagid']))
       {
                $segments[] = $query['tagid'];
                unset( $query['tagid'] );
       };
       if(isset($query['userid']))
       {
                $segments[] = $query['userid'];
                unset( $query['userid'] );
       };
       if(isset($query['type']))
       {
                $segments[] = $query['type'];
                unset( $query['type'] );
       };
       return $segments;
}

function idoblogParseRoute( $segments )
{

       $vars = array();

       switch($segments[0])
       {
               case 'editpost':
                       $vars['task'] = $segments[0];
                       if (!empty($segments[1])) $vars['id'] = $segments[1];
                       break;			   			   
                case 'deletepost':
                       $vars['task'] = $segments[0];
                       $vars['id'] = $segments[1];
                       break;	
               case 'viewpost':
                       $vars['task'] = $segments[0];
                       $vars['id'] = $segments[1];
					   if (!empty($segments[2])) $vars['type'] = $segments[2];
                       break;	
                case 'editprofile':
                       $vars['task'] = $segments[0];
                       break;
                case 'editblog':
                       $vars['task'] = $segments[0];
                       break;
                case 'editfriends':
                       $vars['task'] = $segments[0];
                       break;
               case 'avatarform':
					   $vars['task'] = $segments[0];
					   break;
               case 'fileform':
					   $vars['task'] = $segments[0];
					   break;
               case 'profile':
                       $vars['task'] = $segments[0];
                       $vars['userid'] = $segments[1];
   					   if (!empty($segments[2])) $vars['type'] = $segments[2];
                       break;	
               case 'viewtag':
                       $vars['task'] = $segments[0];
                       $vars['tagid'] = $segments[1];
					   if (!empty($segments[2])) $vars['type'] = $segments[2];
                       break;	
               case 'userblog':
                       $vars['task'] = $segments[0];
                       $vars['userid'] = $segments[1];
					   if (!empty($segments[2])) $vars['type'] = $segments[2];
                       break;	 
       }

       return $vars;

}


?>