<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

class idoblogControllerEntry extends idoblogController
{
    /**
     * метод вывода на экран
     *
     * @access    public
     */
    function display()
    {
        parent::display();
    }

    function save()
    {
     $model = $this->getModel('entry'); 
	
	    if ($model->save($post)) {
        $msg = JText::_( 'Greeting Saved!' );
    } else {
        $msg = JText::_( 'Error Saving Greeting' );
    }
	 
		$menu = &JSite::getMenu();
		$Items	= $menu->getItems('link', 'index.php?option=com_idoblog&view=idoblog');
		$Itemid=$Items[0]->id;
	$link = JRoute::_( 'index.php?option=com_idoblog&Itemid='.$Itemid);
    $this->setRedirect(htmlspecialchars_decode($link), $msg); 
    }

	function cancel()
	{
		$menu = &JSite::getMenu();
		$Items	= $menu->getItems('link', 'index.php?option=com_idoblog&view=idoblog');
		$Itemid=$Items[0]->id;
	 $link = JRoute::_( 'index.php?option=com_idoblog&Itemid='.$Itemid );
    $this->setRedirect($link, $msg); 	
	}



}

?>