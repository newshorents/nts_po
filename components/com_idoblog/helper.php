<?php
//getUserId return userid if user registered, else show 403
//parameters
// post - if $post=1 then get userid in POST DATA
//-----------------------------------------------------
function getUserId($post) {GLOBAL $mainframe;
if ($post=='1') $userid=JRequest::getVar('userid', 0, '', 'int');
if (empty($userid)) {
		$user= &JFactory::getUser();
		$userid=$user->id;

		if (empty($userid)) {
							$mainframe->redirect( 'index.php?option=com_user&view=login' );
							return;
							}
					}
return $userid;
}


//getUserTemplate return name of user template
//parameters
// userTemplate - user can selected your template for blog
//-----------------------------------------------------
function getUserTemplate($userTemplate) {
GLOBAL $config;
if (!empty($userTemplate) && $config->template_enabled=="1") $template=$userTemplate; else $template=$config->shablon;

return $template;
}

//get path var or other
//-----------------------------------------------------
function getMyVar($type, $user, $option="") {
$Itemid=JRequest::getVar('Itemid', 0, '', 'int');
global $config;

if ($type=='userlink') return JRoute::_( 'index.php?option=com_idoblog&task=userblog&userid='. $user->userid.'&Itemid='.$Itemid );

if ($type=='profile')  return JRoute::_( 'index.php?option=com_idoblog&task=profile&userid='. $user->userid.'&Itemid='.$Itemid );

if ($type=='adsense' && !empty($option->adsense_pub) && !empty($option->adsense_slot) && $config->adsense_enabled=="1")  return '<script type="text/javascript"><!--
google_ad_client = "'.$option->adsense_pub.'";
google_ad_slot = "'.$option->adsense_slot.'";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>';

if ($type=='readmore') return JRoute::_( 'index.php?option=com_idoblog&task=viewpost&id='. $option->id.'&Itemid='.$Itemid );

if ($type=='username') if ($config->typename=='1') return $user->username; else return $user->name;

if ($type=='editlink') return JRoute::_( 'index.php?option=com_idoblog&task=editpost&id='. $option->id.'&Itemid='.$Itemid );

if ($type=='deletelink') return JRoute::_( 'index.php?option=com_idoblog&task=deletepost&id='. $option->id.'&Itemid='.$Itemid );
}


//get one tooltip
//-----------------------------------------------------
function gettooltip($row,$friends)
{
GLOBAL $config;
$user =& JFactory::getUser();


$text="<div id='supernote-note-demo".$row->userid."' class='snp-mouseoffset pinnable notedefault'><a name='demo".$row->userid."'></a><table width='150' border='0'><tr>
					<td><a href='".$row->userlink."'>".$row->username." ".JText::_( "Blog" )." </a></td></tr>
					<tr>
					<td><a href='".$row->profile."'>".JText::_( "Profile" )."</a></td>
					</tr>";

//check friends
$check=0;
if (empty($friends)) $friends=0;
for ($z=0;$z<count($friends);$z++) {if ($friends[$z]==$row->userid) $check=1;}

if ($user->id>0 && $user->id!=$row->userid && $check!=1) $text.="<tr>
		<td><span id='friend".$row->userid."'><a href='javascript:void(0)' onclick='xajax_addfriend(".$row->userid.")'>".JText::_( "Add to friendlist" )."</a></span></td></tr>";


$text.="</table></div>";
return $text;
}


//check online user or not
//-----------------------------------------------------
function getsex($sex,$userid,$online) {
GLOBAL $config, $live_site;
$sexava="";
if ($sex=='f') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/female.gif' border='0'>";
if ($sex=='m') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/male.gif' border='0'>";

if ($config->online_enabled=="1") {
if (empty($online)) {
if ($sex=='f') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/female_offline.gif' border='0'>";
if ($sex=='m') $sexava="<img src='".$live_site."components/com_idoblog/assets/images/male_offline.gif' border='0'>";
}
}
return $sexava;
}

//get user avatar
//-----------------------------------------------------
function getavatar($avatar,$comment) {
GLOBAL $config, $live_site;
jimport('joomla.filesystem.file');
if ($comment==true) $width=$config->smallavatarwidth; else $width=$config->avatarwidth;
if (empty($avatar) || !JFile::exists(JPATH_BASE.'/images/idoblog/'.$avatar))
return '<img src="'.$live_site.'images/idoblog/gallery/default.gif" border="0"  width="'.$width.'">';
else
return '<img src="'.$live_site.'images/idoblog/'.$avatar.'" border="0" width="'.$width.'">';
}


//create blog and file archive for user
//-----------------------------------------------------
function createaccaunt() {
global $config;
$user=& JFactory::getUser();
	$db =& JFactory::getDBO();
$query="SELECT idcategory FROM #__idoblog_user_reffer WHERE iduser=".$user->id;
$db->setQuery( $query );
$idcategory=$db->loadResult();

if (empty($idcategory)) {

	$query="INSERT INTO `#__categories` ( `id` , `parent_id` , `title` , `name` , `alias` , `image` , `section` , `image_position` , `description` , `published` , `checked_out` , `checked_out_time` , `editor` , `ordering` , `access` , `count` , `params` )
VALUES (
'', '0', '".$user->username."', '".$user->username."', '".$user->username."', '', '".$config->idblog."', 'left', '', '1', '0', '0000-00-00 00:00:00', NULL , '1', '0', '0', ''
);";
			$db->setQuery( $query );
			$db->query();
			$idcategory=$db->insertid();

	$query="INSERT INTO `#__idoblog_user_reffer` ( `iduser` , `idcategory` )
			VALUES (
			'".$user->id."', '".$idcategory."');";
			$db->setQuery( $query );
			$db->query();

			$query="INSERT INTO `#__idoblog_users` ( `iduser` , `birthday` , `avatar` , `icq` , `about`, `small_avatar` )
VALUES (
'".$user->id."', '0000-00-00', '', '', '','');";
			$db->setQuery( $query );
			$db->query();

	jimport('joomla.filesystem.folder');
	jimport('joomla.filesystem.path');
	$path=JPATH_SITE.DS."images".DS."idoblog".DS."upload".DS.JSTRING::strtolower($user->username);
	JPath::clean($path);
	$path2=JPATH_SITE.DS."images".DS."idoblog".DS."upload".DS.JSTRING::strtolower($user->username.DS."preview");
	JPath::clean($path2);

	JFolder::create($path,"0755");
	JPath::isOwner($path);
	JPath::setPermissions($path, '0755');

	JFolder::create($path2,"0755");
	JPath::isOwner($path2);
	JPath::setPermissions($path2, '0755');

	$session =& JFactory::getSession();
	$session->set('catid', $idcategory, 'idoblog');
}

return $idcategory;
}

function myget(&$var) {
if (empty($var)) $var="";
}

?>
