<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class idoblogViewEntry extends JView
{
    function display($tpl = null)
    {
GLOBAL $mainframe, $config, $live_site;
$model 		 =& $this->getModel();
$user		 =& JFactory::getUser();
$document	 =& JFactory::getDocument();
$dispatcher	 =& JDispatcher::getInstance();
$layout		 =  JRequest::getVar('layout');
$breadcrumbs = & $mainframe->getPathWay();
$session	 =& JFactory::getSession();
$acl	  	 =& JFactory::getACL();


$gid	= $user->get( 'gid' );
if ($gid==0) $gid=29;
$gids 	= $acl->get_group_parents( $gid, 'users', 'RECURSE' );
$gids[]=$gid;


if (!in_array( $config->access_write, $gids ) && !in_array( $config->access_moderate, $gids ) ) {
echo JText::_("NON PERMISSONS WRITE");
}
else
{

$articleid=JRequest::getVar('id');
$article=$model->getarticle($articleid);


$test = getUserId(1);

if (empty($article->catid) || ($article->catid==0))
$catid=$model->getcatid($config->idblog);
else
{
$catid=$article->catid;
if ($session->get('catid', null, 'idoblog')) createaccaunt();

$breadcrumbs->addItem( JText::_( 'edit article' ), '' );
}

if (JString::strlen($article->fulltext) > 1)
		$article->text = $article->introtext.'<hr id="system-readmore" />'.$article->fulltext;
		else
		$article->text = $article->introtext;

		if (empty($article->created_by)) $article->created_by=$user->id;
		//подключаем редактор
		$editor =& JFactory::getEditor();
		$this->assignRef( 'editor', $editor );

		$files=$model->getyourfiles();
		$this->assignRef( 'files', $files );

	$alltags=$model->getalltags();
if ($articleid>0) $articletags=$model->gettags($articleid);

		$this->assignRef( 'catid', $catid );
		$this->assignRef( 'sectionid', $config->idblog );
		$this->assignRef('user',	$user);
		$this->assignRef('article',	$article);
		$this->assignRef('alltags',	$alltags);
		$this->assignRef('articletags',	$articletags);

		$this->assignRef('config',	$config);
        parent::display($tpl);
		}
    }
}

?>