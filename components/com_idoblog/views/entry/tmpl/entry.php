<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

?>

<form action="index.php" method="post" name="adminForm" >

<span id ="title_write_blog"><?php echo JText::_('Write to blog'); ?></span>
<table class="adminform" width="100%">
<tr>
	<td>
		<div style="float: left;">
			<label for="title">
				<strong>
					<?php echo JText::_( 'Title' ); ?>:
				</strong>
			</label>
			<br>
			<input class="inputbox" type="text" id="title" name="title" size="96" maxlength="100" value="<?php if (!empty($this->article->title)) echo $this->article->title; ?>" />
		</div>
	</td>
</tr>
</table>

<?php
echo $this->editor->display("text", $this->article->text, "100%", "300px", "50", "30", true); ?>


<?php /*
if ($this->config->file_archive==1) { ?>
<fieldset>
<legend><?php echo JText::_('File archive'); ?></legend>
<a name="f"></a>
<div class="filemanager">
<?php for($z=0;$z<count($this->files);$z++) {
$file=$this->files[$z];
echo $file->show;
}
?>
<span id="n<?php echo $z; ?>"></span>
</div>
<input name="number" id="number" type="hidden" value="<?php echo $z; ?>" />
<iframe frameborder="0" width="450" height="50" scrolling="no" src="<?php echo JRoute::_( 'index.php?option=com_idoblog&task=fileform&tmpl=component'); ?>"></iframe>
</fieldset>
<?php } */?>


<?php /*
?>
<fieldset>
<legend><?php echo JText::_('Tags'); ?></legend>
<input class="inputbox" type="text" id="tags" name="tags" size="200" style="width:100%" value="<?php echo $this->articletags; ?>" />
<br />
<i>(<?php echo JText::_('SELECT TAGS OR WRITE TAGS COMMA-SEPARATED') ?>)</i>
<br /><br />
<?php
for ($i=0, $n=count( $this->alltags ); $i < $n; $i++)
    {
        $row =& $this->alltags[$i];
        ?>
<span style="cursor:pointer;" class="tag<?php echo $row->div; ?>" onclick="document.getElementById('tags').value+=document.getElementById('tag<?php echo $i; ?>').value + ', '"><?php echo $row->tagname; ?></span><input id="tag<?php echo $i; ?>" type="hidden" value="<?php echo $row->tagname; ?>" />
		<?php
	}
	?>

</fieldset>

<?php */
?>


<div id="btn_frm_blog">
			<button type="button" onclick="submitbutton('save')" class="btnBlog">
				<?php echo JText::_('Save') ?>
			</button>
			<button type="button" onclick="submitbutton('cancel')" class="btnBlog">
				<?php echo JText::_('Cancel') ?>
			</button>
</div>


<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="id" value="<?php echo $this->article->id; ?>" />
<input type="hidden" name="controller" value="entry" />
<input type="hidden" name="state" value="1" />
<input type="hidden" name="sectionid" value="<?php echo $this->sectionid; ?>" />
<input type="hidden" name="catid" value="<?php echo $this->catid; ?>" />
<input type="hidden" name="created_by" value="<?php echo $this->article->created_by; ?>" />
<input type="hidden" name="referer" value="<?php echo @$_SERVER['HTTP_REFERER']; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="task" value="" />
</form>

