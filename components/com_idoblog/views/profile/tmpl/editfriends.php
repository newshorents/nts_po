<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
  <script type="text/javascript" src="/components/com_idoblog/assets/js/wz_tooltip.js"></script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
    <fieldset class="adminform">
        <legend><?php echo JText::_( 'Edit friends' ); ?></legend>
<table width="100%" cellpadding="5" cellspacing="0">
<tr bgcolor="#CCCCCC"  style="font-weight:bold;"  height="25">
<td width="5%" align="center"><img src="components/com_idoblog/assets/images/arrow-mutual.gif" /></td>
<td width="35%"><?php echo JText::_( 'Login' ); ?></td>
<td width="50%"><?php echo JText::_( 'Nameis' ); ?></td>
<td width="10%"><?php echo JText::_( 'Action' ); ?></td>
</tr>
<?php
    for ($i=0, $n=count( $this->friends ); $i < $n; $i++)
    {
        $friend =& $this->friends[$i];
	    ?>
<tr height="20" id="friend<?php echo $friend->id; ?>"><td align="center"><?php if ($friend->both==0) { ?>
<img src="components/com_idoblog/assets/images/arrow-friend.gif" />
<?php } else { ?>
<img src="components/com_idoblog/assets/images/arrow-mutual.gif" />
<?php } ?>
</td>
<td>
<a href="<?php echo $friend->link; ?>"><?php echo $friend->username; ?></a></td>
<td><?php echo $friend->name; ?></td>
<td><a href="javascript:void(0)" onclick="xajax_deletefriend(<?php echo $friend->id; ?>, <?php echo $friend->both; ?>)"><?php echo JText::_( 'Delete' ); ?></a></td>
</tr>

<?php } ?>

<?php
    for ($i=0, $n=count( $this->friendsat ); $i < $n; $i++)
    {
        $friend =& $this->friendsat[$i];
	    ?>
<?php if ($friend->both==0) { ?>
<tr height="20"><td align="center">
<img src="components/com_idoblog/assets/images/arrow-friendof.gif" />
</td>
<td>
<a href="<?php echo $friend->link; ?>"><?php echo $friend->real_username; ?></a></td>
<td><?php echo $friend->real_name; ?></td>
<td><span id="friend<?php echo $friend->id; ?>"><a href="javascript:void(0)" onclick="xajax_addfriend(<?php echo $friend->id; ?>)"><?php echo JText::_( 'Add' ); ?></a></span></td>
</tr>
<?php } ?>

<?php } ?>
</table>
    </fieldset>
</div>



<div class="clr"></div>

<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="id" value="<?php echo $this->userdata->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="referer" value="<?php echo @$_SERVER['HTTP_REFERER']; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>