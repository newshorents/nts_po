<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form name="iform" action="" method="post" enctype="multipart/form-data" style="margin: 0,0,0,0px;">
<?php echo JText::_( 'Upload avatar' ); ?>: <input id="file1" type="file" name="image1" style="width:40"/>
<input name="send" type="button" value="<?php echo JText::_( 'Upload' ); ?>" style="width:40" onClick="document.iform.submit();">

<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="iduser" value="<?php echo $this->userdata->id; ?>" />
<input type="hidden" name="task" value="upavatar" />
<input type="hidden" name="referer" value="<?php echo @$_SERVER['HTTP_REFERER']; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>