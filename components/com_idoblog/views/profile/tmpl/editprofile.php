<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<?php

	JHTML::_('behavior.mootools');
			jimport('joomla.html.pane');

		$pane =& JPane::getInstance('Tabs');
echo $pane->startPane('myPane');
{

echo $pane->startPanel(JText::_( 'About' ), 'panel2');
?>
<br /><br />
<?php
echo $this->editor->display("text", $this->userdata->about, "100%", "300px", "50", "30", false);

echo $pane->endPanel();
echo $pane->startPanel(JText::_( 'Edit profile' ), 'panel1');

?>

        <table class="admintable">
        <tr>
            <td width="100" align="right" class="key">
                <label for="nick">
                    <?php echo JText::_( 'User_name' ); ?>:
                </label>
            </td>
            <td>
                <?php echo $this->userdata->real_username; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="Fio">
                    <?php echo JText::_( 'NAMEIS' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="text" name="name" id="name" size="32" maxlength="250" value="<?php echo $this->userdata->real_name;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="Email">
                    <?php echo JText::_( 'E-mail' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="text" name="email" id="email" size="32" maxlength="250" value="<?php echo $this->userdata->email;?>" />
            </td>
        </tr>
<tr>
            <td width="100" align="right" class="key">
                <label for="ICQ">
                    <?php echo JText::_( 'ICQ' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="text" name="icq" id="icq" size="32" maxlength="10" value="<?php echo $this->userdata->icq;?>" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="Password">
                    <?php echo JText::_( 'Birthday' ); ?>:
                </label>
            </td>
            <td>
			<?php echo JHTML::_('behavior.calendar'); ?>
			<input class="text_area" type="text" name="birthday" id="birthday" size="25" maxlength="19" value="<?php echo $this->userdata->birthday;?>" /> <input type="reset" class="button" value="..." onclick="return showCalendar('birthday','<?php echo "%Y-%m-%d" ?>');" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="Password">
                    <?php echo JText::_( 'Sex' ); ?>:
                </label>
            </td>
            <td>
				<?php echo JHTML::_('select.genericlist',   $this->selectsex, 'sex', 'size="1" class="inputbox"', 'value', 'text', $this->userdata->sex ); ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="Password">
                    <?php echo JText::_( 'Password' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="password" name="password" id="password" size="32" maxlength="250" value="" />
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="Password2">
                    <?php echo JText::_( 'Verify Password' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="password" name="password2" id="password2" size="32" maxlength="250" value="" />
            </td>
        </tr>
    </table>
<?php
echo $pane->endPanel();
if ($this->config->show_avatars=="1") {
echo $pane->startPanel(JText::_( 'Avatar' ), 'panel3');
?>
	<br />
<br />
		<table width="100%" style="margin-left:-10px">
<tr valign="top"><td width="100"><div class="avatar-border" style="float:left">
<div class="user-icon" id="myavatar" name="myavatar"><?php echo $this->userdata->myavatar; ?>
<input name='myavatarname' type='hidden' value='<?php echo $this->userdata->avatar ?>'/>
<input name='mysmallavatarname' type='hidden' value='<?php echo $this->userdata->small_avatar ?>'/>
</div>
</div>
</td>
<td width="5"></td>
<td>
<iframe frameborder="0" width="450" height="100" scrolling="no" src="<?php echo $this->framepath; ?>"></iframe>
</td>
</tr>
</table>

<?php
echo $pane->endPanel();
}
}
echo $pane->endPane();
?>

<fieldset>
<div style="float: right;">
			<button type="button" onclick="if (fieldfull()==true) {submitbutton('saveprofile')}" class="btnBlog">
				<?php echo JText::_('Save') ?>
			</button>
			<button type="button" onclick="submitbutton('cancel')"  class="btnBlog">
				<?php echo JText::_('Cancel') ?>
			</button>
		</div>
</fieldset>



<script>
function fieldfull() {
if (document.getElementById('name').value=="") {
		alert('<?php echo JText::_('Be sure to be filled field name') ?>');
		document.getElementById('name').focus();
		return false;
		}

if (document.getElementById('email').value=="") {
		alert('<?php echo JText::_('Be sure to be filled field email') ?>');
		document.getElementById('email').focus();
		return false;
		}

if (document.getElementById('password').value!=document.getElementById('password2').value) {
		alert('<?php echo JText::_('Passwords do not match') ?>');
		document.getElementById('password').focus();
		return false;
		}

return true;
}
</script>

<div class="clr"></div>

<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="id" value="<?php echo $this->userdata->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="referer" value="<?php echo @$_SERVER['HTTP_REFERER']; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>