<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">

<div class="componentheading">

<?php echo JText::_( 'EDIT BLOG' ); ?>

	</div>

<?php

			JHTML::_('behavior.modal', 'a.modal');
			jimport('joomla.html.pane');
			JHTML::_('behavior.tooltip');
		$pane =& JPane::getInstance('Tabs');
echo $pane->startPane('myPane');
{
echo $pane->startPanel(JText::_( "Blog's description" ), 'panel1');

echo $this->editor->display("text", $this->userdata->description, "100%", "300px", "50", "30", false);

echo $pane->endPanel();

if ($this->config->template_enabled=='1') {
echo $pane->startPanel(JText::_( 'Templates' ), 'panel2');

    for ($i=0, $n=count( $this->shablons ); $i < $n; $i++)
    {
	$shablon =$this->shablons[$i];
	$sel="";
	if ($this->userdata->template==$shablon['name']) $sel='checked="checked"';
?>
    <table class="template">
	<tr><td>
	<a href="components/com_idoblog/assets/templates/<?php echo $shablon['name'];?>/template.gif" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 600}}"><img src="components/com_idoblog/assets/templates/<?php echo $shablon['name'];?>/small_template.gif" width="150" /></a>
	</td>
	</tr>
	   <tr><td>	<input name="templ_select" id="temp<?php echo $i; ?>" onclick="document.getElementById('templ').value=this.value" type="radio" value="<?php echo $shablon['name'];?>" <?php echo $sel; ?> /><?php echo $shablon['name'];?></td></tr></table>
<?php
	}
}
echo $pane->endPanel();

if ($this->config->adsense_enabled=='1') {
echo $pane->startPanel(JText::_( 'Adsense' ), 'panel3');
?>
<i><?php echo JText::_( 'CREATE_ADSENSE_ACCOUNT' ); ?></i>
        <table class="admintable">
        <tr>
            <td width="100" align="right" class="key">
                <label for="nick">
                    <?php echo JText::_( 'Adsense client' ); ?>:
                </label>
            </td>
            <td>
                <input name="adsense_pub" type="text" value="<?php echo $this->userdata->adsense_pub; ?>" /> <?php echo JHTML::_('tooltip', JText::_( 'EXAMPLE ADSENSE CLIENT' )); ?>
            </td>
			<td></td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="nick">
                    <?php echo JText::_( 'Adsense slot' ); ?>:
                </label>
            </td>
            <td>
                <input name="adsense_slot" type="text" value="<?php echo $this->userdata->adsense_slot; ?>" /> <?php echo JHTML::_('tooltip', JText::_( 'EXAMPLE ADSENSE SLOT' )); ?>
            </td>
        </tr>
		        <tr>
            <td width="100" align="right" class="key">
                <label for="nick">
                    <?php echo JText::_( 'Adsense type' ); ?>:
                </label>
            </td>
			<td>
			<?php
			$typeadsense = array(
			JHTML::_('select.option',  '', JText::_( 'DISABLE' ) ),
			JHTML::_('select.option',  'b', JText::_( 'BOTTOM BLOG' ) ),
			JHTML::_('select.option',  't', JText::_( 'TOP BLOG' ) )
		);

		echo JHTML::_('select.genericlist',   $typeadsense, 'typeadsense', 'size="1" class="inputbox"', 'value', 'text', $this->userdata->adsense_type );
			?>

			</td>
        </tr>
	</table>

<?php
echo $pane->endPanel();
}

}
echo $pane->endPane();
?>

<fieldset>
<div style="float: left;">
			<button type="button" onclick="submitbutton('saveblog')">
				<?php echo JText::_('Save') ?>
			</button>
			<button type="button" onclick="submitbutton('cancel')">
				<?php echo JText::_('Cancel') ?>
			</button>
		</div>
</fieldset>

<div class="clr"></div>
<input name="templ" id="templ" type="hidden" value="<?php echo $this->userdata->template ?>" />
<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="id" value="<?php echo $this->userdata->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="referer" value="<?php echo @$_SERVER['HTTP_REFERER']; ?>" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>