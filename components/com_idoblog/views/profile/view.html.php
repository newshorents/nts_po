<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class idoblogViewProfile extends JView
{
    function display($tpl = null)
    {
GLOBAL $config, $mainframe, $live_site;
$dispatcher	   =& JDispatcher::getInstance();
$document	   =& JFactory::getDocument();
$task 		   = JRequest::getVar('task');
$model =& $this->getModel();

$userid = getUserId(1);


$document->addScript($live_site.'components/com_idoblog/assets/js/supernote.js');
		$document->addScript($live_site.'components/com_idoblog/assets/js/comment.js');

//create accaunt
$myid=JRequest::getVar('userid');
if (empty($myid)) {
$session	 =& JFactory::getSession();
$myses=$session->get('catid', null, 'idoblog');
if (empty($myses)) createaccaunt();
}


$title=$document->getTitle();
$document->setTitle($title.' | '.$mainframe->getCfg('sitename'));
$document->addStyleSheet($live_site.'components/com_idoblog/assets/templates/'.$config->shablon.'/default.css');

$userdata=$model->getuser();
$friends=$model->getyourfriends();
$friendsat=$model->getyourfriendsat();

if ($task=='editprofile')
{
		//подключаем редактор
		$editor =& JFactory::getEditor();
		$this->assignRef( 'editor', $editor );
		$this->assignRef( 'framepath', JRoute::_( 'index.php?option=com_idoblog&task=avatarform&tmpl=component') );
		$selectsex=$model->getselectsex($userdata->sex);
		$this->assignRef( 'selectsex', $selectsex );
}

if ($task=='editblog')
{
		//подключаем редактор
		$editor =& JFactory::getEditor();
		$this->assignRef( 'editor', $editor );

		$shablons=JFolder::listFolderTree(JPATH_COMPONENT_SITE.DS."assets".DS."templates", ".", $maxLevel = 1, $level = 0, $parent = 0);
  		$this->assignRef( 'shablons', $shablons );
}

	if ($config->import_plugins=='1') {
	JPluginHelper::importPlugin('content');
	if (empty($limitstart)) $limitstart="";
	$results = $dispatcher->trigger('onPrepareContent', array (& $userdata, & $params, $limitstart));
}

		$this->assignRef( 'userdata', $userdata );
 		$this->assignRef( 'friends', $friends );
 		$this->assignRef( 'friendsat', $friendsat );
  		$this->assignRef( 'config', $config );
        parent::display($tpl);
    }
}

?>