<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class idoblogViewidoblog extends JView
{
    function display($tpl = null)
    {
	GLOBAL $mainframe, $config, $live_site;

//get global values
$uri 		=& JFactory::getURI();
if (empty($config->limit)) $limit = 20; else $limit = JRequest::getVar('limit',	$config->limit,	'', 'int');
$limitstart	= JRequest::getVar('limitstart', 0, '', 'int');
$layout		= JRequest::getVar('layout');
$task		= JRequest::getVar('task');
$Itemid		=JRequest::getVar('Itemid', 0, '', 'int');
$dispatcher =& JDispatcher::getInstance();
$user 		=& JFactory::getUser();
$document 	=& JFactory::getDocument();
$acl	  	 =& JFactory::getACL();
$session	 =& JFactory::getSession();



//import plugins group content
if ($config->import_plugins=='1') JPluginHelper::importPlugin('content');

//set title
$title=$document->getTitle();
$document->setTitle($title.' | '.$mainframe->getCfg('sitename'));


if ($layout=='yourblog') $userid=getUserId(1);
if ($layout=='friend') $userid=getUserId(0);


$model =& $this->getModel();
if (empty($userid)) $userid="";

//get posts
$items =$model->getData($limitstart,$limit,$userid);

if (empty($items[0]->template)) $items[0]->template="";
$config->shablon=getUserTemplate($items[0]->template);

$document->addStyleSheet($live_site.'components/com_idoblog/assets/templates/'.$config->shablon.'/default.css');

$document->addScript($live_site.'components/com_idoblog/assets/js/supernote.js');
		$document->addScript($live_site.'components/com_idoblog/assets/js/comment.js');
if ($task=='viewpost') {


		$comments=$model->getComments();

		//enabled smiles
		$smiley="";
		if ($config->enabled_smiley=='1') $smiley=$model->getSmiley();
		$this->assignRef('smiley', $smiley);

		//enabled bbcode
		$bbcode="";
		if ($config->enabled_bbcode=='1') $bbcode=$model->getBBCode();
		$this->assignRef('bbcode', $bbcode);

		$newhits=$items[0]->hits + 1;
		$db =& JFactory::getDBO();
		$query="UPDATE `#__content` SET `hits` = '".$newhits."' WHERE `id` =".$items[0]->id." ";
		$db->setQuery( $query );
		$db->query();

		//handling content plugins for comment
		if ($config->import_plugins=='1') {
		  for ($i=0, $n=count( $comments ); $i < $n; $i++)
    		{
        $comment = & $comments[$i];
		$results = $dispatcher->trigger('onPrepareContent', array (& $comment, & $params,0));
			}
		}

		$document->setTitle($items[0]->title.' | '.$mainframe->getCfg('sitename'));
		$this->assignRef('comments', $comments);

		GLOBAL $keystring;
		$keystring=$model->getkeystring();
		$session->set('mystring', rand(100,500), 'idoblog');
		$session->set('keystring', $keystring, 'idoblog');
		}

$tooltips =& $this->get('tooltip');
$total=& $this->get('totalposts');

jimport('joomla.html.pagination');
$pagination = new JPagination($total, $limitstart, $limit);


$shabpath=$live_site."components/com_idoblog/assets/templates/".$config->shablon."/";

//create rss
$link   = '&format=feed&limitstart=';
$attribs = array('type' => 'application/rss+xml', 'title' => 'RSS 2.0');
$document->addHeadLink(JRoute::_($link.'&type=rss'), 'alternate', 'rel', $attribs);
$attribs = array('type' => 'application/atom+xml', 'title' => 'Atom 1.0');
$document->addHeadLink(JRoute::_($link.'&type=atom'), 'alternate', 'rel', $attribs);


if (empty($items)) echo JText::_('Blog is empty');

//handling content plugins for comment
if ($config->import_plugins=='1') {
		  for ($i=0, $n=count( $items ); $i < $n; $i++)
    		{
        $item = & $items[$i];
	$results = $dispatcher->trigger('onPrepareContent', array (& $item, & $params, $limitstart));
			}
}

//calculate who can write comment and who can moderate comment
$gid	= $user->get( 'gid' );
if ($gid==0) $gid=29;
$gids 	= $acl->get_group_parents( $gid, 'users', 'RECURSE' );
$gids[]=$gid;

if (in_array( $config->access_comment, $gids ) || in_array( $config->access_moderate, $gids )) $this->access_comment=true; else $this->access_comment=false;
if (in_array( $config->access_moderate, $gids )) $this->access_moderate=true; else $this->access_moderate=false;

		$this->assignRef('items', $items);
		$this->assignRef('shabpath', $shabpath);
		$this->assignRef('myshablon', $config->shablon);
		$this->assignRef('pagination', $pagination);
		$this->assignRef('user', $user);
		$this->assignRef('tooltips', $tooltips);
		$this->assignRef('Itemid', $Itemid);
		$this->assignRef('config', $config);
		$this->assignRef('live_site', $live_site);



if (!empty($items[0]->title))
        parent::display($tpl); else echo JText::_( 'BLOG IS EMPTY' );
    }
}

?>