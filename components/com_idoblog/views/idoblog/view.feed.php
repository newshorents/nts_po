<?php

defined('_JEXEC') or die();

jimport( 'joomla.application.component.view');

class idoblogViewidoblog extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $config;
		$db			=& JFactory::getDBO();
		$document	=& JFactory::getDocument();


$limit	= JRequest::getVar('limit',	$config->limit,	'', 'int');
$limitstart	= JRequest::getVar('limitstart', 0, '', 'int');
$layout	= JRequest::getVar('layout');
$task	= JRequest::getVar('task');
$user =& JFactory::getUser();
$document =& JFactory::getDocument();

//set title
$title=$document->getTitle();
$document->setTitle($title.' | '.$mainframe->getCfg('sitename'));	

if ($layout=='yourblog') $userid=getUserId(1);
if ($layout=='friend') $userid=getUserId(0);

$model =& $this->getModel();
$items =$model->getData($limitstart,$limit,$userid);
		
if ($task=='viewpost') $comments=$model->getComments();	
		
    for ($i=0, $n=count( $items ); $i < $n; $i++)
    {
        $row =& $items[$i];

			// load individual item creator class
			$item = new JFeedItem();
			$item->title 		= $row->title;
			$item->link 		= $row->readmore;
			$item->description 	= $row->introtext;
			$item->author			= $row->username;
			$item->date=	$row->fulldate;
			$item->category   	= $row->tags;

			// loads item info into rss array
			$document->addItem( $item );
	}		
	
	
	}
}
?>