<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

class idoblogController extends JController
{
    /**
     * метод вывода на экран
     *
     * @access    public
     */
    function view()
    {
            JRequest::setVar( 'view', 'idoblog' );
    JRequest::setVar( 'layout', 'default'  );

 	   parent::display();
    }

    function editpost()
    {
            JRequest::setVar( 'view', 'entry' );
    JRequest::setVar( 'layout', 'entry'  );

 	   parent::display();
    }

    function userblog()
    {
            JRequest::setVar( 'view', 'idoblog' );
    JRequest::setVar( 'layout', 'yourblog'  );

 	   parent::display();
    }

	function cancel()
	{
		$menu = &JSite::getMenu();
		$Items	= $menu->getItems('link', 'index.php?option=com_idoblog&view=idoblog');
		$Itemid=$Items[0]->id;
	 $link = JRoute::_( 'index.php?option=com_idoblog&Itemid='.$Itemid );
    $this->setRedirect($link, $msg); 	
	}

    function avatarform()
    {
            JRequest::setVar( 'view', 'profile' );
    JRequest::setVar( 'layout', 'avatarform'  );

 	   parent::display();
    }

    function upavatar()
    {
   	$model = $this->getModel('profile');
            JRequest::setVar( 'view', 'profile' );
    JRequest::setVar( 'layout', 'avatarform'  );
	$script=$model->upavatar();
 	echo $script;
	   parent::display();
    }

    function viewtag()
    {
            JRequest::setVar( 'view', 'idoblog' );
    JRequest::setVar( 'layout', 'tag'  );

 	   parent::display();
    }

    function viewpost()
    {
    JRequest::setVar( 'view', 'idoblog' );
    JRequest::setVar( 'layout', 'post'  );
 	   parent::display();
    }

    function profile()
    {
    JRequest::setVar( 'view', 'profile' );
    JRequest::setVar( 'layout', 'profile'  );
 	   parent::display();
    }

    function editprofile()
    {
    JRequest::setVar( 'view', 'profile' );
    JRequest::setVar( 'layout', 'editprofile'  );
 	parent::display();
    }

    function editfriends()
    {
    JRequest::setVar( 'view', 'profile' );
    JRequest::setVar( 'layout', 'editfriends'  );
 	parent::display();
    }

	function deletepost()
	{
	$Itemid=JRequest::getVar('Itemid','');
	$model = $this->getModel('entry');

    if ($model->delete($post)) {
        $msg = JText::_( 'Post greeting delete!' );
    } else {
        $msg = JText::_( 'Error delete post' );
    }
    $link = JRoute::_('index.php?option=com_idoblog&Itemid='.$Itemid);
    $this->setRedirect($link, $msg);	
	}

	function saveprofile()
	{
	$Itemid=JRequest::getVar('Itemid',  '');
	$user =& JFactory::getUser();
	$model=$this->getModel('profile');
	    if ($model->saveprofile($post)) {
        $msg = JText::_( 'Greeting Saved!' );
    } else {
        $msg = JText::_( 'Error Saving Greeting' );
    }

	 $link = JRoute::_(@$_SERVER['HTTP_REFERER']);
	 
    $this->setRedirect(htmlspecialchars_decode($link), $msg); 
	}
	
    function editblog()
    {
    JRequest::setVar( 'view', 'profile' );
    JRequest::setVar( 'layout', 'editblog'  );
 	parent::display();
    }
	
	function saveblog()
	{
	$Itemid=JRequest::getVar('Itemid',  '');
	$user =& JFactory::getUser();
	$model=$this->getModel('profile');
	    if ($model->saveblog($post)) {
        $msg = JText::_( 'Greeting Saved!' );
    } else {
        $msg = JText::_( 'Error Saving Greeting' );
    }

	 $link = JRoute::_('index.php?option=com_idoblog&task=userblog&Itemid='.$Itemid);
    $this->setRedirect($link, $msg); 
	}
	
    function fileform()
    {
            JRequest::setVar( 'view', 'entry' );
    JRequest::setVar( 'layout', 'file'  );

 	   parent::display();
    }

    function upfile()
    {
   	$model = $this->getModel('entry');
            JRequest::setVar( 'view', 'entry' );
    JRequest::setVar( 'layout', 'file'  );
	$script=$model->upfile();
 	echo $script;
	   parent::display();
    }

}

?>