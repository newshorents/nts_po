var supernote = new SuperNote('supernote', {});

// Available config options are:
//allowNesting: true/false    // Whether to allow triggers within triggers.
//cssProp: 'visibility'       // CSS property used to show/hide notes and values.
//cssVis: 'inherit'
//cssHid: 'hidden'
//IESelectBoxFix: true/false  // Enables the IFRAME select-box-covering fix.
//showDelay: 0                // Millisecond delays.
//hideDelay: 500
//animInSpeed: 0.1            // Animation speeds, from 0.0 to 1.0; 1.0 disables.
//animOutSpeed: 0.1

// You can pass several to your "new SuperNote()" command like so:
//{ name: value, name2: value2, name3: value3 }


// All the script from this point on is optional!

// Optional animation setup: passed element and 0.0-1.0 animation progress.
// You can have as many custom animations in a note object as you want.
function animFade(ref, counter)
{
 //counter = Math.min(counter, 0.9); // Uncomment to make notes translucent.
 var f = ref.filters, done = (counter == 1);
 if (f)
 {
  if (!done && ref.style.filter.indexOf("alpha") == -1)
   ref.style.filter += ' alpha(opacity=' + (counter * 100) + ')';
  else if (f.length && f.alpha) with (f.alpha)
  {
   if (done) enabled = false;
   else { opacity = (counter * 100); enabled=true }
  }
 }
 else ref.style.opacity = ref.style.MozOpacity = counter*0.999;
};
supernote.animations[supernote.animations.length] = animFade;



// Optional custom note "close" button handler extension used in this example.
// This picks up click on CLASS="note-close" elements within CLASS="snb-pinned"
// notes, and closes the note when they are clicked.
// It can be deleted if you're not using it.
addEvent(document, 'click', function(evt)
{
 var elm = evt.target || evt.srcElement, closeBtn, note;

 while (elm)
 {
  if ((/note-close/).test(elm.className)) closeBtn = elm;
  if ((/snb-pinned/).test(elm.className)) { note = elm; break }
  elm = elm.parentNode;
 }

 if (closeBtn && note)
 {
  var noteData = note.id.match(/([a-z_\-0-9]+)-note-([a-z_\-0-9]+)/i);
  for (var i = 0; i < SuperNote.instances.length; i++)
   if (SuperNote.instances[i].myName == noteData[1])
   {
    setTimeout('SuperNote.instances[' + i + '].setVis("' + noteData[2] +
     '", false, true)', 50);
	cancelEvent(evt);
   }
 }
});


// Extending the script: you can capture mouse events on note show and hide.
// To get a reference to a note, use 'this.notes[noteID]' within a function.
// It has properties like 'ref' (the note element), 'trigRef' (its trigger),
// 'click' (whether its shows on click or not), 'visible' and 'animating'.
addEvent(supernote, 'show', function(noteID)
{
 // Do cool stuff here!
});
addEvent(supernote, 'hide', function(noteID)
{
 // Do cool stuff here!
});

function getcommentform(commentid) {
	document.getElementById('commentform'+commentid).style.display='block';
var lastparent=document.getElementById('parent').value;
document.getElementById('text').innerHTML="";
if (lastparent!=commentid) {
	document.getElementById('commentform'+commentid).innerHTML=document.getElementById('commentform'+lastparent).innerHTML;
	document.getElementById('commentform'+lastparent).innerHTML=document.getElementById('lasttextcomment').value;
	document.getElementById('lasttextcomment').value="";
}
document.getElementById('parent').value=commentid;
return false;
}

function geteditform(commentid) {
var lastparent=document.getElementById('parent').value;
var textcomment=document.getElementById('commentform'+commentid).innerHTML;

document.getElementById('text').innerHTML="Loading...";
if (lastparent!=commentid) {


	document.getElementById('commentform'+commentid).innerHTML=document.getElementById('commentform'+lastparent).innerHTML;

document.getElementById('commentform'+lastparent).innerHTML=document.getElementById('lasttextcomment').value;

document.getElementById('lasttextcomment').value=textcomment;

document.getElementById('parent').value=commentid;
}


return false;
}


function sendcomment() {
if (document.getElementById('itsusername').value=="") {
	document.getElementById('itsusername').style.border="2px solid red";
		document.getElementById('itsusername').focus();
		return false;
		}

if (document.getElementById('itsemail').value=="") {
	document.getElementById('itsemail').style.border="2px solid red";
		document.getElementById('itsemail').focus();
		return false;
		}

var username=document.getElementById('itsusername').value;
var email=document.getElementById('itsemail').value;
var keystring=document.getElementById('keystring').value;

document.getElementById('thiscomment').value=document.getElementById('text').value;

var lastparent=document.getElementById('parent').value;
if (lastparent.indexOf("-")==-1) document.getElementById('answer'+lastparent).style.display='none';

document.getElementById('commentform-1').innerHTML=document.getElementById('commentform'+lastparent).innerHTML;

	document.getElementById('commentform'+lastparent).innerHTML='';

document.getElementById('parent').value=-1;

if (lastparent.indexOf("-")==-1) document.getElementById('newcomment'+lastparent).innerHTML='<img src="/components/com_idoblog/assets/images/indicator.gif">';
else
document.getElementById('commentform'+lastparent).innerHTML='<img src="/components/com_idoblog/assets/images/indicator.gif">';

xajax_addcomment(document.getElementById('thiscomment').value,document.getElementById('idarticle').value,lastparent,document.getElementById('created_by').value,document.getElementById('shabpath').value, username, email, keystring);
}


function smile(symbol)
  {
if (navigator.appName.indexOf("Microsoft")!=-1) {
//for ie
  obj_ta=document.getElementById('text');
  obj_ta.focus();
  var sel=document.selection;
  var rng=sel.createRange();
  rng.colapse;
  var podst=rng.text+symbol;
  rng.text=podst;
}
else {
//for mozilla and opera
var es = document.getElementById('text').selectionEnd; //
   var txt1 = document.getElementById('text').value.substring(0,es); //  txt1
   var txt2 = document.getElementById('text').value.substring(es,document.getElementById('text').value.length); //  txt2 ,
   document.getElementById('text').value = txt1 + symbol + txt2;
}

  }

function bbcode(symbol)
  {
if (navigator.appName.indexOf("Microsoft")!=-1) {
//for ie
  obj_ta=document.getElementById('text');
  obj_ta.focus();
  var sel=document.selection;
  var rng=sel.createRange();
  rng.colapse;
   if (symbol=='URL')   var podst="["+symbol+"=http://]"+rng.text+"[/"+symbol+"]";
  else var podst="["+symbol+"]"+rng.text+"[/"+symbol+"]";
  rng.text=podst;
}
else {
//for mozilla and opera
var ee = document.getElementById('text').selectionEnd;
var es = document.getElementById('text').selectionStart;
   var txt1 = document.getElementById('text').value.substring(0,es); //  txt1
    var txt2 = document.getElementById('text').value.substring(es,ee); //  txt2 ,
   var txt3 = document.getElementById('text').value.substring(ee,document.getElementById('text').value.length); //  txt2 ,
   if (symbol=='URL') document.getElementById('text').value = txt1 + "["+symbol+"=http://]" + txt2 + "[/"+symbol+"]" + txt3;
   else document.getElementById('text').value = txt1 + "["+symbol+"]" + txt2 + "[/"+symbol+"]" + txt3;
}

  }