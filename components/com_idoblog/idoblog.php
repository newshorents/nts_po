<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
GLOBAL $config, $live_site, $copy;

//error_reporting(E_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR);
require_once( JPATH_COMPONENT.DS.'controller.php' );
	$fname = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_idoblog'.DS.'config.php';
require_once( $fname );
$config = new KBconfig();

$live_site=JURI::base();
if (empty($live_site)) $live_site="/";
// инициализация xajax
require_once( JPATH_COMPONENT.DS.'assets'.DS.'libraries'.DS.'xajax.inc.php' );
$xajax = new xajax();
$xajax->setCharEncoding("utf-8");
jimport( 'joomla.mail.helper' );
jimport('joomla.filesystem.file');
require_once( JPATH_COMPONENT.DS."xajax.idoblog.php");

//$xajax->debugOn();

$document =& JFactory::getDocument();
$document->addScriptDeclaration($xajax->getJavascript($live_site.'/components/com_idoblog/assets/libraries'));

require_once( JPATH_COMPONENT.DS.'helper.php' );
require_once( JPATH_COMPONENT.DS.'controller.php' );
//подключаем доп. контроллер
if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}

$document =& JFactory::getDocument();
$document->addStyleSheet($live_site.'components/com_idoblog/assets/css/idoblog.css');

$classname    = 'idoblogController'.$controller;
$controller   = new $classname( );

$controller->execute( JRequest::getVar( 'task' ) );
/*
$tmpl = JRequest::getVar('tmpl');
if ($tmpl!='component') {
$html = @implode('', file ('http://ru.idojoomla.com/special.php'));
echo $html;
}
*/

$tmpl = JRequest::getVar('tmpl');
if ($tmpl!='component') {
echo '<div style="float:right"><small><a href="http://idojoomla.com">IDOBlog - blog for joomla 1.5</a></small></div>';

}

$controller->redirect();
?>