<?php

function updateavatar($filename,$smallfilename)
{
$objResponse = new xajaxResponse();
GLOBAL $config, $live_site;
$text="";
	$text.='<img src="'.$live_site.'images/idoblog/'.$filename.'" border="0" width="'.$config->avatarwidth.'">';
$text.="<input name='myavatarname' type='hidden' value='".$filename."'/>";
$text.="<input name='mysmallavatarname' type='hidden' value='".$smallfilename."'/>";
	$objResponse->addAssign("myavatar","innerHTML",$text);
	return $objResponse->getXML();
}

function editcomment($idcomment)
{
$objResponse = new xajaxResponse();
$db =& JFactory::getDBO();
GLOBAL $live_site;
$query="SELECT * FROM #__idoblog_comments as c WHERE c.id=".$idcomment;
						$db->setQuery( $query );
						$comment=$db->loadObject();
						$comment=str_replace("<br>","",$comment->text);
						$comment=str_replace("<br />","",$comment);

	$objResponse->addAssign("text","innerHTML",$comment);
	return $objResponse->getXML();
}

function addcomment($comment,$idarticle,$parent,$created_by,$shabpath, $myusername, $myemail, $keystring)
{
$objResponse = new xajaxResponse();
GLOBAL $mainframe, $config, $live_site;
$user =& JFactory::getUser();
$user->username=$myusername;
$user->name=$myusername;
$user->email=$myemail;
$session	 =& JFactory::getSession();
$wrong="0";
$texterror="";
$sess_key=$session->get('keystring', null, 'idoblog');

if ($config->captcha==2 || ($config->captcha==1 && $user->id==0))
{
	if ($sess_key != $keystring) $wrong="1";
}

if ($wrong=="0") {
$myses=$session->get('catid', null, 'idoblog');
if (empty($myses) && $user->id>0) xcreateaccaunt();

//обработка коммента
$comment=htmlspecialchars(nl2br($comment));
$comment=str_replace("&lt;br&gt;","<br>",$comment);
$comment=str_replace("&lt;br /&gt;","<br>",$comment);

if ($config->typename=='1') $username=$user->username; else $username=$user->name;

$menu = &JSite::getMenu();
$Items	= $menu->getItems('link', 'index.php?option=com_idoblog&view=idoblog');
$Itemid=$Items[0]->id;

$shablon=@file_get_contents($shabpath."ajaxcomment.php");

if (empty($shablon)) $shablon="<table width='100%'><tr><td width='100'>{avatar}</td><td>{sex}{shabusername}
{comment}</td></tr></table>";
//if user authorized
if ($user->id>0) {
$userlink=JRoute::_( 'index.php?option=com_idoblog&task=profile&userid='. $user->id.'&Itemid='.$Itemid );
$shabusername='<a href="'.$userlink.'"><small>'.$username.'</small></a>';
}
else
{
$shabusername='<small>'.$myusername.'</small>';
}

$shablon=str_replace("{shabpath}",$shabpath,$shablon);
$shablon=str_replace("{comment}",$comment,$shablon);
//$shablon=str_replace("{userlink}",$userlink,$shablon);
$shablon=str_replace("{shabusername}",$shabusername,$shablon);

$date="NOW()";
$parents=$parent;
$db =& JFactory::getDBO();
$id='';
if (strstr($parent,'-'))
		{
			$id=split('-',$parent);
			$id=$id[0];
			$query="SELECT * FROM #__idoblog_comments WHERE id=".$id;
			$db->setQuery( $query );
			$mycomment=$db->loadObject();
			$date="'".$mycomment->date."'";
			$parents=$mycomment->parent;
		}


$query="REPLACE INTO `#__idoblog_comments` ( `id` , `parent` , `idarticle` , `created_by` , `date` , `text`, `publish`, `username`, `email` )
VALUES (
'".$id."', '".$parents."', '".$idarticle."', '".$created_by."', ".$date.", '".addslashes($comment)."', '1', '".$username."', '".$myemail."');";
$db->setQuery( $query );
$db->query();

if (empty($id))
		{
		$id=$db->insertid();
		$query="SELECT c.*,u.username, u.name, u.email, u.id as iduser FROM #__content as c, #__users as u WHERE c.id=".$idarticle." AND u.id=c.created_by";
		$db->setQuery( $query );
		$article=$db->loadObject();

		$myuser="";
		if ($user->id>0) {
			$query="SELECT small_avatar, sex FROM #__idoblog_users WHERE iduser='".$user->id."'";
			$db->setQuery( $query );
			$myuser=$db->loadObject();
			}
		//create avatar
		$width=$config->smallavatarwidth;

		if (empty($myuser->small_avatar)) $avatar='<img src="'.$live_site.'images/idoblog/gallery/default.gif" border="0"  width="'.$width.'">';
		else
		$avatar='<img src="'.$live_site.'images/idoblog/'.$myuser->small_avatar.'" border="0" width="'.$width.'">';

		if ($user->id>0) {
		$avatar='<a href="'.$userlink.'" class="supernote-hover-demo'.$user->id.'">'.$avatar.'</a>';
		}

		if (empty($config->show_avatars)) $avatar="";
		$shablon=str_replace("{avatar}",$avatar,$shablon);
		$shablon=str_replace("{userid}",$user->id,$shablon);

		//create sex
		$sex="";
		if ($myuser->sex=='f') $sex="<img src='".$live_site."components/com_idoblog/assets/images/female.gif' border='0'>";
		if ($myuser->sex=='m') $sex="<img src='".$live_site."/components/com_idoblog/assets/images/male.gif' border='0'>";
		$shablon=str_replace("{sex}",$sex,$shablon);


		$SiteName 	= $mainframe->getCfg('sitename');
		$MailFrom 	= $mainframe->getCfg('mailfrom');
		$FromName 	= $mainframe->getCfg('fromname');

		$uri		= &JFactory::getURI();

		$usersend=array();
		$usersend[$myusername]=1;

		$message=$config->letter;
		$message=str_replace("[authorname]",$user->username,$message);
		$message=str_replace("[message]",strip_tags($comment),$message);
		$message=str_replace("[topicname]",$article->title,$message);
		$message=str_replace("[sitename]",$SiteName,$message);
		$message=str_replace("[link]",$uri->toString( array('scheme', 'host', 'port')).JRoute::_( 'index.php?option=com_idoblog&task=viewpost&id='. $idarticle.'&Itemid='.$Itemid.'#comment'.$id ),$message);

		if ($config->typename=='1') $article->username=$article->username; else $article->username=$article->name;
		$body=str_replace("[username]",$article->username,$message);

		$subject=JText::_( 'Topic reply notification' );
		$email=$article->email;
		// Clean the email data
		$subject = JMailHelper::cleanSubject($subject);
		$body	 = JMailHelper::cleanBody($body);
		//отправляем уведомление автору
		if ($config->authornewcomment==1 && empty($usersend[$article->username])) {
		JUtility::sendMail($MailFrom, $FromName, $email, $subject, $body);
		$usersend[$article->username]=1;
			}

		//уведомляем админа
		if ($config->adminnewcomment==1 && empty($usersend[$config->adminemail])) {
				$body=str_replace("[username]",JText::_( 'Administrator' ),$message);
				$email=$config->adminemail;
				JUtility::sendMail($MailFrom, $FromName, $email, $subject, $body);
		}


		if ($parents>0 && $config->usernewcomment==1)
			{
				//отправляем уведомления всем родителям ветки
				$query="SELECT * FROM #__idoblog_comments as kc WHERE kc.idarticle=".$idarticle;
				$db->setQuery( $query );
				$comments2=$db->loadObjectList();

				for($z=0;$z<count($comments2);$z++)
				$comments3[$comments2[$z]->id]=$comments2[$z];
				$myparent=$parents;

				while($myparent>0)
					{
					$parentcom=$comments3[$myparent];
					$email=$parentcom->email;
					//тут отправляем сообщение
					if ($parentcom->publish==1 && empty($usersend[$parentcom->username])) {
						$body=str_replace("[username]",$parentcom->username,$message);
						JUtility::sendMail($MailFrom, $FromName, $email, $subject, $body);
						$usersend[$parentcom->username]=1;
						}
					$myparent=$parentcom->parent;
					}

			}

		$shablon=str_replace("{commentid}",$id,$shablon);
		if ($user->id>0) $edittext=JText::_( 'edit' ); else $edittext="";
		$deletetext=JText::_( 'delete' );
		$areyousure=JText::_( 'Are you sure?' );
		$shablon=str_replace("{areyousure}",$areyousure,$shablon);
		$shablon=str_replace("{edittext}",$edittext,$shablon);
		$shablon=str_replace("{deletetext}",$deletetext,$shablon);
		$shablon=replace_smile($shablon);
		$shablon=replace_bbcode($shablon);
		$objResponse->addAssign("newcomment".$parent,"innerHTML",$shablon);
		//if ($parent=='0') $objResponse->addAssign("commentform0","innerHTML","");
	}
	else
	{
		$comment=replace_smile($comment);
		$comment=replace_bbcode($comment);
		$objResponse->addAssign("commentform".$parent,"innerHTML",$comment);
		$objResponse->addAssign("commentform0","style.display","none");
	}
}

$textrror="";
if ($wrong=='1') {
$objResponse->addscript("document.getElementById('commentform".$parent."').innerHTML=document.getElementById('commentform-1').innerHTML;");
$objResponse->addscript("document.getElementById('commentform-1').innerHTML='';");
$objResponse->addscript("document.getElementById('parent').value=".$parent);
$objResponse->addAssign("newcomment".$parent,"innerHTML","");
$objResponse->addscript("document.getElementById('text').innerHTML='".$comment."'");
$texterror="<br><big style='color:red'>".JText::_( 'Error. Try again.' )."</big>";
}

//create new captcha
$allowed_symbols = "23456789abcdeghkmnpqsuvxyz";
$length = mt_rand(5,6);
	while(true){
		$keystring='';
		for($i=0;$i<$length;$i++){
			$keystring.=$allowed_symbols{mt_rand(0,strlen($allowed_symbols)-1)};
			}
		if(!preg_match('/cp|cb|ck|c6|c9|rn|rm|mm|co|do|cl|db|qp|qb|dp|ww/', $keystring)) break;
		}

$sess_key=$session->set('keystring', $keystring, 'idoblog');
$newcaptcha='<img src="'.$live_site.'components/com_idoblog/assets/libraries/captcha/?sess='.base64_encode(serialize($_SESSION['__idoblog'])).'">';
$objResponse->addAssign("captcha","innerHTML",$newcaptcha);
$objResponse->addAssign("error","innerHTML",$texterror);
	return $objResponse->getXML();
}

function replace_smile($comment) {
GLOBAL $live_site;
//обработка смайлов
$path=$live_site.'components/com_idoblog/assets/images/smiley/';
$comment=str_replace(":)","<img src=".$path."regular_smile.gif>",$comment);
$comment=str_replace(":D","<img src=".$path."teeth_smile.gif>",$comment);
$comment=str_replace(":(","<img src=".$path."sad_smile.gif>",$comment);
$comment=str_replace(";)","<img src=".$path."wink_smile.gif>",$comment);
$comment=str_replace(":o","<img src=".$path."whatchutalkingabout_smile.gif>",$comment);
$comment=str_replace(":P","<img src=".$path."tounge_smile.gif>",$comment);
$comment=str_replace("8)","<img src=".$path."shades_smile.gif>",$comment);
$comment=str_replace(":up:","<img src=".$path."thumbs_up.gif>",$comment);
$comment=str_replace(":down:","<img src=".$path."thumbs_down.gif>",$comment);
$comment=str_replace(":-(","<img src=".$path."cry_smile.gif>",$comment);
$comment=str_replace(":devil:","<img src=".$path."angry_smile.gif>",$comment);
$comment=str_replace(":kiss:","<img src=".$path."kiss.gif>",$comment);
return $comment;
}

function replace_bbcode($comment) {
$comment=str_replace("[B]","<b>",$comment);
$comment=str_replace("[/B]","</b>",$comment);
$comment=str_replace("[I]","<i>",$comment);
$comment=str_replace("[/I]","</i>",$comment);
$comment=str_replace("[U]","<u>",$comment);
$comment=str_replace("[/U]","</u>",$comment);
$comment=str_replace("[S]","<s>",$comment);
$comment=str_replace("[/S]","</s>",$comment);
$comment=str_replace("[IMG]",'<img src="',$comment);
$comment=str_replace("[/IMG]",'">',$comment);
$comment=preg_replace("#\[URL=((?:[^]]|](?=\.\w{2,4}))+)\]((?:(?!\[/URL]).)+)\[/URL]#",'<a href="\\1">\\2</a>',$comment);
return $comment;
}

function deletecomment($commentid) {
$objResponse = new xajaxResponse();
$db =& JFactory::getDBO();

$query="SELECT id FROM `#__idoblog_comments` WHERE `parent` =".$commentid." ";
$db->setQuery( $query );
$check=$db->loadResult();

if (!empty($check)) {
$query="UPDATE `#__idoblog_comments` SET `publish` = '0' WHERE `id` =".$commentid." ";
$db->setQuery( $query );
$db->query();
}
else
{
$query="DELETE FROM `#__idoblog_comments` WHERE id=".$commentid." ";
$db->setQuery( $query );
$db->query();
}
	$objResponse->addAssign("commentactions".$commentid,"innerHTML","");
	$objResponse->addAssign("commentform".$commentid."-".$commentid,"innerHTML",JText::_( 'Comment delete' ));
	return $objResponse->getXML();
}



function addfriend($friendid) {
$objResponse = new xajaxResponse();
$user =& JFactory::getUser();
$db =& JFactory::getDBO();
$session	 =& JFactory::getSession();
$myses=$session->get('catid', null, 'idoblog');
if (empty($myses)) xcreateaccaunt();

if ($user->id>0) {
$query="SELECT * FROM `#__idoblog_friends` WHERE id=".$user->id." AND idfriend=".$friendid;
$db->setQuery( $query );
$db->query();
$check=$db->getNumRows();

if ($check==0) {

$both=0;
$query="SELECT * FROM `#__idoblog_friends` WHERE id=".$friendid." AND idfriend=".$user->id;
$db->setQuery( $query );
$db->query();
$both=$db->getNumRows();
//$objResponse->addAlert($both);
$query="INSERT INTO `#__idoblog_friends` ( `id` , `idfriend` , `both` ) VALUES ('".$user->id."', '".$friendid."', '".$both."');";
$db->setQuery( $query );
$db->query();
if ($db->getErrorNum() > 0){
$objResponse->addAlert($db->getErrorMsg());
}

if ($both==1) {
$query="UPDATE `#__idoblog_friends` SET `both` = '".$both."' WHERE `id` =".$friendid." AND `idfriend`=".$user->id;
$db->setQuery( $query );
$db->query();
}

$msq=JText::_( 'User added your friends' );
$objResponse->addAlert($msq);
$objResponse->addAssign("friend".$friendid,"innerHTML","");
}
else
{
$msq=JText::_( 'User is your friend yet' );
$objResponse->addAlert($msq);
}
}
return $objResponse->getXML();
}

function deletefriend($friendid, $both) {
$objResponse = new xajaxResponse();
$user =& JFactory::getUser();
$db =& JFactory::getDBO();

if ($user->id>0) {
$query="DELETE FROM `#__idoblog_friends` WHERE `id` =".$user->id." AND `idfriend`=".$friendid;
$db->setQuery( $query );
$db->query();

if ($both==1) {
$query="UPDATE `#__idoblog_friends` SET `both` = '0' WHERE `id` =".$friendid." AND `idfriend`=".$user->id;
$db->setQuery( $query );
$db->query();
}
$msq=JText::_( 'Friend delete succesfull' );
$objResponse->addAlert($msq);
$objResponse->addAssign("friend".$friendid,"style.display","none");
}

return $objResponse->getXML();
}

function deletefile($filename,$z) {
$objResponse = new xajaxResponse();
$user =& JFactory::getUser();
$yourpath=JPATH_ROOT.DS."images".DS."idoblog".DS."upload".DS.$user->username;
$ext=strtolower(JFile::getExt($filename));
JFile::delete($yourpath.DS.$filename);
if ($ext=='jpg' || $ext=='gif' || $ext=='png') {
JFile::delete($yourpath.DS."preview".DS."small_".$filename);
}

$objResponse->addAssign("f".$z,"innerHTML","");
return $objResponse->getXML();
}

function addfile($file,$number) {
$objResponse = new xajaxResponse();
GLOBAL $live_site;
$ext=strtolower(JFile::getExt($file));
$user =& JFactory::getUser();
$number2=round($number)+1;
$yourpath=JPATH_ROOT."/images/idoblog/upload/".JSTRING::strtolower($user->username);
$sitepath=$live_site."images/idoblog/upload/".JSTRING::strtolower($user->username);
$imagepath=$live_site."components/com_idoblog/assets/images";
$filename=strtok($file,".");

if ($ext=='jpg' || $ext=='gif' || $ext=='png') {
	$text="<span  id='f".$number."'><table width='100' style='float:left'><tr><td align='center'><small>".$filename."<a href='#f' onClick='xajax_deletefile(\"".$file."\",$number)'><img src='".$imagepath."/delete.png'></a></small></td></tr><tr height='100'><td style='border:1px solid black' align='center'><img src='".$sitepath.DS."preview".DS."small_".$file."' width='100' onclick='jInsertEditorText(\"<img src=".addslashes($sitepath."/".$file).">\",\"text\");' style='cursor:pointer'></td></tr></table></span><span id='n".$number2."'></span>";
	}

if ($ext=='zip') {
	$text="<span  id='f".$number."'><table width='100' style='float:left'><tr><td align='center'><small>".$filename."<a href='#f' onClick='xajax_deletefile(\"".$file."\",$number)'><img src='".$imagepath."/delete.png'></a></small></td></tr><tr height='100'><td style='border:1px solid black' align='center'><img src='".$imagepath."/archive_zip.gif"."' width='80' onclick='jInsertEditorText(\"<a href=".addslashes($sitepath."/".$file).">".$file."</a>\",\"text\");' style='cursor:pointer'></td></tr></table></span><span id='n".$number2."'></span>";
	}

if ($ext=='rar') {
	$text="<span  id='f".$number."'><table width='100' style='float:left'><tr><td align='center'><small>".$filename."<a href='#f' onClick='xajax_deletefile(\"".$file."\",$number)'><img src='".$imagepath."/delete.png'></a></small></td></tr><tr height='100'><td style='border:1px solid black' align='center'><img src='".$imagepath."/archive_rar.gif"."' width='80' onclick='jInsertEditorText(\"<a href=".addslashes($sitepath."/".$file).">".$file."</a>\",\"text\");' style='cursor:pointer'></td></tr></table></span><span id='n".$number2."'></span>";
	}

$objResponse->addAssign("n".$number,"innerHTML",$text."");
$objResponse->addAssign("number","value",$number2);
return $objResponse->getXML();
}


function xcreateaccaunt() {
global $config;
$user=& JFactory::getUser();
	$db =& JFactory::getDBO();
$query="SELECT idcategory FROM #__idoblog_user_reffer WHERE iduser=".$user->id;
$db->setQuery( $query );
$idcategory=$db->loadResult();

if (empty($idcategory)) {

	$query="INSERT INTO `#__categories` ( `id` , `parent_id` , `title` , `name` , `alias` , `image` , `section` , `image_position` , `description` , `published` , `checked_out` , `checked_out_time` , `editor` , `ordering` , `access` , `count` , `params` )
VALUES (
'', '0', '".$user->username."', '".$user->username."', '".$user->username."', '', '".$config->idblog."', 'left', '', '1', '0', '0000-00-00 00:00:00', NULL , '1', '0', '0', ''
);";
			$db->setQuery( $query );
			$db->query();
			$idcategory=$db->insertid();

	$query="INSERT INTO `#__idoblog_user_reffer` ( `iduser` , `idcategory` )
			VALUES (
			'".$user->id."', '".$idcategory."');";
			$db->setQuery( $query );
			$db->query();

			$query="INSERT INTO `#__idoblog_users` ( `iduser` , `birthday` , `avatar` , `icq` , `about`, `small_avatar` )
VALUES (
'".$user->id."', '0000-00-00', '', '', '','');";
			$db->setQuery( $query );
			$db->query();

	jimport('joomla.filesystem.folder');
	jimport('joomla.filesystem.path');
	$path=JPATH_SITE.DS."images".DS."idoblog".DS."upload".DS.JSTRING::strtolower($user->username);
	JPath::clean($path);
	$path2=JPATH_SITE.DS."images".DS."idoblog".DS."upload".DS.JSTRING::strtolower($user->username.DS."preview");
	JPath::clean($path2);

	JFolder::create($path,"0755");
	JPath::isOwner($path);
	JPath::setPermissions($path, '0755');

	JFolder::create($path2,"0755");
	JPath::isOwner($path2);
	JPath::setPermissions($path2, '0755');

	$session =& JFactory::getSession();
	$session->set('catid', $idcategory, 'idoblog');
}

return $idcategory;
}


$xajax->registerFunction("addfile");
$xajax->registerFunction("editcomment");
$xajax->registerFunction("deletefile");
$xajax->registerFunction("deletefriend");
$xajax->registerFunction("addcomment");
$xajax->registerFunction("addfriend");
$xajax->registerFunction("updateavatar");
$xajax->registerFunction("deletecomment");
$xajax->processRequests();
?>

