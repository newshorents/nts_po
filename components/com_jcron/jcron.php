<?php
/**
* @package JCron Scheduler
* @email admin@joomlaplug.com
* @Copyright (C) 2006 JoomlaPlug.com. All rights reserved.
* JCron is a free software released under the GNU license.
* GNU General Access License.
*/

// Restrict direct access
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if($from_template!=1)
 exit;


require_once( $mosConfig_absolute_path."/administrator/components/com_jcron/CronParser.class.php");

########## GET CONFIG VARIABLES ##########################
$query = "SELECT * FROM #__jcron_config";
$database->setQuery( $query );
$data =  $database->loadObjectList();
foreach($data as $key=>$value){
    if (!get_magic_quotes_gpc()){
	    $value->value = stripslashes($value->value);
    }
    $_CONFIG[$value->name] = $value->value;
    }
#########################################################


$query = "SELECT* from #__jcron_tasks WHERE published=1";
$database->setQuery( $query );
$data =  $database->loadObjectList();

foreach($data as $key=>$value){

    $cron = new CronParser($value->mhdmd);
	$lastRan = $cron->getLastRan(); //Array (0=minute, 1=hour, 2=dayOfMonth, 3=month, 4=week, 5=year)

    //Convert to Unix timestamp
	$cron_ran = mktime ( $lastRan[1] ,$lastRan[0],0 , $lastRan[3] ,$lastRan[2], $lastRan[5]);
 
    $time_ran = date("Y-m-d H:i:s",$cron_ran);
    //Now, we look for a log entry for this app's cron job running OK
	//We're looking for a time within 45 secs or so of the $cron_ran time.
	//You may want to adjust this if your job takes a while to run...
	$query = "SELECT * FROM `#__jcron_tasks` WHERE  id='".$value->id."' and published=1 and ran_at  <= '".($time_ran)."' " .
			" ORDER BY  ran_at  DESC ";

    $database->setQuery( $query );
    $data_run =  $database->loadObjectList();


    foreach($data_run as $key=>$crondata){

        #echo "Running ".$crondata->task."<br/>";
        $command = $crondata->file;
        $query = "UPDATE #__jcron_tasks SET ran_at= now(), log_text='".addslashes(htmlentities($command))."' WHERE id='".$crondata->id."'";
        $database->setQuery( $query );
        $database->query();
        
        
        if($crondata->type==0) 
         exec($command, $log);
        else{
         if($fp = fopen($command, 'r')){
          while(!feof($fp))  
            $log .= fread($fp,1024);
          fclose($fp);
         }
         else
          $log = "Unable to open ".$command; 
        }
            
        
        if($_CONFIG['dlog'])
         if($crondata->ok)
         {
          if(is_array($log))
           $logdata = implode("\n",$log);
          else
           $logdata .= $log;
          
          $logdata .= "\n\n\n LOCAL CRON DEBUG:\n\n".$cron->debug;
         }

        ########### STORE DABASE RUN TIME AND LOGS ################################
        $query = "UPDATE #__jcron_tasks SET ran_at= now(), log_text='".addslashes($logdata)."' WHERE id='".$crondata->id."'";
        $database->setQuery( $query );
        if (!$database->query()) {

	 	  echo $database->getErrorMsg()." on Query: ".$query." \n";
		  exit();

	     }

        ########## SEND EMAIL LOGS ################################################
        if($_CONFIG['cemail']){

            $subject = $crondata->task." - ".$crondata->file;
            $host = $mosConfig_live_site;

            #mosMail( "noreply@noreply.com", "JCron Daemon", $_CONFIG['email'], $subject, $logdata, '0', '', '', $attachment );
            mail( $_CONFIG['email'], $subject, $logdata, 'JCron Daemon' );

            }
        

        }
    
    
    }


#include $mosConfig_absolute_path."/administrator/components/com_jcron/images/empty.png";
#exit;

?>
