<?php // no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<div id="last_news_r">
<h1 id="title_last_blog"><?php echo JText::_('LAST NEWS') ?></h1>
<ul class="latestnewsintranet<?php echo $params->get('moduleclass_sfx'); ?>">
<?php foreach ($list as $item) :  ?>
	<li class="latestnewsintranet<?php echo $params->get('moduleclass_sfx'); ?>">
		<a href="<?php echo $item->link; ?>" class="latestnewsintranet<?php echo $params->get('moduleclass_sfx'); ?>">
			<?php echo $item->text; ?>
		</a>
		<p><?php echo $item->introtext; ?></p>
		<span id="read_more"><a href="<?php echo $item->link; ?>"><?php echo JText::_('Read more.') ?></a></span>
	</li>
	<br>
<?php endforeach; ?>
</ul>
</div>