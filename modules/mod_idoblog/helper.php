<?php
/**
* @version		$Id: helper.php 11074 2008-10-13 04:54:12Z ian $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class modIdoblogHelper
{
	function getBlogRecent(){	
		$fname = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_idoblog'.DS.'config.php';
		require_once( $fname );
		$config = new KBconfig();
				
		$id = JRequest::getVar('id', 0, '', 'int');
		$tagid = JRequest::getVar('tagid', 0, '', 'int');
		$layout=JRequest::getVar('layout');
		$db =& JFactory::getDBO();

		$select="c.id, c.hits, c.introtext, c.fulltext, c.created, c.title, u.username, u.name, u.id as userid, ku.avatar, ku.sex";
		$from="#__content as c, #__users as u, #__idoblog_users as ku";
		$where="c.sectionid='".$config->idblog."' AND c.state=1 AND c.created_by=u.id AND u.id=ku.iduser";
		$order="created desc";

		$rows = Array();
		$query="SELECT ".$select." FROM ".$from." WHERE ".$where." ORDER BY ".$order;
		//$db->setQuery($query);
		//$rows = $db->loadObjectList();	
		
		$db->setQuery($query, 0, 2);
		$rows = $db->loadObjectList();

		return $rows;
	}
}
