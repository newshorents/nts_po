INSERT INTO `nts_produccion`.`jos_tm_currency` (`id`, `name`) VALUES ('3', 'Libra esterlina');

ALTER TABLE `nts_produccion`.`jos_tm_currency_conversion` 
ADD COLUMN `id_currency` INT(11) NOT NULL FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`year`, `month`, `id_currency`);

UPDATE `nts_produccion`.`jos_tm_currency_conversion` 
SET id_currency=2;

INSERT INTO `nts_produccion`.`jos_tm_currency_conversion` (`id_currency`, `year`, `month`, `conversion_value`) VALUES ('1', '2016', '0', '1.00');
INSERT INTO `nts_produccion`.`jos_tm_currency_conversion` (`id_currency`, `year`, `month`, `conversion_value`) VALUES ('1', '2016', '1', '1.00');
INSERT INTO `nts_produccion`.`jos_tm_currency_conversion` (`id_currency`, `year`, `month`, `conversion_value`) VALUES ('1', '2016', '2', '1.00');
INSERT INTO `nts_produccion`.`jos_tm_currency_conversion` (`id_currency`, `year`, `month`, `conversion_value`) VALUES ('1', '2016', '3', '1.00');
INSERT INTO `nts_produccion`.`jos_tm_currency_conversion` (`id_currency`, `year`, `month`, `conversion_value`) VALUES ('1', '2016', '4', '1.00');
INSERT INTO `nts_produccion`.`jos_tm_currency_conversion` (`id_currency`, `year`, `month`, `conversion_value`) VALUES ('1', '2016', '5', '1.00');
