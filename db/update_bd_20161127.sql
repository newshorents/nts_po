--Active field in category table
ALTER TABLE `nts_produccion`.`jos_tm_category` 
ADD COLUMN `active` TINYINT(2) NOT NULL DEFAULT 1 AFTER `id_country`;

--Update active category C%
UPDATE nts_produccion.jos_tm_category
SET active = 0
WHERE name LIKE 'C%';

--Update category B[0-9]
UPDATE nts_produccion.jos_tm_category
SET active = 0
WHERE name REGEXP '^B[0-9]';

UPDATE nts_produccion.jos_tm_category
SET active = 0
WHERE name REGEXP '^BE[0-9]';

--Delete UK country
DELETE FROM `nts_produccion`.`jos_tm_country` WHERE `id`='3';
UPDATE `nts_produccion`.`jos_tm_office` SET `id_country`='2' WHERE `id`='5';

--Update UK category
UPDATE `nts_produccion`.`jos_tm_category` SET `id_country`='2' WHERE `id`='198';

--Country code field add in office table
ALTER TABLE `nts_produccion`.`jos_tm_office` 
ADD COLUMN `country_code` VARCHAR(45) NOT NULL DEFAULT 'BCN' AFTER `id_country`;
UPDATE `nts_produccion`.`jos_tm_office` SET `country_code`='MZL' WHERE `id`='1';
UPDATE `nts_produccion`.`jos_tm_office` SET `country_code`='ARM' WHERE `id`='2';
UPDATE `nts_produccion`.`jos_tm_office` SET `country_code`='MAD' WHERE `id`='4';
UPDATE `nts_produccion`.`jos_tm_office` SET `country_code`='UK' WHERE `id`='5';
INSERT INTO `nts_produccion`.`jos_tm_office` (`id`, `name`, `id_city`, `id_country`, `country_code`) VALUES ('6', 'Medellin', '6', '1', 'MED');
INSERT INTO `nts_produccion`.`jos_tm_office` (`id`, `name`, `id_city`, `id_country`, `country_code`) VALUES ('7', 'Barcelona', '7', '2', 'ESP');
