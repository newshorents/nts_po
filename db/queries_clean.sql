### Scorecard
# Cerrar 2014 & 2012

### User
# Delete office user
DELETE ua 
FROM jos_tm_user_area AS ua 
JOIN jos_tm_data_user AS du ON(ua.id_user = du.user_id) 
WHERE (YEAR(end_date) < 2015 AND end_date <> '0000-00-00');

# Delete User category
DELETE uc
FROM jos_tm_user_category AS uc
JOIN jos_tm_data_user AS du ON(uc.id_user = du.user_id) 
WHERE (YEAR(end_date) < 2015 AND end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# Delete user project
DELETE up
FROM nts_produccion.jos_tm_user_project AS up
JOIN jos_tm_data_user AS du ON(up.id_user = du.user_id) 
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# Delete user date
DELETE ud
FROM jos_tm_user_date AS ud
JOIN jos_tm_data_user AS du ON(ud.id_user = du.user_id) 
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# Delete hour impute project day
DELETE idp
FROM nts_produccion.jos_tm_impute_day_project AS idp
JOIN jos_tm_impute_week_project AS iwp ON(iwp.id = idp.id_week)
JOIN jos_tm_data_user AS du ON(iwp.id_user = du.user_id) 
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# Delete hour impute project week
DELETE iwp 
FROM jos_tm_impute_week_project AS iwp
JOIN jos_tm_data_user AS du ON(iwp.id_user = du.user_id) 
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# Delete hour impute absence day
DELETE ida
FROM jos_tm_impute_day_absence AS ida
JOIN jos_tm_impute_week_absence AS iwa ON(iwa.id = ida.id_week)
JOIN jos_tm_data_user AS du ON(iwa.id_user = du.user_id) 
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# Delete hour impute absence week
DELETE iwa
FROM jos_tm_impute_week_absence AS iwa
JOIN jos_tm_data_user AS du ON(iwa.id_user = du.user_id) 
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# DELETE vacation prev
DELETE vp
FROM jos_tm_vacations_prev AS vp
JOIN jos_tm_data_user AS du ON(du.user_id = vp.id_user) 
WHERE (YEAR(end_date) < 2015 AND end_date <> '0000-00-00');

# DELETE user joomla
DELETE u
FROM jos_users AS u
JOIN jos_tm_data_user AS du ON(du.user_id = u.id) 
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00');

# Delete user data
DELETE du
FROM jos_tm_data_user AS du
WHERE (YEAR(du.end_date) < 2015 AND du.end_date <> '0000-00-00')
OR du.personal_type_id = 1;

# Delete personal type
DELETE FROM jos_tm_personal_type WHERE id='1';

### Project
# Delete day impute project
DELETE idp
FROM jos_tm_impute_day_project AS idp
JOIN jos_tm_impute_week_project AS iwp ON(iwp.id = idp.id_week)
WHERE iwp.year < 2015;

# Delete week impute project
DELETE iwp
FROM jos_tm_impute_week_project AS iwp
WHERE iwp.year < 2015;

# Delete day impute absence
DELETE ida
FROM jos_tm_impute_day_absence AS ida
JOIN jos_tm_impute_week_absence AS iwa ON(iwa.id = ida.id_week)
WHERE iwa.year < 2015;

# Delete week impute absence
DELETE iwa
FROM jos_tm_impute_week_absence AS iwa
WHERE iwa.year < 2015;

# Delete projects
DELETE p
FROM jos_tm_projects AS p
LEFT JOIN jos_tm_impute_week_project AS iwp ON(iwp.id_project = p.id)
WHERE iwp.id_project IS NULL
AND (YEAR(p.end_date) < 2015 OR YEAR(p.end_date) >= 2100);

# Delete cost project
DELETE c
FROM nts_produccion.jos_tm_costs AS c
LEFT JOIN jos_tm_projects AS p ON(p.id = c.project)
WHERE p.id IS NULL;

# DELETE docuemnt cost
DELETE dc
FROM jos_tm_document_cost AS dc
LEFT JOIN jos_tm_costs AS c ON(c.id = dc.id_cost)
WHERE c.id IS NULL;

# DELETE document project
DELETe dc
FROM jos_tm_document_project AS dc
LEFT JOIN jos_tm_projects AS p ON(p.id = dc.id_project)
WHERE p.id IS NULL;

# DELETE future resource
DELETE fr
FROM jos_tm_future_resources AS fr
LEFT JOIN jos_tm_projects AS p ON(p.id = fr.id_project)
WHERE p.id IS NULL;

# DELETE income project
DELETE ip
FROM jos_tm_income_project AS ip
LEFT JOIN jos_tm_projects AS p ON(p.id = ip.id_project)
WHERE p.id IS NULL;

# DELETE project biling
DELETE pb
FROM jos_tm_project_billing AS pb
LEFT JOIN jos_tm_projects AS p ON(p.id = pb.id_project)
WHERE p.id IS NULL;

DELETE dpb
FROM jos_tm_date_projectbilling AS dpb
LEFT JOIN jos_tm_project_billing AS pb ON(pb.id = dpb.id_projectbilling)
WHERE pb.id IS NULL;

DELETE db
FROM jos_tm_document_billing db
LEFT JOIN jos_tm_project_billing AS pb ON(pb.id = db.id_project_billing)
WHERE pb.id IS NULL;

# DELETE Project expenditures
DELETE pe
FROM jos_tm_projected_expenditures AS pe
LEFT JOIN jos_tm_projects AS p ON(p.id = pe.id_project)
WHERE p.id IS NULL;

# DELETE user project
DELETE up
FROM jos_tm_user_project AS up
LEFT JOIN jos_tm_projects AS p ON(p.id = up.id_project)
WHERE p.id IS NULL;

## Client
# Delete client
DELETE c
FROM jos_tm_clients AS c
LEFT JOIN jos_tm_projects AS p ON(p.client = c.id)
WHERE p.client IS NULL;

# DELETE payday client
DELETE cp
FROM jos_tm_client_payday AS cp
LEFT JOIN jos_tm_clients AS c ON(c.id = cp.client_id)
WHERE c.id IS NULL;