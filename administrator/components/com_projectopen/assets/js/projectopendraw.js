//Imputation
function dropTable() {
    var tablaInpute = document.getElementById('tableImpute');
    var tablaComment = document.getElementById('tableImputeComment');
    var lengthTables = tablaInpute.rows.length;
    for(var i = 1; i < lengthTables; i++) {
        tablaInpute.rows[1].remove();
    }
    lengthTables = tablaComment.rows.length;
    for(var i = 1; i < lengthTables; i++) {
        tablaComment.rows[1].remove();
    }
}

function daysInMonth(month, year) {
    return new Date(year || new Date().getFullYear(), month, 0).getDate();
}

function pintarSemana(){
    dateSelected = new Date(nuevoValor);
    dayweek = dateSelected.getDay();
    daymonth = dateSelected.getDate();
    monthSelected = dateSelected.getMonth();
    yearSelected = dateSelected.getFullYear();

    monthEndSelected = monthSelected;
    yearEndSelected = yearSelected;

    if(dayweek!=0){
        dayStartWeek = (daymonth - dayweek)+1;
    }else{
        dayStartWeek = (daymonth - dayweek)-6;
    }

    if(dayStartWeek<0){
        monthSelected = monthSelected - 1 ;
        if(monthSelected<0){
            yearSelected = yearSelected - 1;
            monthSelected =11;
        }
        daysMonthBack = daysInMonth(monthSelected+1, yearSelected);
        dayStartWeek = daysMonthBack - Math.abs(dayStartWeek);
    }

    dayEndWeek = dayStartWeek + 6;
    daysMonth = daysInMonth(monthSelected+1, yearSelected);
    if(dayEndWeek > daysMonth){
        monthEndSelected = monthSelected + 1 ;
        if(monthEndSelected > 11){
            yearEndSelected = yearEndSelected +1;
            monthEndSelected=0;
        }
        dayEndWeek = dayEndWeek - daysMonth;
    }

    dateStart = new Date(yearSelected, monthSelected, dayStartWeek);
    dateEnd = new Date(yearEndSelected, monthEndSelected, dayEndWeek);
    dateStart2 = new Date(yearSelected, monthSelected, dayStartWeek);


    var range = document.getElementById("range_date");
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    startText = meses[monthSelected] + " " + dayStartWeek + ", " + yearSelected;
    endText = meses[monthEndSelected] + " " + dayEndWeek + ", " + yearEndSelected;
    range.textContent = startText + " - " + endText;

    drawHeadProject(dateStart);
}


function drawHeadProject(dateStartPH){
    dateAll  = dateStartPH;
    day_header = new Array();
    for(var i=0; i<7; i++){
        document.getElementById("d"+i).textContent = dateAll.getDate();
        day_header.push(dateAll.getDate());
        dateAll.setTime(dateAll.getTime()+1*24*60*60*1000);
    }
}

var day_header = new Array();
function addProjectWeek(dataProjectWeek){
    var html="";
    var projAnt = 0;


    jQuery.each(dataProjectWeek, function (i, val){
        var dateAux1 = (jQuery("#impute_date").val()).split("-");
        var dateAux = new Date(dateAux1[2]+'/'+dateAux1[1]+'/'+dateAux1[0]);
        if(val.id != projAnt){
            projAnt = val.id;
            html += "<tr id='0-p' idproject='"+val.id+"' class='row1'>";
            html += "<td><label>"+val.name+"</label></td>";
            for(o=0;o<5;o++){
                if(!validateDate((dateAux.getFullYear()+'-'+(dateAux.getMonth()+1)+'-'+dateAux.getDate()), val.start_project, val.end_project,val.id)){
                    html += "<td><input tabindex='-1' onkeypress='return numeros(event)' value='N/A' disabled='''></td>";
                }else{
                    html += "<td><input tabindex='-1' onkeypress='return numeros(event)' value='0.00'></td>";
                }
                dateAux.setTime(dateAux.getTime()+(1*24*60*60*1000));
            }

            html += "<td><input tabindex='-1' onkeypress='return numeros(event)' value='N/A' disabled='''></td>";
            html += "<td><input tabindex='-1' onkeypress='return numeros(event)' value='N/A' disabled='''></td>";
            html += "<td><label>0.00</label></td>";
            html += "<td>";
            html += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><input style='position: relative; top: -2px;' type='radio' value='1' name='"+val.name+"' onchange='changeState(this)'/><br>"+
            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar' /> <input style='position: relative; top: -2px;' type='radio' value='3' name='"+val.name+"' onchange='changeState(this)' checked/><br>"+
            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar' /> <input style='position: relative; top: -2px;' type='radio' value='2' name='"+val.name+"' onchange='changeState(this)'/>"
            html += "</td>";
            html += "</tr>";
        }
    });
    jQuery("#tableImpute").append(html);
}

function addImputeProject(data, manager, isAdmin) {
    var dataProject = data;
    var projectManager = manager;
    var id_project = 0;
    var i = 0;
    var count = 0;
    var tabla = "";
    var flagproject = (isAdmin == 1) ? true : false;
    var flagprojectOld = (isAdmin == 1) ? true : false;
    var flagcero = false;

    document.getElementById("state").innerHTML = dataProject[0].state_name;
    document.getElementById("user_name").innerHTML = dataProject[0].name;

    for(i=0;i<dataProject.length;i++) {
        var dateDiv = dataProject[i].impute_date.split("-");
        var objDate = new Date(dateDiv[0]+'/'+dateDiv[1]+'/'+dateDiv[2]);
        for(j=0;j<projectManager.length;j++) {
            if(projectManager[j].id == dataProject[i].id_project){
                flagproject = true;
                j = projectManager.length;
            }
        }
        if(id_project!=dataProject[i].id_project) {
            if(count<7 && i!=0) {
                for(var j=0;j<(7-count);j++) {
                    if((objDate.getDay() != 0 && objDate.getDay() != 6) && validateDateUser(dataProject[i-1].impute_date) && (validateDate(dataProject[i-1].impute_date, dataProject[i-1].start_date, dataProject[i-1].end_date,dataProject[i-1].id_project))) {
                        tabla += "<td>";
                        tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)' value='0' />";
                        tabla += "</td>";
                    }else {
                        tabla += "<td>";
                        tabla += "<input disabled onKeyPress='return numeros(event)' value='N/A' />";
                        tabla += "</td>";
                    }
                }
            }
            if(i!=0){
                tabla += "<td>";
                tabla += "<label>"+dataProject[i-1].hour_week+"</label>";
                tabla += "</td>";
                tabla += "<td align='center'>";
                if(isAdmin == 1){
                    tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><input style='position: relative; top: -2px;' type='radio' value='1' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' "+((dataProject[i-1].id_state == 1)?"checked":"")+"/><br>"+
                    "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar' /> <input style='position: relative; top: -2px;' type='radio' value='3' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' "+((dataProject[i-1].id_state == 3)?"checked":"")+"/><br>"+
                    "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar' /> <input style='position: relative; top: -2px;' type='radio' value='2' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' "+((dataProject[i-1].id_state == 2)?"checked":"")+"/>";
                }else{
                    if(flagprojectOld) {
                        if(dataProject[i-1].id_state == 3) {
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/> ";
                        }else if(dataProject[i-1].id_state == 2 ) {
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado'/><br><br>"+
                            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar' /> <input style='position: relative; top: -2px;' type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' /><br>"+
                            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar' /> <input style='position: relative; top: -2px;' type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' checked />";
                        }else {
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><br><br>"+
                            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' /><br>"+
                            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' />";
                        }
                    } else {
                        if(dataProject[i-1].id_state == 3 ) {
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/>";
                        }else if(dataProject[i-1].id_state == 2 ) {
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado' />";
                        } else {
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/>";
                        }
                    }
                }
                tabla += "</td>";
                tabla += "</tr>";

                addImputeComment(dataProject[i-1].project_name, dataProject[i-1].comment, dataProject[i-1].comment2, dataProject[i-1].id_state, flagprojectOld);
            }
            var codAux = ((isAdmin == 1) && dataProject[i].hour_week == 0) ? "class='row1'" :  "class='row1'";
            tabla += "<tr id='"+dataProject[i].id+"-p' idproject='"+dataProject[i].id_project+"'"+codAux+">";
            tabla += "<td>";
            tabla += "<label>"+dataProject[i].project_name+"</label>";
            tabla += "</td>";
            id_project = dataProject[i].id_project;
            count = 0;
        }
        while(day_header[count] != dataProject[i].impute_date.split("-")[2]) {
            if((objDate.getDay() != 0 && objDate.getDay() != 6) && validateDateUser(dataProject[i].impute_date) && (validateDate(dataProject[i].impute_date, dataProject[i].start_date, dataProject[i].end_date,dataProject[i].id_project))) {
                tabla += "<td>";
                tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)' value='0' />";
                tabla += "</td>";
            }else {
                tabla += "<td>";
                tabla += "<input disabled onKeyPress='return numeros(event)' value='N/A' />";
                tabla += "</td>";
            }
            count++;
        }
        tabla += "<td>";
        if(flagproject) {
            tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)'";
        }else {
            tabla += "<input disabled onKeyPress='return numeros(event)'";
        }
        if((objDate.getDay() != 0 && objDate.getDay() != 6) && validateDateUser(dataProject[i].impute_date) && (validateDate(dataProject[i].impute_date, dataProject[i].start_date, dataProject[i].end_date,dataProject[i].id_project))) {
            tabla +=  "value='"+dataProject[i].number_hours+"' />";
            if(parseInt(dataProject[i].number_hours) != 0) {
                flagcero = true;
            }
        }else {
            tabla +=  "value='N/A' disabled />";
        }
        tabla += "</td>";
        count++;
        flagprojectOld = flagproject;
        flagproject = (isAdmin == 1) ? true : false;
    }
    if(count<7 && i!=0) {
        for(var j=0;j<(7-count);j++) {
            if((objDate.getDay() != 0 && objDate.getDay() != 6) && validateDateUser(dataProject[i-1].impute_date) && (validateDate(dataProject[i-1].impute_date, dataProject[i-1].start_date, dataProject[i-1].end_date,dataProject[i-1].id_project))) {
                tabla += "<td>";
                tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)' value='0' />";
                tabla += "</td>";
            }else {
                tabla += "<td>";
                tabla += "<input disabled onKeyPress='return numeros(event)' value='N/A' />";
                tabla += "</td>";
            }
        }
    }
    tabla += "<td>";
    tabla += "<label>"+dataProject[i-1].hour_week+"</label>";
    tabla += "</td>";
    tabla += "<td align='center'>";
    if(isAdmin == 1){
        tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><input style='position: relative; top: -2px;' type='radio' value='1' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' "+((dataProject[i-1].id_state == 1)?"checked":"")+"/><br>"+
        "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar' /> <input style='position: relative; top: -2px;' type='radio' value='3' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' "+((dataProject[i-1].id_state == 3)?"checked":"")+"/><br>"+
        "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar' /> <input style='position: relative; top: -2px;' type='radio' value='2' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' "+((dataProject[i-1].id_state == 2)?"checked":"")+"/>";
    }else{
        if(flagprojectOld) {
            if(dataProject[i-1].id_state == 3 ) {
                tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/>";
            }else if(dataProject[i-1].id_state == 2 ) {
                tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado'/><br><br>"+
                "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' /><br>"+
                "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' checked />";
            }else {
                tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><br><br>"+
                "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar'/> <input type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' /><br>"+
                "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar' /> <input type='radio' name='"+dataProject[i-1].project_name+"' onchange='changeState(this)' />";
            }
        }else {
            if(dataProject[i-1].id_state == 3 ) {
                tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/>";
            }else if(dataProject[i-1].id_state == 2 ) {
                tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado'/>";
            }else {
                tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/>";
            }
        }
    }
    tabla += "</td>";
    tabla += "</tr>";
    addImputeComment(dataProject[i-1].project_name, dataProject[i-1].comment, dataProject[i-1].comment2, dataProject[i-1].id_state,flagprojectOld);
    flagprojectOld = (isAdmin == 1) ? true : false;
    jQuery('#tableImpute tr:last').after(tabla);
}

function addImputeAbsence(data) {
    var dataAbsence = data;
    var id_absence = 0;
    var i = 0;
    var count = 0;
    var tabla = "";
    var value=0;

    for(i=0;i<dataAbsence.length;i++) {
        var dateDiv = dataAbsence[i].impute_date.split("-");
        var objDate = new Date(dateDiv[0]+'/'+dateDiv[1]+'/'+dateDiv[2]);

        if(id_absence!=dataAbsence[i].id_absence) {
            if(count<7 && i!=0) {
                for(var j=0;j<(7-count);j++) {
                    tabla += "<td>";
                    if((parseInt(count+j)==5 || parseInt(count+j)==6) || !validateDateUser(dataAbsence[i].impute_date)){
                        tabla += "<input tabindex='-1' readonly disabled onKeyPress='return numeros(event)' value='N/A' />";
                    }
                    else{
                        tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)' value='0' />";
                    }
                    tabla += "</td>";
                }
            }
            if(i!=0){
                tabla += "<td>";
                tabla += "<label>"+dataAbsence[i-1].hour_week+"</label>";
                tabla += "</td>";
                tabla += "<td align='center'>";
                if(isAdmin == 1){
                    tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><input style='position: relative; top: -2px;' type='radio' value='1' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' "+((dataAbsence[i-1].id_state == 1)?"checked":"")+"/><br>"+
                    "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar' /> <input style='position: relative; top: -2px;' type='radio' value='3' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' "+((dataAbsence[i-1].id_state == 3)?"checked":"")+"/><br>"+
                    "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar' /> <input style='position: relative; top: -2px;' type='radio' value='2' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' "+((dataAbsence[i-1].id_state == 2)?"checked":"")+"/>";
                }else{
                    if(dataAbsence[i-1].id_state == 3 ) {
                        tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/>";
                    }else if(dataAbsence[i-1].id_state == 2 ) {
                        tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado'/><br><br>";
                        if(isAdmin == 2){
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' /><br>"+
                            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' checked onchange='changeState(this)' />";
                        }
                    }else {
                        tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><br><br>";
                        if(isAdmin == 2){
                            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' /><br>"+
                            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' />";
                        }
                    }
                }
                tabla += "</td>";
                tabla += "</tr>";

                addImputeComment(dataAbsence[i-1].absence_name, dataAbsence[i-1].comment, dataAbsence[i-1].comment2, dataAbsence[i-1].id_state, true);
            }
            var codAux = ((isAdmin == 1) && dataAbsence[i].hour_week == 0) ? "class='row1 rowHidden' style='display:none;'" :  "class='row1'";
            tabla += "<tr id='"+dataAbsence[i].id+"-a' "+codAux+">";
            tabla += "<td>";
            tabla += "<label>"+dataAbsence[i].absence_name+"</label>";
            tabla += "</td>";
            id_absence = dataAbsence[i].id_absence;
            count = 0;
        }
        while(day_header[count] != dataAbsence[i].impute_date.split("-")[2]) {
            tabla += "<td>";
            tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)' value='0' />";
            tabla += "</td>";
            count++;
        }
        tabla += "<td>";
        var datei=(new Date((dataAbsence[i].impute_date).replace(/-/gi,"/"))).getDay();
        if(datei != 6 && datei!=0 && validateDateUser(dataAbsence[i].impute_date)){
            tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)' value='"+dataAbsence[i].number_hours+"' />";
        }else{
            tabla += "<input tabindex='-1' readonly disabled onKeyPress='return numeros(event)' value='N/A' />";
        }
        tabla += "</td>";
        count++;
    }
    if(count<7 && i!=0) {
        for(var j=0;j<(7-count);j++) {
            tabla += "<td>";
            if(parseInt(count+j)==5 || parseInt(count+j)==6){
                tabla += "<input tabindex='-1' readonly disabled onKeyPress='return numeros(event)' value='N/A' />";
            }
            else{
                tabla += "<input tabindex='-1' "+((isAdmin != 1)?"readonly":"")+" onKeyPress='return numeros(event)' value='0' />";
            }
            tabla += "</td>";
        }
    }
    tabla += "<td>";
    tabla += "<label>"+dataAbsence[i-1].hour_week+"</label>";
    tabla += "</td>";
    tabla += "<td align='center'>";
    if(isAdmin == 1){
        tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><input style='position: relative; top: -2px;' type='radio' value='1' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' "+((dataAbsence[i-1].id_state == 1)?"checked":"")+"/><br>"+
        "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar' /> <input style='position: relative; top: -2px;' type='radio' value='3' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' "+((dataAbsence[i-1].id_state == 3)?"checked":"")+"/><br>"+
        "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar' /> <input style='position: relative; top: -2px;' type='radio' value='2' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' "+((dataAbsence[i-1].id_state == 2)?"checked":"")+"/>";
    }else{
        if(dataAbsence[i-1].id_state == 3 ) {
            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/>";
        }else if(dataAbsence[i-1].id_state == 2 ) {
            tabla += "<img width='20' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado'/><br><br>";
            if(isAdmin == 2){
            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' /><br>"+
            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' checked onchange='changeState(this)' />";
            }
        }else {
            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/><br><br>";
            if(isAdmin == 2){
            tabla += "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' /><br>"+
            "<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazar'/> <input style='position: relative; top: -2px;' type='radio' name='"+dataAbsence[i-1].absence_name+"' onchange='changeState(this)' />";
            }
        }
    }
    tabla += "</td>";
    tabla += "</tr>";
    addImputeComment(dataAbsence[i-1].absence_name, dataAbsence[i-1].comment, dataAbsence[i-1].comment2, dataAbsence[i-1].id_state, true);
    jQuery('#tableImpute tr:last').after(tabla);
}

function drawTotalDay(){
    var htmlv = "";
    htmlv += "<tr id='trTotalDay' class='row1'>";
    htmlv += "<th><label>Total</label></th>";
    htmlv += "<td><input disabled type='text' maxlength='4' value='' tabindex='-1' readonly /></td>";
    htmlv += "<td><input disabled type='text' maxlength='4' value='' tabindex='-1' readonly/></td>";
    htmlv += "<td><input disabled type='text' maxlength='4' value='' tabindex='-1' readonly/></td>";
    htmlv += "<td><input disabled type='text' maxlength='4' value='' tabindex='-1' readonly /></td>";
    htmlv += "<td><input disabled type='text' maxlength='4' value='' tabindex='-1' readonly/></td>";
    htmlv += "<td><input disabled type='text' maxlength='4' value='' tabindex='-1' readonly/></td>";
    htmlv += "<td><input disabled type='text' maxlength='4' value='' tabindex='-1' readonly/></td>";
    htmlv += "<th><label id='totalWeek'></label></th>";
    htmlv += "<td></td>";
    htmlv += "</tr>";
    //totalDay++;
    jQuery('#tableImpute tr:last').after(htmlv);
}

function addImputeComment(name_project, comment1, comment2, id_state, flagproject) {
    var tableComment = "";
    tableComment += "<tr class='row1'>";
    tableComment += "<td>";
    tableComment += "<label>"+name_project+"</label>";
    tableComment += "</td>";
    tableComment += "<td>";
    if(id_state == 3 || !flagproject) {
        tableComment += "<textarea tabindex='-1' tabindex='-1' readonly cols='50' rows='5'>"+comment1+"</textarea>";
    }else {
        tableComment += "<textarea cols='50' rows='5'>"+comment1+"</textarea>";
    }
    tableComment += "</td>";
    tableComment += "<td>";
    tableComment += "<textarea tabindex='-1' tabindex='-1' readonly cols='50' rows='5'>"+comment2+"</textarea>";
    tableComment += "</td>";
    tableComment += "</tr>";
    jQuery('#tableImputeComment tr:last').after(tableComment);
}

function drawHeadAbsenceComment() {
    var headAbsenceComment = "<tr><th class='encabezado'><?php echo JText::_('Absence'); ?></th>";
    headAbsenceComment += "<th colspan='2' class='encabezado'></th></tr>";
    jQuery('#tableImputeComment tr:last').after(headAbsenceComment);
}

function sumaHorasDia() {
    var tablaInpute = document.getElementById('tableImpute');
    var totalday = 0;
    var totalweek = 0;
    for(var j = 1; j < 8; j++) {
        totalday = 0;
        for(var i = 1; i < (tablaInpute.rows.length-1); i++) {
            var hour = tablaInpute.rows[i].cells[j].getElementsByTagName('input');
            if (hour.length > 0) {
                if(!isNaN(hour[0].value)){
                    totalday = totalday+parseFloat(hour[0].value);
                }
            }
        }
        tablaInpute.rows[(tablaInpute.rows.length)-1].cells[j].getElementsByTagName('input')[0].value = totalday;
        totalweek =totalweek + totalday
    }
    document.getElementById('totalWeek').innerHTML = totalweek;
}

//Project
function recuperarSelected(adicionar){
    if(jQuery("#start_date").val() != ""){
        var select = document.getElementById("select_users1");
        var posNewsRecurse = new Array();
        if(adicionar){
            for(var i=0;i<select.length;i++){
                if(select.options[i].selected){
                    valor = select.options[i].value.replace(/_/gi," ");
                    if(select.options[i].id <= 0) {
                        select.options[i].id = select.options[i].id - 1;
                    }
                    seleccionados[k] = new Array(select.options[i].id, valor, select.options[i].title, jQuery("#"+select.options[i].id).attr("dateini"));
                    posNewsRecurse.push(k);
                    k++;
                    if(select.options[i].id > 0) {
                //select.remove(i);
                //i=i-1;
                }
                }
            }
        //            updateHoursTeam();
        //countUserBack2=seleccionados.length;
        }

        var posl=countUserBack2.valueOf();
        var posNewsRecurseRepeat = new Array();
        //Ciclo para determinar cuales de los nuevos nombres seleccionados ya se encuentra entre los miembros del equipo
        for(var v=0; v<posNewsRecurse.length; v++) {
            for(var j=0; j<seleccionados.length; j++){
                if(posNewsRecurse[v] != j) {
                    if(seleccionados[posNewsRecurse[v]][0] == seleccionados[j][0]) {
                        if(posNewsRecurseRepeat.indexOf(posNewsRecurse[v]) < 0) {
                            posNewsRecurseRepeat.push(posNewsRecurse[v]);
                        }
                    }
                }
            }
        }

        var tableUserSelected = document.getElementById("tableUsersSelected");
        var recurseIsRepeat = false;
        var userDelete = new Array();
        var msjDateInvalid = false;
        for(var j=posl; j<seleccionados.length; j++){
            recurseIsRepeat = false;
            //            var param1 = 'return showCalendar("start_date_user'+j+'", "%d-%m-%Y")';
            var value1 = seleccionados[j][3].split("-");
            ////getDateCurrent().split("/");
            //        if(parseInt(value1[2])<10){
            //            value1[2]="0"+value1[2];
            //        }
            //        if(parseInt(value1[1])<10){
            //            value1[1]="0"+value1[1];
            //        }
            var startProject = formatDate(jQuery("#start_date").val());
            var startUser = formatDate(value1[2]+"-"+value1[1]+"-"+value1[0]);
            if(startUser.valueOf() > startProject.valueOf()){
                value1=value1[2]+"-"+value1[1]+"-"+value1[0];
            }
            else{
                value1=jQuery("#start_date").val();
            }

            var StartDate = "<input origen='N' class='start_date_usr' id='start_date_user"+j+"' value=''  name='start_date_user' size='20' type='text' readonly/>";

            //            var param2 = 'return showCalendar("end_date_user'+j+'", "%d-%m-%Y")';
            var EndDate = "<input class='end_date_usr' id='end_date_user"+j+"' value='' name='end_date_user' size='20' type='text' readonly/>"+
            "";

            //ciclo que me determina cual es la posicion de los miembros repetidos en el array
            for(var r=0; r<posNewsRecurseRepeat.length; r++) {
                if(posNewsRecurseRepeat[r] == j) {
                    recurseIsRepeat = true;
                }
            }

            if(!recurseIsRepeat){
                sales=colocarPuntosComa2(seleccionados[j][2]);
                var htmlv = "";
                htmlv += "<tr id='trn"+j+"' class='row1' iduser='"+seleccionados[j][0]+"'>";
                if(seleccionados[j][0] > 0) {
                    htmlv += "<td><label>"+seleccionados[j][1]+"</label></td>";
                }else {
                    listUser = listUser.replace("idGeneralResource",seleccionados[j][0]);
                    listUser = listUser.replace("filaGeneralResource",j);
                    htmlv += "<td><label>"+listUser+"</label></td>";
                }
                htmlv += "<td><label>"+sales+"</label></td>";
                htmlv += "<td>"+ StartDate +"</td>";
                htmlv += "<td>"+ EndDate +"</td>";
                htmlv += "<td><input class='inputbox' type='text' id='sales_user_selected"+j+"' name='tariff_user_selected'"+
                "size='20' maxlength='11' onkeypress='return numeros(event)' value='"+sales+"' /></td>";//onkeyup='pointCurrency(this,this.value.charAt(this.value.length-1))'
                htmlv += "<td id='icons'><input class='inputbox clock hours_users' type='text' id='hours_user_selected"+j+"' name='hours_user_selected'"+
                "size='5' maxlength='1' onkeypress='return numeros(event)' onblur='validateMinMaxValue(0, 8, this)' value='8' /> <li "+((seleccionados[j][0] < 0) ? "style='display: none'" : "")+" onclick='hoursuserProject("+seleccionados[j][0]+", this, \""+seleccionados[j][1]+"\")' class='ui-state-default ui-corner-all' title='clock'><span class='ui-icon ui-icon-clock'></span></li> </td>";
                htmlv +="<td id='icons' ></td>";
                htmlv +="<td id='icons' >"+"<li id='eliminar"+seleccionados[j][0] +"' name='eliminar' onclick='quitarSelected(this,"+seleccionados[j][0]+", false)' class='ui-state-default ui-corner-all' title='Eliminar'><span class='ui-icon ui-icon-closethick'></span></li></td>";
                htmlv += "</tr>";

                jQuery('#tableUsersSelected tr:last').after(htmlv);
                filasTabla++;
                /*trAllTeamHours({
                    start_date: value1,
                    end_date: "",
                    id_user: seleccionados[j][0],
                    name_user: seleccionados[j][1],
                    hours_defect: 8
                },dateNow);*/
            }else {
                //ciclo que me permite obtener la fila de la tabla en la que se encuentra el miembro del equipo repetido
                for(var q=1; q<tableUserSelected.rows.length; q++) {
                    var labelNameRecurse = tableUserSelected.rows[q].cells[0].getElementsByTagName("label")
                    if(labelNameRecurse.length > 0) {
                        if(labelNameRecurse[0].getElementsByTagName("select").length <= 0){
                            if(tableUserSelected.rows[q].cells[0].getElementsByTagName("label")[0].innerHTML == seleccionados[j][1]) {
                                var endDateAnt = jQuery("#"+tableUserSelected.rows[q].id+" td:eq(3) input:last").val();
                                var endDateAntD = formatDate(endDateAnt);
                                endDateAntD.setDate(endDateAntD.getDate()+1);
                                var endDateAnt2 = endDateAntD.getDate()+"-"+(endDateAntD.getMonth()+1)+"-"+endDateAntD.getFullYear();
                                if(endDateAnt != "" && endDateAnt != jQuery("#end_date").val()) {
                                    StartDate = StartDate.replace(value1, endDateAnt2);
                                    jQuery("#"+tableUserSelected.rows[q].id+" td:eq(2)").append("<br/>"+StartDate);
                                    jQuery("#"+tableUserSelected.rows[q].id+" td:eq(3)").append("<br/>"+EndDate);
                                    jQuery("#"+tableUserSelected.rows[q].id+" td:eq(5)").append("<br/><input class='inputbox clock hours_users' type='text' id='hours_user_selected"+j+"' name='hours_user_selected'"+
                                        "size='5' maxlength='1' onkeypress='return numeros(event)' onblur='validateMinMaxValue(0, 8, this)' value='8' />");
                                    tableUserSelected.rows[q].cells[6].innerHTML = "<td id='icons' >"+"<li id='eliminar"+seleccionados[j][0] +"' name='eliminar' onclick='quitarSelected(this,"+seleccionados[j][0]+", false)' class='ui-state-default ui-corner-all' title='Eliminar'><span class='ui-icon ui-icon-calendar'></span></li></td>";
                                    //tableAllTeamHours(dateNow);
                                    //updateHoursTeam();
                                }else {
                                    userDelete.push(j);
                                    msjDateInvalid = true;
                                }
                            }
                        }
                    }
                }
            }
            removeSelectGeneralResource(seleccionados[j][0]);
        }
        for(var l=(userDelete.length-1); l>=0; l--) {
            seleccionados.splice(userDelete[l],1);
            k--;
        }
        if(msjDateInvalid) {
            alert(msjAddUser);
        }
        countUserBack2=seleccionados.length;

        jQuery('.end_date_usr').datepicker({
            showOn: "button",
            buttonImage: "images/calendar.gif",
            buttonImageOnly: true,
            dateFormat: 'dd-mm-yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
        }).focus();
        //            return false;

        //        jQuery('.start_date_usr:not(.ui-datepicker)').live('focus',function() {
        jQuery('.start_date_usr').datepicker({
            showOn: "button",
            buttonImage: "images/calendar.gif",
            buttonImageOnly: true,
            dateFormat: 'dd-mm-yy',
            firstDay: 1,
            changeMonth: true,
            changeYear: true
        }).focus();
    //            return false;
    //        });
    //        jQuery(".start_date_usr").datepicker({
    //            showOn: "button",
    //            buttonImage: "images/calendar.gif",
    //            buttonImageOnly: true,
    //            dateFormat: 'dd-mm-yy',
    //            firstDay: 1,
    //            changeMonth: true,
    //            changeYear: true
    //        });
    }
    else{
        alert('Debe definir la fecha de inicio y fin del proyecto');
    }
}

function removeSelectGeneralResource(idUser) {
    jQuery("#tableUsersSelected").find("select").each(function (index2) {
        var obt = jQuery(this).find("[value*="+idUser+"]");
        obt.remove();
    });
}

function changeVisible(){
    type = document.getElementById("type").value;
    loadTableAmount();
    var div= document.getElementById("tableAmount");
//    if(type==1 || type==3){
//        div.style.display = '';
//    }
//    else{
//        div.style.display = 'none';
//    }
    recuperarSelected(false);
}


//Result Scorecard
function drawHeadScorecard(year) {
    var rowHead = "";
    rowHead += "<thead><tr style='background-color: #F0F0F0;'><th id='vacia'></th>";
    rowHead += "<th id='month'>Enero-"+year+"</th>";
    rowHead += "<th id='month'>Febrero-"+year+"</th>";
    rowHead += "<th id='month'>Marzo-"+year+"</th>";
    rowHead += "<th id='month'>Abril-"+year+"</th>";
    rowHead += "<th id='month'>Mayo-"+year+"</th>";
    rowHead += "<th id='month'>Junio-"+year+"</th>";
    rowHead += "<th id='month'>Julio-"+year+"</th>";
    rowHead += "<th id='month'>Agosto-"+year+"</th>";
    rowHead += "<th id='month'>Septiembre-"+year+"</th>";
    rowHead += "<th id='month'>Octubre-"+year+"</th>";
    rowHead += "<th id='month'>Noviembre-"+year+"</th>";
    rowHead += "<th id='month'>Diciembre-"+year+"</th>";
    rowHead += "<th align='right' id='total'>TOTAL</th></tr></thead>";
    return rowHead;
}

function drawHeadScorecardNoi(year) {
    var rowHead = "";
    rowHead += "<thead><tr><th style='background-color: #CCF;' id='vacia'></th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Enero-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Febrero-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Marzo-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Abril-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Mayo-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Junio-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Julio-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Agosto-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Septiembre-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Octubre-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Noviembre-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' id='month'>Diciembre-"+year+"</th>";
    rowHead += "<th style='background-color: #CCF;' align='right' id='total'>TOTAL</th></tr></thead>";
    return rowHead;
}

function drawTotalScorecard(title) {
    var rowTitle = "";
    rowTitle += "<tr><th>"+title+"</th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\" ></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
    rowTitle += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\"></th></tr>";
    return rowTitle;
}

function sum(tablaSum) {
    var rows = tablaSum.rows.length;
    var cols = tablaSum.rows[0].cells.length;
    var sumyear = 0;
    var summonth = new Array(0,0,0,0,0,0,0,0,0,0,0,0);
    var sumtotal1 = 0;
    var sumtotal2 = 0;
    var h = 0;
    for(var i=2; i<rows; i++) {
        sumyear = 0;
        h = 0;
        for(var j=1; j<(cols-1); j++) {
            sumyear = sumyear + parseInt(tablaSum.rows[i].cells[j].innerHTML);
            summonth[h] = summonth[h] + parseInt(tablaSum.rows[i].cells[j].innerHTML);
            h++;
        }
        tablaSum.rows[i].cells[(cols-1)].innerHTML = sumyear;
        sumtotal1 = sumtotal1 + sumyear;
    }
    h=0;
    for(var j=1; j<(cols-1); j++) {
        tablaSum.rows[1].cells[j].innerHTML = summonth[h];
        sumtotal2 = sumtotal2 + summonth[h];
        h++;
    }
    if(sumtotal1 == sumtotal2) {
        tablaSum.rows[1].cells[(cols-1)].innerHTML = sumtotal1;
    }else {
        alert("Error de calculo");
    }
}

function sumClients(tablaSum){
    var rows = tablaSum.rows.length;
    var cols = tablaSum.rows[0].cells.length;

    for(var i=2; i<rows; i++){
        if(tablaSum.rows[i].onclick!==null){
            var j=i+1;
            var sumyear=0;
            var summonth = new Array(0,0,0,0,0,0,0,0,0,0,0,0);

            while(tablaSum.rows[j].onclick === null){
                var h=0;
                for(var k=1; k<(cols-1); k++){
                    sumyear = sumyear + parseInt(tablaSum.rows[j].cells[k].innerHTML);
                    summonth[h] = summonth[h] + parseInt(tablaSum.rows[j].cells[k].innerHTML);
                    h++;
                }
                j++;
                if(tablaSum.rows[j]==undefined){
                    break;
                }
            }

            tablaSum.rows[i].cells[(cols-1)].innerHTML = sumyear;
            if(sumyear == 0) {
                tablaSum.rows[i].style.display = 'none';
            }
            var posSum=0;
            for(var p=1; p<(cols-1); p++) {
                tablaSum.rows[i].cells[p].innerHTML = summonth[posSum];
                posSum++;
            }
            i=j-1;
        }
    }
}
