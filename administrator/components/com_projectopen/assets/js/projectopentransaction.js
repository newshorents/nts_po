//Category
function categoriaObj(name, id_currency, salary, cost_hour, sales_hour, currency_sales) {
    this.name = name;
    this.id_currency = id_currency;
    this.salary = salary;
    this.cost_hour = cost_hour;
    this.sales_hour = sales_hour;
    this.currency_sales = currency_sales;
}

//imputation
function imputationProject(id_imputation,id_state, comment, project, range_date,days,projectid) {
    this.id_imputation = id_imputation;
    this.id_state = id_state;
    this.comment = comment;
    this.project = project;
    this.range_date = range_date;
    this.days = days;
    this.projectid = projectid;
}

function imputationAbsence(id_imputation,id_state, comment, project, range_date,days) {
    this.id_imputation = id_imputation;
    this.id_state = id_state;
    this.comment = comment;
    this.project = project;
    this.range_date = range_date;
    this.days=days;
}

function imputationData(isAdmin) {

    var arrImputationDataProject = new Array();
    var arrImputationDataAbsence = new Array();
    if(isAdmin != 1){
        var tablaInpute = document.getElementById('tableImpute');
        var tablaComment = document.getElementById('tableImputeComment');
        var stater = 0;
        var comment;
        var project;
        var range_date;
        var state;
        var flagChange = false;
        for(var i = 1; i < tablaInpute.rows.length; i++) {
            flagChange = false;
            var idTable = tablaInpute.rows[i].id.split("-");
            if(idTable.length > 1 && !isNaN(parseInt(idTable[0]))) {
                state = tablaInpute.rows[i].cells[9].getElementsByTagName('input');
                if(state.length > 0) {
                    for(var j=0; j < changeStateId.length; j++) {
                        if(changeStateId[j].idproject == tablaInpute.rows[i].id) {
                            flagChange = true;
                        }
                    }
                    if(flagChange) {
                        if(state[0].checked == false && state[1].checked == false) {
                            stater = 1;
                        } else if(state[0].checked == false) {
                            stater = 2;
                        }else {
                            stater = 3;
                        }
                        comment = tablaComment.rows[i].cells[1].getElementsByTagName('textarea')[0];
                        project = tablaComment.rows[i].cells[0].getElementsByTagName('label')[0].innerHTML;
                        range_date = document.getElementById("range_date").innerHTML;
                        if(idTable[1] == "p") {
                            arrImputationDataProject.push(new imputationProject(idTable[0],stater,comment.value, project, idTable[0]));
                        }else {
                            arrImputationDataAbsence.push(new imputationAbsence(idTable[0],stater,comment.value, project, range_date));
                        }
                    }
                }else {
                    comment = tablaComment.rows[i].cells[1].getElementsByTagName('textarea')[0];
                    project = tablaComment.rows[i].cells[0].getElementsByTagName('label')[0].innerHTML;
                    range_date = document.getElementById("range_date").innerHTML;
                    for(var h=0; h<projectManagerRow.length; h++) {
                        if(i == projectManagerRow[h]) {
                            state = tablaInpute.rows[i].cells[9].getElementsByTagName('img');
                            if(state[0].title == "Rechazado") {
                                stater = 2;
                            }else if(state[0].title == "Validado") {
                                stater = 3;
                            }else {
                                stater = 1;
                            }
                            arrImputationDataProject.push(new imputationProject(idTable[0],stater,comment.value, project, range_date,tablaInpute.rows[i].id));
                        }
                    }
                }
            }
        }
    }
    else{
        var ind=0;
        var range_date = jQuery("#range_date").html();
        jQuery("#tableImpute tbody tr").each(function (index) {
            var idp = jQuery(this).attr("id").split('-')[0];
            var idproj = jQuery(this).attr("idproject");
            var timput =jQuery(this).attr("id").split('-')[1];
            var nproj = jQuery(this).children("td:eq(0)").find('label').html();
            var state = jQuery('[name="'+nproj+'"]:checked').val();
            var comment = jQuery("#tableImputeComment tr:eq("+ind+") td:eq(1)").find("textarea").val();
            var days =  new Array();

            if(nproj!=null){
                for(var e=1;e<6;e++){
                    days.push(parseFloat(jQuery(this).children("td:eq("+e+")").find('input').val()));
                }
                days.push(parseFloat(jQuery(this).children("td:eq(8)").find('label').html()));
                if(timput == "p") {
                    arrImputationDataProject.push(new imputationProject(idp,state,comment, nproj, range_date, days,idproj));
                }else if(timput == "a"){
                    arrImputationDataAbsence.push(new imputationAbsence(idp,state,comment, nproj, range_date, days));
                }
            }
            ind++;
        })
    }
    document.getElementById('imputation_project').value = JSON.stringify(arrImputationDataProject);
    document.getElementById('imputation_absence').value = JSON.stringify(arrImputationDataAbsence);

//return true;
}

function loadImputation(){
    dateSelected = new Date(nuevoValor);
    dayweek = dateSelected.getDay();
    daymonth = dateSelected.getDate();
    monthSelected = dateSelected.getMonth();
    yearSelected = dateSelected.getFullYear();

    monthEndSelected = monthSelected;
    yearEndSelected = yearSelected;

    if(dayweek!=0){
        dayStartWeek = (daymonth - dayweek)+1;
    }else{
        dayStartWeek = (daymonth - dayweek)-6;
    }
    if(dayStartWeek<0){
        monthSelected = monthSelected - 1 ;
        if(monthSelected<0){
            yearSelected = yearSelected - 1;
            monthSelected =11;
        }
        daysMonthBack = daysInMonth(monthSelected+1, yearSelected);
        dayStartWeek = daysMonthBack - Math.abs(dayStartWeek);
    }

    dayEndWeek = dayStartWeek + 6;
    daysMonth = daysInMonth(monthSelected+1, yearSelected);
    if(dayEndWeek > daysMonth){
        monthEndSelected = monthSelected + 1 ;
        if(monthEndSelected > 11){
            yearEndSelected = yearEndSelected +1;
            monthEndSelected=0;
        }
        dayEndWeek = dayEndWeek - daysMonth;
    }

    var dateStartQuery = yearSelected +"-"+parseInt(parseInt(monthSelected)+1)+"-"+dayStartWeek;
    var dateEndQuery = yearEndSelected +"-"+parseInt(parseInt(monthEndSelected)+1)+"-"+dayEndWeek;
    startDateWeek = yearSelected +"-"+parseInt(parseInt(monthSelected)+1)+"-"+dayStartWeek;
    var argsArray = new Array();
    argsArray.push(newArg("dateImputation", startDateWeek));
    argsArray.push(newArg("id", idUser));
    argsArray.push(newArg("c", "imputation"));
    argsArray.push(newArg("option", "com_projectopen"));
    argsArray.push(newArg("task", "getImputationAjax"));
    var argsStr = argsArray.join("&");
    sendRequest("index.php", argsStr, dataImputation);
}

function dataImputation() {
    if (isValidResponse(this)) {
        var res = this.responseText;
        dropTable();
        if (res != "") {
            var dataImputations = res.split("&");
            var tablaInpute = document.getElementById('tableImpute');
            var tablaComment = document.getElementById('tableImputeComment');
            var dataProject = JSON.parse(dataImputations[0]);
            var dataAbsence = JSON.parse(dataImputations[1]);
            if(isAdmin == 1){
                var dataProjectWeek = JSON.parse(dataImputations[3]);
            }
            datesProject = JSON.parse(dataImputations[4]);
            var projectManager = JSON.parse(dataImputations[2]);

            pintarSemana();
            if(dataProject.length > 0) {
                //pintarSemana();
                addImputeProject(dataProject,projectManager,isAdmin);
                if(isAdmin == 1){
                    addProjectWeek(dataProjectWeek);
                }
            }
            if(dataAbsence.length > 0) {
                drawHeadAbsence();
                drawHeadAbsenceComment();
                addImputeAbsence(dataAbsence);
            }
            if(dataProject.length > 0 || dataAbsence.length > 0) {
                drawTotalDay();
                sumaHorasDia();
                tablaInpute.style.display = '';
                tablaComment.style.display='';
                if(dataProject.length == 0) {
                    document.getElementById("headProject").style.display = 'none';
                }
            }else {
                tablaInpute.style.display = 'none';
                tablaComment.style.display='none';
            }
        }
    }
}

//Project
function team(idUser, startDate, endDate, tariff, hoursDefect, nameUser){
    this.idUser=idUser;
    this.nameUser = nameUser;
    this.startDate = startDate;
    this.endDate = endDate;
    this.tariff= tariff;
    this.hoursDefect = hoursDefect;
}

function expense(month, year, amount){
    this.month= month;
    this.year = year;
    this.amount = amount;
}

function quitarLastAjax(){
    if (isValidResponse(this)) {
        var res = this.responseText;
        if(res==1){
            var currentDate = getDateCurrent();
            var textDate=currentDate.split('/');
            if(parseInt(textDate[2])<10){
                textDate[2]="0"+textDate[2];
            }
            if(parseInt(textDate[1])<10){
                textDate[1]="0"+textDate[1];
            }
            var tr = rowGlobal;

            if(idQuitarGlobal > 0) {
                var end_dateUser= tr.cells[3].getElementsByTagName('input')[0];
                end_dateUser.value= textDate[2]+"-"+textDate[1]+"-"+textDate[0];
                var butDelete= tr.cells[6].getElementsByTagName('li')[0];
                butDelete.style.display='none';
                var imgEnd=tr.cells[3].getElementsByTagName('img')[0];
                imgEnd.style.display = 'none';
            }

            var select = document.getElementById("select_users1");
            var valor="";
            var sales="";
            var pos=-1;
            for(var i=0; i<seleccionados.length; i++){
                if(seleccionados[i][0]==idQuitarGlobal){
                    valor = seleccionados[i][1];
                    //valor = valor.replace(" ","_");
                    sales= seleccionados[i][2];
                    pos=i;
                    if(idQuitarGlobal < 0) {
                        seleccionados.splice(i, 1);
                        var table = tr.parentNode;
                        table.removeChild(tr);
                        k--;
                    }
                    i=seleccionados.length;
                }
            }
            addSelectGeneralResource(idQuitarGlobal, valor);
        }
    }
}

//Users
function userCategory(effectiveDate, idCategory) {
    this.effectiveDate = effectiveDate;
    this.idCategory = idCategory;
}

//function userCategoryStr() {
//    var arrUserCategory = new Array();
//    var tablaCategory = document.getElementById('category_table');
//    for(var i = 1; i < tablaCategory.rows.length; i++) {
//        var effectiveDate = tablaCategory.rows[i].cells[0].getElementsByTagName('input')[0];
//        if(!validateDateEffective(effectiveDate)) {
//            return false;
//        }
//        var idCategory = tablaCategory.rows[i].cells[1].getElementsByTagName('select')[0];
//        arrUserCategory.push(new userCategory(effectiveDate.value,idCategory.value));
//    }
//    document.getElementById('user_category').value = JSON.stringify(arrUserCategory);
//    return true;
//}
function userCategoryStr() {
    var arrUserCategory = new Array();
    var tablaCategory = document.getElementById('category_table');
    for(var i = 1; i < tablaCategory.rows.length; i++) {
        var effectiveDate = tablaCategory.rows[i].cells[0].getElementsByTagName('input')[0];
        if(i == (tablaCategory.rows.length)-1){
            if(!validateDateEffective(effectiveDate)) {
                return false;
            }
        }
        var idCategory = tablaCategory.rows[i].cells[1].getElementsByTagName('select')[0];
        arrUserCategory.push(new userCategory(effectiveDate.value,idCategory.value));
    }
    document.getElementById('user_category').value = JSON.stringify(arrUserCategory);
    return true;
}
function removeCategory(element) {
    var td = element.parentNode;
    var tr = td.parentNode;
    var tabla = tr.parentNode;
    tabla.removeChild(tr);
}

//Report Impute
function changeProjects(){
    var selectClient = document.getElementById("client");
    var clientId = selectClient.options[selectClient.selectedIndex].value;

    var argsArray = new Array();
    argsArray.push(newArg("clientId", clientId));
    argsArray.push(newArg("c", "report"));
    argsArray.push(newArg("option", "com_projectopen"));
    argsArray.push(newArg("task", "getProjectsClient"));
    var argsStr = argsArray.join("&");
    sendRequest("index.php", argsStr, drawSelectedProject);
}

function sendMail(){
    arrayUser = new Array();
    for(var i=0; i<dataUsers['id'].length; i++){
        arrayUser.push(new user(dataUsers['username'][i], dataUsers['email'][i], dataUsers['dates'][i]));
    }
    document.getElementById("usersSelected").value= JSON.stringify(arrayUser);
    submitform('sendMail');
}

function user(username, email, dates){
    this.username=username;
    this.email = email;
    this.dates = dates;
}

//Result Scorecard
function recoverData(){
    var tableOthers = document.getElementById('cov');
    //var tableIncomes = document.getElementById('tableIncomes');
    //var tableIndExpenses = document.getElementById('ie');
    var tableDirExpenses = document.getElementById('de');
    var tableInternalCost = document.getElementById('ic');
    var tableOtherOperatingExpenses = document.getElementById('ooe');
    //var tableNetOperatingIncome =document.getElementById("noi");
    var arrayScorecard = new Array();
    var rows = tableDirExpenses.rows.length;
    var cols = tableDirExpenses.rows[0].cells.length;
    for(var i=2; i<rows; i++) {
        if(tableDirExpenses.rows[i].onclick==null){
            for(var j=1; j<(cols-1); j++) {
                var  check= document.getElementById("ccbs"+(j-1));
                var state=0;
                if(check.disabled!=true){
                    if(check.checked == true){
                        state= 1;
                        var project = tableDirExpenses.rows[i].id;
                        /*var project = tableIncomes.rows[i].id;
                        if(tableIncomes.rows[i].cells[j].innerHTML!=""){
                            var income = quitarPuntos(tableIncomes.rows[i].cells[j].innerHTML);
                        }else{
                            var income=0;
                        }*/
                        var income=0;
                        if(tableDirExpenses.rows[i].cells[j].innerHTML!=""){
                            var dirExp = quitarPuntos(tableDirExpenses.rows[i].cells[j].innerHTML);
                        }else{
                            var dirExp=0;
                        }

                        /*if(tableIndExpenses.rows[i].cells[j].innerHTML!=""){
                            var indExp = quitarPuntos(tableIndExpenses.rows[i].cells[j].innerHTML);
                        }else{
                            var indExp=0;
                        }*/
                        var indExp=0;
                        /*if(tableNetOperatingIncome.rows[i].cells[j].getElementsByTagName('label').length>1){
                            var opMar = quitarPuntos(tableNetOperatingIncome.rows[i].cells[j].getElementsByTagName('label')[0].innerHTML);
                            var popMar = tableNetOperatingIncome.rows[i].cells[j].getElementsByTagName('label')[1].innerHTML;
                        }else{
                            var opMar= 0;
                            var popMar=0;
                        }*/
                        var opMar= 0;
                        var popMar=0;

                        var con= quitarPuntos(tableOthers.rows[2].cells[j].innerHTML);
                        var month=j-1;
                        arrayScorecard.push(new scorecard(project, null, income, dirExp, indExp, opMar, popMar, 0, 0, con, month, yearGlobal, state));
                    }
                }
            }
        }
    }

    for(var i=2; i<tableInternalCost.rows.length; i++) {
        if(tableInternalCost.rows[i].getAttribute("class")!="trClientReport"){
            for(var j=1; j<(cols-1); j++) {
                var  check= document.getElementById("ccbs"+(j-1));
                var state=0;
                if(check.disabled!=true){
                    if(check.checked == true){
                        state= 1;
                        var absence = tableInternalCost.rows[i].id;
                        if(tableInternalCost.rows[i].cells[j].innerHTML!=""){
                            var intCos = quitarPuntos(tableInternalCost.rows[i].cells[j].innerHTML);
                        }else{
                            var intCos=0;
                        }

                        var con= quitarPuntos(tableOthers.rows[2].cells[j].innerHTML);
                        var month=j-1;
                        arrayScorecard.push(new scorecard(null, absence, 0, 0, 0, 0, 0, intCos, 0, con, month, yearGlobal, state));
                    }
                }
            }
        }
    }

    for(var i=2; i<tableOtherOperatingExpenses.rows.length; i++) {
        if(tableOtherOperatingExpenses.rows[i].getAttribute("class")!="trClientReport"){
            for(var j=1; j<(cols-1); j++) {
                var  check= document.getElementById("ccbs"+(j-1));
                var state=0;
                if(check.disabled!=true){
                    if(check.checked == true){
                        state= 1;
                        var absence = tableOtherOperatingExpenses.rows[i].id;
                        if(tableOtherOperatingExpenses.rows[i].cells[j].innerHTML!=""){
                            var otherOperExpen = quitarPuntos(tableOtherOperatingExpenses.rows[i].cells[j].innerHTML);
                        }else{
                            var otherOperExpen=0;
                        }

                        var con= quitarPuntos(tableOthers.rows[2].cells[j].innerHTML);
                        var month=j-1;
                        arrayScorecard.push(new scorecard(null, absence, 0, 0, 0, 0, 0, 0, otherOperExpen, con, month, yearGlobal, state));
                    }
                }
            }
        }
    }

    return JSON.stringify(arrayScorecard);
}

function exportTabla(tabla, nomtabla) {
    var rows = tabla.rows.length;
    var cols = tabla.rows[0].cells.length;
    var tablaArray = new Array();

    var col0 = "";
    var col1 = "";
    var col2= "";
    var col3 = "";
    var col4 = "";
    var col5 = "";
    var col6= "";
    var col7= "";
    var col8= "";
    var col9= "";
    var col10="";
    var col11="";
    var col12="";
    var col13="";

    for(var i=0; i<rows; i++) {
        var cola = new Array();
        for(var j=0; j<cols; j++) {
            if(nomtabla == "otro" && i == 1 && j == 0) {
                cola.push(quitarPuntos(tabla.rows[i].cells[j].innerHTML));
            }else if(nomtabla == "otro" && i == 1 && j != 0) {
                cola.push(quitarPuntos(tabla.rows[i].cells[j].getElementsByTagName("label")[0].innerHTML));
            }/*else if(nomtabla == "margen" && i == 1 && j == 0) {
                    cola.push(tabla.rows[i].cells[j].innerHTML);
                }else if(nomtabla == "margen" && i != 1 && j == 0) {
                    cola.push(tabla.rows[i].cells[j].innerHTML);
                }else if(nomtabla == "margen" && i == 1 && j != 0) {
                    cola.push(tabla.rows[i].cells[j].getElementsByTagName("label")[0].innerHTML);
                }*/else {
                cola.push(quitarPuntos(tabla.rows[i].cells[j].innerHTML));
            }
        }
        for(var h=0; h<cola.length;h++) {
            switch(h){
                case 0:
                    col0 = cola[h];
                    break;
                case 1:
                    col1 = cola[h];
                    break;
                case 2:
                    col2 = cola[h];
                    break;
                case 3:
                    col3 = cola[h];
                    break;
                case 4:
                    col4 = cola[h];
                    break;
                case 5:
                    col5 = cola[h];
                    break;
                case 6:
                    col6 = cola[h];
                    break;
                case 7:
                    col7 = cola[h];
                    break;
                case 8:
                    col8 = cola[h];
                    break;
                case 9:
                    col9 = cola[h];
                    break;
                case 10:
                    col10 = cola[h];
                    break;
                case 11:
                    col11 = cola[h];
                    break;
                case 12:
                    col12 = cola[h];
                    break;
                case 13:
                    col13 = cola[h];
                    break;
            }
        }
        tablaArray.push(new exportScorecard(col0,col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13));
    }
    return JSON.stringify(tablaArray);
}

function exportScorecard(col0,col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13){
    this.col0= col0;
    this.col1 = col1;
    this.col2= col2;
    this.col3 = col3;
    this.col4 = col4;
    this.col5 = col5;
    this.col6= col6;
    this.col7= col7;
    this.col8= col8;
    this.col9=col9;
    this.col10=col10;
    this.col11=col11;
    this.col12=col12;
    this.col13=col13;
}

function scorecard(id_project, id_absence, income, dirExp, indExp, opMar, popMar, intCos, otherOperExpen, conversion, month, year, state){
    this.id_project= id_project;
    this.id_absence= id_absence;
    this.income = income;
    this.dirExp= dirExp;
    this.indExp = indExp;
    this.opMar = opMar;
    this.popMar = popMar;
    this.intCos = intCos;
    this.otherOperExpen = otherOperExpen;
    this.conversion= conversion;
    this.month= month;
    this.year= year;
    this.state=state;
}
