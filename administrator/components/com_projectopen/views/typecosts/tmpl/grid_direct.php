<?php defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->search); ?>" onchange="document.adminForm.submit();" />
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
        </tr>
    </table>
    <table class="adminlist">
    <thead>       
        <tr>
            <th width="20">
                <?php echo JText::_( 'NUM' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Name' ); ?>
            </th>            
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="2">
                <?php echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
    </tfoot>
    <tbody>
    <?php $i = 0; $k = 0; ?>
    <?php foreach ($this->typecosts as $typecost) : ?>
        <?php
            // Get the current iteration and set a few values
            $link     = 'index.php?option=com_projectopen&c=typecosts&amp;task=editTypeCostsDirect&amp;id='. $typecost->id;
        ?>
        <tr class="<?php echo "row". $k; ?>">
            <td width="30">
                <?php echo $this->pagination->limitstart + 1 + $i; ?>
            </td>
            <td>
                <a href="<?php echo $link; ?>">
                    <?php echo $typecost->name; ?></a>
            </td>          
            
           </tr>
        <?php $i++; $k = 1 - $k; ?>
    <?php endforeach; ?>
    </tbody>
    </table>

    <input type="hidden" name="c" value="typecosts" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="viewTypeCostsDirect" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_( 'form.token' ); ?>
</form>

