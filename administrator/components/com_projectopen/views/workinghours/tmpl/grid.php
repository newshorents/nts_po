<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>

<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <div style=" margin-bottom: 10px;  text-align: right;">
                    <label for="path"><?php echo JText::_('Years working hours'); ?></label>
                    <?php echo $this->lists['year']; ?>
                </div>
            </td>
        </tr>
    </table>
    <table align="center" class="adminlist" id="tableGrid">
        <thead>
            <tr>
                <th rowspan="2" width="20">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th rowspan="2" class="title" nowrap="nowrap">
                    <?php echo JText::_('Years'); ?>
                </th>
                <th colspan="12" class="title" nowrap="nowrap">
                    <?php echo JText::_('Months'); ?>
                </th>
            </tr>
            <tr>
                <th width="20">
                    <?php echo JText::_('January'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('February'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('March'); ?>
                </th>
                <th width="20">
                    <?php echo JText::_('April'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('May'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('June'); ?>
                </th>
                <th width="20">
                    <?php echo JText::_('July'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('August'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('September'); ?>
                </th>
                <th width="20">
                    <?php echo JText::_('October'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('November'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('December'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="14">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php
            $i = 0;
            $k = 0;
            $year = 0;
            $fil = 1;
            foreach ($this->workinghours as $workinghour) :
                if ($year != $workinghour->year) {
                    if ($i != 0) {
                        while($i%12 != 0){
                            echo "<td align='center' width='7%'>0</td>";
                            $i++;
                        }
                        echo "</tr>";
                    }
                    $link = 'index.php?option=com_projectopen&c=workinghours&amp;task=editWorkinghours&amp;yearid=' . $workinghour->year;
                    echo "<tr class='row" . $k . "'><td align='center' width='30'>" . ($fil) . "</td>";
                    echo "<td align='center' width='40'><a href='" . $link . "' >" . $workinghour->year . "</a></td>";
                    $year = $workinghour->year;
                    $fil++;
                }
                echo "<td align='center' width='7%'>" . $workinghour->hours . "</td>";
                $i++;
                $k = 1 - $k;
            endforeach;
            while($i%12 != 0){
                echo "<td align='center' width='7%'>0</td>";
                $i++;
            }
            echo "</tr></table>";
            ?>
        </tbody>
    </table>

    <input type="hidden" name="c" value="workinghours" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="viewWorkinghours" />

    <?php echo JHTML::_('form.token'); ?>
</form>

