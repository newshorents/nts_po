<?php defined('_JEXEC') or die('Restricted access'); ?>

<script language="javascript" type="text/javascript">
<!--
    function submitbutton(task)
    {
        var f = document.adminForm;
         if(task == 'remove'){
            if(confirm('¿Esta seguro de eliminar el registro?'))submitform('remove');
         }else{
            submitform(task);
         }
    }

    function menu_listItemTask( id, task, option )
    {
        var f = document.adminForm;
        cb = eval( 'f.' + id );
        if (cb) {
            cb.checked = true;
            submitbutton(task);
        }
        return false;
    }
//-->
</script>

<form action="index.php" method="post" name="adminForm">

    <table class="adminlist">
    <thead>
        <tr>
            <th width="20">
                <?php echo JText::_( 'NUM' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Name' ); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="2">
                <?php //echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
    </tfoot>
    <tbody>
    <?php $i = 0; $k = 0; ?>
    <?php foreach ($this->statecosts as $statecost) : ?>
        <?php
            // Get the current iteration and set a few values
            $link     = 'index.php?option=com_projectopen&c=statecosts&amp;task=editStateCosts&amp;id='. $statecost->id;
        ?>
        <tr class="<?php echo "row". $k; ?>">
            <td width="30">
                <?php echo $this->pagination->limitstart + 1 + $i; ?>
            </td>
            <td>
            <span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit State Cost Name' );?>::<?php echo $statecost->name; ?>">
                <a href="<?php echo $link; ?>">
                    <?php echo $statecost->name; ?></a></span>
            </td>

           </tr>
        <?php $i++; $k = 1 - $k; ?>
    <?php endforeach; ?>
    </tbody>
    </table>

    <input type="hidden" name="c" value="statecosts" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_( 'form.token' ); ?>
</form>

