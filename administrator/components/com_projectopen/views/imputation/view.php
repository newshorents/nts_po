<?php

/**
 * @version		1.0.0 projectopen $
 * @package		projectopen
 * @copyright	Copyright © 2010 - All rights reserved.
 * @license		GNU/GPL
 * @author
 * @author mail	nobody@nobody.com
 *
 *
 * @MVC architecture generated by MVC generator tool at http://www.alphaplug.com
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.html.pane');

class projectopenViewImputation extends Jview {

    private function _menuImputation() {
        JSubMenuHelper::addEntry(JText::_('Menu'), 'index.php?option=com_projectopen');
    }

    public function gridImputation($tpl = null) {
        $this->_layout = 'grid';
        $this->_menuImputation();

        JToolBarHelper::title(JText::_('Imputations Manager'), 'menumgr.png');
        //JToolBarHelper::addNewX('addImputation');
        $bar = & JToolBar::getInstance('toolbar');
        $bar->appendButton('Popup', 'help', 'Help', 'index.php?option=com_projectopen&c=helpmodal&task=viewHelpModal&id=141', 740, 520);
        $document = & JFactory::getDocument();
        $document->setTitle(JText::_('View Imputation'));
        $limitstart = JRequest::getVar('limitstart', '0', '', 'int');
        $pagination = &$this->get('Pagination');
        $imputations = &$this->get('Data');
        $this->assignRef('imputations', $imputations);
        $this->assignRef('pagination', $pagination);
        $this->assignRef('limitstart', $limitstart);

        $search = JRequest::getVar('search');
        $this->assignRef('search', $search);
        $searchdate = JRequest::getVar('searchdate');
        $this->assignRef('searchdate', $searchdate);

        $db = & JFactory::getDBO();
        $query = "Select * from #__tm_state_impute 
                  where #__tm_state_impute.id=1 || #__tm_state_impute.id=3 || #__tm_state_impute.id=2
                  order by name";
        $db->setQuery($query, 0);

        $states[] = JHTML::_('select.option', '0', '- ' . JText::_('Select State') . ' -');
        foreach ($db->loadObjectList() as $obj) {
            $states[] = JHTML::_('select.option', $obj->id, JText::_($obj->name));
        }
        if (JRequest::getVar('filter_state') == "") {
            $lists['state'] = JHTML::_('select.genericlist', $states, 'filter_state', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', 1);
        } else {
            $lists['state'] = JHTML::_('select.genericlist', $states, 'filter_state', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', JRequest::getVar('filter_state'));
        }
        $this->assignRef('lists', $lists);

        JHTML::_('behavior.tooltip');

        parent::display($tpl);
    }

    function editForm($edit, $tpl = null) {
        JRequest::setVar('hidemainmenu', 1);
        $text = ( ($edit) ? JText::_('EDIT DATA') : JText::_('New') );
        global $mainframe;
        $user = & JFactory::getUser();
        $type = $user->get('usertype');

        $this->_layout = 'edit';
        JToolBarHelper::title(JText::_('Edit Imputations') . ': <small><small>[ ' . $text . ' ]</small></small>', 'menumgr.png');
        JToolBarHelper::custom('saveImputation', 'save.png', 'save_f2.png', 'save', false);
        if ($type == 'Super Administrator') {
            JToolBarHelper::custom('remove', 'delete.png', 'delete_f2.png', 'remove', false);
        }
        $text = JText::_('Edit');
        $rowsproject = &$this->get('ImputationEditProject');
        $rowsabsence = &$this->get('ImputationEditAbsence');
        if ($type == 'Super Administrator') {
            $rowprojectweek = &$this->get('ProjectWeek');
        }
        $rowdatesproj = &$this->get('DatesProject');
        $userdates = &$this->get('UserDates');
        $project = &$this->get('Project');
        $user = &$this->get('User');
        $impute_date = JRequest::getVar('dateImputation');

        $impute_date2 = preg_split("/[\/.-]/", $impute_date);
        $impute_date = $impute_date2[2] . "-" . $impute_date2[1] . "-" . $impute_date2[0];

        $this->assignRef('impute_date', $impute_date);
        $this->assignRef('rowsproject', $rowsproject);
        $this->assignRef('rowsabsence', $rowsabsence);
        $this->assignRef('rowdatesproj', $rowdatesproj);
        $this->assignRef('userdates', $userdates);

        if ($type == 'Super Administrator') {
            $this->assignRef('rowprojectweek', $rowprojectweek);
        }
        $this->assignRef('project', $project);
        $this->assignRef('user', $user);

        //JToolBarHelper::custom('acceptImputation', 'save.png', 'save_f2.png', 'accept', false);
        //JToolBarHelper::custom('rejectImputation', 'save.png', 'save_f2.png', 'reject', false);
        JToolBarHelper::cancel();
        $bar = & JToolBar::getInstance('toolbar');
        $bar->appendButton('Popup', 'help', 'Help', 'index.php?option=com_projectopen&c=helpmodal&task=viewHelpModal&id=142', 740, 520);

        JHTML::_('behavior.tooltip');

        parent::display($tpl);
    }

    function save() {
        $res = &$this->get('SaveImputation');
        return $res;
        //$this->assignRef('res', $res);
    }

    function accept() {
        $res = &$this->get('SaveImputation');
        $this->assignRef('res', $res);
    }

    function reject() {
        $res = &$this->get('SaveImputation');
        $this->assignRef('res', $res);
    }

    public function getImputationAjax() {
        $user = & JFactory::getUser();
        $type = $user->get('usertype');
        $rowsproject = &$this->get('ImputationEditProject');
        $rowsabsence = &$this->get('ImputationEditAbsence');
        $project = &$this->get('Project');
        if ($type == 'Super Administrator') {
            $rowprojectweek = &$this->get('ProjectWeek');
        }
        $rowdatesproj = &$this->get('DatesProject');

        $json = json_encode($rowsproject);
        $json = $json . "&" . json_encode($rowsabsence);
        $json = $json . "&" . json_encode($project);
        $json = $json . "&" . json_encode($rowprojectweek);
        $json = $json . "&" . json_encode($rowdatesproj);
        return $json;
    }

    public function deleteImputation() {
        $resDP = $this->get('DeleteImputation');
        return $resDP;
    }

    public function getIsCloseMonth() {
        $res = &$this->get('IsCloseMonth');
        return json_encode($res);
    }

}

?>