<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>

<script type="text/javascript">

</script>

<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->search); ?>" onchange="document.adminForm.submit();" />
                <input id='searchdate' name='searchdate' size='20' type='text' value="<?php echo htmlspecialchars($this->searchdate); ?>" readonly onchange="document.adminForm.submit();"/>
                <img class='calendar' onclick='return showCalendar("searchdate", "%d-%m-%Y")'
                 src='templates/system/images/calendar.png' alt='calendar' style="top:5px;"/>
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';document.getElementById('searchdate').value='';document.getElementById('filter_state').value=1;this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
            <td>
                <div style=" margin-bottom: 10px;  text-align: right;">
                    <label for="path"><?php echo JText::_('State Impute'); ?></label>
                    <?php echo $this->lists['state']; ?>
                </div>
            </td>
        </tr>
    </table>
    <table class="adminlist" id="tableGrid">
        <thead>
            <tr>
                <th width="20">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Name user'); ?>
                </th>
                <th width="150" class="title" nowrap="nowrap">
                    <?php echo JText::_('Start date Week'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Name project'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('State'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="5">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
        <?php
            $i = 0;
            $k = 0;
            $idUser = 0;
            $dateImputeUser = "";
            $tabla = "";
            $count = 0;
            $nameUser = "";
            $dateImpute = "";
            if(count($this->imputations) != 0){
                foreach ($this->imputations as $imputation) :
                    if($idUser != $imputation->id || $dateImputeUser != $imputation->impute_date) {
                        if($i != 0) {
                            $tabla = "<tr class='row".(1-$k)."'><td rowspan='".$count."'>".$tabla;
                            $tabla = str_replace($nameUser, str_replace("<td>", "<td rowspan='".$count."'>", $nameUser), $tabla);
                            $tabla = str_replace($dateImpute, str_replace("<td>", "<td rowspan='".$count."'>", $dateImpute), $tabla);
                            echo $tabla;
                            $tabla = "";
                            $count = 0;
                        }
                        $idUser = $imputation->id;
                        $dateImputeUser = $imputation->impute_date;
                        list($year, $month, $day) = preg_split("/[\/.-]/", $dateImputeUser);
                        if($month == 12 && $day >= 27) {
                            $link = 'index.php?option=com_projectopen&c=imputation&amp;task=editImputation&amp;id=' . $imputation->id.'&amp;dateImputation='.($year+1).'-01-01';
                        }else {
                            $link = 'index.php?option=com_projectopen&c=imputation&amp;task=editImputation&amp;id=' . $imputation->id.'&amp;dateImputation='.$imputation->impute_date;
                        }
                        $tabla = $tabla.($this->pagination->limitstart + 1 + $i)."</td>";
                        $tabla = $tabla."<td><a href=".$link.">".$imputation->name."</a></td>";
                        $tabla = $tabla."<td>".$imputation->impute_date."</td>";
                        $tabla = $tabla."<td>".$imputation->name_project."</td><td>";
                        if($imputation->state_impute == 3 ) {
                            $tabla = $tabla."<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/>";
                        }else if($imputation->state_impute == 2 ) {
                            $tabla = $tabla."<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado' />";
                        } else {
                            $tabla = $tabla."<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/>";
                        }

                        $nameUser = "</td><td><a href=".$link.">".$imputation->name."</a></td>";
                        $dateImpute = "<td>".$imputation->impute_date."</td>";

                        $i++;
                        $k = 1 - $k;
                        $count++;
                    }else {
                        $tabla = $tabla."<tr class='row".(1-$k)."'><td>".$imputation->name_project."</td><td>";
                        if($imputation->state_impute == 3 ) {
                            $tabla = $tabla."<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/validate.png' title='Validado'/>";
                        }else if($imputation->state_impute == 2 ) {
                            $tabla = $tabla."<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/novalidate.png' title='Rechazado' />";
                        } else {
                            $tabla = $tabla."<img width='15' style='float:left' heigth='20' name='statep' src='../media/system/images/pending.png' title='Pendiente'/>";
                        }
                        $tabla = $tabla."</td></tr>";
                        $count++;
                    }

                endforeach;
                $tabla = "<tr class='row".(1-$k)."'><td rowspan='".$count."'>".$tabla;
                $tabla = str_replace($nameUser, str_replace("<td>", "<td rowspan='".$count."'>", $nameUser), $tabla);
                $tabla = str_replace($dateImpute, str_replace("<td>", "<td rowspan='".$count."'>", $dateImpute), $tabla);
                echo $tabla;
                $tabla = "";
            }
		?>
        </tbody>
    </table>

    <input type="hidden" name="c" value="imputation" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="viewImputation" />

<?php echo JHTML::_('form.token'); ?>
</form>
