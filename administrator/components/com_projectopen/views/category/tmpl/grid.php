<?php defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->search); ?>" onchange="document.adminForm.submit();" />
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
        </tr>
    </table>
    <table class="adminlist" id="tableGrid">
        <thead>
            <tr>
                <th>
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th width="20%" class="title" nowrap="nowrap">
                    <?php echo JText::_('Name'); ?>
                </th>

                <th width="20%" class="title"  nowrap="nowrap">
                    <?php echo JText::_('Currency'); ?>
                </th>
                <th width="20%">
                    <?php echo JText::_('Cost hour'); ?>
                </th>
                <th width="20%" class="title" nowrap="nowrap">
                    <?php echo JText::_('Sales hour'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="6">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php $i = 0;
                    $k = 0; ?>
            <?php foreach ($this->categories as $category) : ?>
            <?php
                        // Get the current iteration and set a few values
                        $link = 'index.php?option=com_projectopen&c=category&amp;task=editCategory&amp;id=' . $category->id;
            ?>
                        <tr class="<?php echo "row" . $k; ?>">
                            <td width="30">
                    <?php echo $this->pagination->limitstart + 1 + $i; ?>
                    </td>

                    <td>
                        <a href="<?php echo $link; ?>">
                        <?php echo $category->name; ?></a>
                </td>
                <td>
                    <?php
                        echo $category->currency_name;
                    ?>
                    </td>
                    <td> <label><?php echo number_format($category->cost_hour, 2, ',', '.'); ?></label>
                    </td>
                    <td><label><?php echo number_format($category->sales_hour, 2, ',', '.'); ?></label>
                    </td>

                </tr>
            <?php $i++;
                        $k = 1 - $k; ?>
            <?php endforeach; ?>
                    </tbody>
                </table>

                <input type="hidden" name="c" value="category" />
                <input type="hidden" name="option" value="com_projectopen" />
                <input type="hidden" name="task" value="viewCategories" />
                <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_('form.token'); ?>
</form>

