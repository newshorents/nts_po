<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">

        <tr>
            <td  class="key">
                <label for="start_date"><?php echo JText::_('Start Date'); ?> <label style='color:#FF0000' >*</label></label>
            </td>
            <td colspan="5">
                <input id='start_date' name='start_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("start_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>
        <tr>
            <td  class="key">
                <label for="end_date"><?php echo JText::_('End Date'); ?> <label style='color:#FF0000' >*</label></label>
            </td>
            <td colspan="5">
                <input id='end_date' name='end_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("end_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr> 
        <tr>
            <td  class="key">
                <label for="office"><?php echo JText::_('Office'); ?></label>
            </td>
            <td><?php echo JHTML::_('select.genericlist', $this->offices, 'office', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?></td>

<!--            <td  class="key"><label for="user"><?php //echo JText::_('Area'); ?></label></td>
            <td><?php //echo JHTML::_('select.genericlist', $this->areas, 'area', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?>
                <div style="display:none;" id="load2"><img border="0" src="<?php //echo JURI::root(true) ?>/images/loadinfo.gif" /></div>
            </td>
            <td  class="key"><label for="user"><?php //echo JText::_('Client'); ?></label></td>
            <td><?php //echo JHTML::_('select.genericlist', $this->clients, 'client', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?>
                <div style="display:none;" id="load2"><img border="0" src="<?php //echo JURI::root(true) ?>/images/loadinfo.gif" /></div>
            </td>
            <td  class="key"><label for="user"><?php //echo JText::_('Project'); ?></label></td>
            <td><?php //echo JHTML::_('select.genericlist', $this->projects, 'project', 'class="inputbox" size="1" style="width:200px" ', 'id_project', 'clientProject', 0); ?>
                <div style="display:none;" id="load2"><img border="0" src="<?php //echo JURI::root(true) ?>/images/loadinfo.gif" /></div>
            </td>-->
        </tr> 
        <tr>
            <td class="key"><label for="user"><?php echo JText::_('User'); ?></label></td>
            <td colspan="5"><?php echo JHTML::_('select.genericlist', $this->userNewshore, 'user', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?>
                <div style="display:none;" id="load2"><img border="0" src="<?php echo JURI::root(true) ?>/images/loadinfo.gif" /></div>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <div id="tableUsersDiv">
        <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
        <table id="headerReport" border="0" cellspacing="0" class="headerreport">
        </table>
        <br>
        <table class="adminlist" id="tableStaffing" style="">
        </table> 
    </div>
    <div id="aux"></div>
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="weeks" name="weeks" value="" />
    <input type="hidden" id="headers" name="headers" value="" />

    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    jQuery.noConflict();
    var dataExport = new Array();
    var arrWeek = new Array();
    jQuery(document).ready(function(){
        jQuery("#office").change(function(){
            jQuery("#user").html("");
            var users = "<option value='0' selected>Todos los usuarios</option>";
            jQuery("#load2").show();
            jQuery.ajax({
                url: "index.php?option=com_projectopen&c=report&task=getUserOffice",
                global: false,
                async: false,
                type: "POST",
                data: ({id_office: jQuery(this).val()}),
                async: false,
                success: function(msg){
                    var usersO = jQuery.parseJSON(msg);
                    jQuery.each(usersO, function (i, val){
                        users += "<option value='"+val.id+"'>"+val.name+"</option>";
                    });
                    jQuery("#load2").hide();
                }
            }).responseText;
            
            jQuery("#user").html(users);
        });
        
        jQuery("#toolbar-send").css("display", "none");
        
        jQuery('.trUser').live("click",function(){
            var num = jQuery(this).attr('id').split('_')[1];
            if(jQuery('.tr_'+num).is(':visible')){
                jQuery('.tr_'+num).hide();
                jQuery(this).find('td').attr("rowspanIni", jQuery(this).find('td:eq(0)').attr('rowspan'));
                jQuery(this).find('td:eq(0)').attr("rowspan", "1");
                jQuery(this).find('td:eq(1)').attr("rowspan", "1");
                jQuery(this).find('td:eq(0) img').attr("src", "<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>");
            }
            else{
                jQuery('.tr_'+num).show();
                jQuery(this).find('td:eq(0)').attr("rowspan", jQuery(this).find('td:eq(0)').attr('rowspanIni'));
                jQuery(this).find('td:eq(1)').attr("rowspan", jQuery(this).find('td:eq(0)').attr('rowspanIni'));
                jQuery(this).find('td:eq(0) img').attr("src", "<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow_down.png' ?>");
            }                   
        });
    });
    function searchUsers(){
        jQuery("#toolbar-send").css("display", "none");
        //        document.getElementById("toolbar-send").style.display = 'none';
        var meses = new Array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
        var start_date =document.getElementById("start_date");
        var end_date = document.getElementById("end_date");
        
        if(start_date.value=="" || end_date.value==""){
            alert('<?php echo JText::_('Selected the search dates'); ?>')
        }else{
            jQuery("#tableStaffing").html("");
            textStartDate = start_date.value.split("-");
            textEndDate = end_date.value.split("-");
            start_date = textStartDate[2]+"/"+textStartDate[1]+"/"+textStartDate[0];
            end_date = textEndDate[2]+"/"+textEndDate[1]+"/"+textEndDate[0];
            var dstart_date = new Date(start_date);
            var dend_date = new Date(end_date);
            if(dend_date<dstart_date){
                alert('<?php echo JText::_('Start date must be less or equal than the end date'); ?>');
            }else{
                var html = "<tr><th width='3%'>Id</th><th width='10%'>Nombre</th><th width='10%'>Proyecto</th>";
                
                var dateTempini = new Date();
                dateTempini.setTime(dstart_date.getTime()-((dstart_date.getDay()-1)*24*60*60*1000));
                var dateTempfin = new Date();
                dateTempfin.setTime(dend_date.getTime()-((dend_date.getDay()-1)*24*60*60*1000));
                dateAux = dateTempini;
                html += "<th width='4%'>"+dateAux.getDate()+"-"+meses[dateAux.getMonth()]+"</th>";
                arrWeek.push(dateAux.getDate()+"-"+meses[dateAux.getMonth()]);
                while(dateAux.valueOf() < dateTempfin.valueOf()){
                    dateAux.setTime(dateAux.getTime()+(1*7*24*60*60*1000));
                    html += "<th width='4%'>"+dateAux.getDate()+"-"+meses[dateAux.getMonth()]+"</th>";
                    arrWeek.push(dateAux.getDate()+"-"+meses[dateAux.getMonth()]);
                }
                html +="</tr>";

                jQuery("#tableStaffing").html(html);
                jQuery("#load").show();
                var office = jQuery("#office").val();
                var area = jQuery("#area").val();
                var project = jQuery("#project").val();
                var client = jQuery("#client").val();
                if(jQuery("#user").val() == 0){
                    var resultUsers = jQuery.ajax({
                        url: "index.php?option=com_projectopen&c=report&task=getUserStaffing",
                        global: false,
                        async: false,
                        type: "POST",
                        data: ({start_date: start_date, end_date: end_date, office:office,area:area,project:project,client:client}),
                        async: false,
                        success: function(msg){
                        }
                    }).responseText;
                    var dataUsers = jQuery.parseJSON(resultUsers);
                }else{
                    var dataUsers = jQuery.parseJSON('[{"id_user":"'+jQuery("#user").val()+'","user_name":"'+jQuery("#user option:selected").html()+'"}]');
                }
               
               
                j=0;
                if(dataUsers != null){
                    loadUser(j, dataUsers);
                }else{
                    jQuery("#load").hide();
                }
            }
        }
    }

    function loadUser(j,dataUsers){
        var start_date =document.getElementById("start_date");
        var end_date = document.getElementById("end_date");
        start_date = textStartDate[2]+"/"+textStartDate[1]+"/"+textStartDate[0];
        end_date = textEndDate[2]+"/"+textEndDate[1]+"/"+textEndDate[0];
        if(j >= dataUsers.length){
            jQuery("#load").hide();
        }
        jQuery.ajax({
            url: "index.php?option=com_projectopen&c=report&task=getReportStaffing",
            global: false,
            type: "POST",
            data: ({start_date: start_date, end_date: end_date, id_user: dataUsers[j].id_user,index:(j+1),name_user: dataUsers[j].user_name}),
            //                        async: false,
            success: function(msg){
                var data = JSON.parse(msg);
                jQuery("#tableStaffing").append(data.html);
                dataExport.push(new Array(dataUsers[j].user_name, data.array));
                if(j< dataUsers.length){
                    loadUser(j+1, dataUsers);
                }
            }
        }); 
    }

    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportPhpexcel"){
            document.getElementById("datos").value = JSON.stringify(dataExport);
            var curdate = new Date();
            var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
            var headers = new Object(); 
            headers.user = ((jQuery("#user").val() != 0) ? jQuery("#user option:selected").html(): "");
            headers.start = (jQuery("#start_date").val() != "") ? jQuery("#start_date").val() : "";
            headers.end = (jQuery("#end_date").val() != "") ? jQuery("#end_date").val() : "";
            headers.office = (jQuery("#office").val() != 0) ? jQuery("#office option:selected").html() : "";

            headers.toJSON = function(key)
            {
                var replacement = new Object();
                for (var val in this)
                {
                    if (typeof (this[val]) === 'string')
                        replacement[val] = this[val].toUpperCase();
                    else
                        replacement[val] = this[val]
                }
                return replacement;
            };

            var jsonText = JSON.stringify(headers);
            jQuery("#weeks").val(JSON.stringify(arrWeek));
            jQuery("#headers").val(jsonText);
            
            dataExport=new Array();
            arrWeek = new Array();
           
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>