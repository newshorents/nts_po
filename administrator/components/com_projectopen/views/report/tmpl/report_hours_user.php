<?php defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar'); ?>
<!--Reporte de imputacion de horas por usuario -->
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="user"><?php echo JText::_('User'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->userNewshore, 'user', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?></td>
        </tr>
        <tr>
            <td  class="key">
                <label for="start_date"><?php echo JText::_('Start Date'); ?></label>
            </td>
            <td>
                <input id='start_date' name='start_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("start_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>
        <tr>
            <td  class="key">
                <label for="end_date"><?php echo JText::_('End Date'); ?></label>
            </td>
            <td>
                <input id='end_date' name='end_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("end_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>        
    </table>
    <br>
    <br>
    
    <div id="usersHours"></div>

    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <input type="hidden" id="datosDel" name="datosDel" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    jQuery.noConflict();

    window.onload= function(){
        //document.getElementById("toolbar-send").style.display = 'none';
    }

    var dataUsers;

    function searchUsers(){
        var tableUsers = document.getElementById("usersHours");
        tableUsers.innerHTML = '';
        //document.getElementById("toolbar-send").style.display = 'none';
        
        var user = document.getElementById("user");
        var start_date =document.getElementById("start_date");
        var end_date = document.getElementById("end_date");
        var con = false;
        if(user.options[user.selectedIndex].value == 0){
            if(start_date.value=="" || end_date.value==""){
                alert('<?php echo JText::_('Selected the search dates'); ?>');
            }else {
                var textStartDate = start_date.value.split("-");
                var textEndDate = end_date.value.split("-");
                start_date = textStartDate[2]+"/"+textStartDate[1]+"/"+textStartDate[0];
                end_date = textEndDate[2]+"/"+textEndDate[1]+"/"+textEndDate[0];
                var dstart_date = new Date(start_date);
                var dend_date = new Date(end_date);
                var dcurrent_date = new Date(getDateCurrent());
                if(dend_date.valueOf()<dstart_date.valueOf()){
                    alert('<?php echo JText::_('Start date must be less or equal than the end date'); ?>');
                    con = true;
                }else if(dend_date.valueOf() > dcurrent_date.valueOf()){
                    alert('<?php echo JText::_('End date must be less than or equal to the current date'); ?>');
                    con = true;
                }
                
                var dataRes = jQuery.ajax({
                    url: "index.php?option=com_projectopen&c=report&task=getUserImputeHoursUser",
                    global: false,
                    type: "POST",
                    data: ({userId: 0, start_date: start_date, end_date: end_date}),
                    async: false,
                    success: function(msg){
                        //alert(msg);
                    }
                }).responseText;
                var hoursUsers = JSON.parse(dataRes);
                drawTableUsersAll(hoursUsers);
            }
        }else{
            var idUser = user.options[user.selectedIndex].value;
            if(start_date.value!="" || end_date.value!=""){
                var textStartDate = start_date.value.split("-");
                var textEndDate = end_date.value.split("-");
                start_date = textStartDate[2]+"/"+textStartDate[1]+"/"+textStartDate[0];
                end_date = textEndDate[2]+"/"+textEndDate[1]+"/"+textEndDate[0];
                var dstart_date = new Date(start_date);
                var dend_date = new Date(end_date);
                var dcurrent_date = new Date(getDateCurrent());
                if(dend_date.valueOf()<dstart_date.valueOf()){
                    alert('<?php echo JText::_('Start date must be less or equal than the end date'); ?>');
                    con = true;
                }else if(dend_date.valueOf() > dcurrent_date.valueOf()){
                    alert('<?php echo JText::_('End date must be less than or equal to the current date'); ?>');
                    con = true;
                }
            }else {
                start_date = "";
                end_date = "";
            }
            if(!con) {
                var argsArray = new Array();
                argsArray.push(newArg("userId", idUser));
                argsArray.push(newArg("start_date", start_date));
                argsArray.push(newArg("end_date", end_date));
                argsArray.push(newArg("c", "report"));
                argsArray.push(newArg("option", "com_projectopen"));
                argsArray.push(newArg("task", "getUserImputeHoursUser"));
                var argsStr = argsArray.join("&");
                sendRequest("index.php", argsStr, drawTableUsers);
            }
        }
    }

    //Report Impute
    function drawTableUsers(){
        if (isValidResponse(this)) {
            var res = this.responseText;
            if (res != "") {
                dataUsers = JSON.parse(res);
                var tableUsers = document.getElementById("usersHours");
                var k = 0;
                var htmlv="<table border=1 class='adminlist' id='hoursUser' >";
                htmlv +="<tr>";
                htmlv +="<th align='center' ><?php echo JText::_('Name Project/Ausence'); ?></th>";
                htmlv +="<th align='center' ><?php echo JText::_('Number of hours'); ?></th></tr>";
                if(dataUsers != null){
                    if(dataUsers.length != 0) {
                        var user = document.getElementById("user");
                        var nameUser = user.options[user.selectedIndex].text;
                        for(var i=0; i<dataUsers.length; i++){
                            htmlv += "<tr class='row"+k+"' id='"+dataUsers[i].id_project+"'>";
                            htmlv += "<td align='center'>"+dataUsers[i].name+"</td>";
                            htmlv += "<td align='center'>"+colocarPuntosComa2(dataUsers[i].total_hours)+"</td>";
                            htmlv += "</tr>";
                            k = 1 - k;
                        }
                        htmlv += "</table>";
                        tableUsers.innerHTML = "<h2>"+nameUser+"</h2><br>"+htmlv;
                        
                    }else{
                        alert('<?php echo JText::_('The search had no results'); ?>');
                    }
                }else {
                    alert('<?php echo JText::_('Must select a user to continue the search'); ?>');
                }
            }
        }
    }
    
    function drawTableUsersAll(dataUsersAll){
        var tableUsers = document.getElementById("usersHours");
        var k = 0;
        var idUser = 0;
        var sumHours = 0;
        var count = 0;
        var userNameTemp = "";
        var totalHoursTemp = "";
        var htmlv="<table border=1 class='adminlist' id='hoursUser' >";
        htmlv +="<tr>";
        htmlv +="<th align='center' ><?php echo JText::_('User'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Name Project/Ausence'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Number of hours'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Total hours'); ?></th></tr>";
        
        if(dataUsersAll.length != 0) {
            for(var i=0; i<dataUsersAll.length; i++){
                if(dataUsersAll[i].id_user != idUser) {
                    if(i!=0) {
                        htmlv = htmlv.replace(userNameTemp, userNameTemp.replace("<td", "<td rowspan='"+count+"'"));
                        htmlv = htmlv.replace(totalHoursTemp, totalHoursTemp.replace("0", sumHours).replace("<td", "<td rowspan='"+count+"'"));
                        htmlv += "</tr>";
                        sumHours = 0;
                        count = 0;
                        userNameTemp = "";
                        totalHoursTemp = "";
                        k = 1 - k;
                    }
                    idUser = dataUsersAll[i].id_user;
                }
                htmlv += "<tr class='row"+k+"' id='"+dataUsersAll[i].id_user+"'>";
                if(userNameTemp == "") {
                    htmlv += "<td align='center'>"+dataUsersAll[i].user_name+"</td>";
                    userNameTemp = "<td align='center'>"+dataUsersAll[i].user_name+"</td>";
                }
                htmlv += "<td align='center'>"+dataUsersAll[i].name+"</td>";
                htmlv += "<td align='center'>"+colocarPuntosComa2(parseFloat(dataUsersAll[i].total_hours))+"</td>";
                if(totalHoursTemp == "") {
                    htmlv += "<td align='center'>0</td>";
                    totalHoursTemp = "<td align='center'>0</td>";
                }
                
                sumHours = sumHours + parseFloat(dataUsersAll[i].total_hours);
                count++;
            }
            if(i!=0) {
                htmlv = htmlv.replace(userNameTemp, userNameTemp.replace("<td", "<td rowspan='"+count+"'"));
                htmlv = htmlv.replace(totalHoursTemp, totalHoursTemp.replace("0", sumHours).replace("<td", "<td rowspan='"+count+"'"));
                htmlv += "</tr>";
                sumHours = 0;
                count = 0;
                userNameTemp = "";
                totalHoursTemp = "";
                k = 1 - k;
            }
            htmlv += "</table>";
            tableUsers.innerHTML = htmlv;

        }else{
            alert('<?php echo JText::_('The search had no results'); ?>');
        }
    }
    
    function dateReport(start_date, end_date) {
        var curdate = new Date();
        var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        var htmlDate = "<div><h2>Imputacion de horas por usuario</h2><br/><table>";
        htmlDate += "<tr><td><strong>Usuario: </strong></td>";
        htmlDate += "<td>"+jQuery("#user option:selected").html()+"</td></tr>";
        htmlDate += "<tr><td><strong>Fecha de inicio: </strong></td>";
        htmlDate += "<td>"+start_date+"</td></tr>";
        htmlDate += "<tr><td><strong>Fecha de fin: </strong></td>";
        htmlDate += "<td>"+end_date+"</td></tr>";
        htmlDate += "<tr><td><strong>Generado el: </strong></td>";
        htmlDate += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
        htmlDate += "<tr><td></td><td></td></tr>";
        htmlDate += "</table><div><br/>";
        return htmlDate;
    }
    
    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportsreport"){
            document.getElementById("datos").value = document.getElementById("usersHours").innerHTML;
            var start_date =document.getElementById("start_date").value;
            var end_date = document.getElementById("end_date").value;
            if(start_date != "" || end_date != "") {
                document.getElementById("datosAdd").value = dateReport(start_date, end_date);
                document.getElementById("datosDel").value = "<h2>"+jQuery("#user option:selected").html()+"</h2>";
            }
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>