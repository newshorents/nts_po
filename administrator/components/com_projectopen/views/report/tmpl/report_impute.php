<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>
<!--Reporte de horas pendientes-->
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">

    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="client"><?php echo JText::_('Client'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->clients, 'client', 'class="inputbox" size="1" style="width:200px" onchange="changeProjects();"', 'id', 'name', 0); ?></td>
            <td  class="key"><label for="project" id="projects"><?php echo JText::_('Project'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->projects, 'project', 'class="inputbox" size="1" style="width:200px"', 'id_project', 'clientProject', 0); ?></td>
        </tr>
        <tr>
            <td class="key">
                <label for="start_date"><?php echo JText::_('Start Date'); ?> <label style='color:#FF0000' >*</label></label>
            </td>
            <td>
                <input id='start_date' name='start_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("start_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>
        <tr>
            <td class="key">
                <label for="end_date"><?php echo JText::_('End Date'); ?> <label style='color:#FF0000' >*</label></label>
            </td>
            <td>
                <input id='end_date' name='end_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("end_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>
        <tr>
            <td class="key">
                <label for="state"><?php echo JText::_('State'); ?></label>
            </td>
            <td>
                <select id="state" name="state">
                    <option value="0"><?php echo JText::_('All'); ?></option>
                    <option value="-1"><?php echo JText::_('Not Charged'); ?></option>
                    <option value="1"><?php echo JText::_('Pending'); ?></option>
                    <option value="2"><?php echo JText::_('Rejected'); ?></option>
                </select>
            </td>
        </tr>
    </table>
    <br>
    <br>

    <div id="tableUsersDiv">
        <br/>
        <table border="1" class="adminlist" id="tableUsers" style="display:none">
            <thead>
                <tr>
                    <th width="5%">
                        <?php echo JText::_('NUM'); ?>
                    </th>
                    <th width="25%"  nowrap="nowrap">
                        <?php echo JText::_('Usuario'); ?>
                    </th>
                    <th width="25%"  nowrap="nowrap">
                        <?php echo JText::_('Project'); ?>
                    </th>
                    <th width="30%" nowrap="nowrap">
                        <?php echo JText::_('Dates without impute 8 hours'); ?>
                    </th>
                    <th width="15%" nowrap="nowrap">
                        <?php echo JText::_('State'); ?>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
    <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    jQuery.noConflict();
    var headertable='<thead><tr><th width="5%"><?php echo JText::_('NUM'); ?></th><th width="25%"  nowrap="nowrap"><?php echo JText::_('Usuario'); ?></th><th width="25%"  nowrap="nowrap"><?php echo JText::_('Project'); ?></th><th width="30%" nowrap="nowrap"><?php echo JText::_('Dates without impute 8 hours'); ?></th><th width="15%" nowrap="nowrap"><?php echo JText::_('State'); ?></th></tr></thead>';
    window.onload= function(){
        addOptionClient();
        addOptionProject();
        document.getElementById("toolbar-send").style.display = 'none';
    }

    function addOptionClient(){
        //All Clients
        var selectClient = document.getElementById("client");
        var newOption = document.createElement('option');
        newOption.text = "<?php echo JText::_('All Clients'); ?>";
        newOption.value = 0;
        var elOptOld = selectClient.options[selectClient.selectedIndex];
        newOption.selected =true;
        try {
            selectClient.add(newOption, elOptOld); // standards compliant; doesn't work in IE
        }
        catch(ex) {
            selectClient.add(newOption); // IE only
        }
    }

    function addOptionProject(){
        //All Projects
        var selectProject = document.getElementById("project");
        var newOption = document.createElement('option');
        newOption.text = "<?php echo JText::_('All Projects'); ?>";
        newOption.value = 0;
        var elOptOld = selectProject.options[selectProject.selectedIndex];
        newOption.selected =true;
        try {
            selectProject.add(newOption, elOptOld); // standards compliant; doesn't work in IE
        }
        catch(ex) {
            selectProject.add(newOption); // IE only
        }
    }

    function drawSelectedProject(){
        if (isValidResponse(this)) {
            var res = this.responseText;
            if (res != "") {
                var dataProjects = res;
                var selectProject = document.getElementById("project");
                var selectClient = document.getElementById("client")
                var clientId = selectClient.options[selectClient.selectedIndex].value;
                lengthSelect = selectProject.length;
                if(dataProjects=='[]'){
                    alert('<?php echo JText::_('The client selected has no projects'); ?>')
                    for(var i=0;i<lengthSelect;i++){
                        selectProject.remove(0);
                    }
                }else{
                    dataProjects = JSON.parse(dataProjects);
                    var selectProject = document.getElementById("project");
                    for(var i=0;i<lengthSelect;i++){
                        selectProject.remove(0);
                    }
                    for(var i=0; i<dataProjects.length; i++){
                        var newOption = document.createElement('option');
                        if(clientId==0){
                            newOption.text = dataProjects[i].clientProject;
                        }else{
                            newOption.text = dataProjects[i].name_project;
                        }
                        newOption.value = dataProjects[i].id_project;
                        try {
                            selectProject.add(newOption, null); // standards compliant; doesn't work in IE
                        }
                        catch(ex) {
                            selectProject.add(newOption); // IE only
                        }
                    }
                    addOptionProject();
                }
            }
        }
    }

    var dataUsers;

    function searchUsers(){
        var tableUsers = document.getElementById("tableUsers");
        tableUsers.style.display = 'none';
        document.getElementById("toolbar-send").style.display = 'none';
        if(dataUsers!=null){
            if(dataUsers['id'] != undefined) {
                for(var i=0; i<dataUsers['id'].length; i++){
                    var tr= document.getElementById('tr'+dataUsers['id'][i]);
                    if(tr!=null){
                        var table = tr.parentNode;
                        table.removeChild(tr);
                    }
                }
            }
        }
        var selectProject = document.getElementById("project");
        var selectClient = document.getElementById("client");
        if(selectProject.length < 1){
            alert('<?php echo JText::_('The client selected has no projects'); ?>')
        }else{
            var idProject = selectProject.options[selectProject.selectedIndex].value;
            var idClient = selectClient.options[selectClient.selectedIndex].value;
            var start_date =document.getElementById("start_date");
            var end_date = document.getElementById("end_date");
            var state = document.getElementById("state").value;
            if(start_date.value=="" || end_date.value==""){
                alert('<?php echo JText::_('Selected the search dates'); ?>')
            }else{
                textStartDate = start_date.value.split("-");
                textEndDate = end_date.value.split("-");
                start_date = textStartDate[2]+"/"+textStartDate[1]+"/"+textStartDate[0];
                end_date = textEndDate[2]+"/"+textEndDate[1]+"/"+textEndDate[0];
                var dstart_date = new Date(start_date);
                var dend_date = new Date(end_date);
                var dcurrent_date = new Date(getDateCurrent());
                if(dend_date<dstart_date){
                    alert('<?php echo JText::_('Start date must be less or equal than the end date'); ?>');
                }else if(dend_date > dcurrent_date){
                    alert('<?php echo JText::_('End date must be less than or equal to the current date'); ?>');
                }else{
                    jQuery("#load").show();
                    jQuery("#headerReport").html("");
                    jQuery.ajax({
                        url: "index.php?option=com_projectopen&c=report&task=getUserWithoutImpute",
                        global: false,
                        type: "POST",
                        data: ({projectId: idProject, clientId: idClient, start_date: start_date, end_date: end_date, state: state}),
                        //                        async: false,
                        success: function(msg){
                            var dataUsersr = JSON.parse(msg);
                            console.log(dataUsersr);

                            jQuery("#tableUsers").html(headertable);
                            drawTableUsers(dataUsersr);
                            jQuery("#load").hide();
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                        }
                    });

                }
            }
        }
    }

    //Report Impute
    function drawTableUsers(dataUsers){
        var cantUser=0;
        var project = "";
        var projectTemp = "";
        var projectTemp2 = "";
        var usrTemp = "";
        var usrTemp2 = "";
        var countproj = 0;
        var textState = "";
        var tableUsers = document.getElementById("tableUsers");
        var htmlv="";
        if(dataUsers != null) {
            if(dataUsers['id'] != undefined) {
                for(var i=0; i<dataUsers['id'].length; i++){
                    if(dataUsers['dates'][i]!=""){
                        var dates = dataUsers['dates'][i].split(',');
                        var textDate='';
                        var cont=1;
                        for(var j=0; j< dates.length; j++){
                            var day = dates[j].split('-');
                            dates[j] = day[2]+"-"+day[1]+"-"+day[0];
                            if(cont%5==0){
                                textDate = textDate+dates[j]+"<br>";
                            }else{
                                textDate = textDate+dates[j]+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                            cont++;
                        }
                        if(i%2==0){
                            htmlv+="<tr class='row0' id='tr"+dataUsers['id'][i]+"'>";
                        }else{
                            htmlv+="<tr class='row1' id='tr"+dataUsers['id'][i]+"'>";
                        }

                        //                        htmlv += "<td>"+dataUsers['username'][i]+"</td>";
                        if(project != dataUsers['username'][i]) {

                            if(i!=0){
                                projectTemp2 = projectTemp.replace("<td", "<td rowspan='"+countproj+"' ");
                                usrTemp2 = usrTemp.replace("<td", "<td rowspan='"+countproj+"' ");
                                htmlv = htmlv.replace(projectTemp, projectTemp2);
                                htmlv = htmlv.replace(usrTemp, usrTemp2);
                                countproj = 0;
                            }
                            htmlv += "<td align='center'>"+(cantUser+1)+"</td>";
                            htmlv += "<td align='middle' >"+dataUsers['username'][i]+"</td>";
                            usrTemp = "<td align='center'>"+(cantUser+1)+"</td>";
                            projectTemp = "<td align='middle' >"+dataUsers['username'][i]+"</td>";
                            project = dataUsers['username'][i];
                            countproj++;
                            cantUser++;
                        }else {
                            countproj++;
                        }
                        htmlv += "<td>"+dataUsers['project'][i]+"</td>";
                        htmlv += "<td align='center'>"+textDate+"</td>";
                        if(dataUsers['state'][i] == 2) {
                            textState = "<?php echo JText::_('Rejected'); ?>";
                        }else if(dataUsers['state'][i] == 1){
                            textState = "<?php echo JText::_('Pending'); ?>";
                        }else {
                            textState = "<?php echo JText::_('Not Charged'); ?>";
                        }
                        htmlv += "<td align='center'>"+textState+"</td>";
                        htmlv += "</tr>";
                        //jQuery('#tableUsers tr:last').after(htmlv);
                    }
                }

                if(i!=0){
                    projectTemp2 = projectTemp.replace("<td", "<td rowspan='"+countproj+"' ");
                    htmlv = htmlv.replace(projectTemp, projectTemp2);
                    usrTemp2 = usrTemp.replace("<td", "<td rowspan='"+countproj+"' ");
                    htmlv = htmlv.replace(usrTemp, usrTemp2);
                    countproj = 0;
                }

            }
            if(cantUser>0){
                tableUsers.innerHTML +=htmlv;
                tableUsers.style.display= '';
                document.getElementById("toolbar-send").style.display = '';
            }else{
                alert('<?php echo JText::_('The search had no results'); ?>');
            }
        }else {
            alert('<?php echo JText::_('Must select a client or a project to continue the search'); ?>');
        }
    }

    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportpending"){
            document.getElementById("datos").value = document.getElementById("tableUsersDiv").innerHTML;

            var curdate = new Date();
            var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

            var htmlr = "<h2>Imputacion de horas pendientes</h2><br/><table>";
            htmlr += (jQuery('#project').val() != 0) ? '<tr><td><strong>Proyecto: </strong></td><td>' + jQuery('#project option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery('#client').val() != 0) ? '<tr><td><strong>Cliente: </strong></td><td>' +  jQuery('#client option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery('#start_date').val() != "") ? '<tr><td><strong>Fecha de inicio: </strong></td><td>' + jQuery('#start_date').val() + '</td></tr>' : "";
            htmlr += (jQuery('#end_date').val() != "") ? '<tr><td><strong>Fecha de fin: </strong></td><td>' + jQuery('#end_date').val() + '</td></tr>' : "";
            htmlr += (jQuery('#state').val() != 0) ? '<tr><td><strong>Estado: </strong></td><td>' + jQuery('#state option:selected').html() + '</td></tr>' : '<td class="key">Estado:</td><td>Todos</td></tr>';
            htmlr += "<tr><td><strong>Generado el: </strong></td>";
            htmlr += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
            htmlr += "<tr><td></td><td></td></tr>";
            htmlr += "</table>";
            jQuery("#datosAdd").val(htmlr);

            submitform(p);
        }else {
            sendMail();
        }
    }
</script>
