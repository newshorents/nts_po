<?php defined('_JEXEC') or die('Restricted access'); ?>
<form action="index.php" method="post" name="adminForm">

    <table class="adminlist">
    <thead>
        <tr>
            <th width="20">
                <?php echo JText::_( 'Dato' ); ?>
            </th>
            <th width="15%"  nowrap="nowrap">
                <?php echo JText::_( 'Cliente' ); ?>
            </th>
            <th width="15%" nowrap="nowrap">
                <?php echo JText::_( 'Proyecto' ); ?>
            </th>
            <th width="10%" nowrap="nowrap">
                <?php echo JText::_( 'Tarea' ); ?>
            </th>
			<th width="10%" nowrap="nowrap">
                <?php echo JText::_( 'Empleado' ); ?>
            </th>
			<th width="10%" nowrap="nowrap">
                <?php echo JText::_( 'Descripcion' ); ?>
            </th>
			<th width="10%" nowrap="nowrap">
                <?php echo JText::_( 'Horas' ); ?>
            </th>
			<th width="10%" nowrap="nowrap">
                <?php echo JText::_( 'Precio hora' ); ?>
            </th>
			<th width="10%" nowrap="nowrap">
                <?php echo JText::_( 'Importe' ); ?>
            </th>
			<th width="10%" nowrap="nowrap">
                <?php echo JText::_( 'Fecha' ); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="12">
                <?php //echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
    </tfoot>
    <tbody>
    <?php $i = 0; $k = 0; ?>
    <?php foreach ($this->reports as $report) : ?>
        <?php
            // Get the current iteration and set a few values
            $link     = 'index.php?option=com_projectopen&c=user&amp;task=editUser&amp;id='. $report->id;
        ?>
        <tr class="<?php echo "row". $k; ?>">
            <td align="center" width="30">
                <?php echo $this->pagination->limitstart + 1 + $i; ?>
            </td>
            
            <td>
            <span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit User Name' );?>::<?php echo $report->client; ?>">
                <a href="<?php echo $link; ?>">
                    <?php echo $report->client; ?></a></span>
            </td>
            <td>
                <?php  echo $report->proy;  ?>
            </td>           
            <td align="center">
                <?php echo $report->tarea; ?>
            </td>
			<td align="center">
                <?php echo $report->user; ?>
            </td>
			<td align="center">
                <?php echo $report->descrip; ?>
            </td>
			<td align="center">
                <?php echo number_format($report->hora, 2, ",", " "); ?>
            </td>
			<td align="center">
                <?php echo number_format($report->precio_hora, 2, ",", " "); ?>
            </td>
			<td align="center">
                <?php echo number_format(($report->precio_hora * $report->hora), 2, ",", " "); ?>
            </td>
			<td align="center">
                <?php echo $report->fecha; ?>
            </td>
           </tr>
        <?php $i++; $k = 1 - $k; ?>
    <?php endforeach; ?>
    </tbody>
    </table>

    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_( 'form.token' ); ?>
</form>
