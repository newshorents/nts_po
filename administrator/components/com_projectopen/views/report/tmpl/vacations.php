<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="client"><?php echo JText::_('Users'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->users, 'user', 'class="inputbox" size="1" style="width:200px"', 'id', 'name', 0); ?></td>
            <td  class="key"><label for="project" id="projects"><?php echo JText::_('Year'); ?></label></td>
            <td>
                <select name="year" id="year">
                    <option selected value="0">Todos los años</option>
                    <?php for ($x = 2008; $x <= (date("Y")); $x++) {
                        ?>
                        <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>  
    </table>
    <br>
    <br>
    <div id="tableUsersDiv">
        <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
        <table id="headerReport" border="0" cellspacing="0" class="headerreport">
        </table>
        <br>
        <table class="adminlist" id="tableVacations" style="display: none;">
        </table> 
    </div>
    <div id="aux"></div>
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    var headerTableFilter ='<tr class="titTr"><th><?php echo JText::_('Name user'); ?></th><th><?php echo JText::_('Year'); ?></th><th><?php echo JText::_('Total vacations'); ?><br><small>(Hasta la fecha)</small></th><th><?php echo JText::_('Total year'); ?></th><th><?php echo JText::_('Vacations taken'); ?></th><th><?php echo JText::_('Remaining vacations'); ?></th><th><?php echo JText::_('Total Remaining vacations'); ?></th></tr>';
    var headerTable ='<tr class="titTr"><th><?php echo JText::_('Name user'); ?></th><th><?php echo JText::_('Total vacations'); ?><br><small>(Hasta la fecha)</small></th><th><?php echo JText::_('Total year'); ?></th><th><?php echo JText::_('Vacations taken'); ?></th><th><?php echo JText::_('Remaining vacations'); ?></th></tr>';
    
    jQuery.noConflict();
    jQuery(document).ready(function(){
        var optionsc = jQuery("#user").html();
        optionsc = "<option value='0'><?php echo JText::_('All Users'); ?></option>"+optionsc;
        jQuery("#user").html(optionsc);   
        jQuery("#toolbar-send").css("display", "none");
    });
    
    function searchUsers(){
        var selectUser = jQuery("#user").val();
        var selectYear = jQuery("#year").val();
        jQuery("#load").show();
        var resultVacations = jQuery.ajax({
            url: "index.php?option=com_projectopen&c=report&task=getReportVacations",
            global: false,
            type: "POST",
            data: ({idUser:selectUser, year:selectYear}),
            success: function(msg){
                jQuery("#tableVacations").html("");
                jQuery("#tableVacations").show();
                if(selectUser != 0 || selectYear != 0){
                    jQuery("#tableVacations").html(headerTableFilter+msg);
                }else{
                    jQuery("#tableVacations").html(headerTable+msg);
                }
                jQuery("#load").hide();
                
                //                jQuery("#aux").append(msg);
            }
        }).responseText;
    }

    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportpending"){
            document.getElementById("datos").value = document.getElementById("tableUsersDiv").innerHTML;
            var curdate = new Date();
            var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
            var htmlr = "<h2>Vacaciones</h2><br/><table>";
            htmlr += (jQuery("#user").val() != 0) ? '<tr><td><strong>Usuario: </strong></td><td>' + jQuery('#user option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery("#year").val() != 0) ? '<tr><td><strong>Año: </strong></td><td>' + jQuery('#year option:selected').html() + '</td></tr>' : "";
            htmlr += "<tr><td><strong>Generado el: </strong></td>";
            htmlr += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
            htmlr += "</table>";
            htmlr += "<br>";
            jQuery("#datosAdd").val(htmlr);
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>