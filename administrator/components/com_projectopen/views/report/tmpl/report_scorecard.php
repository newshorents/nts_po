<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.mootools');
JHTML::_('behavior.calendar');
?>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery("#year, #scorecard_year").change(function(){
            var options = "<option selected value=''>Seleccione cliente</option>";
            jQuery.ajax({
                url: "index.php?option=com_projectopen&task=getClientsScorecardSelect&c=report",
                global: false,
                type: "POST",
                data: ({year: jQuery(this).val(), startmonth: jQuery("#start_month").val(), endmonth: jQuery("#end_month").val()}),
                success: function(res){
                    var resjson = JSON.parse(res);
                    jQuery.each(resjson, function (i, val){
                        options += '<option value="'+val.id_client+'">'+val.name_client+'</option>';
                    });
                    jQuery("#client").html(options);
                }
            });
        });

        jQuery("#client").change(function(){
            var options = "<option selected value=''>Seleccione proyecto</option>";
            jQuery.ajax({
                url: "index.php?option=com_projectopen&task=getProjectsClient&c=report",
                global: false,
                type: "POST",
                data: ({clientId: jQuery(this).val()}),
                success: function(res){
                    var resjson = JSON.parse(res);
                    jQuery.each(resjson, function (i, val){
                        options += '<option value="'+val.id_project+'">'+val.name_project+'</option>';
                    });
                    jQuery("#project").html(options);
                }
            });
        });
    });
    function submitbutton(pressbutton){
        if(pressbutton == 'reportScorecard'){
            var start_month = document.getElementById("start_month").value;
            var year = document.getElementById("year").value;
            var end_month = document.getElementById("end_month").value;
            var scorecard_year = document.getElementById("scorecard_year");
            if(scorecard_year.value == 0) {
                if(year==""){
                    alert(<?php echo '"' . JText::_('Enter a year') . '"'; ?>);
                }else if(year.length != 4){
                    alert(<?php echo '"' . JText::_('Enter a valid year') . '"'; ?>);
                }else {
                    submitform(pressbutton);
                }
            }else {
                var patron = /No cerrado/i;
                var state = scorecard_year.options[scorecard_year.selectedIndex].text;
                if(state.search(patron) == -1) {
                    document.getElementById("state").value = 1;
                }else {
                    document.getElementById("state").value = 0;
                }
                submitform(pressbutton);
            }

        }else{
            submitform(pressbutton);
        }
    }
</script>
<form action="index.php" method="post" name="adminForm">
    <fieldset class="adminform">
        <div>
            <div id="selectCuadro">
                <label><?php echo JText::_('Saved scorecard'); ?> <span style='color:#FF0000' >*</span></label>
                <?php echo JHTML::_('select.genericlist', $this->reports, 'scorecard_year', 'class="inputbox" size="1"', 'year', 'year_state', $this->reports[0]["year"]); ?>
            </div>
            <div id="calendars">
                <div id="divYear">
                    <label><?php echo JText::_('Year'); ?> <span style='color:#FF0000' >*</span></label>
                    <select name="year" id="year">
                        <option value="">Seleccione año</option>
                        <?php for ($i = 2011; $i <= date("Y"); $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
<!--                    <input class="inputbox" type="text" name="year" id="year" maxlength="4" onkeypress="return numeros(event)" value="" />-->
                </div>
                <div id="divMonth">
                    <label><?php echo JText::_('Start Month'); ?></label>
                    <select name="start_month" id="start_month" onchange="validMonthEnd()" >
                        <?php
                        for ($i = 0; $i < count($this->months); $i++) {
                            echo "<option value='" . $i . "'>" . $this->months[$i] . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div id="divMonth">
                    <label id="endMonth"><?php echo JText::_('End Month'); ?></label>
                    <select name="end_month" id="end_month" onchange="validMonthStart()" >
                        <?php
                        for ($i = 0; $i < count($this->months); $i++) {
                            echo "<option value='" . $i . "' ".(($i==(count($this->months)-1)) ? "selected" : "").">" . $this->months[$i] . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div id="divMonth">
                    <label for="client"><?php echo JText::_('Client'); ?></label>
                    <?php echo JHTML::_('select.genericlist', $this->clients, 'client', 'class="inputbox" size="1" style="width:200px" onchange="changeProjects();"', 'id', 'name', 0); ?>
                </div>
                <div id="divMonth">
                    <label for="project"><?php echo JText::_('Project'); ?></label>
                    <?php echo JHTML::_('select.genericlist', $this->clients, 'project', 'class="inputbox" size="1" style="width:200px" onchange="changeProjects();"', 'id', 'name', 0); ?>
                </div>

                <div id="divMonth">
                    <label for="country"><?php echo JText::_('Accounting country'); ?></label>
                    <?php echo JHTML::_('select.genericlist', $this->countries, 'country', 'class="inputbox" size="1" style="width:200px"', 'id', 'name', 0); ?>
                </div>

            </div>
        </div>
    </fieldset>

    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="task" value="viewScorecard" />
    <input type="hidden" name="state" id="state" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
