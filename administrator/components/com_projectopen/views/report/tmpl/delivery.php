<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="client"><?php echo JText::_('Client'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->clients, 'client', 'class="inputbox" size="1" style="width:200px"', 'id', 'name', 0); ?></td>
            <td  class="key"><label for="project" id="projects"><?php echo JText::_('Project'); ?></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->projects, 'project', 'class="inputbox" size="1" style="width:200px"', 'id_project', 'clientProject', 0); ?></td>
        </tr>  
    </table>
    <br>
    <br>
    <div id="tableUsersDiv">
        <table id="headerReport" border="0" cellspacing="0" class="headerreport">
        </table><br/>
        <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
        <table class="adminlist" id="tableDelivery" style="display: none;">
            <tr class="titTr">
                <th>Id</th>
                <th><?php echo JText::_('Client'); ?></th>
                <th><?php echo JText::_('Project'); ?></th>
                <th><?php echo JText::_('Duration'); ?></th>
                <th><?php echo JText::_('Remaining days'); ?></th>
                <th><?php echo JText::_('Amount2'); ?></th>
                <th><?php echo JText::_('Incomes'); ?></th>
                <th><?php echo JText::_('Expenses'); ?></th>
                <th><?php echo JText::_('Invoiced'); ?></th>
                <th><?php echo JText::_('Charged'); ?></th>
                <th><?php echo JText::_('Percent complete'); ?></th>
                <th><?php echo JText::_('Estimated margin'); ?></th>
            </tr>
        </table> 
    </div>
    <div id="aux"></div>
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    var headerTable ='<tr class="titTr"><th>Id</th><th><?php echo JText::_('Client'); ?></th><th><?php echo JText::_('Project'); ?></th><th><?php echo JText::_('Duration'); ?></th><th><?php echo JText::_('Remaining days'); ?></th><th><?php echo JText::_('Amount2'); ?></th><th><?php echo JText::_('Incomes'); ?></th><th><?php echo JText::_('Expenses'); ?></th><th><?php echo JText::_('Invoiced'); ?></th><th><?php echo JText::_('Charged'); ?></th><th><?php echo JText::_('Percent complete'); ?></th><th><?php echo JText::_('Estimated margin'); ?></th></tr>';
    
    jQuery.noConflict();
    jQuery(document).ready(function(){
        var optionsc = jQuery("#client").html();
        optionsc = "<option value='0'><?php echo JText::_('All Clients'); ?></option>"+optionsc;
        jQuery("#client").html(optionsc);
        
        var optionsp = jQuery("#project").html();
        optionsp = "<option value='0'><?php echo JText::_('Select a project'); ?></option>"+optionsp;
        jQuery("#project").html(optionsp);
        
        jQuery("#client").change(function(){
            var clientId = jQuery(this).val(); 
            var dataClient = jQuery.ajax({
                url: "index.php?option=com_projectopen&c=report&task=getProjectsClientCurrent",
                global: false,
                type: "POST",
                data: ({clientId: clientId}),
                async: false,
                success: function(msg){
                    //alert(msg);
                }
            }).responseText;

            var clients = JSON.parse(dataClient);
            var htmlA="<option value='0'>Seleccione un proyecto</option>";
            jQuery.each(clients, function (i, val){
                htmlA += '<option value="'+val.id_project+'">'+((clientId != 0)? val.name_project : val.clientProject)+'</option>';
            });
            jQuery("#project").html(htmlA)
            
        });
        
        jQuery("#toolbar-send").css("display", "none");
    });
    function searchUsers(){
        var selectProject = jQuery("#project").val();
        var selectClient = jQuery("#client").val();
        jQuery("#load").show();
        jQuery("#tableDelivery").html("");
        var resultUsers = jQuery.ajax({
            url: "index.php?option=com_projectopen&c=report&task=getReportDelivery",
            global: false,
            type: "POST",
            data: ({project:selectProject, client:selectClient}),
            success: function(msg){
                jQuery("#tableDelivery").show();
                jQuery("#tableDelivery").html(headerTable+msg);
                
                jQuery("#load").hide();
                //                jQuery("#aux").append(msg);
            }
        }).responseText;
    }

    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportpending"){
            document.getElementById("datos").value = document.getElementById("tableUsersDiv").innerHTML;
            var curdate = new Date();
            var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
            var htmlr = "<h2>Informe delivery</h2><br/><table>";
            htmlr += (jQuery("#client").val() != 0) ? '<tr><td><strong>Cliente: </strong></td><td>' + jQuery('#client option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery("#project").val() != 0) ? '<tr><td><strong>Proyecto: </strong></td><td>' + jQuery('#project option:selected').html() + '</td></tr>' : "";
            htmlr += "<tr><td><strong>Generado el: </strong></td>";
            htmlr += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
            htmlr += "</table>";
            htmlr += "<br>";
            jQuery("#datosAdd").val(htmlr);
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>