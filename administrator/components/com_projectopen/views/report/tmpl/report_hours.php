<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>
<!-- Reporte de imputacion de horas por cliente -->
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="client"><?php echo JText::_('Client'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->clients, 'client', 'class="inputbox" size="1" style="width:200px" onchange="changeProjects();"', 'id', 'name', 0); ?></td>
            <td  class="key"><label for="project" id="projects"><?php echo JText::_('Project'); ?></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->projects, 'project', 'class="inputbox" size="1" style="width:200px"', 'id_project', 'clientProject', 0); ?></td>
        </tr>
        <tr>
            <td  class="key">
                <label for="start_date"><?php echo JText::_('Start Date'); ?></label>
            </td>
            <td>
                <input id='start_date' name='start_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("start_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>
        <tr>
            <td  class="key">
                <label for="end_date"><?php echo JText::_('End Date'); ?></label>
            </td>
            <td>
                <input id='end_date' name='end_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("end_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>        
    </table>
    <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
    <br>
    <br>

    <br/>
    <div id="usersHours"></div>

    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    jQuery.noConflict();

    window.onload= function(){
        addOptionClient();
        //document.getElementById("toolbar-send").style.display = 'none';
    }

    function addOptionClient(){
        var selectClient = document.getElementById("client");
        var newOption = document.createElement('option');
        newOption.text = "<?php echo JText::_('All Clients'); ?>";
        newOption.value = 0;
        var elOptOld = selectClient.options[selectClient.selectedIndex];
        newOption.selected =true;
        try {
            selectClient.add(newOption, elOptOld); // standards compliant; doesn't work in IE
        }
        catch(ex) {
            selectClient.add(newOption); // IE only
        }
    }

    function drawSelectedProject(){
        document.getElementById("start_date").value = "";
        document.getElementById("end_date").value = "";
        if (isValidResponse(this)) {
            var res = this.responseText;
            if (res != "") {
                var dataProjects = res;
                var selectProject = document.getElementById("project");
                var selectClient = document.getElementById("client")
                var clientId = selectClient.options[selectClient.selectedIndex].value;
                var lengthSelect = selectProject.length;
                if(dataProjects=='[]'){
                    alert('<?php echo JText::_('The client selected has no projects'); ?>')
                    for(var i=0;i<lengthSelect;i++){
                        selectProject.remove(0);
                    }
                }else{
                    dataProjects = JSON.parse(dataProjects);
                    var selectProject = document.getElementById("project");
                    for(var i=0;i<lengthSelect;i++){
                        selectProject.remove(0);
                    }
                    for(var i=0; i<dataProjects.length; i++){
                        var newOption = document.createElement('option');
                        if(clientId==0){
                            newOption.text = dataProjects[i].clientProject;
                        }else{
                            newOption.text = dataProjects[i].name_project;
                        }
                        if(i==0) {
                            var newOption1 = document.createElement('option');
                            newOption1.text = "<?php echo JText::_('Select a project'); ?>";
                            newOption1.value = 0;
                            newOption1.selected =true;
                            try {
                                selectProject.add(newOption1, null); // standards compliant; doesn't work in IE
                            }
                            catch(ex) {
                                selectProject.add(newOption1); // IE only
                            }
                        }
                        newOption.value = dataProjects[i].id_project;
                        try {
                            selectProject.add(newOption, null); // standards compliant; doesn't work in IE
                        }
                        catch(ex) {
                            selectProject.add(newOption); // IE only
                        }
                    }
                }
            }
        }
    }

    var dataUsers;

    function searchUsers(){
        var tableUsers = document.getElementById("usersHours");
        tableUsers.innerHTML = '';
        //document.getElementById("toolbar-send").style.display = 'none';
        
        var selectProject = document.getElementById("project");
        var selectClient = document.getElementById("client");
        var con = false;
        if(selectProject.length < 1){
            alert('<?php echo JText::_('The client selected has no projects'); ?>');
        }else if(selectProject.value == 0 && selectClient.value == 0) {
            alert('<?php echo JText::_('Must select a client or a project to continue the search'); ?>');
        }else{
            var idProject = selectProject.options[selectProject.selectedIndex].value;
            var idClient = selectClient.options[selectClient.selectedIndex].value;
            var start_date =document.getElementById("start_date");
            var end_date = document.getElementById("end_date");
            
            if(start_date.value!="" || end_date.value!=""){
                var textStartDate = start_date.value.split("-");
                var textEndDate = end_date.value.split("-");
                start_date = textStartDate[2]+"/"+textStartDate[1]+"/"+textStartDate[0];
                end_date = textEndDate[2]+"/"+textEndDate[1]+"/"+textEndDate[0];
                var dstart_date = new Date(start_date);
                var dend_date = new Date(end_date);
                var dcurrent_date = new Date(getDateCurrent());
                if(dend_date<dstart_date){
                    alert('<?php echo JText::_('Start date must be less or equal than the end date'); ?>');
                    con = true;
                }else if(dend_date > dcurrent_date){
                    alert('<?php echo JText::_('End date must be less than or equal to the current date'); ?>');
                    con = true;
                }
                
            }else {
                start_date = "";
                end_date = "";
            }
            if(!con) {
                jQuery("#load").show();
                jQuery("#headerReport").html("");
                jQuery.ajax({
                    url: "index.php?option=com_projectopen&c=report&task=getUserImputeHours",
                    global: false,
                    type: "POST",
                    data: ({projectId: idProject, clientId: idClient, start_date: start_date, end_date: end_date}),
                    //                        async: false,
                    success: function(msg){
                        var dataUsersr = JSON.parse(msg);
                        //                        jQuery("#tableUsers").html(headertable);
                        drawTableUsers(dataUsersr);
                        
                        jQuery("#load").hide();
                    }
                });
            }
        }
    }

    //Report Impute
    function drawTableUsers(dataUsers){
        var conver;
        var tableUsers = document.getElementById("usersHours");
        var idProject = 0;
        var idUser = 0;
        var k = 0;
        var countusu = 1;
        var countpro = 1;
        var sumHourpro = new Array();
        var sumCostpro = new Array();
        var sumTariffpro = new Array();
        var sumHour = 0;
        var sumTotal = "";
        var costTotal = "";
        var tariffTotal = "";
        var costHours = "";
        var tariffHours = "";
        var totalCostHour = 0;
        var totalTariffHour = 0;
        var totalHours = 0;
        var totalHourstxt = '';
        var htmlv="<table border=1 class='adminlist' id='hoursUserProject' >";
        htmlv +="<tr><th align='center' ><?php echo JText::_('NUM'); ?></th>";
        htmlv +="<th align='center' width=300 ><?php echo JText::_('Name User'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Name Project'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Number of hours'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Cost hour'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Total cost'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('Sales hour'); ?></th>";
        htmlv +="<th align='center' ><?php echo JText::_('total sales'); ?></th></tr>";
        var idTabla = "hoursUserProject";
        if(dataUsers != null){
            if(dataUsers.length != 0) {
                        
                //Tabla de conversion para los valores que estan en pesos
                conver = jQuery.ajax({
                    url: "index.php?option=com_projectopen&c=report&task=getConversionReport",
                    global: false,
                    type: "POST",
                    data: ({year_ini: dataUsers[0].year, year_fin: dataUsers[(dataUsers.length-1)].year}),
                    async: false,
                    success: function(msg){
                        //alert(msg);
                    }
                }).responseText;
                conver = JSON.parse(conver);

                var conversion_currency = {};
                for (var i = 0; i < conver.length; i++) {
                    if(!conversion_currency.hasOwnProperty(conver[i].id_currency)) {
                        conversion_currency[conver[i].id_currency] = [];
                    }
                    conversion_currency[conver[i].id_currency].push(conver[i]);
                }
                console.log(conversion_currency);

                for(var i=0; i<dataUsers.length; i++){
                    if(idProject != dataUsers[i].id_project) {
                        if(i != 0){
                            htmlv = htmlv.replace(sumTotal, "<td align='center' ><u>"+colocarPuntosComa2(sumHourpro[countpro])+"</u></td>");
                            htmlv = htmlv.replace(costTotal, "<td colspan=2 align='center' ><u>"+colocarPuntosComa2(sumCostpro[countpro])+"</u></td>");
                            htmlv = htmlv.replace(tariffTotal, "<td colspan=2 align='center' ><u>"+colocarPuntosComa2(sumTariffpro[countpro])+"</u></td>");
                        }
                        htmlv += "<tr onclick='desHourUser("+dataUsers[i].id_project+", \""+idTabla+"\")' class='row"+(1-k)+"' id='"+dataUsers[i].id_project+"'>";
                        htmlv += "<td><img id='imgDes' src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>'><u>"+countpro+"</u></td>";
                        htmlv += "<td colspan=2 align='center' ><u>"+dataUsers[i].project_name+"</u></td>";
                        htmlv += "<td id="+countpro+" align='center' >0</td>";
                        htmlv += "<td colspan=2 id='"+countpro+"c' align='center' >0</td>";
                        htmlv += "<td colspan=2 id='"+countpro+"t' align='center' >0</td></tr>";
                        sumTotal = "<td id="+countpro+" align='center' >0</td>";
                        costTotal = "<td colspan=2 id='"+countpro+"c' align='center' >0</td>";
                        tariffTotal = "<td colspan=2 id='"+countpro+"t' align='center' >0</td>";
                        idProject = dataUsers[i].id_project;
                        k = 1 - k;
                        countpro++;
                        countusu = 1;
                        sumHourpro[countpro] = 0;
                        sumCostpro[countpro] = 0;
                        sumTariffpro[countpro] = 0;
                        sumHour = 0;
                    }
                            
                    var conversion_value = 1;
                    if(dataUsers[i].id_currency > 1) {
                        conver = conversion_currency[dataUsers[i].id_currency];
                        for(var w=0; w<conver.length; w++){
                            if(conver[w].year < dataUsers[i].year) {
                                if(parseInt(conver[w].month) < parseInt(dataUsers[i].month)) {
                                    conversion_value = parseFloat(conver[w].conversion_value);
                                }else if(parseInt(conver[w].month) == parseInt(dataUsers[i].month)) {
                                    conversion_value = parseFloat(conver[w].conversion_value);
                                }
                            }else if(parseInt(conver[w].year) == parseInt(dataUsers[i].year)) {
                                if(parseInt(conver[w].month) < parseInt(dataUsers[i].month)) {
                                    conversion_value = parseFloat(conver[w].conversion_value);
                                }else if(parseInt(conver[w].month) == parseInt(dataUsers[i].month)) {
                                    conversion_value = parseFloat(conver[w].conversion_value);
                                }
                            }
                        }
                    }
                    if(i != 0) {
                        if(costHours == "") {
                            costHours += "</br>"+colocarPuntosComa2((parseFloat(dataUsers[i].cost_hour)/parseFloat(conversion_value)));
                        }else {
                            costHours += ((((parseFloat(dataUsers[i].cost_hour)/parseFloat(conversion_value))) != ((parseFloat(dataUsers[(i-1)].cost_hour)/parseFloat(conversion_value)))) ? "</br>"+colocarPuntosComa2((parseFloat(dataUsers[i].cost_hour)/parseFloat(conversion_value))) : "");
                        }
                    }else {
                        costHours += colocarPuntosComa2((parseFloat(dataUsers[i].cost_hour)/parseFloat(conversion_value)));
                    }
                    totalCostHour = totalCostHour + ((parseFloat(dataUsers[i].hour_total)*(parseFloat(dataUsers[i].cost_hour)/parseFloat(conversion_value))));

                    conversion_value = 1;
                    if(dataUsers[i].id_currency_sales > 1) {
                        var conversion_value = 0;
                        conver = conversion_currency[dataUsers[i].id_currency];
                        for(var k=0; k<conver.length; k++){
                            if(parseInt(conver[k].year) < parseInt(dataUsers[i].year)) {
                                if(parseInt(conver[k].month) < parseInt(dataUsers[i].month)) {
                                    conversion_value = parseInt(conver[k].conversion_value);
                                }else if(conver[k].month == dataUsers[i].month) {
                                    conversion_value = parseInt(conver[k].conversion_value);
                                }
                            }else if(parseInt(conver[k].year) == parseInt(dataUsers[i].year)) {
                                if(parseInt(conver[k].month) < parseInt(dataUsers[i].month)) {
                                    conversion_value = parseInt(conver[k].conversion_value);
                                }else if(parseInt(conver[k].month) == parseInt(dataUsers[i].month)) {
                                    conversion_value = parseInt(conver[k].conversion_value);
                                }
                            }
                        }
                    }
                    if(i != 0) {
                        if(tariffHours == ""){
                            tariffHours += "</br>"+colocarPuntosComa2((parseFloat(dataUsers[i].tariff_hour)/parseFloat(conversion_value)));
                        }else {
                            tariffHours += ((((parseFloat(dataUsers[i].tariff_hour)/parseFloat(conversion_value))) != ((parseFloat(dataUsers[(i-1)].tariff_hour)/parseFloat(conversion_value)))) ? "</br>"+colocarPuntosComa2((parseFloat(dataUsers[i].tariff_hour)/parseFloat(conversion_value))) : ""); 
                        }
                    }else {
                        tariffHours += colocarPuntosComa2((parseFloat(dataUsers[i].tariff_hour)/parseFloat(conversion_value)));
                    }
                    totalTariffHour = totalTariffHour + ((parseFloat(dataUsers[i].hour_total)*(parseFloat(dataUsers[i].tariff_hour)/parseFloat(conversion_value))));
                    totalHours = totalHours + parseFloat(dataUsers[i].hour_total);
                    if(i != 0) {
                        totalHourstxt += "</br>" + colocarPuntosComa2(parseFloat(dataUsers[i].hour_total));    
                    }else{
                        totalHourstxt = colocarPuntosComa2(parseFloat(dataUsers[i].hour_total));
                    }
                      
                    sumHour = sumHour + parseFloat(dataUsers[i].hour_total);
                    sumHourpro[countpro] = sumHourpro[countpro] + parseFloat(dataUsers[i].hour_total);
                            
                    if(i != (dataUsers.length-1)) {
                        if(dataUsers[i].id_user != dataUsers[(i+1)].id_user){
                            htmlv += "<tr class='row"+k+"' id='"+dataUsers[i].id_user+"' style='display:none'>";
                            htmlv += "<td align='center'>"+countusu+"</td>";
                            htmlv += "<td align='center'>"+dataUsers[i].user_name+"</td>";
                            htmlv += "<td align='center'>"+dataUsers[i].project_name+"</td>";
                            htmlv += "<td align='center'>"+totalHourstxt+"</td>";
                            htmlv += "<td align='center'>"+(costHours)+"</td>";
                            htmlv += "<td align='center'>"+colocarPuntosComa2(totalCostHour)+"</td>";
                            htmlv += "<td align='center'>"+(tariffHours)+"</td>";
                            htmlv += "<td align='center'>"+colocarPuntosComa2(totalTariffHour)+"</td>";
                            htmlv += "</tr>";
                                    
                            sumCostpro[countpro] = sumCostpro[countpro] + totalCostHour;
                            sumTariffpro[countpro] = sumTariffpro[countpro] + totalTariffHour;
                            countusu++;
                                    
                            costHours = "";
                            tariffHours = "";
                            totalCostHour = 0;
                            totalTariffHour = 0;
                            totalHours = 0;
                            totalHourstxt = '';
                        }else if(dataUsers[i].id_project != dataUsers[(i+1)].id_project){
                            htmlv += "<tr class='row"+k+"' id='"+dataUsers[i].id_user+"' style='display:none'>";
                            htmlv += "<td align='center'>"+countusu+"</td>";
                            htmlv += "<td align='center'>"+dataUsers[i].user_name+"</td>";
                            htmlv += "<td align='center'>"+dataUsers[i].project_name+"</td>";
                            htmlv += "<td align='center'>"+totalHourstxt+"</td>";
                            htmlv += "<td align='center'>"+(costHours)+"</td>";
                            htmlv += "<td align='center'>"+colocarPuntosComa2(totalCostHour)+"</td>";
                            htmlv += "<td align='center'>"+(tariffHours)+"</td>";
                            htmlv += "<td align='center'>"+colocarPuntosComa2(totalTariffHour)+"</td>";
                            htmlv += "</tr>";
                                    
                            sumCostpro[countpro] = sumCostpro[countpro] + totalCostHour;
                            sumTariffpro[countpro] = sumTariffpro[countpro] + totalTariffHour;
                            countusu++;
                                    
                            costHours = "";
                            tariffHours = "";
                            totalCostHour = 0;
                            totalTariffHour = 0;
                            totalHours = 0;
                            totalHourstxt = '';
                        }
                    }else {
                        htmlv += "<tr class='row"+k+"' id='"+dataUsers[i].id_user+"' style='display:none'>";
                        htmlv += "<td align='center'>"+countusu+"</td>";
                        htmlv += "<td align='center'>"+dataUsers[i].user_name+"</td>";
                        htmlv += "<td align='center'>"+dataUsers[i].project_name+"</td>";
                        htmlv += "<td align='center'>"+totalHourstxt+"</td>";
                        htmlv += "<td align='center'>"+(costHours)+"</td>";
                        htmlv += "<td align='center'>"+colocarPuntosComa2(totalCostHour)+"</td>";
                        htmlv += "<td align='center'>"+(tariffHours)+"</td>";
                        htmlv += "<td align='center'>"+colocarPuntosComa2(totalTariffHour)+"</td>";
                        htmlv += "</tr>";
                                    
                        sumCostpro[countpro] = sumCostpro[countpro] + totalCostHour;
                        sumTariffpro[countpro] = sumTariffpro[countpro] + totalTariffHour;
                        countusu++;
                                    
                        costHours = "";
                        tariffHours = "";
                        totalCostHour = 0;
                        totalTariffHour = 0;
                        totalHours = 0;
                        totalHourstxt = '';
                    }
                }
                if(i != 0){
                    htmlv = htmlv.replace(sumTotal, "<td align='center' ><u>"+colocarPuntosComa2(sumHourpro[countpro])+"</u></td>");
                    htmlv = htmlv.replace(costTotal, "<td colspan=2 align='center' ><u>"+colocarPuntosComa2(sumCostpro[countpro])+"</u></td>");
                    htmlv = htmlv.replace(tariffTotal, "<td colspan=2 align='center' ><u>"+colocarPuntosComa2(sumTariffpro[countpro])+"</u></td>");
                }
                htmlv += "</table>";
               
                tableUsers.innerHTML = htmlv;
            }else{
                alert('<?php echo JText::_('The search had no results'); ?>');
            }
        }else {
            alert('<?php echo JText::_('The search had no results'); ?>');
        }
    }
    
    function desHourUser(idProject, tablaid){
        var tabla = document.getElementById(tablaid);
        var rows = tabla.rows.length;
        var cols = tabla.rows[0].cells.length;

        for(var i=1; i<rows; i++){
            if(tabla.rows[i].onclick!=null && tabla.rows[i].id == idProject){
                var image= tabla.rows[i].cells[0].getElementsByTagName('img')[0];
                var j=i+1;
                while(tabla.rows[j].onclick == null){
                    if(tabla.rows[j].style.display=='none'){
                        tabla.rows[j].style.display='';
                        image.src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow_down.png' ?>';
                    }else{
                        tabla.rows[j].style.display='none';
                        image.src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>';
                    }
                    j++;
                    if(tabla.rows[j]==undefined){
                        break;
                    }
                }
                i=j-1;
            }
        }
    }

    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportsreport"){
            document.getElementById("datos").value = document.getElementById("usersHours").innerHTML;
            var curdate = new Date();
            var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
            var htmlr = "<h2>Imputacion de horas por cliente</h2><br/><table>";
            htmlr += (jQuery('#project').val() != 0) ? '<tr><td><strong>Proyecto: </strong></td><td>' + jQuery('#project option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery('#client').val()  != 0) ? '<tr><td><strong>Cliente: </strong></td><td>' +  jQuery('#client option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery('#start_date').val() != "") ? '<tr><td><strong>Fecha de inicio: </strong></td><td>' +  jQuery('#start_date').val() + '</td></tr>' : "";
            htmlr += (jQuery('#end_date').val() != "") ? '<tr><td><strong>Fecha de fin: </strong></td><td>' + jQuery('#end_date').val() + '</td></tr>' : "";
            htmlr += "<tr><td><strong>Generado el: </strong></td>";
            htmlr += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
            htmlr += "<tr><td></td><td></td></tr>";
            htmlr += "</table>";
            jQuery("#datosAdd").val(htmlr);
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>