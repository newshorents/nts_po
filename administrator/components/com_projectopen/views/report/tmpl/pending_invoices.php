<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="client"><?php echo JText::_('Client'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->clients, 'client', 'class="inputbox" size="1" style="width:200px"', 'id', 'name', 0); ?></td>
            <td  class="key"><label for="project" id="projects"><?php echo JText::_('Project'); ?></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->projects, 'project', 'class="inputbox" size="1" style="width:200px"', 'id_project', 'clientProject', 0); ?></td>
            <td  class="key"><label for="type" id=""><?php echo JText::_('Type'); ?></label></td>
            <td>
                <select id="type" name="type" class="inputbox" size="1" style="width:200px">
                    <option value="0"><?php echo JText::_('Select type report'); ?></option>
                    <option value="1"><?php echo JText::_('Report pending for invoice'); ?></option>
                    <option value="2"><?php echo JText::_('Report pending invoices to expire'); ?></option>
                </select>
            </td>
        </tr>  
    </table>
    <br>
    <br>
    <div id="tableUsersDiv">
        <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
        <table id="headerReport" border="0" cellspacing="0" class="headerreport">
        </table>
        <br>
        <table class="adminlist" id="tableInvoice" style="display: none;">
            <tr>
                <th><?php echo JText::_('Client'); ?></th>
                <th><?php echo JText::_('Project'); ?></th>
                <th><?php echo JText::_('Number'); ?></th>
                <th><?php echo JText::_('Description'); ?></th>
                <th><?php echo JText::_('Date'); ?></th>
                <th><?php echo JText::_('Amount2'); ?></th>
                <th><?php echo JText::_('Expiration'); ?></th>
<!--                <th><?php //echo JText::_('Remaining days');    ?></th>-->
                <th><?php echo JText::_('State'); ?></th>
            </tr>
        </table> 
    </div>
    <div id="aux"></div>
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    var headerTable ='<tr><th><?php echo JText::_('Client'); ?></th><th><?php echo JText::_('Project'); ?></th><th><?php echo JText::_('Number'); ?></th><th><?php echo JText::_('Description'); ?></th><th><?php echo JText::_('Date'); ?></th><th><?php echo JText::_('Amount2'); ?></th><th><?php echo JText::_('Expiration'); ?></th><th><?php echo JText::_('State'); ?></th></tr>';
    
    jQuery.noConflict();
    jQuery(document).ready(function(){
        var optionsc = jQuery("#client").html();
        optionsc = "<option value='0'><?php echo JText::_('All Clients'); ?></option>"+optionsc;
        jQuery("#client").html(optionsc);
        
        var optionsp = jQuery("#project").html();
        optionsp = "<option value='0'><?php echo JText::_('Select a project'); ?></option>"+optionsp;
        jQuery("#project").html(optionsp);
        
        jQuery("#client").change(function(){
            var clientId = jQuery(this).val(); 
            var dataClient = jQuery.ajax({
                url: "index.php?option=com_projectopen&c=report&task=getProjectsClient",
                global: false,
                type: "POST",
                data: ({clientId: clientId}),
                async: false,
                success: function(msg){
                    //alert(msg);
                }
            }).responseText;

            var clients = JSON.parse(dataClient);
            var htmlA="<option value='0'>Seleccione un proyecto</option>";
            jQuery.each(clients, function (i, val){
                htmlA += '<option value="'+val.id_project+'">'+((clientId != 0)? val.name_project : val.clientProject)+'</option>';
            });
            jQuery("#project").html(htmlA)
            
        });
        
        jQuery("#toolbar-send").css("display", "none");

    });
    function searchUsers(){
        var selectProject = jQuery("#project").val();
        var selectClient = jQuery("#client").val();
        var type = jQuery("#type").val();
        if(type!=0){
            jQuery("#load").show();
            var resultUsers = jQuery.ajax({
                url: "index.php?option=com_projectopen&c=report&task=getReportPendingInvoices",
                global: false,
                type: "POST",
                data: ({project:selectProject, client:selectClient, type: type}),
                success: function(msg){
                    jQuery("#load").hide();              
                    if(msg != 0){
                        jQuery("#tableInvoice").html(headerTable+msg);
                        jQuery("#tableInvoice").show();  
                        
                    }else{
                        alert("<?php echo JText::_('THE SEARCH HAD NO RESULTS'); ?>");
                    }
                    //                jQuery("#aux").append(msg);
                }
            }).responseText;
        }else{
            alert("Seleccione el tipo de reporte");
        }
    }

    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportpending"){
            document.getElementById("datos").value = document.getElementById("tableUsersDiv").innerHTML;
             var curdate = new Date();
            var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
             var htmlr = "<h2>Facturacion</h2><br/><table>";
            htmlr += (jQuery("#client").val() != 0) ? '<tr><td><strong>Cliente: </strong></td><td>' + jQuery('#client option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery("#project").val() != 0) ? '<tr><td><strong>Proyecto: </strong></td><td>' + jQuery('#project option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery("#type").val() != "") ? '<tr><td><strong>Tipo: </strong></td><td>' + jQuery('#type option:selected').html() + '</td></tr>' : "";
            htmlr += "<tr><td><strong>Generado el: </strong></td>";
            htmlr += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
            htmlr += "</table>";
            jQuery("#datosAdd").val(htmlr);
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>