<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<!-- Reporte de imputacion de horas detallado por día -->
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="year"><input style="float: left;" id="pMonth" type="radio" name="report" checked="true" /> <?php echo JText::_('Year'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><input class="inputbox" type="text" name="year" id="year" maxlength="4" onkeypress="return numeros(event)" value="" /></td>
            <td  class="key"><label for="month"><?php echo JText::_('Month'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><select name="month" id="month" >
                    <?php
                    for ($i = 0; $i < count($this->months); $i++) {
                        echo "<option value='" . $i . "'>" . $this->months[$i] . "</option>";
                    }
                    ?>
                </select></td>
        </tr>
        <tr>
            <td  class="key">
                <label for="start_date"> <input style="float: left;" id="pRango" type="radio" name="report"/> <?php echo JText::_('Start Date'); ?></label>
            </td>
            <td>
                <input id='start_date' name='start_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("start_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
            <td  class="key">
                <label for="end_date"><?php echo JText::_('End Date'); ?></label>
            </td>
            <td>
                <input id='end_date' name='end_date' size='13' type='text' value="" readonly/>
                <img class='calendar' onclick='return showCalendar("end_date", "%d-%m-%Y")'
                     src='templates/system/images/calendar.png' alt='calendar' />
            </td>
        </tr>
        <tr>
            <td  class="key"><label for="user"><?php echo JText::_('User'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->userNewshore, 'user', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?></td>
            <td  class="key"><label for="client"><?php echo JText::_('Client'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->clients, 'client', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?></td>
        </tr>
    </table>
    <br>
    <br>
    <div id="usersHours"></div>
    <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    jQuery.noConflict();

    window.onload= function(){
        //document.getElementById("toolbar-send").style.display = 'none';
    }

    var dataUsers;

    function searchUsers(){
        var tableUsers = document.getElementById("usersHours");
        tableUsers.innerHTML = '';
        //document.getElementById("toolbar-send").style.display = 'none';
        
        var user = document.getElementById("user");
        var client = document.getElementById("client");
        var pMonth = document.getElementById("pMonth");
        var pRango = document.getElementById("pRango");
        var idUser = user.options[user.selectedIndex].value;
        var idClient = client.options[client.selectedIndex].value;
        
        var con = false;
        if(pMonth.checked) {
            var month = document.getElementById("month").value;
            var year = document.getElementById("year").value;
            if(year==""){
                alert(<?php echo '"' . JText::_('Enter a year') . '"'; ?>);
            }else if(year.length != 4){
                alert(<?php echo '"' . JText::_('Enter a valid year') . '"'; ?>);
            }else if(month == -1){
                alert(<?php echo '"' . JText::_('Selected a month') . '"'; ?>);
            }else {
                jQuery("#load").show();
                jQuery.ajax({
                    url: "index.php?option=com_projectopen&c=report&task=getUserHoursProjectClient",
                    global: false,
                    type: "POST",
                    data: ({userId: idUser, clientId: idClient, month: month, year: year}),
                    success: function(msg){
                        drawTableUsers(msg);
                        jQuery("#load").hide();
                    }
                })
        
                //                var argsArray = new Array();
                //                argsArray.push(newArg("userId", idUser));
                //                argsArray.push(newArg("clientId", idClient));
                //                argsArray.push(newArg("month", month));
                //                argsArray.push(newArg("year", year));
                //                argsArray.push(newArg("c", "report"));
                //                argsArray.push(newArg("option", "com_projectopen"));
                //                argsArray.push(newArg("task", "getUserHoursProjectClient"));
                //                var argsStr = argsArray.join("&");
                //                sendRequest("index.php", argsStr, drawTableUsers);
            }
        }else {
            var start_date =document.getElementById("start_date");
            var end_date = document.getElementById("end_date");
            if(start_date.value!="" || end_date.value!=""){
                var textStartDate = start_date.value.split("-");
                var textEndDate = end_date.value.split("-");
                start_date = textStartDate[2]+"/"+textStartDate[1]+"/"+textStartDate[0];
                end_date = textEndDate[2]+"/"+textEndDate[1]+"/"+textEndDate[0];
                var dstart_date = new Date(start_date);
                var dend_date = new Date(end_date);
                var dcurrent_date = new Date(getDateCurrent());
                if(dend_date.valueOf()<dstart_date.valueOf()){
                    alert('<?php echo JText::_('Start date must be less or equal than the end date'); ?>');
                    con = true;
                }else if(dend_date.valueOf() > dcurrent_date.valueOf()){
                    alert('<?php echo JText::_('End date must be less than or equal to the current date'); ?>');
                    con = true;
                }
            }else {
                start_date = "";
                end_date = "";
                alert(<?php echo '"' . JText::_('Enter a valid dates') . '"'; ?>);
            }
            if(!con) {
                jQuery("#load").show();
                jQuery.ajax({
                    url: "index.php?option=com_projectopen&c=report&task=getUserHoursProjectClient",
                    global: false,
                    type: "POST",
                    data: ({userId: idUser, clientId: idClient, start_date: start_date, end_date: end_date}),
                    success: function(msg){
                        drawTableUsers(msg);
                        jQuery("#load").hide();
                    }
                })
                //                var argsArray = new Array();
                //                argsArray.push(newArg("userId", idUser));
                //                argsArray.push(newArg("clientId", idClient));
                //                argsArray.push(newArg("start_date", start_date));
                //                argsArray.push(newArg("end_date", end_date));
                //                argsArray.push(newArg("c", "report"));
                //                argsArray.push(newArg("option", "com_projectopen"));
                //                argsArray.push(newArg("task", "getUserHoursProjectClient"));
                //                var argsStr = argsArray.join("&");
                //                sendRequest("index.php", argsStr, drawTableUsers);
            }
        }
    }

    //Report Impute
    function drawTableUsers(res){
        if (res != "") {
            //document.write(res);
            var dataUsers = JSON.parse(res);
            var tableUsers = document.getElementById("usersHours");
            var k = 0;
            var htmlv="<table border=1 class='adminlist' id='hoursUser' >";
            htmlv +="<tr>";
            htmlv +="<th align='center' ><?php echo JText::_('NUM'); ?></th>";
            htmlv +="<th align='center' ><?php echo JText::_('Client'); ?></th>";
            htmlv +="<th align='center' ><?php echo JText::_('Project/Absence'); ?></th>";
            htmlv +="<th align='center' ><?php echo JText::_('Employee'); ?></th>";
            htmlv +="<th align='center' ><?php echo JText::_('Hours'); ?></th>";
            htmlv +="<th align='center' ><?php echo JText::_('Cost hour'); ?></th>";
            htmlv +="<th align='center' ><?php echo JText::_('Import'); ?></th>";
            htmlv +="<th align='center' ><?php echo JText::_('Date'); ?></th></tr>";
            if(dataUsers != null){
                if(dataUsers.length != 0) {
                    for(var i=0; i<dataUsers.length; i++){
                        htmlv += "<tr class='row"+k+"'>";
                        htmlv += "<td align='center'>"+(i+1)+"</td>";
                        htmlv += "<td align='center'>"+dataUsers[i].client_name+"</td>";
                        htmlv += "<td align='center'>"+dataUsers[i].project_name+"</td>";
                        htmlv += "<td align='center'>"+dataUsers[i].user_name+"</td>";
                        htmlv += "<td align='center'>"+dataUsers[i].number_hours.replace(".", ",")+"</td>";
                        htmlv += "<td align='center'>"+parseFloat(dataUsers[i].tariff_hours).toFixed(2).toString().replace(".", ",")+"</td>";//dataUsers[i].tariff_hours
                        htmlv += "<td align='center'>"+parseFloat(dataUsers[i].importe).toFixed(2).toString().replace(".", ",")+"</td>";//dataUsers[i].importe
                        htmlv += "<td align='center'>"+dataUsers[i].impute_date+"</td>";
                        htmlv += "</tr>";
                        k = 1 - k;
                    }
                    htmlv += "</table>";
                        
                    tableUsers.innerHTML = htmlv;
                        
                }else{
                    alert('<?php echo JText::_('The search had no results'); ?>');
                }
            }
        }
        jQuery("#load").hide();
    }
    
    //change clients
    function changeClients() {
        var userId = document.getElementById("user").value;
        var dataRes = jQuery.ajax({
            url: "index.php?option=com_projectopen&c=report&task=getClientsUser",
            global: false,
            type: "POST",
            data: ({userId : userId}),
            async: false,
            success: function(msg){
                //alert(msg);
            }
        }).responseText;
        var clients = JSON.parse(dataRes);
        
        var client = document.getElementById("client");
        if(clients.length > 0) {
            while(client.length > 1){
                client.remove(1);
            }
            for(var i=0; i<clients.length; i++){
                var newOption = document.createElement('option');
                newOption.text = clients[i].name;
                newOption.value = clients[i].id;
                try {
                    client.add(newOption, null); // standards compliant; doesn't work in IE
                }
                catch(ex) {
                    client.add(newOption); // IE only
                }
            }
        }
    }
    
    //change users
    function changeUsers() {
        var clientId = document.getElementById("client").value;
        var dataRes = jQuery.ajax({
            url: "index.php?option=com_projectopen&c=report&task=getUsersClient",
            global: false,
            type: "POST",
            data: ({clientId : clientId}),
            async: false,
            success: function(msg){
                //alert(msg);
            }
        }).responseText;
        var users = JSON.parse(dataRes);
        
        var user = document.getElementById("user");
        if(users.length > 0) {
            while(user.length > 1){
                user.remove(1);
            }
            for(var i=0; i<users.length; i++){
                var newOption = document.createElement('option');
                newOption.text = users[i].name;
                newOption.value = users[i].id;
                try {
                    user.add(newOption, null); // standards compliant; doesn't work in IE
                }
                catch(ex) {
                    user.add(newOption); // IE only
                }
            }
        }
    }
    
    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportsreport"){
            document.getElementById("datos").value = document.getElementById("usersHours").innerHTML;
            var curdate = new Date();
            var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        
            var htmlr = "<h2>Imputacion de horas detallado por dia</h2><br/><table>";
            htmlr += (jQuery('#year').val() != 0) ? '<tr><td><strong>Año: </strong></td><td>' + jQuery('#year').val() + '</td></tr>' : "";
            htmlr += (jQuery('#month').val()  != 0) ? '<tr><td><strong>Mes: </strong></td><td>' +  jQuery('#month option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery('#start_date').val() != "") ? '<tr><td><strong>Fecha de inicio: </strong></td><td>' +  jQuery('#start_date').val() + '</td></tr>' : "";
            htmlr += (jQuery('#end_date').val() != "") ? '<tr><td><strong>Fecha de fin: </strong></td><td>' + jQuery('#end_date').val() + '</td></tr>' : "";
            htmlr += (jQuery('#user').val() != 0) ? '<tr><td><strong>Usuario: </strong></td><td>' + jQuery('#user option:selected').html() + '</td></tr>' : "";
            htmlr += (jQuery('#client').val()  != 0) ? '<tr><td><strong>Cliente: </strong></td><td>' +  jQuery('#client option:selected').html() + '</td></tr>' : "";
            htmlr += "<tr><td><strong>Generado el: </strong></td>";
            htmlr += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
            htmlr += "<tr><td></td><td></td></tr>";
            htmlr += "</table>";
            jQuery("#datosAdd").val(htmlr);
            
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>