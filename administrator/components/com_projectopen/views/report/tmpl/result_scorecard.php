<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.mootools');
JHTML::_('behavior.calendar');
$user = & JFactory::getUser();
$type = $user->get('usertype'); ?>

<link rel='stylesheet' type='text/css' href='<?php echo JURI::root(true) . '/libraries/jquery/css/custom-theme/jquery-ui-1.8.13.custom.css' ?>' />
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-ui-1.8.13.custom.min.js' ?>'></script>
<script type='text/javascript'> jQuery.noConflict(); </script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<script type="text/javascript">
   var clientReport = '<?php echo (isset($this->clientReport) && $this->clientReport != null) ? $this->clientReport : 0; ?>';
   var projectReport = '<?php echo (isset($this->projectReport) && $this->projectReport != null) ? $this->projectReport : 0; ?>';

    var month = new Array(0, 1, 2, 3,4,5, 6, 7,8,9,10,11);
    var monthName = new Array("<?php echo JText::_('January'); ?>", "<?php echo JText::_('February'); ?>", "<?php echo JText::_('March'); ?>", "<?php echo JText::_('April'); ?>","<?php echo JText::_('May'); ?>","<?php echo JText::_('June'); ?>", "<?php echo JText::_('July'); ?>", "<?php echo JText::_('August'); ?>","<?php echo JText::_('September'); ?>","<?php echo JText::_('October'); ?>","<?php echo JText::_('November'); ?>","<?php echo JText::_('December'); ?>");
    var yearGlobal;
    var clients ;
    var monthStart = -1;
    var monthEnd = -1;
    var conversion_value = <?php echo json_encode($this->conversion); ?>;

    var typeuser = '<?php if(($type == 'Super Administrator')) echo 1; else if($type == 'Admin newshore') echo 2; else if($type == 'Team boss') echo 3; else if($type == 'Administrator') echo 4; else echo 0; ?>';
    window.onload = function () {
        clients = <?php echo json_encode($this->clients); ?>;
        var incomes = <?php echo json_encode($this->incomes); ?>;
        var inCerrado = <?php echo $this->inCerrado; ?>;
        var indirect_expenses = <?php echo json_encode($this->indirect_expenses); ?>;
        var direct_expenses = <?php echo json_encode($this->direct_expenses); ?>;
        var internal_cost = <?php echo json_encode($this->internal_cost); ?>;
        var other_operating_expenses = <?php echo json_encode($this->other_operating_expenses); ?>;
        var conversion = <?php echo json_encode($this->conversion); ?>;
        monthStart = document.getElementById("start_month").value;
        monthEnd = document.getElementById("end_month").value;
        if(document.getElementById("scorecard_year").value != 0) {
            var fecha=new Date();
            monthStart = 0;
            var dateTemp=new Date();
            dateTemp.setMonth(fecha.getMonth()-1);
            if(parseInt(document.getElementById("scorecard_year").value) < fecha.getFullYear()) {
                monthEnd = 11;
            }else {
                monthEnd = parseInt(dateTemp.getMonth());
            }
        }
        //Limit Month of Query
        try{
            monthIndexStart = parseInt(isNaN(document.getElementById("end_month").value)?0:parseInt(document.getElementById("end_month").value));
        }catch(e){
            monthIndexStart = 0;
        }
        try{
            monthIndexLimit = parseInt(isNaN(document.getElementById("end_month").value)?0:parseInt(document.getElementById("end_month").value));
        }catch(e){
            monthIndexLimit = 0;
        }
        //EOF /Limit Month of Query
        if(conversion.length == 0){
            alert('<?php echo JText::_('Unable to build the scorecard because there are no conversions stored'); ?>');
            location.href = "index.php?option=com_projectopen&c=report&task=viewScorecard";
        }else{
            /*if(parseInt(inCerrado)==0){
                drawIncomes(incomes);
            }else{
                monthEnd=11;
                drawIncomesClose(incomes);
            }
            sum(document.getElementById('tableIncomes'));
            sumClients(document.getElementById('tableIncomes'));*/

            drawIndirectExpenses(indirect_expenses);
            sum(document.getElementById("ie"));
            sumClients(document.getElementById('ie'));

            drawDirectExpenses(direct_expenses);
            sum(document.getElementById("de"));
            sumClients(document.getElementById('de'));

            drawInternalCost(internal_cost);
            sum(document.getElementById("ic"));
            sumInternalCost(document.getElementById('ic'));

            drawOtherOperatingExpenses(other_operating_expenses);
            sum(document.getElementById("ooe"));
            sumInternalCost(document.getElementById('ooe'));

            //drawNetOperatingIncome(document.getElementById('tableIncomes'), document.getElementById("ie"), document.getElementById("de"));

            //pointsCurrencyIm(document.getElementById('tableIncomes'));
            pointsCurrencyIm(document.getElementById("ie"));
            pointsCurrencyIm(document.getElementById("de"));
            pointsCurrencyIm(document.getElementById("ic"));
            pointsCurrencyIm(document.getElementById("ooe"));
            //pointsCurrencyNOI(document.getElementById("noi"));

            var states = <?php echo json_encode($this->states); ?>;
            drawOthers(conversion, states);

            var ic = document.getElementById("ic");
            for(var i=0; i<fil.length; i++) {
                ic.rows[fil[i]].cells[col[i]].innerHTML = 0;
                ic.rows[fil[i]].cells[(ic.rows[fil[i]].cells.length - 1)].innerHTML = 0;
            }

           if(typeuser != 1 || clientReport != 0 || projectReport != 0){
                jQuery("#conversion tr:eq(1)").hide();
           }
            /*formulaExcel(document.getElementById("tableIncomes"));
            formulaExcel(document.getElementById("de"));
            formulaExcel(document.getElementById("ie"));
            formulaExcel(document.getElementById("noi"));*/
        }

        jQuery("#ic tr").each(function (index) {
            if(jQuery(this).find('td').length > 0){
                jQuery(this).click(function () {
                    if(jQuery(this).attr('id') != "c1") {
                    usersDetail(jQuery(this).attr('id'), jQuery(this).find('td')[0].innerHTML);
                    }
                });
            }
        });

        jQuery("#ie tbody tr").each(function (index) {
            if(jQuery(this).find('td').length > 0){
                jQuery(this).click(function () {
                    if(jQuery(this).attr('class') != "trClientReport") {
                        costDetail(jQuery(this).attr('id'), jQuery(this).find('td')[0].innerHTML);
                    }
                });
            }
        });
    }

    function drawIndirectExpenses(indirect_expenses) {
        var idproject = 0;
        var idclient = 0;
        var count = 0;
        var i = 0;
        var j = 0;
        var c = 0;
        yearGlobal=indirect_expenses[0].year;
        var tablaie = "<table border=1 class='adminlist' id='ie' >";
        //Resume the months cols to print
        var lastMonth = monthIndexLimit;

        tablaie += drawHeadScorecard(indirect_expenses[0].year,isNaN(lastMonth)?0:lastMonth);
        tablaie += drawTotalScorecard("<?php echo JText::_('Indirect expenses'); ?>",isNaN(lastMonth)?0:lastMonth);

        var idTabla = "ie";
        for(c=0; c<clients.length; c++) {
            if(idclient != clients[c].id_client) {
                var tdclient = 0;
                tablaie += "<tr class='trClientReport' id=c"+clients[c].id_client+" onclick='desClient("+clients[c].id_client+", \""+idTabla+"\")' >";
                tablaie += "<td><img id='imgDes' src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>'>"+clients[c].name_client+"</td>";
                idclient = clients[c].id_client;
                
                while(tdclient < /*12*/ (lastMonth+1)){
                    if(tdclient < (parseInt(monthStart)) || tdclient > (parseInt(monthEnd))){
                        tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\" >0.00</td>";
                    }else {
                        tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';\" >0.00</td>";
                    }
                    tdclient++;
                }

                tablaie += "<th  align='right' style=\"mso-number-format:\'#,##0\';\" >calc_total</th>";
                tablaie += "</tr>";
                idproject = 0;
            }
            var flagTemp = false;
            for(i=0; i<indirect_expenses.length; i++) {
                if(clients[c].id_project == indirect_expenses[i].project_id) {
                    if(idproject != indirect_expenses[i].project_id){
                        tablaie += "<tr id="+indirect_expenses[i].project_id+" style='display:none'>";
                        tablaie += "<td>"+indirect_expenses[i].project_name+"</td>";

                        idproject = indirect_expenses[i].project_id;
                    }
                    if(indirect_expenses[i].month < /*12*/ (lastMonth+1) ) {
                        while(indirect_expenses[i].month != month[j] && count < (lastMonth+1)){
                            if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                                tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\" >0.00</td>";
                            }else {
                                tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';\" >0.00</td>";
                            }
                            j++;
                            count++
                        }

                        if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                            tablaie += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\" >"+Math.round(parseFloat(indirect_expenses[i].indirect_expenses_suma))+"</td>";
                        }else {
                            tablaie += "<td align='right' style=\"mso-number-format:\'#,##0\';\" >"+Math.round(parseFloat(indirect_expenses[i].indirect_expenses_suma))+"</td>";
                        }

                        count++;
                        j++;
                    }
                    //if(indirect_expenses[i].month == /*11*/parseInt(monthEnd)){ //lastMonth
                    if(indirect_expenses[i].month == /*11*/parseInt(monthEnd) && indirect_expenses[i].year == yearGlobal){
                        if(count</*12*/(lastMonth+1)) {
                            for(var h=0;h<(/*12*/(lastMonth+1) - count);h++) {
                                if(h < (parseInt(monthStart)) || h > (parseInt(/*monthEnd*/lastMonth))){
                                    tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\" >0.00</td>";
                                }else {
                                    tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';\" >0.00</td>";
                                }
                            }
                        }
                        tablaie += "<th  align='right' style=\"mso-number-format:\'#,##0\';\" >calc_total</th>";
                        tablaie += "</tr>";
                        count = 0;
                        j = 0;
                        flagTemp = true;
                        i = indirect_expenses.length;
                    }
                }
            }
            if(count</*12*/(lastMonth+1) && !flagTemp) {
                for(var h=0;h<((lastMonth+1)-count);h++) {
                    if(h < (parseInt(monthStart)) || h > (parseInt(monthEnd))){
                        tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\" >0.00</td>";
                    }else {
                        tablaie += "<td  align='right' style=\"mso-number-format:\'#,##0\';\" >0.00</td>";
                    }
                }
                tablaie += "<th  align='right' style=\"mso-number-format:\'#,##0\';\" >calc_total</th>";
                tablaie += "</tr>";
                count = 0;
                j = 0;
            }

        }
        tablaie += "</table>";

        document.getElementById("indirectExpenses").innerHTML = tablaie;
        tablaie = '';
    }

    function drawIncomes(incomes){
        var tablain = "<table border=1 class='adminlist' id='tableIncomes'>";
        var idClient=0;
        var idTabla="tableIncomes";

        //Resume the months cols to print
        var lastMonth = monthIndexLimit;
        tablain += drawHeadScorecard(incomes[0].year,lastMonth);
        tablain += drawTotalScorecard("<?php echo JText::_('Incomes'); ?>",lastMonth);

        for(var j=0; j<clients.length; j++){
            if(idClient!= clients[j].id_client){
                idClient=clients[j].id_client;
                tablain += "<tr class='trClientReport' id='c"+clients[j].id_client+"' onclick='desClient("+clients[j].id_client+", \""+idTabla+"\")'>";
                tablain += "<td><img id='imgDes' src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>'>"+clients[j].name_client+"</td>";
                for(var l=0; l</*12*/(lastMonth+1); l++){
                    var valor=0;

                    if(l < (parseInt(monthStart)) || l > (parseInt(monthEnd))){
                        tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+valor+"</td>";
                    }else {
                        tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';\">"+valor+"</td>";
                    }

                }
                tablain += "<th  align='right' style=\"mso-number-format:\'#,##0\';\"></th>";
                tablain +="</tr>";

                for(var i=0; i<incomes.length; i++) {
                    if(clients[j].id_client == incomes[i].client){
                        tablain += "<tr id='"+incomes[i].id+"' style='display:none'>";
                        tablain += "<td>"+incomes[i].name+"</td>";

                        for(var l=0; l</*12*/(lastMonth+1); l++){

                            if(incomes[i].income[l]!= null ){
                                var valor=Math.round(parseFloat(incomes[i].income[l]));
                                if(l < (parseInt(monthStart)) || l > (parseInt(monthEnd))){
                                    tablain += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\" >"+valor+"</td>";
                                }else {
                                    tablain += "<td align='right' style=\"mso-number-format:\'#,##0\';\" >"+valor+"</td>";
                                }
                            }else{
                                var valor=0;
                                if(l < (parseInt(monthStart)) || l > (parseInt(monthEnd))){
                                    tablain += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\" >"+valor+"</td>";
                                }else {
                                    tablain += "<td align='right' style=\"mso-number-format:\'#,##0\';\" >"+valor+"</td>";
                                }
                            }

                        }
                        tablain += "<th  align='right' style=\"mso-number-format:\'#,##0\';\" ></th>";
                        tablain +="</tr>";
                    }
                }

            }
        }

        tablain += "</tr>";
        tablain += "</table>";
        document.getElementById("incomes").innerHTML = tablain;
        tablain = '';
    }

    function drawIncomesClose(incomes) {
        var idproject = 0;
        var count = 0;
        var i = 0;
        var j = 0;
        var c = 0;
        var tablain = "<table border=1 class='adminlist' id='tableIncomes' >";
        tablain += drawHeadScorecard(incomes[0].year);
        tablain += drawTotalScorecard("<?php echo JText::_('Incomes'); ?>");
        var idclient = 0;

        var idTabla = "tableIncomes";
        for(c=0; c<clients.length; c++) {
            if(idclient != clients[c].id_client) {
                var tdclient = 0;
                tablain += "<tr class='trClientReport' id=c"+clients[c].id_client+" onclick='desClient("+clients[c].id_client+", \""+idTabla+"\")' >";
                tablain += "<td><img id='imgDes' src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>'>"+clients[c].name_client+"</td>";

                idclient = clients[c].id_client;

                while(tdclient < 12){
                    if(tdclient < (parseInt(monthStart)) || tdclient > (parseInt(monthEnd))){
                        tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                    tdclient++;
                }

                tablain += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                tablain += "</tr>";
                idproject = 0;
            }
            var flagTemp = false;
            for(i=0; i<incomes.length; i++) {
                if(clients[c].id_project == incomes[i].id) {
                    if(idproject != incomes[i].id){
                        tablain += "<tr id="+incomes[i].id+" style='display:none'>";
                        tablain += "<td>"+incomes[i].name+"</td>";

                        idproject = incomes[i].id;
                    }
                    if(incomes[i].month != 12) {
                        while(incomes[i].month != month[j]){
//                            alert('inc '+incomes[i].month);
//                            alert('mo '+month[j]);
                            if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                                tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                            }else {
                                tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                            }
                            j++;
                            count++
                        }
                        if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                            tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                        }else {
                            tablain += "<td align='right' style=\"mso-number-format:\'#,##0\';\">"+Math.round(parseFloat(incomes[i].income))+"</td>";
                        }
                        count++;
                        j++;
                    }
                    if(incomes[i].month == 11) {
                        if(count<12) {
                            for(var h=0;h<(12-count);h++) {
                                if(h < (parseInt(monthStart)) || h > (parseInt(monthEnd))){
                                    tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                                }else {
                                    tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                                }
                            }
                        }
                        tablain += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                        tablain += "</tr align='right'>";
                        count = 0;
                        j = 0;
                        flagTemp = true;
                        i = incomes.length;
                    }
                }
            }
            if(count<12 && !flagTemp) {
                for(var h=0;h<(12-count);h++) {
                    if(h < (parseInt(monthStart)) || h > (parseInt(monthEnd))){
                        tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablain += "<td  align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                }
                tablain += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                tablain += "</tr>";
                count = 0;
                j = 0;
            }
        }
        tablain += "</table>";

        document.getElementById("incomes").innerHTML = tablain;
        tablain ='';
    }

    function drawDirectExpenses(direct_expenses) {
        var idproject = 0;
        var idclient = 0;
        var count = 0;
        var i = 0;
        var j = 0;
        var c = 0;
        var tablade = "<table border=1 class='adminlist' id='de' >";

        //Resume the months cols to print
        var lastMonth = monthIndexLimit;
        tablade += drawHeadScorecard(yearGlobal,isNaN(lastMonth)?0:lastMonth);
        tablade += drawTotalScorecard("<?php echo JText::_('Direct expenses'); ?>",isNaN(lastMonth)?0:lastMonth );

        var idTabla = "de";
        for(c=0; c<clients.length; c++) {
            if(idclient != clients[c].id_client) {
                var tdclient = 0;
                count = 0;
                j = 0;
                tablade += "<tr class='trClientReport' id=c"+clients[c].id_client+" onclick='desClient("+clients[c].id_client+", \""+idTabla+"\")' >";
                tablade += "<td><img id='imgDes' src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>'>"+clients[c].name_client+"</td>";

                idclient = clients[c].id_client;

                while(tdclient < /*12*/ (lastMonth+1) ){
                    if(tdclient < (parseInt(monthStart)) || tdclient > (parseInt(monthEnd))){
                        tablade += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td  align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                    tdclient++;
                }

                tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                tablade += "</tr>";
                idproject = 0;
            }

            var flagTemp = false;
            count = 0;
            j = 0;

            for(i=0; i<direct_expenses.length; i++) {
                if(clients[c].id_project == direct_expenses[i].project_id) {
                    if(idproject != direct_expenses[i].project_id){
                        tablade += "<tr id="+direct_expenses[i].project_id+" style='display:none'>";
                        tablade += "<td>"+direct_expenses[i].project_name+"</td>";

                        idproject = direct_expenses[i].project_id;
                    }
                    if(direct_expenses[i].month < /*12*/(lastMonth+1)  && direct_expenses[i].year == yearGlobal ) {

                       while(direct_expenses[i].month != month[j]){ //llena de 0 hasta el mes de inicio del proyecto.
                            if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                                tablade += "<td  align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                            }else {
                                tablade += "<td  align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                            }

                            j++;
                            count++;

                        }
                        if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                            tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+Math.round(parseFloat(direct_expenses[i].direct_expenses))+"</td>";
                        }else {
                            tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">"+Math.round(parseFloat(direct_expenses[i].direct_expenses))+"</td>";
                        }

                        count++;
                        j++;
                        if(count == /*12*/(lastMonth+1)) {
                            tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                            flagTemp = true;
                        }
                    }
                    if(direct_expenses[i].month == /*11*/parseInt(monthEnd) && direct_expenses[i].year == yearGlobal){
                        if(count</*12*/(lastMonth+1)) {
                            for(var h=0;h</*(12-count)*/ ((lastMonth+1)-count) ;h++) {
                                if(h < (parseInt(monthStart)) || h > (parseInt(monthEnd))){
                                    tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                                }else {
                                    tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                                }
                            }
                        }
                        if(!flagTemp) {
                            tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                        }
                        tablade += "</tr>";
                        count = 0;
                        j = 0;
                        flagTemp = true;
                        i = direct_expenses.length;
                    }
                }
            }
            if(count</*12*/(lastMonth+1) && !flagTemp) {
                for(var h=0;h</*(12-count)*/((lastMonth+1)-count);h++) {
                    if(h < (parseInt(monthStart)) || h > (parseInt(monthEnd))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\" >0.00</td>";
                    }
                }
                tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                tablade += "</tr>";
                count = 0;
                j = 0;
            }
        }

        tablade += "</table>";

        document.getElementById("directExpenses").innerHTML = tablade;
        tablade = '';
    }

    function desClient(idClient, tablaid){
        var tabla = document.getElementById(tablaid);
        var rows = tabla.rows.length;
        var cols = tabla.rows[0].cells.length;

        for(var i=2; i<rows; i++){
            if(tablaid == "ic" || tablaid == "ooe") {
                i=1;
            }
            if(tabla.rows[i].onclick!=null && tabla.rows[i].id == 'c'+idClient){
                var image= tabla.rows[i].cells[0].getElementsByTagName('img')[0];
                var j=i+1;
                while(tabla.rows[j].onclick == null){
                    if(tabla.rows[j].style.display=='none'){
                        tabla.rows[j].style.display='';
                        image.src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow_down.png' ?>';
                    }else{
                        tabla.rows[j].style.display='none';
                        image.src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>';
                    }
                    j++;
                    if(tabla.rows[j]==undefined){
                        break;
                    }
                }
                i=j-1;
            }
        }
    }

    function drawNetOperatingIncome(income, direct_expenses, indirect_expenses) {
        var rows = direct_expenses.rows.length;
        var cols = direct_expenses.rows[0].cells.length;
        var calc = 0;
        var porc = 0;
        var tablanoi = "<table border=1 class='adminlist' id='noi' >";

        var directTotalValue = 0;
        var incomeTotalValue = 0;
        var indirectTotalValue = 0;

        var colIndex = cols==2?0 : cols - 3;
        //tablanoi += direct_expenses.rows[0].cells[1].innerHTML.split("-")[1];
         tablanoi += drawHeadScorecardNoi(income.rows[0].cells[1].innerHTML.split("-")[1]== undefined ?direct_expenses.rows[0].cells[1].innerHTML.split("-")[1]:income.rows[0].cells[1].innerHTML.split("-")[1] ,colIndex);
        var idTabla = "noi";
        for(var i=1; i<rows; i++) {
            if(direct_expenses.rows[i].onclick != null) {
                tablanoi += "<tr id="+direct_expenses.rows[i].id+" class='trClientReport' onclick='desClient("+direct_expenses.rows[i].id.replace('c','')+", \""+idTabla+"\")' >";
            }else {
                if(i==1) {
                    tablanoi += "<tr>";
                }else {
                    tablanoi += "<tr style='display:none'>";
                }
            }
            for(var j=0; j<cols; j++) {

                //Cols adaptation
                var directExpenseValue;
                var indirectExpenseValue;
                var incomeValue;

                directTotalValue =  directExpenseValue = parseFloat(direct_expenses.rows[i].cells[j].innerHTML);
                indirectTotalValue =  indirectExpenseValue = parseFloat(indirect_expenses.rows[i].cells[j].innerHTML);
                incomeTotalValue = incomeValue = parseFloat(income.rows[i].cells[j].innerHTML);

                if(j==0) {
                    if(i!=1) {
                        tablanoi += "<td>"+income.rows[i].cells[j].innerHTML+"<br>%</td>";
                    }else {
                        tablanoi += "<th><?php echo JText::_('Net operating income'); ?></th>";
                    }
                }else if(j == (cols-1)) {
                    if(i!=1) {
                        //calc = parseFloat(income.rows[i].cells[j].innerHTML) - parseFloat(direct_expenses.rows[i].cells[j].innerHTML) - parseFloat(indirect_expenses.rows[i].cells[j].innerHTML);
                        calc = incomeTotalValue - directTotalValue - indirectTotalValue;

                        //porc = Math.round((calc/parseFloat(income.rows[i].cells[j].innerHTML))*100);
                        porc = (incomeValue != 0?Math.round((calc/incomeValue)*100):0);
                        if(calc <= 0) {
                            porc = 0;
                            tablanoi += "<th  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right'><label style='color:#FF0000' >"+calc+"</label><br style=\"mso-number-format:\'0%\';\"><label>"+porc+"</label>%</th>";
                        }else {
                            tablanoi += "<th  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right' ><label>"+calc+"</label><br style=\"mso-number-format:\'0%\';\"><label>"+porc+"</label>%</th>";
                        }
                    }else {
                        //calc = parseFloat(income.rows[i].cells[j].innerHTML) - parseFloat(direct_expenses.rows[i].cells[j].innerHTML) - parseFloat(indirect_expenses.rows[i].cells[j].innerHTML);
                        calc = incomeTotalValue - directTotalValue - indirectTotalValue;

                        if(calc <= 0) {
                            tablanoi += "<th  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right'><label style='color:#FF0000'  >"+calc+"</label></th>";
                        }else {
                            tablanoi += "<th  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right' ><label>"+calc+"</label></th>";
                        }
                    }
                }else {
                    if(i!=1) {
                        //calc = parseFloat(income.rows[i].cells[j].innerHTML) - parseFloat(direct_expenses.rows[i].cells[j].innerHTML) - parseFloat(indirect_expenses.rows[i].cells[j].innerHTML);
                        calc = incomeValue - directExpenseValue - indirectExpenseValue;

                        //porc = Math.round((calc/parseFloat(income.rows[i].cells[j].innerHTML))*100);
                        porc = incomeValue != 0?Math.round((calc/incomeValue)*100):0;

                        if(calc <= 0) {
                            porc = 0;
                            if((j-1) < (parseInt(monthStart)) || (j-1) > (parseInt(monthEnd))){
                                tablanoi += "<td  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';background-color: #c6d4e1 !important;\" align='right' ><label style='color:#FF0000' >"+calc+"</label><br style=\"mso-number-format:\'0%\';\"><label>"+porc+"</label>%</td>";
                            }else {
                                tablanoi += "<td  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right' ><label style='color:#FF0000' >"+calc+"</label><br style=\"mso-number-format:\'0%\';\"><label>"+porc+"</label>%</td>";
                            }
                        }else {
                            if((j-1) < (parseInt(monthStart)) || (j-1) > (parseInt(monthEnd))){
                                tablanoi += "<td  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';background-color: #c6d4e1 !important;\" align='right' ><label>"+calc+"</label><br style=\"mso-number-format:\'0%\';\"><label>"+porc+"</label>%</td>";
                            }else {
                                tablanoi += "<td  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right' ><label>"+calc+"</label><br style=\"mso-number-format:\'0%\';\"><label>"+porc+"</label>%</td>";
                            }
                        }
                    }else {
                        //calc = parseFloat(income.rows[i].cells[j].innerHTML) - parseFloat(direct_expenses.rows[i].cells[j].innerHTML) - parseFloat(indirect_expenses.rows[i].cells[j].innerHTML);
                        calc = incomeValue - directExpenseValue - indirectExpenseValue;

                        if(calc <= 0) {
                            tablanoi += "<th  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right' ><label style='color:#FF0000' >"+calc+"</label></th>";
                        }else {
                            tablanoi += "<th  style=\"mso-number-format:\'#,##0;[RED]-#,##0\';\" align='right' ><label>"+calc+"</label></th>";
                        }
                    }
                }
            }

            tablanoi += "</tr>";
        }

        tablanoi += "</table>";

        document.getElementById("netOperatingIncome").innerHTML = tablanoi;
    }

    function drawOthers(conversion, states) {
        var antConversionValue = 0
        var tablacov = "<table border=1 class='adminlist' id='cov' ><thead>";
        tablacov += "<tr style='background-color: #F0F0F0;'><th formula='eliecer' id='vacia'></th>";
        var lastMonth = monthIndexLimit;
        var country = <?php echo $this->country; ?>;

        for(i=0;i<=lastMonth;i++)
            tablacov += "<th id='month'>"+monthName[i]+"-"+conversion[0].year+"</th>";

        tablacov += "</tr></thead>";

        if (country == 0) {
            tablacov +="<tr><th><?php echo JText::_('State'); ?></th>";
            var dateCurrent= new Date();
            var monthCurrent = dateCurrent.getMonth();
            var flagDraw = false;
            for(var j=0; j</*12*/(lastMonth+1); j++){
                flagDraw = false;
                for(var h=0; h<states.length; h++){
                    if(states[h].month == j) {
                        if(states[h].state==0){
                            jtLabel ="<?php echo JText::_('No close'); ?>";
                            if(j<=monthCurrent || j == 11){
                                if(j < (parseInt(monthStart)) || j > (parseInt(monthEnd))){
                                    tablacov += "<td style=\"background-color: #c6d4e1 !important;\" ><input type='checkbox' onchange='changeState(this)' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                                }else {
                                    tablacov += "<td><input type='checkbox' onchange='changeState(this)' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                                }
                            }else{
                                if(j < (parseInt(monthStart)) || j > (parseInt(monthEnd))){
                                    tablacov += "<td style=\"background-color: #c6d4e1 !important;\"><input type='checkbox' disabled='disabled' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                                }else {
                                    tablacov += "<td><input type='checkbox' disabled='disabled' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                                }
                            }
                        }else{
                            jtLabel ="<?php echo JText::_('Close'); ?>";
                            if(j < (parseInt(monthStart)) || j > (parseInt(monthEnd))){
                                tablacov += "<td style=\"background-color: #c6d4e1 !important;\"><input type='checkbox' checked='checked' disabled='disabled' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                            }else {
                                tablacov += "<td><input type='checkbox' checked='checked' disabled='disabled' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                            }
                        }
                        flagDraw = true;
                    }
                }
                if(!flagDraw) {
                    jtLabel ="<?php echo JText::_('No close'); ?>";
                    if(j<=monthCurrent || j == 11){
                        if(j < (parseInt(monthStart)) || j > (parseInt(monthEnd))){
                            tablacov += "<td style=\"background-color: #c6d4e1 !important;\"><input type='checkbox' onchange='changeState(this)' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                        }else {
                            tablacov += "<td><input type='checkbox' onchange='changeState(this)' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                        }
                    }else{
                        if(j < (parseInt(monthStart)) || j >= (parseInt(monthEnd))){
                            tablacov += "<td style=\"background-color: #c6d4e1 !important;\"><input type='checkbox' disabled='disabled' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                        }else {
                            tablacov += "<td><input type='checkbox' disabled='disabled' id='ccbs"+j+"' value='cbs"+j+"'><label id='cbs"+j+"'>"+jtLabel+"</label></td>";
                        }
                    }
                }
            }
        }
        
        var conversion_currency = {};
        for (var i = 0; i < conversion.length; i++) {
            if(!conversion_currency.hasOwnProperty(conversion[i].id_currency)) {
                conversion_currency[conversion[i].id_currency] = [];
            }
            conversion_currency[conversion[i].id_currency].push(conversion[i]);
        }
        
        for (cc in conversion_currency) {
            if(conversion_currency[cc].length > 0) {
                antConversionValue = 0;
                tablacov += "<tr><th><?php echo JText::_('Value conversion'); ?> ("+conversion_currency[cc][0].currency_name+")</th>";
                for(var i=0; i</*12*/(lastMonth+1);i++) {

                    var element = jQuery.grep(conversion_currency[cc], function( n, index ) {
                        return n.month == i;
                    });
                    
                    if(element.length > 0) {
                        if(element[0].month == i) {
                            antConversionValue = element[0].conversion_value;
                            antConversionValue = colocarPuntosComa2(antConversionValue);
                            if(i < (parseInt(monthStart)) || i > (parseInt(monthEnd))){
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+antConversionValue+"</td>";
                            }else {
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';\">"+antConversionValue+"</td>";
                            }
                        }else {
                            if(i < (parseInt(monthStart)) || i > (parseInt(monthEnd))){
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+antConversionValue+"</td>";
                            }else {
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';\">"+antConversionValue+"</td>";
                            }
                        }
                    }else {
                        if(i < (parseInt(monthStart)) || i > (parseInt(monthEnd))){
                            tablacov += "<td style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+antConversionValue+"</td>";
                        }else {
                            tablacov += "<td style=\"mso-number-format:\'#,##0\';\">"+antConversionValue+"</td>";
                        }
                    }
                }
                tablacov += "</tr>";
            }
        }
        tablacov += "</table>";

        document.getElementById("conversion").innerHTML = tablacov;
    }

    var elem = "";
    function changeState(element){
        verificarStateMonth(element.value);
        ele = element;
    }

    function submitbutton(pressbutton){
        if(pressbutton=='savescorecard'){
            var saveData = document.getElementById("scorecard");
            saveData.value = recoverData();
            submitform(pressbutton);
        }else if(pressbutton == 'cancel'){
            submitform(pressbutton);
        }else if(pressbutton == 'printscorecard'){
            printTable('scorecardComplet');
        }else if(pressbutton == 'exportscorecard'){
            jQuery("#indirectExpenses").remove();
            document.getElementById("otro").value = document.getElementById("scorecardComplet").innerHTML;
            /*document.getElementById("otro").value = exportTabla(document.getElementById("cov"),"otro");
            document.getElementById("incom").value = exportTabla(document.getElementById("tableIncomes"),"");
            document.getElementById("directex").value = exportTabla(document.getElementById("de"),"");
            document.getElementById("indirectex").value = exportTabla(document.getElementById("ie"),"");
            document.getElementById("noime").value = exportTabla(document.getElementById("noi"),"margen");*/
            submitform(pressbutton);
        }
    }

    function printTable(obj) {
        var content = document.getElementById(obj).innerHTML;
        var newwin = window.open('');
        /*newwin.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"\n',
        '"http://www.w3.org/TR/html4/strict.dtd">\n',*/

        newwin.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"\n',
        '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n',
        '<html>\n',
        '<head>\n',
        '<title>Cuadro de mando</title>\n',
        '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">\n',
        '<link rel="stylesheet" href="templates/system/css/system.css" type="text/css">\n',
        '<link href="templates/khepri/css/template.css" rel="stylesheet" type="text/css">\n',
        '<link rel="stylesheet" type="text/css" href="templates/khepri/css/rounded.css">\n',
        '<link href="templates/khepri/css/general.css" rel="stylesheet" type="text/css">\n',
        '</head>\n',
        '<body>\n',
        '<div align="center"><img src="<?php echo JURI::root(true) . '/images/banners/bannerScorecard.jpg' ?>" border=0 alt="encabezado"/></div> \n',
        '<br><br>\n',
        ''+content+'\n',
        '</body>\n',
        '</html>');
        newwin.print();
        //newwin.close();
    }

    /*var rowg = 7;
    function formulaExcel(tablaform) {
        var tablaf = tablaform;//document.getElementById("tableIncomes");
        var rows = tablaf.rows.length;
        var cols = tablaf.rows[0].cells.length;
        var charCol = 65; //Ascii de la letra B
        var numRowEx = rowg; //primera fila donde hay una formula
        var rowMin = 10000;
        var colMin = 10000;
        var rowMax = 0;
        var colMax = 0;
        var iform = 1;
        var formula = '';
        var formula2 = '';
        var rowsDiff = new Array();
        for(var j=1; j<cols; j++) {
            for(var i=1; i<rows; i++) {
                if(tablaf.rows[i].onclick != null){
                    if(i < rowMin){
                        rowMin = i-1;
                    }
                    if(j < colMin) {
                        colMin = j;
                    }
                    if(i > rowMax){
                        rowMax = i-1;
                    }
                    if(j > colMax) {
                        colMax = j;
                    }
                    if(iform==1){
                        rowsDiff.push(rowMax);
                    }
                }
            }
            if(iform == 1) {
                formula += '=SUMA(';
                for(var h=0; h<rowsDiff.length; h++) {
                    if(h==(rowsDiff.length-1)) {
                        formula += String.fromCharCode(charCol+colMin)+(numRowEx+rowsDiff[h]).toString()+')';
                        formula2 = '=SUMA('+String.fromCharCode(charCol+colMin)+(numRowEx+rowsDiff[h]+1).toString()+':'+String.fromCharCode(charCol+colMax)+(numRowEx+(rows-1)-1).toString()+')';
                        //tablaf.rows[rowsDiff[h]+1].cells[j].setAttribute('formula',formula2);
                        tablaf.rows[rowsDiff[h]+1].cells[j].innerHTML = formula2;
                    }else{
                        formula += String.fromCharCode(charCol+colMin)+(numRowEx+rowsDiff[h]).toString()+';';
                        formula2 = '=SUMA('+String.fromCharCode(charCol+colMin)+(numRowEx+rowsDiff[h]+1).toString()+':'+String.fromCharCode(charCol+colMax)+(numRowEx+rowsDiff[h+1]-1).toString()+')';
                        //tablaf.rows[rowsDiff[h]+1].cells[j].setAttribute('formula',formula2);
                        tablaf.rows[rowsDiff[h]+1].cells[j].innerHTML = formula2;
                    }
                }
            }
            //tablaf.rows[iform].cells[j].setAttribute('formula',formula);
            tablaf.rows[iform].cells[j].innerHTML = formula;
            rowMin = 10000;
            colMin = 10000;
            rowMax = 0;
            colMax = 0;
            formula = '';
            formula2 = '';
            rowsDiff = new Array();
        }
        rowg = numRowEx + (rows-1) + 3;
    }*/
    var monthClose = ""
    function verificarStateMonth(valuemes){
        var month = valuemes[valuemes.length-1];
        var argsArray = new Array();
        monthClose = month;
        argsArray.push(newArg("monthState", month));
        argsArray.push(newArg("yearState", yearGlobal));
        argsArray.push(newArg("c", "report"));
        argsArray.push(newArg("option", "com_projectopen"));
        argsArray.push(newArg("task", "getStateValidationAjax"));
        var argsStr = argsArray.join("&");
        sendRequest("index.php", argsStr, validateStateMonth);
    }

    var datosExportCost = "";
    function validateStateMonth() {
        if (isValidResponse(this)) {
            var res = this.responseText;
            if (res != "") {
                var dataPendingMonth = res.split("&");
                var dataCost = JSON.parse(dataPendingMonth[0]);
                var dataProject = JSON.parse(dataPendingMonth[1]);
                var dataAbsence = JSON.parse(dataPendingMonth[2]);
                var isWithoutImpute = JSON.parse(dataPendingMonth[3]);

                if(parseInt(isWithoutImpute) == 1) {
                    //msj += "\n\nSi desea descargar un archivo con el reporte, presione 'Aceptar' de lo contrario presione 'Cancelar'.";
                    jQuery( "#dialog-detail-close-month" ).dialog( "option", "title", monthName[monthClose] );
                    jQuery( "#dialog-detail-close-month" ).dialog( "open" );
                    ele.checked = false;
                }else {
                    changeStatePost();
                }

                /*var arrayDisabled = new Array();
                var head = "<table border=1 class='adminlist' id='cost-table' >";
                head += "<tr><th><?php echo JText::_('Name'); ?></th><th><?php echo JText::_('Project'); ?></th><th><?php echo JText::_('Date'); ?></th></tr>";
                var msj = head;
                if(dataCost.length > 0) {
                    for(var i=0; i < dataCost.length; i++){
                        msj += "<tr><td>"+dataCost[i].name_user+"</td><td>"+dataCost[i].name_project+"</td><td>"+dataCost[i].date_pending+"</td></tr>";
                    }
                    msj += "</table>";
                    datosExportCost += msj;
                    jQuery("#tabs-1").html(msj);
                }else {
                    arrayDisabled.push(0);
                    jQuery( "#tabs" ).tabs( "option", "selected", 1 );
                }

                head = "<table border=1 class='adminlist' id='project-table' >";
                head += "<tr><th><?php echo JText::_('Name'); ?></th><th><?php echo JText::_('Project'); ?></th><th><?php echo JText::_('Date'); ?></th></tr>";
                msj = head;
                if(dataProject.length > 0) {
                    for(var i=0; i < dataProject.length; i++){
                        if(dataProject[i].date_pending != 0) {
                            msj += "<tr><td>"+dataProject[i].name_user+"</td><td>"+dataProject[i].name_project+"</td><td>"+dataProject[i].date_pending+"</td></tr>";
                        }
                    }
                    msj += "</table>";
                    datosExportCost += "<br/><br/>"+msj;
                    jQuery("#tabs-2").html(msj);
                }else {
                    arrayDisabled.push(1);
                    jQuery( "#tabs" ).tabs( "option", "selected", 2 );
                }

                head = "<table border=1 class='adminlist' id='absence-table' >";
                head += "<tr><th><?php echo JText::_('Name'); ?></th><th><?php echo JText::_('Absence'); ?></th><th><?php echo JText::_('Date'); ?></th></tr>";
                msj = head;
                if(dataAbsence.length > 0) {
                    for(var i=0; i < dataAbsence.length; i++){
                        if(dataAbsence[i].date_pending != 0) {
                            msj += "<tr><td>"+dataAbsence[i].name_user+"</td><td>"+dataAbsence[i].name_absence+"</td><td>"+dataAbsence[i].date_pending+"</td></tr>";
                        }
                    }
                    msj += "</table>";
                    datosExportCost += "<br/><br/>"+msj;
                    jQuery("#tabs-3").html(msj);
                }else {
                    arrayDisabled.push(2);
                    jQuery( "#tabs" ).tabs( "option", "selected", 3 );
                 }
                 jQuery( "#tabs" ).tabs( "option", "disabled", arrayDisabled );

                if(dataCost.length > 0 || dataProject.length > 0 || dataAbsence.length > 0) {
                    msj += "\n\nSi desea descargar un archivo con el reporte, presione 'Aceptar' de lo contrario presione 'Cancelar'.";
                    jQuery( "#dialog-detail-close-month" ).dialog( "option", "title", monthName[monthClose] );
                    jQuery( "#dialog-detail-close-month" ).dialog( "open" );
                    ele.checked = false;
                }else {
                    changeStatePost();
                }*/
            }
        }
    }

    function changeStatePost(){
        var lab = document.getElementById(ele.value);
        var jtLabel;
        if(ele.checked== true){
            jtLabel ="<?php echo JText::_('Close'); ?>";
        }else{
            jtLabel ="<?php echo JText::_('No close'); ?>";
        }
        lab.textContent = jtLabel;
    }

    var col = new Array();
    var fil = new Array();

    function drawInternalCost(internal_cost) {

        var idabsence = 0;
        var count = 0;
        var i = 0;
        var j = 0;
        var filtemp = 0;
        var tablade = "<table border=1 class='adminlist' id='ic' >";

        //Resume the months cols to print
        var lastMonth = monthIndexLimit;

        tablade += drawHeadScorecard(yearGlobal,lastMonth);
        //tablade += drawTotalScorecard("<?php echo JText::_('Internal cost'); ?>");
        tablade += drawInternalCostExpenses(lastMonth);
        
        for(i=0; i<internal_cost.length; i++) {

            if(idabsence != internal_cost[i].absence_id){
                if(i!=0) {
                    if(count<=/*12*/(lastMonth+1)) {
                        for(var h=0;h<(/*12*/(lastMonth+1) - count);h++) {
                            if((count+h) < (parseInt(monthStart)) || (count+h) > (parseInt(monthEnd))){
                                tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                            }else {
                                tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                            }
                        }
                        tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                        tablade += "</tr>";
                        count = 0;
                        j = 0;
                    }
                }
                while(internal_cost[i].absence_id == 2 || internal_cost[i].absence_id == 4) {
                    i++;
                    if(i == internal_cost.length) {
                        count = /*13*/(lastMonth+2);
                        break;
                    }
                }
                if(i == internal_cost.length) {
                    count = /*13*/(lastMonth+2);
                    break;
                }
                tablade += "<tr style='display: none;' id="+internal_cost[i].absence_id+" >";
                tablade += "<td>"+internal_cost[i].absence_name+"</td>";
                idabsence = internal_cost[i].absence_id;
                filtemp++;
            }

            if(internal_cost[i].month <= lastMonth){

                while(internal_cost[i].month != month[j] ){
                    if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                    j++;
                    count++
                }

                if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">"+Math.round(parseFloat((internal_cost[i].internal_cost == null) ? 0 : internal_cost[i].internal_cost))+"</td>";
                }

                count++;
                j++;
            }
        }

        if(internal_cost.length != 0){
            if(count<=/*12*/(lastMonth+1)) {
                for(var h=0;h<(/*12*/(lastMonth+1) - count);h++) {
                    if((count+h) < (parseInt(monthStart)) || (count+h) > (parseInt(/*monthEnd*/lastMonth))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                }
                tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                tablade += "</tr>";
                count = 0;
                j = 0;
            }
        }

        idabsence=0;
        count = 0;
        i = 0;
        j = 0;

        for(i=0; i<internal_cost.length; i++) {
            if(idabsence != internal_cost[i].absence_id){
                if(i!=0) {
                    if(count<=/*12*/(lastMonth+1)) {
                        for(var h=0;h<(/*12*/(lastMonth+1) - count);h++) {
                            if((count+h) < (parseInt(monthStart)) || (count+h) > (parseInt(/*monthEnd*/lastMonth))){
                                tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                            }else {
                                tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                            }
                        }
                        tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                        tablade += "</tr>";
                        count = 0;
                        j = 0;
                    }
                }
                while(internal_cost[i].absence_id != 2 && internal_cost[i].absence_id != 4) {
                    i++;
                    if(i == internal_cost.length) {
                        count = /*13*/(lastMonth+2);
                        break;
                    }
                }
                if(i == internal_cost.length) {
                    count = /*13*/(lastMonth+2);
                    break;
                }
                /*if(internal_cost[i].absence_id == 4) {
                    tablade += "<tr onclick='vacationUser()' style='display: none;' id="+internal_cost[i].absence_id+" >";
                }else {
                    tablade += "<tr style='display: none;' id="+internal_cost[i].absence_id+" >";
                }*/
                tablade += "<tr style='display: none;' id="+internal_cost[i].absence_id+" >";
                tablade += "<td>"+internal_cost[i].absence_name+"</td>";
                idabsence = internal_cost[i].absence_id;
                filtemp++;
            }

            if(internal_cost[i].month <= lastMonth){
                while(internal_cost[i].month != month[j]  && j < (lastMonth+1) ){
                    if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                    j++;
                    count++
                }

                if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                    tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                }else {
                    tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                }

                count++;
                j++;
                fil.push((filtemp+1));
                col.push((count));
            }
        }

        if(internal_cost.length != 0){
            if(count<=/*12*/(lastMonth+1)) {
                for(var h=0;h<(/*12*/(lastMonth+1)-count);h++) {
                    if((count+h) < (parseInt(monthStart)) || (count+h) > (parseInt(monthEnd))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                }
                tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                tablade += "</tr>";
                count = 0;
                j = 0;
            }
        }

        tablade += "</table>";

        document.getElementById("internalCost").innerHTML = tablade;
    }

    function drawInternalCostExpenses(monthIndex) {
        var rowsAbsence = "";
        rowsAbsence += "<tr id='c1' class='trClientReport' onclick='desClient(1, \"ic\")'><th><img id='imgDes' src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>'><?php echo JText::_('Internal cost'); ?></th>";
        for(i=0;i <= monthIndex;i++)
            rowsAbsence += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";

        rowsAbsence += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th></tr>";

        return rowsAbsence;
    }

    function sumInternalCost(tablaSum){
        var rows = tablaSum.rows.length;
        var cols = tablaSum.rows[0].cells.length;

        for(var i=2; i<rows; i++){
            if(tablaSum.rows[i].getAttribute("class") != null){
                var j=i+1;
                var sumyear=0;
                var summonth = new Array(0,0,0,0,0,0,0,0,0,0,0,0);

                if(j != rows){
                    while(tablaSum.rows[j].getAttribute("class") == null){
                        var h=0;
                        for(var k=1; k<(cols-1); k++){
                            sumyear = sumyear + parseInt(tablaSum.rows[j].cells[k].innerHTML);
                            summonth[h] = summonth[h] + parseInt(tablaSum.rows[j].cells[k].innerHTML);
                            h++;
                        }
                        j++;
                        if(tablaSum.rows[j]==undefined){
                            break;
                        }
                    }

                    tablaSum.rows[i].cells[(cols-1)].innerHTML = sumyear;
                    var posSum=0;
                    for(var p=1; p<(cols-1); p++) {
                        tablaSum.rows[i].cells[p].innerHTML = summonth[posSum];
                        posSum++;
                    }
                    i=j-1;
                }
            }
        }
    }

    //Otros gastos de explotacion
    function drawOtherOperatingExpensesHead(monthIndex) {
        var rowsAbsenceCero = "";
        rowsAbsenceCero += "<tr class='trClientReport' id='c2' onclick='desClient(2, \"ooe\")'><th><img id='imgDes' src='<?php echo JURI::root(true) . '/templates/newshore_intranet/images/j_arrow.png' ?>'><?php echo JText::_('Other Operating Expenses'); ?></th>";
        /*rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\" >0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";
        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";*/
        for(i=0;i <= monthIndex;i++)
            rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th>";

        rowsAbsenceCero += "<th formula='' align='right' style=\"mso-number-format:\'#,##0\';\">0</th></tr>";
        return rowsAbsenceCero;
    }

    function drawOtherOperatingExpenses(other_operating_expenses) {
        var idabsence = 0;
        var count = 0;
        var i = 0;
        var j = 0;
        var filtemp = 0;
        var tablade = "<table border=1 class='adminlist' id='ooe' >";
        //Resume the months cols to print
        var lastMonth = monthIndexLimit;

        tablade += drawHeadScorecard(yearGlobal,isNaN(lastMonth)?0:lastMonth);
        tablade += drawOtherOperatingExpensesHead(isNaN(lastMonth)?0:lastMonth);

        for(i=0; i<other_operating_expenses.length; i++) {
            if(idabsence != other_operating_expenses[i].absence_id){
                if(i!=0) {
                    if(count<=/*12*/(lastMonth+1)) {
                        for(var h=0;h<(/*12*/(lastMonth+1) - count);h++) {
                            if((count+h) < (parseInt(monthStart)) || (count+h) > (parseInt(monthEnd))){
                                tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                            }else {
                                tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                            }
                        }
                        tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                        tablade += "</tr>";
                        count = 0;
                        j = 0;
                    }
                }
                if(i == other_operating_expenses.length) {
                    count = /*13*/(lastMonth+2);
                    break;
                }

                tablade += "<tr style='display: none;' id="+other_operating_expenses[i].absence_id+" >";
                tablade += "<td>"+other_operating_expenses[i].absence_name+"</td>";
                idabsence = other_operating_expenses[i].absence_id;
                filtemp++;
            }

            if(other_operating_expenses[i].month <= lastMonth){
                while(other_operating_expenses[i].month != month[j]){
                    if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                    j++;
                    count++
                }
                if(count < (parseInt(monthStart)) || count > (parseInt(monthEnd))){
                    tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                }else {
                    tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">"+Math.round(parseFloat((other_operating_expenses[i].internal_cost == null) ? 0 : other_operating_expenses[i].internal_cost))+"</td>";
                }
                count++;
                j++;
            }
        }
        if(other_operating_expenses.length != 0){
            if(count<=/*12*/(lastMonth+1)) {
                for(var h=0;h<(/*12*/(lastMonth+1) - count);h++) {
                    if((count+h) < (parseInt(monthStart)) || (count+h) > (parseInt(monthEnd))){
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">0.00</td>";
                    }else {
                        tablade += "<td align='right' style=\"mso-number-format:\'#,##0\';\">0.00</td>";
                    }
                }
                tablade += "<th  align='right' style=\"mso-number-format:\'#,##0\';\">calc_total</th>";
                tablade += "</tr>";
                count = 0;
                j = 0;
            }
        }

        tablade += "</table>";

        document.getElementById("otherOperatingExpenses").innerHTML = tablade;
    }

    //Dialog modal vacaciones
    jQuery(function() {
        jQuery( "#dialog-detail" ).dialog({
            modal: true,
            autoOpen: false,
            height: 600,
            width: '50%',
            buttons: {
                Exportar: function() {
                    document.getElementById("datos").value = document.getElementById("dialog-detail").innerHTML;
                    submitform("exportsreport");
                },
                Ok: function() {
                    jQuery( this ).dialog( "close" );
                }
            }
        });

        jQuery( "#dialog-detail-close-month" ).dialog({
            modal: true,
            autoOpen: false,
            height: 200,
            width: '50%',
            buttons: {
                /*Exportar: function() {
                    document.getElementById("datos").value = "<div>"+datosExportCost+"</div>";
                    submitform("exportsreport");
                },*/
                Ok: function() {
                    jQuery( this ).dialog( "close" );
                }
            }
        });

        //jQuery( "#tabs" ).tabs();
    });

    function usersDetail(idAbsence, absenceName) {
        var dataRes = jQuery.ajax({
            url: "index.php?option=com_projectopen&task=getUsersDetail&c=report",
            global: false,
            type: "POST",
            data: ({yearv: yearGlobal, idAbsence: idAbsence}),
            async: false,
            success: function(msg){
                //alert(msg);
            }
        }).responseText;
        var usersDetail = JSON.parse(dataRes);
        jQuery( "#dialog-detail" ).dialog( "option", "title", absenceName );
        jQuery( "#dialog-detail" ).dialog( "open" );

        var tableDetail = "<table border=1 class='adminlist' id='usersVacation' >";
        tableDetail += "<tr><th><?php echo JText::_('User'); ?></th><th><?php echo JText::_('Month'); ?></th><th><?php echo JText::_('Hours'); ?></th></tr>";
        for(var i=0; i<usersDetail.length; i++) {
            tableDetail += "<tr><td>"+usersDetail[i].name+"</td><td>"+monthName[usersDetail[i].month]+"</td><td>"+usersDetail[i].hours_total+"</td></tr>";
        }
        tableDetail += "</table>";
        document.getElementById("dialog-detail").innerHTML = tableDetail;
    }

    function costDetail(idProject, projectName) {
        var dataRes = jQuery.ajax({
            url: "index.php?option=com_projectopen&task=getCostDetail&c=report",
            global: false,
            type: "POST",
            data: ({yearv: yearGlobal, idProject: idProject}),
            async: false,
            success: function(msg){
                //alert(msg);
            }
        }).responseText;
        var costDetail = JSON.parse(dataRes);
        jQuery( "#dialog-detail" ).dialog( "option", "title", projectName );
        jQuery( "#dialog-detail" ).dialog( "open" );

        var name_type = "";//((costDetail.length > 0) ? costDetail[0].name : "");
        var fila = -1;
        var costProject = new Array();
        var nameCost = new Array();
        for(var i=0; i<costDetail.length; i++) {
            if(name_type == costDetail[i].name) {
                for(var m=0; m<12; m++) {
                    if(m == costDetail[i].month_start){
                        if(costDetail[i].id_currency == 1) {//Euros
                            costProject[fila][m] += parseFloat(costDetail[i].amount);
                        }else if(costDetail[i].id_currency == 2){ //Pesos colombianos
                            costProject[fila][m] += (parseFloat(costDetail[i].amount)/parseFloat(((m < conversion_value.length) ? conversion_value[m].conversion_value : conversion_value[(conversion_value.length)-1].conversion_value)));
                        }
                    }
                }
                name_type = costDetail[i].name;
            }else {
                fila++;
                nameCost.push(costDetail[i].name);
                costProject[fila] = [0,0,0,0,0,0,0,0,0,0,0,0];
                for(var m=0; m<12; m++) {
                    if(m == costDetail[i].month_start){
                        if(costDetail[i].id_currency == 1) {//Euros
                            costProject[fila][m] += parseFloat(costDetail[i].amount);
                        }else if(costDetail[i].id_currency == 2){ //Pesos colombianos
                            costProject[fila][m] += (parseFloat(costDetail[i].amount)/parseFloat(((m < conversion_value.length) ? conversion_value[m].conversion_value : conversion_value[(conversion_value.length)-1].conversion_value)));
                        }

                    }
                }
                name_type = costDetail[i].name;
            }
        }
        var tableDetail = "<table border=1 class='adminlist' id='usersVacation' >";
        tableDetail += "<tr><th><?php echo JText::_('Type Cost'); ?></th><th><?php echo JText::_('Month'); ?></th><th><?php echo JText::_('Cost'); ?></th></tr>";
        for(var c=0; c<nameCost.length; c++) {
            for(var m=0; m<12; m++) {
                if(costProject[c][m] != 0) {
                    tableDetail += "<tr><td>"+nameCost[c]+"</td><td>"+monthName[m]+"</td><td>"+Math.round(costProject[c][m])+" €</td></tr>";
                }
            }
        }
        tableDetail += "</table>";
        document.getElementById("dialog-detail").innerHTML = tableDetail;
    }

    function rowsSpanTable(idTable) {
        var contCell = ""
        var count = 1;
        var table = document.getElementById(idTable);
        var rows = table.rows.length;
        var cols = table.rows[0].cells.length;
        for(var j=0; j < cols; j++) {
            for(var i=2; i < rows; i++) {
                if(table.rows[i].cells[j].innerHtml == table.rows[(i-1)].cells[j].innerHtml){
                    count++;
                    table.rows[i].cells[j].remove = 1;
                }else {
                    table.rows[(i-1)].cells[j].setAttribute("rowspan", count);
                    count = 1;
                }
            }
        }
        alert(jQuery("#"+idTable).find("[remove*=1]").length);
        jQuery("#"+idTable).find(["remove*=1"]).remove();
    }
</script>
<form action="index.php" method="post" name="adminForm">
    <fieldset class="adminform" id="scorecardComplet">
<!--        <div id="issueDate">
            <label style="color: #0B55C4; font-size: 14px;"><strong>Fecha de emisi&oacute;n: &nbsp;</strong></label>
            <?php //echo date("Y-m-d"); ?>
        </div>-->

    <table id="headerReport" border="0" cellspacing="0" class="headerreport" style="width: 38%;">
          <tr>
            <td class="key">Fecha de emisi&oacute;n: &nbsp;</td>
            <td><?php echo date("Y-m-d"); ?></td>
        </tr>
        <tr>
            <td class="key">Año: </td>
            <td><?php echo ($this->scorecard_yearReport != 0) ? $this->scorecard_yearReport : $this->yearReport; ?></td>
        </tr>
        <tr>
            <td class="key">Mes inicial:</td>
            <td><?php echo $this->start_monthReport; ?></td>
        </tr>
        <tr>
            <td class="key">Mes final:</td>
            <td><?php echo $this->end_monthReport; ?></td>
        </tr>
        <?php if (isset($this->clientReport) && $this->clientReport != null){ ?>
        <tr>
            <td class="key">Cliente:</td>
            <td><?php echo $this->clientReport; ?></td>
        </tr>
        <?php } ?>
        <?php if (isset($this->projectReport) && $this->projectReport != null){ ?>
        <tr>
            <td class="key">Proyecto:</td>
            <td><?php echo $this->projectReport; ?></td>
        </tr>
        <?php } ?>
</table>

        <br>
        <br>
        <div id="conversion">
        </div>
        <!--<br>
        <br>-->
        <div id="incomes" style="display:none;">
        </div>
        <br>
        <br>
        <div id="directExpenses">
        </div>
        <!--<br>
        <br>-->
        <div id="indirectExpenses" style="display:none;">
        </div>
        <!--<br>
        <br>-->
        <div id="netOperatingIncome" style="display:none;">
        </div>
        <br>
        <br>
        <div id="internalCost">
        </div>
        <br>
        <br>
        <div id="otherOperatingExpenses">
        </div>
    </fieldset>

    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="task" value="reportScorecard" />
    <input type="hidden" id="scorecard" name="scorecard" value="" />
    <input type="hidden" id="otro" name="otro" value="" />
    <input type="hidden" id="incom" name="incom" value="" />
    <input type="hidden" id="directex" name="directex" value="" />
    <input type="hidden" id="indirectex" name="indirectex" value="" />
    <input type="hidden" id="noime" name="noime" value="" />
    <input type="hidden" id="year" name="year" value="<?php echo JRequest::getVar("year"); ?>" />
    <input type="hidden" id="start_month" name="start_month" value="<?php echo JRequest::getVar("start_month"); ?>" />
    <input type="hidden" id="end_month" name="end_month" value="<?php echo JRequest::getVar("end_month"); ?>" />
    <input type="hidden" id="scorecard_year" name="scorecard_year" value="<?php echo JRequest::getVar("scorecard_year"); ?>" />
    <input type="hidden" id="txt" name="txt" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>

<!-- Detalle vacaciones -->
<div id="dialog-detail" title="Detalles Internos">
</div>
<div id="dialog-detail-close-month" title="Alerta">
    <p><?php echo JText::_('Hours are outstanding imputed or validate'); ?>:</p>
    <p><?php echo JText::_('To check that people impute missing or validate their hours, please go to report'); ?> <a class="link" href="index.php?option=com_projectopen&c=report&task=viewReports"><?php echo JText::_('Pending complaints'); ?></a></p>

    <!--<div id="tabs">
    	<ul>
    		<li><a href="#tabs-1"><?php echo JText::_("Cost Indirect"); ?></a></li>
    		<li><a href="#tabs-2"><?php echo JText::_("Hours Project"); ?></a></li>
    		<li><a href="#tabs-3"><?php echo JText::_("Hours Absence"); ?></a></li>
    	</ul>
    	<div id="tabs-1">

    	</div>
    	<div id="tabs-2">

    	</div>
    	<div id="tabs-3">

    	</div>
    </div>-->
</div>
