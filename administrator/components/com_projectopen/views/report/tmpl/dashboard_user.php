<?php defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar'); ?>
<!--Reporte de imputacion de horas por usuario -->
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/ajax/ajax.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopentransaction.js' ?>'></script>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopendraw.js' ?>'></script>
<form action="index.php" method="post" name="adminForm">
    <table class="admintable" id="tableSelectReportImpute">
        <tr>
            <td  class="key"><label for="user"><?php echo JText::_('User'); ?> <label style='color:#FF0000' >*</label></label></td>
            <td><?php echo JHTML::_('select.genericlist', $this->userNewshore, 'user', 'class="inputbox" size="1" style="width:200px" ', 'id', 'name', 0); ?></td>
        </tr>
        <tr>
            <td class="key">
                <label><?php echo JText::_('Year'); ?> <span style='color:#FF0000' >*</span></label>
            </td>
            <td>
                <select name="year" id="year">
                    <?php for ($i = 2014; $i <= date("Y"); $i++) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td  class="key">
                <label><?php echo JText::_('Start Month'); ?></label>
            </td>
            <td>
                <select name="start_month" id="start_month" onchange="validMonthEnd()" >
                    <?php
                    for ($i = 0; $i < count($this->months); $i++) {
                        echo "<option value='" . $i . "'>" . $this->months[$i] . "</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td  class="key">
                <label id="endMonth"><?php echo JText::_('End Month'); ?></label>
            </td>
            <td>
                <select name="end_month" id="end_month" onchange="validMonthStart()" >
                    <?php
                    for ($i = 0; $i < count($this->months); $i++) {
                        echo "<option value='" . $i . "' ".(($i==(count($this->months)-1)) ? "selected" : "").">" . $this->months[$i] . "</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <div id="load" style="display: none;"><img border="0" src="<?php echo JURI::root(true) ?>/images/loading.gif" /></div>
    <div id="usersHours" style="display: none;">
        <div id="conversion">
        </div>

        <br><br>

        <table border="1" class="adminlist">
            <thead>
                <tr style="background-color: #F0F0F0;">
                    <th></th>
                    <th>Enero <span class="yeard"></span></th>
                    <th>Febrero <span class="yeard"></span></th>
                    <th>Marzo <span class="yeard"></span></th>
                    <th>Abril <span class="yeard"></span></th>
                    <th>Mayo <span class="yeard"></span></th>
                    <th>Junio <span class="yeard"></span></th>
                    <th>Julio <span class="yeard"></span></th>
                    <th>Agosto <span class="yeard"></span></th>
                    <th>Septiembre <span class="yeard"></span></th>
                    <th>Octubre <span class="yeard"></span></th>
                    <th>Noviembre <span class="yeard"></span></th>
                    <th>Diciembre <span class="yeard"></span></th>
                    <th>TOTAL</span></th>
                </tr>
                <tr>
                    <th>Gastos Directos</th>
                    <td id="gdAcumMonth0"></td>
                    <td id="gdAcumMonth1"></td>
                    <td id="gdAcumMonth2"></td>
                    <td id="gdAcumMonth3"></td>
                    <td id="gdAcumMonth4"></td>
                    <td id="gdAcumMonth5"></td>
                    <td id="gdAcumMonth6"></td>
                    <td id="gdAcumMonth7"></td>
                    <td id="gdAcumMonth8"></td>
                    <td id="gdAcumMonth9"></td>
                    <td id="gdAcumMonth10"></td>
                    <td id="gdAcumMonth11"></td>
                    <th id="gdAcumTotal"></th>
                </tr>
            </thead>
            <tbody id="datauser">
            </tbody>
        </table>

        <br><br>

        <table border="1" class="adminlist">
            <thead>
                <tr style="background-color: #F0F0F0;">
                    <th></th>
                    <th>Enero <span class="yeard"></span></th>
                    <th>Febrero <span class="yeard"></span></th>
                    <th>Marzo <span class="yeard"></span></th>
                    <th>Abril <span class="yeard"></span></th>
                    <th>Mayo <span class="yeard"></span></th>
                    <th>Junio <span class="yeard"></span></th>
                    <th>Julio <span class="yeard"></span></th>
                    <th>Agosto <span class="yeard"></span></th>
                    <th>Septiembre <span class="yeard"></span></th>
                    <th>Octubre <span class="yeard"></span></th>
                    <th>Noviembre <span class="yeard"></span></th>
                    <th>Diciembre <span class="yeard"></span></th>
                    <th>TOTAL</span></th>
                </tr>
                <tr>
                    <th>Coste Interno</th>
                    <td id="ciAcumMonth0"></td>
                    <td id="ciAcumMonth1"></td>
                    <td id="ciAcumMonth2"></td>
                    <td id="ciAcumMonth3"></td>
                    <td id="ciAcumMonth4"></td>
                    <td id="ciAcumMonth5"></td>
                    <td id="ciAcumMonth6"></td>
                    <td id="ciAcumMonth7"></td>
                    <td id="ciAcumMonth8"></td>
                    <td id="ciAcumMonth9"></td>
                    <td id="ciAcumMonth10"></td>
                    <td id="ciAcumMonth11"></td>
                    <th id="ciAcumTotal"></th>
                </tr>
            </thead>
            <tbody id="datauser_indirect">
            </tbody>
        </table>

        <br><br>

        <table border="1" class="adminlist">
            <thead>
                <tr style="background-color: #F0F0F0;">
                    <th></th>
                    <th>Enero <span class="yeard"></span></th>
                    <th>Febrero <span class="yeard"></span></th>
                    <th>Marzo <span class="yeard"></span></th>
                    <th>Abril <span class="yeard"></span></th>
                    <th>Mayo <span class="yeard"></span></th>
                    <th>Junio <span class="yeard"></span></th>
                    <th>Julio <span class="yeard"></span></th>
                    <th>Agosto <span class="yeard"></span></th>
                    <th>Septiembre <span class="yeard"></span></th>
                    <th>Octubre <span class="yeard"></span></th>
                    <th>Noviembre <span class="yeard"></span></th>
                    <th>Diciembre <span class="yeard"></span></th>
                    <th>TOTAL</span></th>
                </tr>
            </thead>
            <tbody id="datauser_other">
            </tbody>
        </table>
    </div>

    <input type="hidden" name="c" value="report" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" id="usersSelected" name="usersSelected" value="" />
    <input type="hidden" id="datos" name="datos" value="" />
    <input type="hidden" id="datosAdd" name="datosAdd" value="" />
    <input type="hidden" id="datosDel" name="datosDel" value="" />
    <?php echo JHTML::_('form.token'); ?>
</form>
<script type="text/javascript" language="javascript">
    jQuery.noConflict();

    var monthName = new Array("<?php echo JText::_('January'); ?>", "<?php echo JText::_('February'); ?>", "<?php echo JText::_('March'); ?>", "<?php echo JText::_('April'); ?>","<?php echo JText::_('May'); ?>","<?php echo JText::_('June'); ?>", "<?php echo JText::_('July'); ?>", "<?php echo JText::_('August'); ?>","<?php echo JText::_('September'); ?>","<?php echo JText::_('October'); ?>","<?php echo JText::_('November'); ?>","<?php echo JText::_('December'); ?>");
    var monthStart = -1;
    var monthEnd = -1;

    window.onload= function(){

    }

    var dataUsers;

    function searchUsers(){
        jQuery('#usersHours').hide('fast');
        jQuery("#load").show();
        var user = jQuery("#user").val();
        var year = jQuery('#year').val();
        var start_month = jQuery('#start_month').val();
        var end_month = jQuery('#end_month').val();
        var con = false;

        if(parseInt(user) == 0){
            alert('<?php echo JText::_('Seleciones un usuario'); ?>');
        } else {
            var idUser = user;

            var argsArray = new Array();
            argsArray.push(newArg("userId", idUser));
            argsArray.push(newArg("year", year));
            argsArray.push(newArg("start_month", start_month));
            argsArray.push(newArg("end_month", end_month));
            argsArray.push(newArg("c", "report"));
            argsArray.push(newArg("option", "com_projectopen"));
            argsArray.push(newArg("task", "getDashboardUser"));
            var argsStr = argsArray.join("&");
            sendRequest("index.php", argsStr, drawTableUsers);
        }
    }

    //Report Impute
    function drawTableUsers(){
        if (isValidResponse(this)) {
            var res = this.responseText;
            jQuery("#load").hide();
            if (res != "") {
                jQuery('#datauser').html('');
                jQuery('#datauser_indirect').html('');
                jQuery('#datauser_other').html('');
                dashboard_user = JSON.parse(res);
                console.log(dashboard_user);
                jQuery('.yeard').html(jQuery('#year').val());
                drawDirectExpencesUser(dashboard_user['direct_cost']);
                drawIndirectExpencesUser(dashboard_user['indirect_cost']);
                drawOtherExpencesUser(dashboard_user['operating_expenses']);
                drawOthers(dashboard_user['conversion'], 11);
                jQuery('#usersHours').show('fast');
            }
        }
    }

    function drawDirectExpencesUser(direct_cost) {
        var count = 0;
        var rows = '';
        var acum = 0;
        var id_control = 0;
        var acum_month = [0,0,0,0,0,0,0,0,0,0,0,0];

        if (direct_cost.length == 0) {
            jQuery('#datauser').parent('table').hide();
        } else {
            jQuery('#datauser').parent('table').show();
        }

        for (var i = 0; i < direct_cost.length; i++) {
            if(id_control != direct_cost[i].project_id) {
                id_control = direct_cost[i].project_id;

                if (count > 0 && count <= 11) {
                    for (var j = count; j <= 11; j++) {
                        rows += '<td></td>';
                    }
                }

                if (count > 0) {
                    rows += '<th>';
                    rows += colocarPuntosComa2(acum);
                    rows += '</th>';
                    acum = 0;
                    count = 0;
                }

                if (i > 0) {
                    rows += '</tr>';
                }
                rows += '<tr>';
                rows += '<th>';
                rows += direct_cost[i].project_name;
                rows += '</th>';
            }

            if (count != parseInt(direct_cost[i].month)) {
                for (var h = count; h < parseInt(direct_cost[i].month); h++) {
                    rows += '<td></td>';
                    count++;
                }
            }

            rows += '<td>';
            rows += colocarPuntosComa2(direct_cost[i].direct_expenses);
            rows += '</td>';
            acum += parseFloat(direct_cost[i].direct_expenses);
            acum_month[parseInt(direct_cost[i].month)] += parseFloat(direct_cost[i].direct_expenses);
            count++;
        }

        if (count > 0 && count <= 11) {
            for (var i = count; i <= 11; i++) {
                rows += '<td></td>';
                count++;
            }
        }
        rows += '<th>';
        rows += colocarPuntosComa2(acum);
        rows += '</th>';
        rows += '</tr>';

        var acumTotal = 0;
        for (var i = 0; i < acum_month.length; i++) {
            acumTotal += acum_month[i];
            jQuery('#gdAcumMonth'+i).html(colocarPuntosComa2(acum_month[i]));
        }

        jQuery('#gdAcumTotal').html(colocarPuntosComa2(acumTotal));

        jQuery('#datauser').append(rows);
    }

    function drawIndirectExpencesUser(indirect_cost) {
        var count = 0;
        var rows = '';
        var acum = 0;
        var id_control = 0;
        var acum_month = [0,0,0,0,0,0,0,0,0,0,0,0];

        if (indirect_cost.length == 0) {
            jQuery('#datauser_indirect').parent('table').hide();
        } else {
            jQuery('#datauser_indirect').parent('table').show();
        }

        for (var i = 0; i < indirect_cost.length; i++) {
            var internal_cost = (parseInt(indirect_cost[i].absence_id) == 2 || parseInt(indirect_cost[i].absence_id) == 4) ? 0 : indirect_cost[i].internal_cost;
            internal_cost = (parseInt(indirect_cost[i].absence_id) == 9) ? (internal_cost * 0.30) : internal_cost;
            if(id_control != indirect_cost[i].absence_id) {
                id_control = indirect_cost[i].absence_id;

                if (count > 0 && count <= 11) {
                    for (var j = count; j <= 11; j++) {
                        rows += '<td></td>';
                    }
                }

                if (count > 0) {
                    rows += '<th>';
                    rows += colocarPuntosComa2(acum);
                    rows += '</th>';
                    acum = 0;
                    count = 0;
                }

                if (i > 0) {
                    rows += '</tr>';
                }
                rows += '<tr>';
                rows += '<th>';
                rows += indirect_cost[i].absence_name;
                rows += '</th>';
            }

            if (count != parseInt(indirect_cost[i].month)) {
                for (var h = count; h < parseInt(indirect_cost[i].month); h++) {
                    rows += '<td></td>';
                    count++;
                }
            }

            rows += '<td>';
            rows += colocarPuntosComa2(internal_cost);
            rows += '</td>';
            acum += parseFloat(internal_cost);
            acum_month[parseInt(indirect_cost[i].month)] += parseFloat(internal_cost);
            count++;
        }

        if (count > 0 && count <= 11) {
            for (var i = count; i <= 11; i++) {
                rows += '<td></td>';
                count++;
            }
        }
        rows += '<th>';
        rows += colocarPuntosComa2(acum);
        rows += '</th>';
        rows += '</tr>';

        var acumTotal = 0;
        for (var i = 0; i < acum_month.length; i++) {
            acumTotal += acum_month[i];
            jQuery('#ciAcumMonth'+i).html(colocarPuntosComa2(acum_month[i]));
        }

        jQuery('#ciAcumTotal').html(colocarPuntosComa2(acumTotal));

        jQuery('#datauser_indirect').append(rows);
    }

    function drawOtherExpencesUser(operating_expenses) {
        var count = 0;
        var rows = '';
        var acum = 0;
        var id_control = 0;

        if (operating_expenses.length == 0) {
            jQuery('#datauser_other').parent('table').hide();
        } else {
            jQuery('#datauser_other').parent('table').show();
        }

        for (var i = 0; i < operating_expenses.length; i++) {
            if(id_control != operating_expenses[i].absence_id) {
                id_control = operating_expenses[i].absence_id;

                if (count > 0 && count < 11) {
                    for (var j = count; j <= 11; j++) {
                        rows += '<td></td>';
                    }
                }

                if (count > 0) {
                    rows += '<th>';
                    rows += colocarPuntosComa2(acum);
                    rows += '</th>';
                    acum = 0;
                    count = 0;
                }

                if (i > 0) {
                    rows += '</tr>';
                }
                rows += '<tr>';
                rows += '<th>';
                rows += operating_expenses[i].absence_name;
                rows += '</th>';
            }

            if (count != parseInt(operating_expenses[i].month)) {
                for (var h = count; h < parseInt(operating_expenses[i].month); h++) {
                    rows += '<td></td>';
                    count++;
                }
            }

            rows += '<td>';
            rows += colocarPuntosComa2(operating_expenses[i].internal_cost);
            rows += '</td>';
            acum += parseFloat(operating_expenses[i].internal_cost);
            count++;
        }

        if (count > 0 && count != 11) {
            for (var i = count; i < 11; i++) {
                rows += '<td></td>';
                count++;
            }
        }
        rows += '<th>';
        rows += colocarPuntosComa2(acum);
        rows += '</th>';
        rows += '</tr>';
        jQuery('#datauser_other').append(rows);
    }

    function drawOthers(conversion, end_month) {
        var antConversionValue = 0
        var tablacov = "<table border=1 class='adminlist' id='cov' ><thead>";
        tablacov += "<tr style='background-color: #F0F0F0;'><th formula='eliecer' id='vacia'></th>";
        var lastMonth = end_month;

        for(i=0;i<=lastMonth;i++) {
            tablacov += "<th id='month'>"+monthName[i]+"-"+conversion[0].year+"</th>";
        }

        tablacov += "</tr></thead>"

        var conversion_currency = {};
        for (var i = 0; i < conversion.length; i++) {
            if(!conversion_currency.hasOwnProperty(conversion[i].id_currency)) {
                conversion_currency[conversion[i].id_currency] = [];
            }
            conversion_currency[conversion[i].id_currency].push(conversion[i]);
        }

        for (cc in conversion_currency) {
            if(conversion_currency[cc].length > 0) {
                antConversionValue = 0;
                tablacov += "<tr><th><?php echo JText::_('Value conversion'); ?> ("+conversion_currency[cc][0].currency_name+")</th>";
                for(var i=0; i<(lastMonth+1); i++) {

                    var element = jQuery.grep(conversion_currency[cc], function( n, index ) {
                        return n.month == i;
                    });

                    if(element.length > 0) {
                        if(element[0].month == i) {
                            antConversionValue = element[0].conversion_value;
                            antConversionValue = colocarPuntosComa2(antConversionValue);
                            if(i < (parseInt(monthStart)) || i > (parseInt(monthEnd))){
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+antConversionValue+"</td>";
                            }else {
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';\">"+antConversionValue+"</td>";
                            }
                        }else {
                            if(i < (parseInt(monthStart)) || i > (parseInt(monthEnd))){
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+antConversionValue+"</td>";
                            }else {
                                tablacov += "<td style=\"mso-number-format:\'#,##0\';\">"+antConversionValue+"</td>";
                            }
                        }
                    }else {
                        if(i < (parseInt(monthStart)) || i > (parseInt(monthEnd))){
                            tablacov += "<td style=\"mso-number-format:\'#,##0\';background-color: #c6d4e1 !important;\">"+antConversionValue+"</td>";
                        }else {
                            tablacov += "<td style=\"mso-number-format:\'#,##0\';\">"+antConversionValue+"</td>";
                        }
                    }
                }
                tablacov += "</tr>";
            }
        }
        tablacov += "</table>";

        document.getElementById("conversion").innerHTML = tablacov;
    }

    function dateReport(start_month, end_month) {
        var curdate = new Date();
        var months = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        var htmlDate = "<div><h2>Imputacion de horas por usuario</h2><br/><table>";
        htmlDate += "<tr><td><strong>Usuario: </strong></td>";
        htmlDate += "<td>"+jQuery("#user option:selected").html()+"</td></tr>";
        htmlDate += "<tr><td><strong>Fecha de inicio: </strong></td>";
        htmlDate += "<td>"+start_month+"</td></tr>";
        htmlDate += "<tr><td><strong>Fecha de fin: </strong></td>";
        htmlDate += "<td>"+end_month+"</td></tr>";
        htmlDate += "<tr><td><strong>Generado el: </strong></td>";
        htmlDate += "<td>"+curdate.getDate()+ ' de ' + months[curdate.getMonth()] + ' de ' + curdate.getFullYear()+ "</td></tr>";
        htmlDate += "<tr><td></td><td></td></tr>";
        htmlDate += "</table><div><br/>";
        return htmlDate;
    }

    function submitbutton(p){
        if(p=="") {
            searchUsers();
        }else if(p== "exportsreport"){
            document.getElementById("datos").value = document.getElementById("usersHours").innerHTML;
            var start_month =document.getElementById("start_month").value;
            var end_month = document.getElementById("end_month").value;
            if(start_month != "" || end_month != "") {
                document.getElementById("datosAdd").value = dateReport(start_month, end_month);
                document.getElementById("datosDel").value = "<h2>"+jQuery("#user option:selected").html()+"</h2>";
            }
            submitform(p);
        }else {
            sendMail();
        }
    }
</script>

