<?php

//header("Content-type: application/vnd.ms-excel; name='excel'; charset=latin");
//header("Content-Disposition: attachment; filename=report.xls");
//header("Pragma: no-cache");
//header("Expires: 0");
//
//if($this->datosAdd) {
//    $datosAdd = $this->datosAdd;
//    echo $datosAdd."<br>";
//}
//$datos = $this->datos;
//$datos = str_replace($this->datosDel, "", $datos);
//echo $datos;
//die;



jimport('phpexcel.PHPExcel');

error_reporting(E_ALL);

//date_default_timezone_set('Europe/London');

/** PHPExcel */
//require_once '../Classes/PHPExcel.php';
// Create new PHPExcel object
//echo date('H:i:s') . " Create new PHPExcel object\n";
$objPHPExcel = new PHPExcel();

// Set properties
//echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");

$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);
// Add some data, we will use printing features
//echo date('H:i:s') . " Add some data\n";
// Encabezados reporte
$headers = json_decode($this->headers);
$k = 1;
$objPHPExcel->getActiveSheet()->setCellValue("A$k", 'Generado el');
$objPHPExcel->getActiveSheet()->getStyle("A$k")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue("B$k", date("Y-m-d"));
$k++;

if ($headers->user != "") {
    $objPHPExcel->getActiveSheet()->setCellValue("A$k", 'Usuario');
    $objPHPExcel->getActiveSheet()->getStyle("A$k")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue("B$k", $headers->user);
    $k++;
}
if ($headers->start != "") {
    $objPHPExcel->getActiveSheet()->setCellValue("A$k", 'Fecha inicio');
    $objPHPExcel->getActiveSheet()->getStyle("A$k")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue("B$k", $headers->start);
    $k++;
}
if ($headers->end != "") {
    $objPHPExcel->getActiveSheet()->setCellValue("A$k", 'Fecha fin');
    $objPHPExcel->getActiveSheet()->getStyle("A$k")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue("B$k", $headers->end);
    $k++;
}
if ($headers->office != "") {
    $objPHPExcel->getActiveSheet()->setCellValue("A$k", 'Oficina');
    $objPHPExcel->getActiveSheet()->getStyle("A$k")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue("B$k", $headers->office);
    $k++;
}


$k++;
//nombres columas

$arrWeek = json_decode($this->arrWeek);

$objPHPExcel->getActiveSheet()->setCellValue("A$k", 'Nombre usuario');
$objPHPExcel->getActiveSheet()->setCellValue("B$k", 'Proyecto');

//titulos semanas
$coltit = 'C';
foreach ($arrWeek as $titW) {
    $objPHPExcel->getActiveSheet()->setCellValue($coltit . $k, $titW);
    $coltit++;
}
$objPHPExcel->getActiveSheet()->getStyle("A$k:" . $coltit . $k)->applyFromArray($styleThinBlackBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("A$k:" . $coltit . $k)->getFont()->setBold(true);
$allData = json_decode($this->datos);
$k++;
$indext = 0;
foreach ($allData as $value) {
    $objPHPExcel->getActiveSheet()->setCellValue('A' . $k, $value[0]);
    $indext = $k;
    $k++;
    $sum = array();
    foreach ($value[1] as $proy) {
        $columna = 'B';
        $ind = 0;
        foreach ($proy as $p) {
            $valCell = ($ind != 0) ? $p . '%' : $p;
            $objPHPExcel->getActiveSheet()->setCellValue($columna . $k, $valCell);
            $columna++;
            if ($ind != 0) {
                $valsum = (isset($sum[$ind])) ? $sum[$ind] : 0;
                $sum[$ind] = $valsum + floatval($p);
            }
            $ind++;
        }
        $k++;
    }

    $colTit = 'C';
    foreach ($sum as $s) {
        $objPHPExcel->getActiveSheet()->setCellValue($colTit . $indext, $s . '%');
//        $objPHPExcel->getActiveSheet()->getStyle($colTit . $indext)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
        $objPHPExcel->getActiveSheet()->getStyle($colTit . $indext)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        if ($s > 100)
            $objPHPExcel->getActiveSheet()->getStyle($colTit . $indext)->getFill()->getStartColor()->setRGB('F2982F');
        elseif ($s == 100)
            $objPHPExcel->getActiveSheet()->getStyle($colTit . $indext)->getFill()->getStartColor()->setRGB('7AC047');
        elseif ($s < 100 && $s > 0)
            $objPHPExcel->getActiveSheet()->getStyle($colTit . $indext)->getFill()->getStartColor()->setRGB('F9EA40');
        elseif ($s == 0)
            $objPHPExcel->getActiveSheet()->getStyle($colTit . $indext)->getFill()->getStartColor()->setRGB('D13634');

        $colTit++;
    }

    $objPHPExcel->getActiveSheet()->getStyle('A' . $indext . ':' . $columna . ($k - 1))->applyFromArray($styleThinBlackBorderOutline);
}




// Set fonts and style
foreach (range('A', 'Z') as $tmp) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($tmp)->setAutoSize(true);
}



// Set header and footer. When no different headers for odd/even are used, odd header is assumed.
//echo date('H:i:s') . " Set header/footer\n";
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&G&C&HPlease treat this document as confidential!');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

// Add a drawing to the header
//echo date('H:i:s') . " Add a drawing to the header\n";
$objDrawing = new PHPExcel_Worksheet_HeaderFooterDrawing();
$objDrawing->setName('PHPExcel logo');
//$objDrawing->setPath('./images/phpexcel_logo.gif');
$objPHPExcel->getActiveSheet()->getHeaderFooter()->addImage($objDrawing, PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_LEFT);

// Set page orientation and size
//echo date('H:i:s') . " Set page orientation and size\n";
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('Printing');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a clientâ€™s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="report_staffing_' . date("Y-m-d") . '.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>