<?php defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm">

    <fieldset class="adminform">
		<legend><?php echo JText::_( "GENERADOR DE INFORMES" ); ?></legend>
		<table class="tableSolicitud" width="80%" border="0" cellpadding="0" cellspacing="10">
			<tbody>
				<tr>
					<td width="60px" nowrap="nowrap">
						<label for="created">
								<?php echo JText::_( 'Date' ); ?>:
						</label>
					</td>
					<td colspan="3">
						<select id="mes" onchange="cambiarMes(this)">
							<option value="1" <?php if(date('m') == '1') echo 'selected=true'; ?>><?php echo JText::_('Enero')?></option>
							<option value="2" <?php if(date('m') == '2') echo 'selected=true'; ?>><?php echo JText::_('Febrero')?></option>
							<option value="3" <?php if(date('m') == '3') echo 'selected=true'; ?>><?php echo JText::_('Marzo')?></option>
							<option value="4" <?php if(date('m') == '4') echo 'selected=true'; ?>><?php echo JText::_('Abrl')?></option>
							<option value="5" <?php if(date('m') == '5') echo 'selected=true'; ?>><?php echo JText::_('Mayo')?></option>
							<option value="6" <?php if(date('m') == '6') echo 'selected=true'; ?>><?php echo JText::_('Junio')?></option>
							<option value="7" <?php if(date('m') == '7') echo 'selected=true'; ?>><?php echo JText::_('Julio')?></option>
							<option value="8" <?php if(date('m') == '8') echo 'selected=true'; ?>><?php echo JText::_('Agosto')?></option>
							<option value="9" <?php if(date('m') == '9') echo 'selected=true'; ?>><?php echo JText::_('Septiembre')?></option>
							<option value="10" <?php if(date('m') == '10') echo 'selected=true'; ?>><?php echo JText::_('Octubre')?></option>
							<option value="11" <?php if(date('m') == '11') echo 'selected=true'; ?>><?php echo JText::_('Noviembre')?></option>
							<option value="12" <?php if(date('m') == '12') echo 'selected=true'; ?>><?php echo JText::_('Diciembre')?></option>
						</select>

						<select id="years" onchange="cambiarYears(this)">
							<option value="2008" <?php if(date('Y') == '2008') echo 'selected=true'; ?>><?php echo JText::_('2008')?></option>
							<option value="2009" <?php if(date('Y') == '2009') echo 'selected=true'; ?>><?php echo JText::_('2009')?></option>
							<option value="2010" <?php if(date('Y') == '2010') echo 'selected=true'; ?>><?php echo JText::_('2010')?></option>
							<option value="2011" <?php if(date('Y') == '2011') echo 'selected=true'; ?>><?php echo JText::_('2011')?></option>
							<option value="2012" <?php if(date('Y') == '2012') echo 'selected=true'; ?>><?php echo JText::_('2012')?></option>
							<option value="2013" <?php if(date('Y') == '2013') echo 'selected=true'; ?>><?php echo JText::_('2013')?></option>
							<option value="2014" <?php if(date('Y') == '2014') echo 'selected=true'; ?>><?php echo JText::_('2014')?></option>
						</select>
					</td>
				</tr>
				<tr>
					<td width="60px" nowrap="nowrap">
						<label for="created">
								<?php echo JText::_( 'User' ); ?>:
						</label>
					</td>
					<td colspan="3">
						<select id="user" name="user">
						<option id="todos" value="todos">TODOS</option>
							<?php
							foreach ($this->users as $user){
								echo "<option id='$user->id' value='$user->id'>$user->name</option>";
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="60px" nowrap="nowrap">
						<label for="created">
								<?php echo JText::_( 'Client' ); ?>:
						</label>
					</td>
					<td colspan="3">
						<select id="client" name="client">
							<option id="todos" value="todos">TODOS</option>
							<?php
							foreach ($this->clients as $client){
								echo "<option id='$client->id' value='$client->id'>$client->name</option>";
							}
							?>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>
	<input type="hidden" id="mesReporte" name="mesReporte" value="<?php echo date('m');?>">
	<input type="hidden" id="yearsReporte" name="yearsReporte" value="<?php echo date('Y');?>">
    <input type="hidden" name="option" value="com_projectopen" />
	<input type="hidden" name="c" value="report" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_( 'form.token' ); ?>
</form>

<script>
	function cambiarMes(param){
		document.getElementById('mesReporte').value = param.value;
	}
	
	function cambiarYears(param){
		document.getElementById('yearsReporte').value = param.value;
	}
</script>

