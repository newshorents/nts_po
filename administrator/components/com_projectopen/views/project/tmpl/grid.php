<?php defined('_JEXEC') or die('Restricted access'); ?>
<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->search); ?>" onchange="document.adminForm.submit();" />
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
            <td>
                <div style=" margin-bottom: 10px;  text-align: right;">
                    <label for="path"><?php echo JText::_('Clients'); ?></label>
                    <?php echo JHTML::_('select.genericlist', $this->clients, 'filter_client', ' style="width: 200px;" class="inputbox" size="1" tabindex="2" onchange="document.adminForm.submit( );"', 'id', 'name', JRequest::getVar('filter_client')); ?>
                </div>
            </td>
            <td>
                <div style=" margin-bottom: 10px;  text-align: right;">
                    <label for="path"><?php echo JText::_('Type project'); ?></label>
                    <?php echo JHTML::_('select.genericlist', $this->types, 'filter_type', 'class="inputbox" size="1" tabindex="2" onchange="document.adminForm.submit( );"', 'id', 'name', JRequest::getVar('filter_type')); ?>
                </div>
            </td>
            <td>
                <div style=" margin-bottom: 10px;  text-align: right;">
                    <label for="path"><?php echo JText::_('State project'); ?></label>
                    <select id="filter_project" name="filter_project" onchange="document.adminForm.submit( );">
                        <option value="0" <?php echo (JRequest::getVar('filter_project')==0)? 'selected':'' ?>>Seleccione estado del proyecto</option>
                        <option value="1" <?php echo (JRequest::getVar('filter_project')==1 || JRequest::getVar('filter_project')==null)? 'selected':'' ?>>Vigentes</option>
                        <option value="2" <?php echo (JRequest::getVar('filter_project')==2)? 'selected':'' ?>>Terminados</option>
                        <option value="3" <?php echo (JRequest::getVar('filter_project')==3)? 'selected':'' ?>>Todos</option>
                    </select>
                </div>
            </td>
        </tr>
    </table>
    <table class="adminlist">
    <thead>
        <tr>
            <th width="20">
                <?php echo JText::_( 'NUM' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Name' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Type' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Client' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Project Manager' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Amount2' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'Start Date' ); ?>
            </th>
            <th class="title" nowrap="nowrap">
                <?php echo JText::_( 'End Date' ); ?>
            </th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="13">
                <?php echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
    </tfoot>
    <tbody>
    <?php $i = 0; $k = 0; ?>
    <?php foreach ($this->projects as $project) : ?>
        <?php
            // Get the current iteration and set a few values
            $link     = 'index.php?option=com_projectopen&c=project&amp;task=editproject&amp;id='. $project->id;
        ?>
        <tr class="<?php echo "row". $k; ?>">
            <td width="30">
                <?php echo $this->pagination->limitstart + 1 + $i; ?>
            </td>
            <td>
                <a href="<?php echo $link; ?>">
                    <?php echo $project->name; ?></a>
            </td>
            <td>
                <?php
                echo $project->type_name;
                ?>
            </td>
            <td>
                <?php  echo $project->client_name;  ?>
            </td>
            <td>
                <?php  echo $project->project_manager;  ?>
            </td>
             <td>
                 <label id="amount<?php echo $project->id;  ?>"></label>
            </td>
            <td>
                <?php  
                $start = preg_split("/[\/.-]/", $project->start_date);
                echo $start[2]."-".$start[1]."-".$start[0]; ?>
            </td>
            <td>
                <?php
                $end = preg_split("/[\/.-]/", $project->end_date);
                echo $end[2]."-".$end[1]."-".$end[0]; ?>
            </td>
        </tr>
        <?php $i++; $k = 1 - $k; ?>
    <?php endforeach; ?>
    </tbody>
    </table>
    
    <input type="hidden" name="c" value="project" />    
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="viewProject" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_( 'form.token' ); ?>
</form>
<script type="text/javascript">
window.onload = function() {
    <?php foreach ($this->projects as $project) : ?>
        var amount = document.getElementById("amount"+<?php echo $project->id;?>);        
        amount.textContent = colocarPuntosComa(<?php echo $project->amount;?>);
    <?php endforeach; ?>
}
</script>
