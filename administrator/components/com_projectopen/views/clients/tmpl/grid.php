<?php defined('_JEXEC') or die('Restricted access'); ?>
<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->search); ?>" onchange="document.adminForm.submit();" />
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
        </tr>
    </table>
    <table class="adminlist">
        <thead>
            <tr>
                <th width="20">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Name'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('NIF'); ?>
                </th>
                <th width="18%"  nowrap="nowrap">
                    <?php echo JText::_('Address'); ?>
                </th>
                <th width="10%">
                    <?php echo JText::_('Phone'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Type company'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Website'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="7">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php $i = 0;
                    $k = 0; ?>
            <?php foreach ($this->clients as $client) : ?>
            <?php
                        // Get the current iteration and set a few values
                        $link = 'index.php?option=com_projectopen&c=client&amp;task=editClient&amp;id=' . $client->id;
            ?>
                        <tr class="<?php echo "row" . $k; ?>">
                            <td width="30">
                    <?php echo $this->pagination->limitstart + 1 + $i; ?>
                    </td>

                    <td>

                        <a href="<?php echo $link; ?>">
                        <?php echo $client->name; ?></a>
                </td>
                <td>
                    <?php
                        echo $client->nif;
                    ?>
                    </td>
                    <td>
                    <?php
                        echo $client->address2;
                    ?>
                    </td>
                    <td>
                    <?php
                        echo $client->telephone;
                    ?>
                    </td>
                    <td>
                    <?php echo $client->type_company; ?>
                    </td>
                    <td>
                        <a href="<?php echo "http://" . $client->website; ?>" target="black">
                        <?php echo $client->website; ?></a>
                </td>

            </tr>
            <?php $i++;
                        $k = 1 - $k; ?>
            <?php endforeach; ?>
                    </tbody>
                </table>

                <input type="hidden" name="c" value="client" />
                <input type="hidden" name="option" value="com_projectopen" />
                <input type="hidden" name="task" value="viewClients" />
                <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_('form.token'); ?>
</form>

