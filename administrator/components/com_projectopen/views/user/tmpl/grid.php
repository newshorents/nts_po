<?php defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->search); ?>" onchange="document.adminForm.submit();" />
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
            <td>
                <div style=" margin-bottom: 10px;  text-align: right;">
                    <label for="path"><?php echo JText::_('Been with the company'); ?></label>
                    <?php echo JHTML::_('select.genericlist', $this->listUser, 'filter_laboral', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', JRequest::getVar('filter_laboral')); ?>
                </div>
            </td>
        </tr>
    </table>

    <table class="adminlist">
        <thead>
            <tr>
                <th width="20">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th width="25%" class="title" nowrap="nowrap">
                    <?php echo JText::_('Name'); ?>
                </th>
                <th nowrap="nowrap">
                    <?php echo JText::_('Username'); ?>
                </th>
                <th nowrap="nowrap">
                    <?php echo JText::_('Email'); ?>
                </th>
                <th nowrap="nowrap">
                    <?php echo JText::_('Workday'); ?>
                </th>
                <th nowrap="nowrap">
                    <?php echo JText::_('Current category'); ?>
                </th>
                <th nowrap="nowrap">
                    <?php echo JText::_('End date'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="7">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php $i = 0;
                    $k = 0; ?>
            <?php foreach ($this->users as $user) : ?>
            <?php
                        // Get the current iteration and set a few values
                        $link = 'index.php?option=com_projectopen&c=user&amp;task=editUser&amp;id=' . $user->user_id.'&amp;personal_type=' . $user->personal_type;
            ?>
                        <tr class="<?php echo "row" . $k; ?>">
                    <td width="30">
<?php echo $this->pagination->limitstart + 1 + $i; ?>
                    </td>
                    <td width="30">
                        <a href="<?php echo $link; ?>">
<?php echo $user->user_name; ?></a>
                    </td>
                    <td>
                        <?php echo $user->username; ?>
                    </td>
                    <td>
                        <?php echo $user->email; ?>
                    </td>
                    <td>
                        <?php echo $user->workday; ?>
                    </td>
                    <td>
                        <?php echo $user->category_name; ?>
                    </td>
                    <td>
                        <?php if($user->end_date != "0000-00-00"){ echo $user->end_date; }else{echo JText::_('Indefinite');}  ?>
                    </td>
                </tr>
            <?php $i++;
                        $k = 1 - $k; ?>
<?php endforeach; ?>
        </tbody>
    </table>

                <input type="hidden" name="option" value="com_projectopen" />
                <input type="hidden" name="task" value="viewuser" />
                <input type="hidden" name="c" value="user" />
                <input type="hidden" name="boxchecked" value="0" />
<?php echo JHTML::_('form.token'); ?>
</form>
