<?php
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.calendar');
?>

<script type="text/javascript">
window.onload = function(){
    date1= new Date();
    yearCurrent = date1.getFullYear();
    monthCurrent = date1.getMonth();
    var grid = document.getElementById('tableGrid');
    for(var i = 1; i < grid.rows.length-1; i++) {
        var valor = grid.rows[i].cells[2].getElementsByTagName('label')[0];
        valor.innerHTML = colocarPuntosComa2(valor.innerHTML);
    }
}

</script>

<script type='text/javascript' src='<?php echo JURI::root(true) . '/administrator/components/com_projectopen/assets/js/projectopenvalidation.js' ?>'></script>
<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input id='search' name='search' size='20' type='text' value="<?php echo htmlspecialchars($this->search); ?>" readonly onchange="document.adminForm.submit();"/>
                <img class='calendar' onclick='return showCalendar("search", "%d/%m/%Y")'
                 src='templates/system/images/calendar.png' alt='calendar' style="top:5px;"/>
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
        </tr>
    </table>
    <table class="adminlist" id="tableGrid">
        <thead>
            <tr>
                <th width="20">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Currency'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Date'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Value Conversion'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="4">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php $i = 0;
                    $k = 0;?>
            <?php

            $currentDate=strftime( "%Y-%m-%d", time() );
            $arrayCurrent  = preg_split("/[\/.-]/",$currentDate);
            $yearCurrent = $arrayCurrent[0];
            $monthCurrent = $arrayCurrent[1];
            foreach ($this->currency_conversion as $currency) : ?>
            <?php                        // Get the current iteration and set a few values
           $link = 'index.php?option=com_projectopen&c=currency&amp;task=editCurrency&amp;yearid=' . $currency->year . '&monthid=' . $currency->month . '&currencyid=' . $currency->id_currency;
            ?>
                <tr class="<?php echo "row" . $k; ?>">
                    <td width="30">
                        <?php echo $this->pagination->limitstart + 1 + $i; ?>
                    </td>
                    <td>
                        <?php echo $currency->currency_name; ?>
                    </td>
                    <td>
                        <?php if($currency->year < $yearCurrent) { ?>
                            <a href="<?php echo $link; ?>"><?php echo $currency->year . " - " . $this->months[$currency->month]; ?></a>
                        <?php } else if($currency->year == $yearCurrent){
                                                if($currency->month >= ($monthCurrent-2)){ ?>
                                                    <a href="<?php echo $link; ?>">
                                                        <?php echo $currency->year . " - " . $this->months[$currency->month]; ?></a>
                                          <?php }else { ?>
                                                     <a href="<?php echo $link; ?>">
                                                        <?php echo $currency->year . " - " . $this->months[$currency->month]; ?></a>

                                           <?php      }
                                       }else{ ?>
                                           <a href="<?php echo $link; ?>">
                                                        <?php echo $currency->year . " - " . $this->months[$currency->month]; ?></a>
                                 <?php }  ?>
                    </td>
                    <td>
                        <label><?php echo $currency->conversion_value;?></label>
                    </td>
                </tr>
            <?php $i++;
                        $k = 1 - $k; ?>
<?php endforeach; ?>
                    </tbody>
                </table>

                <input type="hidden" name="c" value="currency" />
                <input type="hidden" name="option" value="com_projectopen" />
                <input type="hidden" name="task" value="viewCurrency" />

<?php echo JHTML::_('form.token'); ?>
</form>

