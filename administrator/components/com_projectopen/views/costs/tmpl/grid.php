<?php defined('_JEXEC') or die('Restricted access'); ?>
<!--Enviar esto al worker1 <input type="text"  class="codigo"
                              value="" /><input type="button" value="Llamar al worker1" 
                              onclick="worker1&&(worker1.postMessage({'cmm':'enviar','caro':'gu'}));" />
<div>Esta es la repuesta del worker1:
    <div id="divwk1" style="color: navy"></div>
</div>     -->
<script type='text/javascript' src='<?php echo JURI::root(true) . '/libraries/jquery/jquery-1.5.2.min.js' ?>'></script>
<script type='text/javascript'> jQuery.noConflict(); </script>
<script>   
    
    //Add a work helper function to the jQuery object
//    jQuery.work = function(args) { 
//        var def = jQuery.Deferred(function(dfd) {
//            var worker;
//            if (window.Worker) {
//                //Construct the Web Worker
//                var worker = new Worker(args.file); 
//                worker.onmessage = function(event) {
//                    //If the Worker reports success, resolve the Deferred
//                    dfd.resolve(event.data); 
//                };
//                worker.onerror = function(event) {
//                    //If the Worker reports an error, reject the Deferred
//                    dfd.reject(event); 
//                };
//                worker.postMessage(args.args); //Start the worker with supplied args
//            } else {
//                //Need to do something when the browser doesn't have Web Workers
//            }
//        });
// 
//        //Return the promise object (an "immutable" Deferred object for consumers to use)
//        return def.promise(); 
//    };
    
    
//    var worker1 = null;
//    if (typeof(Worker)=="undefined"){
//        document.getElementById("divwk1").innerHTML = 
//            "<b style='color:red'>" +
//            "Workers no soportado</b>";
//    } else {  
//        jQuery("#load2").show();
//        worker1 = new Worker("<?php echo JURI::root(true) . '/libraries/worker1.js' ?>");
//        worker1.onmessage = function(event){
//            document.getElementById("divwk1").innerHTML = event.data;
//            jQuery("#load2").hide();
//        }                
//    }
//    function ciclo(){
//        //Call the helper work function with the name of the Worker file and arguments.
//        jQuery.work({file: '<?php echo JURI::root(true) . '/libraries/worker1.js' ?>', args: { anArg: "hello!" }}).then(function(data) {
//            //Worker completed successfully
//            jQuery("#prueba").html(data);
//        }).fail(function(data){
//            //Worker threw an error
//            jQuery("#prueba").html(data);
//        });
//    }
</script>
<!--<input type="text" value="cl" onclick="ciclo()"/>
<div id="prueba"></div>
<div style="position: absolute; top: 223px; left: 46px; " id="load2"><img border="0" src="<?php echo JURI::root(true) ?>/images/loadinfo.gif" /></div>-->
<form action="index.php?option=com_projectopen" method="post" name="adminForm">
    <table>
        <tr>
            <td width="100%">
                <?php echo JText::_('Filter'); ?>:
                <input type="text" name="search" id="search" value="<?php echo htmlspecialchars($this->search); ?>" onchange="document.adminForm.submit();" />
                <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
                <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
            </td>
            <td>
                <div style=" margin-bottom: 10px;  text-align: right;">
                    <label for="path"><?php echo JText::_('State Cost'); ?></label>
                    <?php echo $this->lists['state']; ?>
                </div>
            </td>
        </tr>
    </table>
    <table class="adminlist">
        <thead>
            <tr>
                <th width="20">
                    <?php echo JText::_('NUM'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Type'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Date'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Project'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Creator spending'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('Amount'); ?>
                </th>
                <th class="title" nowrap="nowrap">
                    <?php echo JText::_('State'); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="7">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php $i = 0;
            $k = 0; ?>
            <?php foreach ($this->costs as $cost) : ?>
                <?php
                // Get the current iteration and set a few values
                $link = 'index.php?option=com_projectopen&c=costs&amp;task=editCosts&amp;id=' . $cost->id;
                ?>

                <tr class="<?php echo "row" . $k; ?>">
                    <td width="30">
                        <?php echo $this->pagination->limitstart + 1 + $i; ?>
                    </td>
                    <td>
                        <a href="<?php echo $link; ?>">
                            <?php echo $cost->name_type; ?></a>
                    </td>
                    <td>
                        <?php
                        echo $cost->date_cost;
                        ?>
                    </td>

                    <td >
                        <?php
                        echo $cost->project_name;
                        ?>
                    </td>
                    <td >
                        <?php
                        echo $cost->user_name;
                        ?>
                    </td>
                    <td>
                        <?php echo number_format($cost->amount, 2, ',', '.'); ?>
                    </td>
                    <td>
                        <?php echo $cost->name_state; ?>
                    </td>
                </tr>
                <?php
                $i++;
                $k = 1 - $k;
            endforeach;
            ?>
        </tbody>
    </table>

    <input type="hidden" name="c" value="costs" />
    <input type="hidden" name="option" value="com_projectopen" />
    <input type="hidden" name="task" value="viewCosts" />
    <input type="hidden" name="boxchecked" value="0" />
    <?php echo JHTML::_('form.token'); ?>
</form>

