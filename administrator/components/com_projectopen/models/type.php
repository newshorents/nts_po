<?php
/**
 * @version		1.0.0 projectopen $
 * @package		projectopen
 * @copyright	Copyright © 2010 - All rights reserved.
 * @license		GNU/GPL
 * @author		
 * @author mail	nobody@nobody.com
 *
 *
 * @MVC architecture generated by MVC generator tool at http://www.alphaplug.com
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.model' );

class projectopenModelType extends Jmodel
{
	 public function getTypes()
     {
        $db=& JFactory::getDBO();
        $query = "Select * from #__po_type";
        $db->setQuery($query,0);
        $val=$db->loadObjectList();
        return $val;
     }
     
     public function getTypeEdit()
     {
         $id = $this->_getId();
         $db=& JFactory::getDBO();
         $query = "Select * from #__po_type where id = ". $this->_getId(); 
         $db->setQuery($query,0);
         $val=$db->loadObjectList();
       
       return $val[0];
         
     }
     
     
     public function getSaveType() 
     {
        $db=& JFactory::getDBO();
        
        $objeto->name = $this->_getName();
        
        if(!$this->_getId()) {
            $db->insertObject("#__po_type", $objeto, 'id',true);
        }
        else {
            $objeto->id = $this->_getId();
            $db->updateObject("#__po_type", $objeto, 'id',true);
        }
        
     }
     
     
     public function getRemove()
     {
        $db=& JFactory::getDBO();
        $query = "Delete from #__po_type where id = ". $this->_getId();
        $db->setQuery($query);
        $db->query();
    }
     
     
     
     protected function _getId()
     {
         return JRequest::getVar('id');
     }
     
      
     protected function _getName() 
     {
        return JRequest::getVar('name');
     }

     
     public function getPagination()
     {
        global $mainframe;
        
        $clientstypes = ProjectopenHelper::getClientsList();
        $total        = count( $clientstypes );
        $limit        = $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
        $limitstart = $mainframe->getUserStateFromRequest( 'com_projectopen.limitstart', 'limitstart', 0, 'int' );

        jimport('joomla.html.pagination');
        $pagination = new JPagination( $total, $limitstart, $limit );
        return $pagination;
    }



}
?>