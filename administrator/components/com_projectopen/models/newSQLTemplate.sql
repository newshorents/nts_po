select project_name, project_id, id_user, sum((number_hours/numproject)) as number_hours,
year(impute_date) as year,
month(impute_date) as month, 
id_absence
 FROM(
		SELECT
								'' as project_name,
								jos_tm_user_project.id_project as project_id,
								impute_date,
								jos_tm_impute_week_absence.id_absence,
							  jos_tm_impute_day_absence.number_hours,
								jos_users.id AS id_user,								
								(SELECT count(DISTINCT id_project) as numproject FROM jos_tm_user_project 
									INNER JOIN jos_tm_projects on jos_tm_projects.id = jos_tm_user_project.id_project
                  WHERE id_user = jos_users.id AND  impute_date BETWEEN jos_tm_user_project.start_date AND jos_tm_user_project.end_date
									AND impute_date BETWEEN jos_tm_projects.start_date AND jos_tm_projects.end_date) as numproject
								FROM
								jos_users
								INNER JOIN jos_tm_impute_week_absence ON jos_tm_impute_week_absence.id_user = jos_users.id
								INNER JOIN jos_tm_impute_day_absence ON jos_tm_impute_day_absence.id_week = jos_tm_impute_week_absence.id
								INNER JOIN jos_tm_user_project ON jos_tm_user_project.id_user = jos_users.id
								WHERE jos_tm_impute_week_absence.id_absence <> 2
									AND jos_tm_impute_week_absence.id_absence <> 4
									AND jos_tm_impute_week_absence.id_absence <> 6
									AND jos_tm_impute_week_absence.id_absence <> 7
									AND jos_tm_impute_week_absence.id_absence <> 8
									AND jos_tm_impute_day_absence.impute_date BETWEEN jos_tm_user_project.start_date AND jos_tm_user_project.end_date
									AND jos_tm_impute_day_absence.impute_date < DATE_FORMAT(CONCAT(YEAR(CURDATE()),'-', MONTH(CURDATE()),'-','01'),'%Y-%m-%d')
								  and number_hours > 0
									ORDER BY project_id asc, id_user asc, impute_date
) as t2
GROUP BY project_id, id_user, year, month, id_absence
ORDER BY project_id asc, id_user asc, impute_date


