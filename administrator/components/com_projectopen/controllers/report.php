<?php

/**
 * @version		1.0.0 projectopen $
 * @package		projectopen
 * @copyright	Copyright © 2010 - All rights reserved.
 * @license		GNU/GPL
 * @author
 * @author mail	nobody@nobody.com
 *
 *
 * @MVC architecture generated by MVC generator tool at http://www.alphaplug.com
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class projectopenControllerReport extends JController {

    public function indexReports() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->indexReports();
    }

    public function viewReports() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportImpute();
    }

    public function getProjectsClient() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getProjectsClient();
        die();
    }

    public function getUserWithoutImpute() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUserWithoutImpute();
        die();
    }

    public function getClientsScorecardSelect() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getClientsScorecardSelect();
        die;
    }

    public function getUserStaffing() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUserStaffing();
        die;
    }

    public function getUserOffice() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUserOffice();
        die;
    }

    public function getReportDelivery() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getReportDelivery();
        die;
    }

    public function getProjectsClientCurrent() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getProjectsClientCurrent();
        die;
    }

    public function getReportPendingInvoices() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getReportPendingInvoices();
        die;
    }

    public function getReportVacations() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getReportVacations();
        die;
    }

    public function getReportStaffing() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getReportStaffing();
        die;
    }

    public function sendMail() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->sendMail();
        $msg = JText::_('Las notificaciones a los Usuarios fueron enviadas');
        $this->setRedirect('index.php?option=com_projectopen&c=report&task=viewReports', $msg);
    }

    public function viewScorecard() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportScorecardf();
    }

    public function reportScorecard() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->generateReportScorecard();
    }

    function cancel() {
        $this->getReportScorecard();
    }

    function getReportScorecard($msg ='') {
        $this->setRedirect('index.php?option=com_projectopen&c=report&task=viewScorecard', $msg);
    }

    function saveScorecard() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        if ($view->saveScorecard()) {
            $msg = JText::_('El cuadro de mando ha sido almacenado con éxito');
            $this->getReportScorecard($msg);
        } else {
            $msg = JText::_('El cuadro de mando no se pudo almacenar');
            $this->setRedirect('index.php?option=com_projectopen&c=report&task=viewScorecard', $msg, 'error');
        }
    }

    function exportScorecard() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->exportScorecard();
    }

    public function getStateValidationAjax() {
        $user = & JFactory::getUser();
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getStateValidationAjax();
        die;
    }

    public function downloadTxt() {
        $enlace = JRequest::getVar("txt");
        header("Content-Disposition: attachment; filename=pendientes.txt");
        header("Content-Type: application/octet-stream");
        print($enlace);
        die;
    }

    public function viewReportsHoursImpute() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportHoursImpute();
    }

    public function getUserImputeHours() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUserImputeHours();
        die;
    }

    public function viewReportsHoursUser() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportHoursUser();
    }

    public function viewDashboardUser() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->dashboardUser();
    }

    public function getDashboardUser() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getDashboardUser();
        die;
    }

    /*public function viewReportStaffing() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportStaffing();
    }*/

    /*public function viewReportDelivery() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportDelivery();
    }*/

    /*public function viewReportPendingInvoices() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportPendingInvoices();
    }*/

    public function viewVacations() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->reportVacations();
    }

    public function getUserImputeHoursUser() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUserImputeHoursUser();
        die;
    }

    function exportsreport() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->exportReports();
    }

    function exportpending() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->exportReports();
    }

    public function viewUserHoursProjectClient() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        $view->UserHoursProjectClient();
    }

    public function getUserHoursProjectClient() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUserHoursProjectClient();
        die;
    }

    public function getClientsUser() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getClientsUser();
        die;
    }

    public function getUsersClient() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUsersClient();
        die;
    }

    public function getUsersDetail() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getUsersDetail();
        die;
    }

    public function getConversionReport() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getConversionReport();
        die;
    }

    public function getCostDetail() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->getCostDetail();
        die;
    }

     public function exportPhpexcel() {
        $model = &$this->getModel('report');
        $view = &$this->getView('report');
        $view->setModel($model, true);
        echo $view->exportPhpexcel();
        die;
    }

}

?>
