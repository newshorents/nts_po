<?php

/**
 * @version		1.0.0 projectopen $
 * @package		projectopen
 * @copyright	Copyright © 2010 - All rights reserved.
 * @license		GNU/GPL
 * @author
 * @author mail	nobody@nobody.com
 *
 *
 * @MVC architecture generated by MVC generator tool at http://www.alphaplug.com
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class ProjectopenControllerHelpmodal extends JController {

    public function viewHelpModal() {
        $model = &$this->getModel('helpmodal');
        $view = &$this->getView('helpmodal');
        $view->setModel($model, true);
        $view->modalHelp();
    }

}

?>
