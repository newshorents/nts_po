<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class idoblogsModelConfig extends JModel
{

function _check($value) {
if (empty($value)) return "0";
else
return "1";
}

function saveconfig()
{
    $data = JRequest::get( 'post' );

 $config = new JRegistry('config');

$config_array = array();
$config_array['idblog']	= $data['idblog'];
$config_array['typename']= $data['typename'];
$config_array['shablon']= $data['shablon'];
$config_array['dateformat']= $data['dateformat'];
$config_array['adminemail']= $data['adminemail'];
$config_array['adminnewentry']= $data['adminnewentry'];
$config_array['adminnewcomment']= $data['adminnewcomment'];
$config_array['authornewcomment']= $data['authornewcomment'];
$config_array['usernewcomment']= $data['usernewcomment'];
$config_array['avatarwidth']= $data['avatarwidth'];
$config_array['smallavatarwidth']= $data['smallavatarwidth'];
$config_array['letter']= $data['letter'];
$config_array['adsense_enabled']= $this->_check($data['adsense_enabled']);
$config_array['template_enabled']= $this->_check($data['template_enabled']);
$config_array['online_enabled']= $this->_check($data['online_enabled']);
$config_array['show_hits']= $this->_check($data['show_hits']);
$config_array['show_avatars']= $this->_check($data['show_avatars']);
$config_array['import_plugins']= $this->_check($data['import_plugins']);
$config_array['enabled_bbcode']= $this->_check($data['enabled_bbcode']);
$config_array['enabled_smiley']= $this->_check($data['enabled_smiley']);
$config_array['limit']= $data['limit'];
$config_array['access_comment']= $data['access_comment'];
$config_array['access_write']= $data['access_write'];
$config_array['access_moderate']= $data['access_moderate'];
$config_array['captcha']= $data['captcha'];
$config_array['file_archive']= $this->_check($data['file_archive']);
$config->loadArray($config_array);

$fname = JPATH_CONFIGURATION.DS.'administrator'.DS.'components'.DS.'com_idoblog'.DS.'config.php';
jimport('joomla.filesystem.path');

if (!$ftp['enabled'] && JPath::isOwner($fname) && !JPath::setPermissions($fname, '0644')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make config.php writable');
}

jimport('joomla.filesystem.file');
if (JFile::write($fname, $config->toString('PHP', 'config', array('class' => 'KBconfig')))) {
			$msg = JText::_('The Configuration Details have been updated');
		} else {
			$msg = JText::_('ERRORCONFIGFILE');
		}

    return true;
}

function getshablons($shabselect) {
$shablons=JFolder::listFolderTree(JPATH_COMPONENT_SITE.DS."assets".DS."templates", ".", $maxLevel = 1, $level = 0, $parent = 0);

    for ($i=0, $n=count( $shablons ); $i < $n; $i++)
    {
        $shablon =$shablons[$i];
		$myshablons[$i]=JHTML::_('select.option',  $shablon['name'], $shablon['name']);
	}

$shablons=JHTML::_('select.genericlist',   $myshablons, 'shablon', 'size="1" class="inputbox"', 'value', 'text', $shabselect );
return $shablons;
}

function getallblogs() {
$config=new KBconfig();
$db =& JFactory::getDBO();
$query="SELECT * FROM #__idoblog_user_reffer as kuf, #__categories as c WHERE c.section=".$config->idblog." AND kuf.idcategory=c.id";
$db->setQuery( $query );
$db->query();
$all=$db->getNumRows();
return $all;
}

function getallusers() {
$db =& JFactory::getDBO();
$query="SELECT * FROM #__users";
$db->setQuery( $query );
$db->query();
$all=$db->getNumRows();
return $all;
}

}

?>