<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class idoblogsModelTemplates extends JModel
{

function gettemplates() {
$templates=JFolder::listFolderTree(JPATH_COMPONENT_SITE.DS."assets".DS."templates", ".", $maxLevel = 1, $level = 0, $parent = 0);
return $templates;
}

function upload() {
    $data = JRequest::get( 'post' );

jimport('joomla.filesystem.path');
jimport('joomla.filesystem.archive');
		jimport('joomla.filesystem.file');
$userfile = JRequest::getVar('install_package', null, 'files', 'array' );

		$config =& JFactory::getConfig();
		$tmp_dest 	= $config->getValue('config.tmp_path').DS.$userfile['name'];
		$tmp_src	= $userfile['tmp_name'];


if ($userfile['type']!='application/x-zip-compressed')
return false;
			$uploaded = JFile::upload($tmp_src, $tmp_dest);
	
		$archivename = $tmp_dest;

		// Temporary folder to extract the archive into
		$tmpdir = JPATH_SITE.DS.'components'.DS.'com_idoblog'.DS.'assets'.DS.'templates';

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean($tmpdir);
		$archivename = JPath::clean($archivename);
		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);
	
		if ( $result === false ) return false;

return true;
}

function deletetemplate() {
    $data = JRequest::get( 'post' );
	jimport('joomla.filesystem.file');
if (empty($data['id_template'])) return false;

$folder=JPATH_SITE.DS.'components'.DS.'com_idoblog'.DS.'assets'.DS.'templates'.DS.$data['id_template'];
	
$result=JFolder::delete($folder);
		if ( $result === false ) return false;
return true;
}

}

?>