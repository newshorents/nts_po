<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

class idoblogsController extends JController
{
    /**
     * метод вывода на экран
     *
     * @access    public
     */
    function display()
    {
$task=JRequest::getVar( 'task' );	
$controller=JRequest::getVar( 'controller' );
if (empty($task) && empty($controller)) {	
 JRequest::setVar( 'view', 'cpanel' );
JRequest::setVar( 'layout', 'default'  );
}
        parent::display();
    }

    function templates()
    {
JRequest::setVar( 'view', 'templates' );
JRequest::setVar( 'layout', 'default'  );

        parent::display();
}

    function cpanel()
    {
JRequest::setVar( 'view', 'cpanel' );
JRequest::setVar( 'layout', 'default'  );
        parent::display();
    }

	function upload_template()
	{
	$model = $this->getModel('templates');

    if ($model->upload($post)) {
        $msg = JText::_( 'Template succesfully upload!' );
    } else {
        $msg = JText::_( 'Error upload template' );
    }
	 $link = 'index.php?option=com_idoblog&task=templates';
    $this->setRedirect($link, $msg); 	
	}

	function deletetemplate()
	{
	$model = $this->getModel('templates');

    if ($model->deletetemplate($post)) {
        $msg = JText::_( 'Template succesfully deleted!' );
    } else {
        $msg = JText::_( 'Error delete template' );
    }
	 $link = 'index.php?option=com_idoblog&task=templates';
    $this->setRedirect($link, $msg); 	
	}

}

?>