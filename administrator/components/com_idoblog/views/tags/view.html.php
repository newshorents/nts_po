<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class idoblogsViewtags extends JView
{
    function display($tpl = null)
    {
	GLOBAL $mainframe;
$layout	= JRequest::getVar('layout');
	$limit	= JRequest::getVar('limit',	$mainframe->getCfg('list_limit'),	'', 'int');
$limitstart	= JRequest::getVar('limitstart', 0, '', 'int');

		$contents = '';
		ob_start();
		require_once(JPATH_COMPONENT.DS.'views'.DS.'cpanel'.DS.'tmpl'.DS.'navigation.php');
		$contents = ob_get_contents();
		ob_end_clean();	

		$document =& JFactory::getDocument();
		$document->setBuffer($contents, 'modules', 'submenu');

	$model =& $this->getModel();
if ($layout=='tags') {
JToolBarHelper::title(   JText::_( 'Tags IDoBlog' ), 'generic.png' );
JToolBarHelper::custom( 'cpanel', 'back.png', 'back_f2.png', JText::_( 'Cpanel' ), false );
JToolBarHelper::editListX('edittag');
JToolBarHelper::deleteList(JText::_( 'Are you sure?' ),'deletetags');

$tags=$model->gettags($limitstart,$limit);
$alltags=$model->getalltags();

		jimport('joomla.html.pagination');
$pagination = new JPagination($alltags, $limitstart, $limit);
        $this->assignRef( 'tags', $tags );	
         $this->assignRef( 'pagination', $pagination );	
} //end layout tags		 

if ($layout=='edit') {
	$tag=$model->gettag();
    JToolBarHelper::title(   JText::_( 'Edit Tag' ).': <small><small>[ ' . $tag->tagname.' ]</small></small>' );
    JToolBarHelper::save();
    JToolBarHelper::cancel();
      $this->assignRef( 'tag', $tag );	
}
		 
        parent::display($tpl);
    }
}

?>