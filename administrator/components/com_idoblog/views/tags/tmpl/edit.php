<?php defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
    <fieldset class="adminform">
        <legend></legend>
        <table class="admintable">
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( 'Tag name' ); ?>:
                </label>
            </td>
            <td>
                <input class="text_area" type="text" name="tagname" id="tagname" size="32" maxlength="250" value="<?php echo $this->tag->tagname;?>" />
            </td>
        </tr>
    </table>
    </fieldset>
</div>

<div class="clr"></div>

<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="id" value="<?php echo $this->tag->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="tags" />
</form>
