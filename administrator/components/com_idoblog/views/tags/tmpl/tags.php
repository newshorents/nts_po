<?php defined('_JEXEC') or die(); ?>

<form action="index.php" method="post" name="adminForm">


<div>
<?php echo JText::_( 'Add tags' ); ?>: <input name="tags" type="text" style="width:500px;" />
<button type="button" onclick="submitbutton('addtags')">
				<?php echo JText::_('Create tags') ?>
			</button>
</div>
<br />

<div id="editcell">
    <table class="adminlist">
    <thead>
        <tr>
            <th width="5">
                <?php echo JText::_( 'ID' ); ?>
            </th>
                <th width="20">
    <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->tags ); ?>);" />
</th>

            <th>
                <?php echo JText::_( 'Tag' ); ?>
            </th>
        </tr>            
    </thead>
    <?php
    $k = 0;
    for ($i=0, $n=count( $this->tags ); $i < $n; $i++)
    {
        $tag =& $this->tags[$i];
        ?>
        <tr class="<?php echo "row$k"; ?>">
            <td>
                <?php echo $tag->id; ?>
            </td>
			<td>
			<?php $checked    = JHTML::_( 'grid.id', $i, $tag->id ); echo $checked; ?>
			</td>
            <td>
                <?php $link = JRoute::_( 'index.php?option=com_idoblog&controller=tags&task=edittag&cid[]='. $tag->id ); ?>
				<a href="<?php echo $link; ?>"><?php echo $tag->tagname; ?></a>
            </td>
        </tr>
        <?php
        $k = 1 - $k;
    }
    ?>
	<tfoot>
	<tr>
	<td colspan="15">
	<?php echo $this->pagination->getListFooter(); ?>
	</td>
	</tr>
	</tfoot>
    </table>
</div>

<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="task" value="tags" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="tags" />

</form>