<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');


class idoblogsViewCpanel extends JView
{
    function display($tpl = null)
    {
	JToolBarHelper::title( JText::_( 'Cpanel IDoBlog' ), 'generic.png' );
        $this->assignRef( 'greeting', $greet );
$model =& $this->getModel();

		$contents = '';
		ob_start();
		require_once(JPATH_COMPONENT.DS.'views'.DS.'cpanel'.DS.'tmpl'.DS.'navigation.php');
		$contents = ob_get_contents();
		ob_end_clean();

		$document =& JFactory::getDocument();
		$document->setBuffer($contents, 'modules', 'submenu');
jimport('joomla.html.pane');
$pane =& JPane::getInstance('Sliders');
echo '<div style="float:right;width:550px;">';
echo $pane->startPane('news');
{
echo $pane->startPanel(JText::_( 'TIPS' ), 'panel1');
echo "<div style='padding:8px;'> 1) ".JText::_( 'TIPS2' )."<br> 2) ".JText::_( 'TIPS3' )."<br></div>";

$u =& JFactory::getURI();

echo "<br><img src='http://community.idojoomla.com/idoblog.php?u=".$u->_host."' width='1' height='1'>";
echo $pane->endPanel();
}
echo $pane->startPanel(JText::_( 'Information' ), 'panel2');
echo "<div style='padding:8px;'></div>";
echo "<br>";
echo $pane->endPanel();
echo $pane->endPane();
echo "</div>";

	echo "<div id=\"cpanel\">";
		$link = 'index.php?option=com_idoblog&task=config&controller=config&view=config&layout=config';
		$model->quickiconButton( $link, 'icon-48-config.png', JText::_( 'Configuration' ) );

		$link = 'index.php?option=com_idoblog&task=tags&controller=tags&view=tags&layout=tags';
		$model->quickiconButton( $link, 'icon-48-stats.png', JText::_( 'Tags' ) );

		$link = 'index.php?option=com_idoblog&task=templates';
		$model->quickiconButton( $link, 'icon-48-themes.png', JText::_( 'Templates' ) );

		$link = 'index.php?option=com_idoblog&task=update&controller=update';
		$model->quickiconButton( $link, 'icon-48-cpanel.png', JText::_( 'UPDATE' ) );

	echo "</div>";
        parent::display($tpl);

    }

}
?>