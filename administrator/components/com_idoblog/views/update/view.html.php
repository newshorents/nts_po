<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class idoblogsViewupdate extends JView
{
    function display($tpl = null)
    {
	GLOBAL $mainframe, $live_site;
$layout	= JRequest::getVar('layout');

		$contents = '';
		ob_start();
		require_once(JPATH_COMPONENT.DS.'views'.DS.'cpanel'.DS.'tmpl'.DS.'navigation.php');
		$contents = ob_get_contents();
		ob_end_clean();	

		$document =& JFactory::getDocument();
		$document->setBuffer($contents, 'modules', 'submenu');

	$model =& $this->getModel();

JToolBarHelper::title(   JText::_( 'IDoBlog update and repair' ), 'generic.png' );
JToolBarHelper::custom( 'cpanel', 'back.png', 'back_f2.png', JText::_( 'Cpanel' ), false );
		 
        parent::display($tpl);
    }
}

?>