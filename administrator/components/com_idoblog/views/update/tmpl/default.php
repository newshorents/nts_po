<?php defined('_JEXEC') or die(); ?>

<?php JHTML::_('behavior.modal', 'a.modal'); ?>
<form action="index.php" method="post" name="adminForm">

<div id="config-document">
	<div id="page-main">
    <fieldset class="adminform">
        <legend><?php echo JText::_( 'Select action' ); ?>:</legend>
	<table class="adminform">
	<tr>
		<td width="120">
			<label for="update25"><button type="button" class="button" onclick="submitbutton('update25')">
				<?php echo JText::_('UPDATE TO BUILD 25') ?>
			</button></label>
		</td>
		<td>
			<?php echo JText::_('DESCRIPTION UPDATE TO BUILD 25') ?>
		</td>
	</tr>
	</table>
    </fieldset>
</div>
</div>

<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="task" id="task" value="update" />
<input type="hidden" name="controller" value="update" />

</form>