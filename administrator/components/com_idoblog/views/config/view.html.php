<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class idoblogsViewconfig extends JView
{
    function display($tpl = null)
    {
GLOBAl $mainframe;
	$model =& $this->getModel();

		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.switcher');

		// Build the component's submenu
		$contents = '';
		ob_start();
		require_once(JPATH_COMPONENT.DS.'views'.DS.'config'.DS.'tmpl'.DS.'navigation.php');
		$contents = ob_get_contents();
		ob_end_clean();

		$document =& JFactory::getDocument();
		$document->setBuffer($contents, 'modules', 'submenu');


	//Show toolbar
JToolBarHelper::title( JText::_( 'idoblog config' ), 'generic.png' );
		JToolBarHelper::custom( 'cpanel', 'back.png', 'back_f2.png', JText::_( 'Cpanel' ), false );
    JToolBarHelper::save('saveconfig',JText::_( 'Save' ));


	require(JPATH_COMPONENT.DS.'config.php');
	$config=new KBconfig();

//Create content
$idblog = JHTML::_('list.section', 'idblog', $config->idblog, '');


$typename = array(
			JHTML::_('select.option',  '0', JText::_( 'Name' ) ),
			JHTML::_('select.option',  '1', JText::_( 'Username' ) )
		);

$this->typename=JHTML::_('select.genericlist',   $typename, 'typename', 'size="1" class="inputbox"', 'value', 'text', $config->typename );

$dateformat = array(
			JHTML::_('select.option',  '%Y.%m.%d', JText::_( '%Y.%m.%d' ) ),
			JHTML::_('select.option',  '%d.%m.%Y', JText::_( '%d.%m.%Y' ))
		);

$this->dateformat=JHTML::_('select.genericlist',   $dateformat, 'dateformat', 'size="1" class="inputbox"', 'value', 'text', $config->dateformat );

$this->adminemail='<input name="adminemail" value="'.$config->adminemail.'" type="text" />';

$yesno = array(
			JHTML::_('select.option',  '0', JText::_( 'No' ) ),
			JHTML::_('select.option',  '1', JText::_( 'Yes' ))
		);

$this->adminnewentry=JHTML::_('select.genericlist',   $yesno, 'adminnewentry', 'size="1" class="inputbox"', 'value', 'text', $config->adminnewentry );

$this->adminnewcomment=JHTML::_('select.genericlist',   $yesno, 'adminnewcomment', 'size="1" class="inputbox"', 'value', 'text', $config->adminnewcomment );

$this->authornewcomment=JHTML::_('select.genericlist',   $yesno, 'authornewcomment', 'size="1" class="inputbox"', 'value', 'text', $config->authornewcomment );

$this->usernewcomment=JHTML::_('select.genericlist',   $yesno, 'usernewcomment', 'size="1" class="inputbox"', 'value', 'text', $config->usernewcomment );

$this->letter='<textarea name="letter" cols="50" rows="10">'.$config->letter.'</textarea>';

$this->avatarwidth='<input name="avatarwidth" value="'.$config->avatarwidth.'" type="text" />';
$this->smallavatarwidth='<input name="smallavatarwidth" value="'.$config->smallavatarwidth.'" type="text" />';

$this->limit='<input name="limit" value="'.$config->limit.'" type="text" />';

$checked="";
if ($config->adsense_enabled=='1') $checked="checked='checked'";
$this->adsense_enabled='<input name="adsense_enabled" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->template_enabled=='1') $checked="checked='checked'";
$this->template_enabled='<input name="template_enabled" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->online_enabled=='1') $checked="checked='checked'";
$this->online_enabled='<input name="online_enabled" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->show_hits=='1') $checked="checked='checked'";
$this->show_hits='<input name="show_hits" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->show_avatars=='1') $checked="checked='checked'";
$this->show_avatars='<input name="show_avatars" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->import_plugins=='1') $checked="checked='checked'";
$import_plugins='<input name="import_plugins" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->enabled_bbcode=='1') $checked="checked='checked'";
$enabled_bbcode='<input name="enabled_bbcode" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->enabled_smiley=='1') $checked="checked='checked'";
$enabled_smiley='<input name="enabled_smiley" type="checkbox" value="1" '.$checked.' />';

$checked="";
if ($config->file_archive=='1') $checked="checked='checked'";
$this->file_archive='<input name="file_archive" type="checkbox" value="1" '.$checked.' />';

//permissions
$acl		=& JFactory::getACL();
$gtree = $acl->get_group_children_tree( null, 'USERS', false );
$gtree2 = $acl->get_group_children_tree( null, 'USERS', false );
$gtree2[0]->disable=true;

$gtree3 = $acl->get_group_children_tree( null, 'Public Backend', false );
$this->access_comment= JHTML::_('select.genericlist',   $gtree, 'access_comment', 'size="10"', 'value', 'text', $config->access_comment );
$this->access_write= JHTML::_('select.genericlist',   $gtree2, 'access_write', 'size="10"', 'value', 'text', $config->access_write );
$this->access_moderate= JHTML::_('select.genericlist',   $gtree3, 'access_moderate', 'size="10" style="width:180px;"', 'value', 'text', $config->access_moderate );

$captcha = array(
			JHTML::_('select.option',  '0', JText::_( 'None' ) ),
			JHTML::_('select.option',  '1', JText::_( 'Guests' ) ),
			JHTML::_('select.option',  '2', JText::_( 'All' ) )
		);

$this->captcha=JHTML::_('select.genericlist',   $captcha, 'captcha', 'size="3" class="inputbox" style="width:180px;"', 'value', 'text', $config->captcha );

$shablons=$model->getshablons($config->shablon);

$allblogs=$model->getallblogs();
$allusers=$model->getallusers();

        $this->assignRef( 'enabled_bbcode', $enabled_bbcode );
        $this->assignRef( 'enabled_smiley', $enabled_smiley );
        $this->assignRef( 'import_plugins', $import_plugins );
        $this->assignRef( 'idblog', $idblog );
        $this->assignRef( 'idcommunity', $idcommunity );
        $this->assignRef( 'shablons', $shablons );
       $this->assignRef( 'allblogs', $allblogs );
        $this->assignRef( 'allusers', $allusers );
        parent::display($tpl);
    }
}

?>