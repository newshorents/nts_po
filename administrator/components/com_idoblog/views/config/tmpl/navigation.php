<div class="submenu-box">
	<div class="submenu-pad">
		<ul id="submenu" class="configuration">
			<li><a id="main" class="active"><?php echo JText::_( 'Main' ); ?></a></li>
			<li><a id="notifications"><?php echo JText::_( 'Notifications' ); ?></a></li>
			<li><a id="permissions"><?php echo JText::_( 'Permissions' ); ?></a></li>
			<li><a id="statistics"><?php echo JText::_( 'Statistics' ); ?></a></li>
		</ul>
		<div class="clr"></div>
	</div>
</div>
<div class="clr"></div>
