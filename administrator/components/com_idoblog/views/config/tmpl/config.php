<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
?>


<form action="index.php" method="post" name="adminForm" id="adminForm">

<div id="config-document">
	<div id="page-main">
    <fieldset class="adminform">
        <legend><?php echo JText::_( 'Main' ); ?></legend>
        <table class="admintable" width="50%" style="float:left">
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Blog's section" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->idblog; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Type name" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->typename; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Shablon" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->shablons; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="skin">
                    <?php echo JText::_( "Date format" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->dateformat; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="fullavatar">
                    <?php echo JText::_( "Avatar width" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->avatarwidth; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="skin">
                    <?php echo JText::_( "Small avatar width" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->smallavatarwidth; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="skin">
                    <?php echo JText::_( "NUMBER POSTS" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->limit; ?>
            </td>
        </tr>
</table>
        <table class="admintable" width="50%" style="float:left">
        <tr>
            <td width="100" align="right" class="key">
                <label for="skin">
                    <?php echo JText::_( "Users adsense enabled" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->adsense_enabled; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="skin">
                    <?php echo JText::_( "Users templates enabled" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->template_enabled; ?>
            </td>
        </tr>
       <tr>
            <td width="100" align="right" class="key">
                <label for="skin">
                    <?php echo JText::_( "Calculate online users" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->online_enabled; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="hits">
                    <?php echo JText::_( "Show hits" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->show_hits; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="avatars">
                    <?php echo JText::_( "Show avatars" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->show_avatars; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="avatars">
                    <?php echo JText::_( "IMPORT PLUGINS GROUP CONTENT" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->import_plugins; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="avatars">
                    <?php echo JText::_( "ENABLED BBCODE" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->enabled_bbcode; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="avatars">
                    <?php echo JText::_( "ENABLED SMILEY" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->enabled_smiley; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="avatars">
                    <?php echo JText::_( "USER'S PERSONAL FILE ARCHIVE" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->file_archive; ?>
            </td>
        </tr>
    </table>
    </fieldset>
</div>


			<div id="page-notifications">
    <fieldset class="adminform">
        <legend><?php echo JText::_( 'Notifications' ); ?></legend>
        <table class="admintable">
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Admin email" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->adminemail; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Notification admin new entrys" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->adminnewentry; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Notification admin new comments" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->adminnewcomment; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Notification author new comment" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->authornewcomment; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Notification user new comment" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->usernewcomment; ?>
            </td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Notification letter. Use [username], [topicname], [sitename], [link], [authorname], [message]" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->letter; ?>
            </td>
        </tr>
		</table>
    </fieldset>
</div>


<div id="page-permissions">
    <fieldset class="adminform">
        <legend><?php echo JText::_( 'Permissions' ); ?></legend>
       <table class="admintable">
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Who can to comment" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->access_comment; ?>
            </td>
			<td><?php echo JText::_( "Select group, which can to comment. All above selection will be able to comment too." ); ?></td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Who can to write into blog" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->access_write; ?>
            </td>
			<td><?php echo JText::_( "Select group, which can to write into blog. All above selection will be able to write too." ); ?></td>
        </tr>
       <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "Who can to moderate blog" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->access_moderate; ?>
            </td>
			<td><?php echo JText::_( "Select group, which can to moderate blog. All above selection will be able to moderate too." ); ?></td>
        </tr>
       <tr>
            <td width="100" align="right" class="key">
                <label for="captcha">
                    <?php echo JText::_( "Captcha" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->captcha; ?>
            </td>
			<td><?php echo JText::_( "Select group, that will use captcha" ); ?></td>
        </tr>
</table>
	</fieldset>
</div>

			<div id="page-statistics">
    <fieldset class="adminform">
        <legend><?php echo JText::_( 'Statistics' ); ?></legend>
        <table class="admintable">
        <tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "All Users:" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->allusers; ?>
            </td>
			<td rowspan="2" width="90%"></td>
        </tr>
        <tr>
            <td width="100" align="right" class="key">
                <label for="greeting">
                    <?php echo JText::_( "All blogs:" ); ?>:
                </label>
            </td>
            <td>
<?php echo $this->allblogs; ?>
            </td>
        </tr>
		</table>
    </fieldset>
</div>
</div>

<div class="clr"></div>

<input type="hidden" name="option" value="com_idoblog" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="config" />
</form>
