<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

class idoblogsControllertags extends idoblogsController
{
    /**
     * метод вывода на экран
     *
     * @access    public
     */
 
 
    function tags()
    {

	JRequest::setVar( 'view', 'tags' );
	JRequest::setVar( 'layout', 'tags'  );
	    parent::display();
    }
	
    function edittag()
    {

	JRequest::setVar( 'view', 'tags' );
	JRequest::setVar( 'layout', 'edit'  );
	JRequest::setVar('hidemainmenu', 1);
	
	    parent::display();
    }	

function cancel()
{
    $msg = JText::_( 'Operation Cancelled' );
    $this->setRedirect( 'index.php?option=com_idoblog&controller=tags&view=tags&layout=tags', $msg );
}

function save()
{
    $model = $this->getModel('tags');

    if ($model->store()) {
        $msg = JText::_( 'Greeting Saved!' );
    } else {
        $msg = JText::_( 'Error Saving Greeting' );
    }
    $this->setRedirect( 'index.php?option=com_idoblog&controller=tags&view=tags&layout=tags', $msg );

}


function deletetags()
{
    $model = $this->getModel('tags');

    if ($model->deletetags()) {
        $msg = JText::_( 'Greeting Delete!' );
    } else {
        $msg = JText::_( 'Error Delete' );
    }
    $this->setRedirect( 'index.php?option=com_idoblog&controller=tags&view=tags&layout=tags', $msg );
}

function addtags()
{
    $model = $this->getModel('tags');

    if ($model->addtags()) {
        $msg = JText::_( 'Tags created!' );
    } else {
        $msg = JText::_( 'Error creating' );
    }
    $this->setRedirect( 'index.php?option=com_idoblog&controller=tags&view=tags&layout=tags', $msg );
}

}

?>