<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

class idoblogsControllerConfig extends idoblogsController
{
    /**
     * метод вывода на экран
     *
     * @access    public
     */
    function display()
    {
	JRequest::setVar( 'view', 'config' );
	JRequest::setVar( 'layout', 'config'  );

	    parent::display();
    }
	
	function saveconfig()
	{
	$model = $this->getModel('config');

	  if ($model->saveconfig($post)) {
        $msg = JText::_( 'Greeting Saved!' );
    } else {
        $msg = JText::_( 'Error Saving Greeting' );
    }

$link = 'index.php?option=com_idoblog&task=config&controller=config&view=config&layout=config';
$this->setRedirect($link, $msg);
	}


}

?>