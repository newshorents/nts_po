<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
$db =& JFactory::getDBO();

$query="SELECT * FROM #__sections WHERE title='blogs'";
$db->setQuery( $query );
$idblog=$db->loadResult();

$query="CREATE TABLE `#__idoblog_user_reffer` (
`iduser` INT( 11 ) NOT NULL ,
`idcategory` INT( 11 ) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__idoblog_tags_reffer` (
`idarticle` INT( 11 ) NOT NULL,
`idtag` INT( 11 ) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__idoblog_tags` (
  `id` int(11) NOT NULL auto_increment,
  `tagname` varchar(35) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__idoblog_comments` (
`id` INT( 11 ) NOT NULL auto_increment,
`parent` INT( 11 ) NOT NULL,
`idarticle` INT( 11 ) NOT NULL,
`created_by` INT( 11 ) NOT NULL,
  `date` datetime NOT NULL,
  `text` text NOT NULL,
  `publish` int(3) NOT NULL,
  `username` varchar(15) NOT NULL,
  `email` varchar(15) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__idoblog_friends` (
  `id` int(11) NOT NULL,
  `idfriend` int(11) NOT NULL,
  `both` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__idoblog_users` (
  `iduser` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `icq` varchar(10) NOT NULL,
  `about` text NOT NULL,
  `small_avatar` varchar(100) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `description` text NOT NULL,
  `template` varchar(20) NOT NULL,
  `adsense_pub` varchar(20) NOT NULL,
  `adsense_slot` varchar(15) NOT NULL,
  `adsense_type` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
$db->setQuery( $query );
$db->query();

if (empty($idblog)) {
$query="INSERT INTO #__sections ( `id` , `title` , `name` , `alias` , `image` , `scope` , `image_position` , `description` , `published` , `checked_out` , `checked_out_time` , `ordering` , `access` , `count` , `params` )
VALUES (
'', 'blogs', '', 'blogs', '', 'content', 'left', '', '1', '0', '0000-00-00 00:00:00', '0', '0', '0', '');";
$db->setQuery( $query );
$db->query();
$idblog=$db->insertid();
}

$config = new JRegistry('config');
$config_array = array();

$config_array['idblog']	= $idblog;
$config_array['typename']= '1';
$config_array['shablon']='universal';
$config_array['dateformat']='%Y.%m.%d';
$config_array['adminemail']='';
$config_array['adminnewentry']='0';
$config_array['adminnewcomment']='0';
$config_array['authornewcomment']='1';
$config_array['usernewcomment']='1';
$config_array['avatarwidth']='100';
$config_array['smallavatarwidth']='50';

$config_array['letter']='Hello [username],

You are receiving this notification because you are watching the topic,
[topicname] at [sitename]. You can use the following
link to view the replies made.

If you want to view the newest post made since your last visit, click the
following link:
[link]

________________________________________
[authorname] write:
[message]
________________________________________

--
No reply. This message robot generated.';
$config_array['adsense_enabled']="1";
$config_array['template_enabled']="1";
$config_array['show_avatars']="1";
$config_array['online_enabled']='1';
$config_array['show_hits']="1";
$config_array['import_plugins']="1";
$config_array['enabled_smiley']="1";
$config_array['enabled_bbcode']="1";
$config_array['limit']="20";
$config_array['access_comment']="29";
$config_array['access_write']="18";
$config_array['access_moderate']="24";
$config_array['captcha']="1";
$config_array['file_archive']="1";

$config->loadArray($config_array);

$fname = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_idoblog'.DS.'config.php';
jimport('joomla.filesystem.path');
jimport('joomla.filesystem.archive');
		JClientHelper::setCredentialsFromRequest('ftp');
		$ftp = JClientHelper::getCredentials('ftp');
if (!$ftp['enabled'] && JPath::isOwner($fname) && !JPath::setPermissions($fname, '0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make config.php writable');
}


if (JFile::write($fname, $config->toString('PHP', 'config', array('class' => 'KBconfig')))) {
			$msg = JText::_('The Configuration Details have been updated');
		} else {
			$msg = JText::_('ERRORCONFIGFILE');
		}



		$archivename = JPATH_SITE.DS.'components'.DS.'com_idoblog'.DS.'templates.zip';

		// Temporary folder to extract the archive into
		$tmpdir = JPATH_SITE.DS.'components'.DS.'com_idoblog'.DS.'assets'.DS;

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean($tmpdir);
		$archivename = JPath::clean($archivename);

		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false ) {
		echo $archivename."<br>";
		echo $extractdir."<br>";
		echo "error extract";
		die();
		}
$imagedir=JPATH_SITE.DS.'components'.DS.'com_idoblog'.DS.'assets'.DS.'images';
if (!$ftp['enabled'] && JPath::isOwner($imagedir) && !JPath::setPermissions($imagedir, '0755','0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make images dir writable');
}

if (!JFile::exists(JPATH_SITE.'/images/idoblog/gallery/default.gif'))
{

//Extract images
		$archivename = JPATH_SITE.DS.'components'.DS.'com_idoblog'.DS.'idoblog.zip';

		// Temporary folder to extract the archive into
		$tmpdir = JPATH_SITE.DS.'images'.DS;

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean($tmpdir);
		$archivename = JPath::clean($archivename);

		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false ) {
		echo $archivename."<br>";
		echo $extractdir."<br>";
		echo "error extract";
		die();
		}


$imagesdir=JPATH_SITE.DS.'images'.DS.'idoblog';
if (!$ftp['enabled'] && JPath::isOwner($imagesdir) && !JPath::setPermissions($imagesdir, '0755','0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make media images dir writable');
}

if (!$ftp['enabled'] && JPath::isOwner($imagesdir.DS.'avatars') && !JPath::setPermissions($imagesdir.DS.'avatars', '0755','0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make avatars dir writable');
}

if (!$ftp['enabled'] && JPath::isOwner($imagesdir.DS.'avatars'.DS.'tmp') && !JPath::setPermissions($imagesdir.DS.'avatars'.DS.'tmp', '0755','0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make avatars dir writable');
}

if (!$ftp['enabled'] && JPath::isOwner($imagesdir.DS.'gallery') && !JPath::setPermissions($imagesdir.DS.'gallery', '0755','0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make gallery dir writable');
}

if (!$ftp['enabled'] && JPath::isOwner($imagesdir.DS.'upload') && !JPath::setPermissions($imagesdir.DS.'upload', '0755','0755')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make upload dir writable');
}
}


?>