<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
GLOBAL $live_site;
$live_site=JURI::base();

require_once( JPATH_COMPONENT.DS.'controller.php' );

if($controller = JRequest::getWord('controller')) {
    $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
    if (file_exists($path)) {
        require_once $path;
    } else {
        $controller = '';
    }
}

$classname    = 'idoblogsController'.$controller;
$controller   = new $classname( );
$controller->execute( JRequest::getVar( 'task' ) );

$controller->redirect();

echo '<br>This work is licensed under GPL License.';

?>