SELECT t3.*, jos_tm_user_project.start_date, jos_tm_user_project.end_date, jos_tm_user_project.hours_defect FROM (
SELECT year(date) as yeard, month(date) as monthd, type, t2.id_project, sum(numhours) as tothours, id as id_user, count(date) as numimpute
 FROM (
SELECT min(type) as type, date, id, id_project, numhours
FROM (
			(
			SELECT id,id_project,date, sum(numhours) as numhours, 1 as type from (
			(
			SELECT
						jos_users.id,jos_tm_impute_week_project.id_project,jos_tm_impute_day_project.impute_date AS date,jos_tm_impute_day_project.number_hours AS numhours
						FROM
						jos_users
						INNER JOIN jos_tm_impute_week_project ON jos_tm_impute_week_project.id_user = jos_users.id
						INNER JOIN jos_tm_impute_day_project ON jos_tm_impute_week_project.id = jos_tm_impute_day_project.id_week
						WHERE (WEEKDAY(jos_tm_impute_day_project.impute_date) <> 5 AND WEEKDAY(jos_tm_impute_day_project.impute_date) <> 6)
						AND jos_tm_impute_day_project.impute_date >= DATE_FORMAT(CONCAT(YEAR(CURDATE()),'-', MONTH(CURDATE()),'-','01'),'%Y-%m-%d')
			)
			UNION
			(
			SELECT
					jos_users.id AS id,jos_tm_user_project.id_project as id_project,jos_tm_impute_day_absence.impute_date as date,SUM(jos_tm_impute_day_absence.number_hours) as numhours
					FROM
					jos_users
					INNER JOIN jos_tm_impute_week_absence ON jos_tm_impute_week_absence.id_user = jos_users.id
					INNER JOIN jos_tm_impute_day_absence ON jos_tm_impute_day_absence.id_week = jos_tm_impute_week_absence.id
					INNER JOIN jos_tm_user_project ON jos_tm_user_project.id_user = jos_users.id
					WHERE jos_tm_impute_week_absence.id_absence <> 2
						AND jos_tm_impute_week_absence.id_absence <> 4
						AND jos_tm_impute_week_absence.id_absence <> 6
						AND jos_tm_impute_week_absence.id_absence <> 7
						AND jos_tm_impute_day_absence.impute_date BETWEEN jos_tm_user_project.start_date AND jos_tm_user_project.end_date
						AND jos_tm_impute_day_absence.impute_date >= DATE_FORMAT(CONCAT(YEAR(CURDATE()),'-', MONTH(CURDATE()),'-','01'),'%Y-%m-%d')
					GROUP BY jos_tm_user_project.id_project, jos_users.id, year, jos_tm_impute_day_absence.impute_date, id_absence
					HAVING numhours > 0
			)
			) as st1 
			GROUP BY id_project, id, date
			ORDER BY id_project asc, id asc, date asc
			)
			UNION
			(
			SELECT
			jos_users.id,
			jos_tm_future_resources.id_project,
			jos_tm_future_resources.date_resource AS date,
			jos_tm_future_resources.hours_defect AS numhours,
			2 as type
			FROM
			jos_tm_future_resources
			INNER JOIN jos_users ON jos_tm_future_resources.id_user = jos_users.id
			WHERE jos_tm_future_resources.date_resource BETWEEN 
					(select jos_tm_projects.start_date FROM jos_tm_projects WHERE jos_tm_projects.id=jos_tm_future_resources.id_project)
					 AND 
					(select jos_tm_projects.end_date FROM jos_tm_projects WHERE jos_tm_projects.id=jos_tm_future_resources.id_project)
					AND					
						(SELECT IF((jos_tm_future_resources.date_resource BETWEEN start_date AND end_date),1,0) as validdate FROM	jos_tm_user_project
								WHERE id_project=jos_tm_future_resources.id_project AND id_user = jos_tm_future_resources.id_user
								AND IF((jos_tm_future_resources.date_resource BETWEEN start_date AND end_date),1,0)
							) is not null
			 AND (WEEKDAY(jos_tm_future_resources.date_resource) <> 5 AND WEEKDAY(jos_tm_future_resources.date_resource) <> 6)
			 AND (
					(select if ((SELECT 
												jos_tm_scorecard.`id`
												FROM
												jos_tm_scorecard
												where year = year(jos_tm_future_resources.date_resource) and month = (month(jos_tm_future_resources.date_resource)-1)
												GROUP BY jos_tm_scorecard.`year`, jos_tm_scorecard.`month`) is null, 0, 1) as isclose) = 0
						)
			AND jos_tm_future_resources.date_resource >= DATE_FORMAT(CONCAT(YEAR(CURDATE()),'-', MONTH(CURDATE()),'-','01'),'%Y-%m-%d'
			)
		)
	) as t1
GROUP BY id_project, id, date 
) AS t2
GROUP BY t2.id_project,id, yeard, monthd
ORDER BY t2.id_project, yeard, monthd
) AS t3 
INNER JOIN jos_tm_user_project ON t3.id_project = jos_tm_user_project.id_project AND jos_tm_user_project.id_user=t3.id_user
