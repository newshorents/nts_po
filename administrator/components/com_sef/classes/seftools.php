<?php
/**
 * SEF component for Joomla! 1.5
 *
 * @author      ARTIO s.r.o.
 * @copyright   ARTIO s.r.o., http://www.artio.cz
 * @package     JoomSEF
 * @version     3.1.0
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_sef'.DS.'tables'.DS.'extension.php');
jimport( 'joomla.filesystem.file' );

class SEFTools
{
    function getSEFVersion()
    {
        static $version;

        if( !isset($version) ) {
            $xml = JFactory::getXMLParser('Simple');

            $xmlFile = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_sef'.DS.'sef.xml';

            if( JFile::exists($xmlFile) ) {
                if( $xml->loadFile($xmlFile) ) {
                    $root =& $xml->document;
                    $element =& $root->getElementByPath('version');
                    $version = $element ? $element->data() : '';
                }
            }
        }

        return $version;
    }

    function getExtVersion($extension)
    {
        $xml =& SEFTools::getExtXML($extension);
        $version = null;

        if( $xml ) {
            $root =& $xml->document;
            $ver = $root->attributes('version');
            if( ($root->name() == 'install') && version_compare($ver, '1.5', '>=') && ($root->attributes('type') == 'sef_ext') ) {
                $element =& $root->getElementByPath('version');
                $version = $element ? $element->data() : '';
            }
        }

        return $version;
    }

    /**
     * Returns extension name from its XML file.
     *
     * @return string
     */
    function getExtName($extension)
    {
        $xml =& SEFTools::getExtXML($extension);
        $name = null;

        if( $xml ) {
            $root =& $xml->document;
            $ver = $root->attributes('version');
            if( ($root->name() == 'install') && version_compare($ver, '1.5', '>=') && ($root->attributes('type') == 'sef_ext') ) {
                $element =& $root->getElementByPath('name');
                $name = $element ? $element->data() : '';
            }
        }

        return $name;
    }

    /**
     * Returns the extension XML object
     *
     * @param string $extension     Extension option
     * @return JSimpleXML           Extension XML
     */
    function &getExtXML($extension)
    {
        static $xmls;

        if( !isset($xmls) ) {
            $xmls = array();
        }

        if( !isset($xmls[$extension]) ) {
            $xmls[$extension] = null;

            $xmlFile = JPATH_ROOT.DS.'components'.DS.'com_sef'.DS.'sef_ext'.DS.$extension.'.xml';
            if( JFile::exists($xmlFile) ) {
                $xmls[$extension] = JFactory::getXMLParser('Simple');
                if( !$xmls[$extension]->loadFile($xmlFile) ) {
                    $xmls[$extension] = null;
                }
            }
        }

        return $xmls[$extension];
    }

    function getLangCode($langTag = null)
    {
        // Get current language tag
        if( is_null($langTag) ) {
            $lang =& JFactory::getLanguage();
            $langTag = $lang->getTag();
        }

        $jfm =& JoomFishManager::getInstance();
        $code = $jfm->getLanguageCode($langTag);

        return $code;
    }

    function getLangId($langTag = null)
    {
        // Get current language tag
        if( is_null($langTag) ) {
            $lang =& JFactory::getLanguage();
            $langTag = $lang->getTag();
        }

        $jfm =& JoomFishManager::getInstance();
        $id = $jfm->getLanguageID($langTag);

        return $id;
    }

    function getLangLongCode($langCode = null)
    {
        static $codes;

        // Get current language code
        if( is_null($langCode) ) {
            $lang =& JFactory::getLanguage();
            return $lang->getTag();
        }

        if( is_null($codes) ) {
            $codes = array();

            $jfm =& JoomFishManager::getInstance();
            $langs =& $jfm->getLanguages(false);
            if( !empty($langs) ) {
                foreach($langs as $lang) {
                    $codes[$lang->shortcode] = $lang->code;
                }
            }
        }

        if( isset($codes[$langCode]) ) {
            return $codes[$langCode];
        }

        return null;
    }

    /**
     * Returns JParameter object representing extension's parameters
     *
     * @param	string          Extension name
     * @return	JParameter      Extension's parameters
     */
    function &getExtParams($option)
    {
        $db =& JFactory::getDBO();

        static $exts, $params;

        if( !isset($exts) ) {
            $query = "SELECT `file`, `params` FROM `#__sefexts`";
            $db->setQuery($query);
            $exts = $db->loadObjectList('file');
        }

        if( !isset($params) ) {
            $params = array();
        }
        if( !isset($params[$option]) ) {
            $data = '';
            if( isset($exts[$option.'.xml']) ) {
                $data = $exts[$option.'.xml']->params;
            }
            $params[$option] = new JParameter($data);

            // Set the default parameters renderer
            $xml =& SEFTools::getExtsDefaultParamsXML();
            if( is_a($xml, 'JSimpleXMLElement') ) {
                $p =& $xml->getElementByPath('params');
                $params[$option]->setXML($p);
            }

            // Set the extension's parameters renderer
            $pxml =& SEFTools::getExtParamsXML($option);
            if( is_a($pxml, 'JSimpleXMLElement') ) {
                $params[$option]->setXML($pxml);
            }
        }

        return $params[$option];
    }

    /**
     * Returns the JSimpleXMLElement object representing
     * the default parameters for every extension
     * 
     * @return JSimpleXMLElement	Extensions' default parameters
     */
    function &getExtsDefaultParamsXML() {
        static $xml;

        if( isset($xml) ) {
            return $xml;
        }

        $xml = null;
        $xmlpath = JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_sef'.DS.'extensions_params.xml';

        if( JFile::exists($xmlpath) ) {
            $parser = JFactory::getXMLParser('Simple');
            if( $parser->loadFile($xmlpath) ) {
                $xml =& $parser->document;
            }
        }

        return $xml;
    }

    /**
     * Returns the JSimpleXMLElement object representing
     * the extension's parameters
     *
     * @param string $option		Extension name
     * @return JSimpleXMLElement	Extension's parameters
     */
    function &getExtParamsXML($option) {
        static $xmls;

        if( !isset($xmls) ) {
            $xmls = array();
        }

        if( !isset($xmls[$option]) ) {
            $xmls[$option] = null;

            $xml =& SEFTools::getExtXML($option);

            if( $xml ) {
                $document =& $xml->document;

                if( isset($document->params[0]->param) ) {
                    // Remove the parameters that are duplicate with common ones
                    $hide = array();
                    $hideNames = array('ignoreSource', 'itemid', 'overrideId', 'customNonSef');

                    // Collect elements to remove
                    for( $i=0, $n=count($document->params[0]->param); $i<$n; $i++ ) {
                        if( in_array($document->params[0]->param[$i]->attributes('name'), $hideNames) ) {
                            $hide[]	=& $document->params[0]->param[$i];
                        }
                    }

                    // Remove elements
                    for( $i=0, $n=count($hide); $i<$n; $i++ ) {
                        $document->params[0]->removeChild( $hide[$i] );
                    }

                    $xmls[$option] =& $document->params[0];
                }
            }
        }

        return $xmls[$option];
    }

    /** Returns the array of texts used by the extension for creating URLs
     *  in currently selected language (for JoomFish support)
     *
     * @param	string  Extension name
     * @return	array   Extension's texts
     */
    function getExtTexts ($option, $lang = '')
    {
        $database =& JFactory::getDBO();

        static $extTexts;

        if ($option == '') {
            return false;
        }

        // Set the language
        if ($lang == '') {
            $lang = SEFTools::getLangLongCode();
        }
        if (! isset($extTexts)) {
            $extTexts = array();
        }
        if (! isset($extTexts[$option])) {
            $extTexts[$option] = array();
        }
        if (! isset($extTexts[$option][$lang])) {
            $extTexts[$option][$lang] = array();
            // If lang is different than current language, change it
            if ($lang != SEFTools::getLangLongCode()) {
                $language =& JFactory::getLanguage();
                $oldLang = $language->setLanguage($lang);
                $language->load();
            }
            $query = "SELECT `id`, `name`, `value` FROM `#__sefexttexts` WHERE `extension` = '$option'";
            $database->setQuery($query);
            $texts = $database->loadObjectList();
            if (is_array($texts) && (count($texts) > 0)) {
                foreach (array_keys($texts) as $i) {
                    $name = $texts[$i]->name;
                    $value = $texts[$i]->value;
                    $extTexts[$option][$lang][$name] = $value;
                }
            }
            // Set the language back to previously selected one
            if (isset($oldLang) && ($oldLang != SEFTools::getLangLongCode())) {
                $language =& JFactory::getLanguage();
                $language->setLanguage($oldLang);
                $language->load();
            }
        }
        return $extTexts[$option][$lang];
    }

    function RemoveVariable ($url, $var, $value = '')
    {
        if ($value == '') {
            $newurl = eregi_replace("(&|\?)$var=[^&]*", '\\1', $url);
        } else {
            $trans = array('?' => '\\?', '.' => '\\.', '+' => '\\+', '*' => '\\*', '^' => '\\^', '$' => '\\$');
            $value = strtr($value, $trans);
            $newurl = eregi_replace("(&|\?)$var=$value(&|\$)", '\\1\\2', $url);
        }
        $newurl = trim($newurl, '&?');
        $trans = array('&&' => '&' , '?&' => '?');
        $newurl = strtr($newurl, $trans);
        return $newurl;
    }

    function fixVariable(&$uri, $varName)
    {
        $value = $uri->getVar($varName);
        if( !is_null($value) ) {
            $pos = strpos($value, ':');
            if( $pos !== false ) {
                $value = substr($value, 0, $pos);
                $uri->setVar($varName, $value);
            }
        }
    }

    function ReplaceAll($search, $replace, $subject)
    {
        while( strpos($subject, $search) !== false ) {
            $subject = str_replace($search, $replace, $subject);
        }

        return $subject;
    }
    
    /**
     * Checks whether JoomFish is installed
     *
     * @return boolean
     */
    function JoomFishInstalled()
    {
        static $installed;
        
        if( !isset($installed) ) {
            $installed = JFile::exists(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_joomfish'.DS.'joomfish.php');
        }
        
        return $installed;
    }
    
}

?>
