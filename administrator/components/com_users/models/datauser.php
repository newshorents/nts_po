<?php

/**
 * @version		1.0.0 projectopen $
 * @package		projectopen
 * @copyright	Copyright © 2010 - All rights reserved.
 * @license		GNU/GPL
 * @author
 * @author mail	nobody@nobody.com
 *
 *
 * @MVC architecture generated by MVC generator tool at http://www.alphaplug.com
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class usersModelDatauser extends Jmodel {

    var $dataUserCategory;

    public function dataUser($id) {
        $db = & JFactory::getDBO();

        $query = "SELECT * FROM #__tm_personal_type WHERE name = 'default'";
        $db->setQuery( $query );
        $val = $db->loadObjectList();
        $id_personal_type = $val[0]->id;

        $query = "SELECT * FROM #__tm_category WHERE name = 'default'";
        $db->setQuery( $query );
        $val = $db->loadObjectList();
        $id_category = $val[0]->id;

        $obj->user_id = $id;
        $obj->personal_type_id = $id_personal_type;//JRequest::getVar('personal_type_id');
        $obj->start_date = JRequest::getVar('start_date');
        $obj->end_date = JRequest::getVar('end_date');
        $obj->category_id = $id_category;//JRequest::getVar('personal_type_id');

        //$this->dataUserCategory = json_decode(JRequest::getVar('user_category'), true);
        //$obj->category_id = $id_category;//$this->dataUserCategory[(count($this->dataUserCategory)-1)]["idCategory"];
        //echo JRequest::getVar('id_data_user');die;
        //if ($obj->user_id != "") {
            //$obj->id = JRequest::getVar('id_data_user');
            //$db->updateObject("#__tm_data_user", $obj, 'id', true);
            //echo $db->ErrorMsg();die;
            //$this->userCategory($id);
        //} else {
            if ($db->insertObject("#__tm_data_user", $obj, 'id', true) != 1) {
                //echo $db->ErrorMsg();die;
                //return false;
            }
            //$this->userCategory($id);
       // }
    }

    private function userCategory($idUser) {
        $db = & JFactory::getDBO();
        $query = "Delete from #__tm_user_category where id_user = " . $idUser;
        $db->setQuery($query);
        $db->query();

        $userCategory = null;
        for ($i = 0; $i < count($this->dataUserCategory); $i++) {
            $userCategory->id_user = $idUser;
            $userCategory->id_category = $this->dataUserCategory[$i]["idCategory"];
            $userCategory->effective_date = $this->dataUserCategory[$i]["effectiveDate"];
            if ($db->insertObject("#__tm_user_category", $userCategory, 'id', true) != 1) {
                //echo $db->ErrorMsg();die;
                return false;
            }
        }
        return true;
    }

    public function deleteUser($idUser) {
        $db = & JFactory::getDBO();
        $query = "Delete from #__tm_data_user where user_id = " . $idUser;
        $db->setQuery($query);
        $db->query();
    }

}

?>