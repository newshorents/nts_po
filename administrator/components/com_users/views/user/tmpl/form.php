<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
JHTML::_('behavior.tooltip');
JHTML::_('behavior.calendar');
?>

<?php
$cid = JRequest::getVar('cid', array(0));
$edit = JRequest::getVar('edit', true);
$text = intval($edit) ? JText::_('Edit') : JText::_('New');

JToolBarHelper::title(JText::_('User') . ': <small><small>[ ' . $text . ' ]</small></small>', 'user.png');
JToolBarHelper::save();
JToolBarHelper::apply();
if ($edit) {
    // for existing items the button is renamed `close`
    JToolBarHelper::cancel('cancel', 'Close');
} else {
    JToolBarHelper::cancel();
}
JToolBarHelper::help('screen.users.edit');
$cparams = JComponentHelper::getParams('com_media');
?>

<?php
// clean item data
JFilterOutput::objectHTMLSafe($this->user, ENT_QUOTES, '');

if ($this->user->get('lastvisitDate') == "0000-00-00 00:00:00") {
    $lvisit = JText::_('Never');
} else {
    $lvisit = JHTML::_('date', $this->user->get('lastvisitDate'), '%Y-%m-%d %H:%M:%S');
}
?>
<script language="javascript" type="text/javascript">
    function submitbutton(pressbutton) {
        var form = document.adminForm;
        if (pressbutton == 'cancel') {
            submitform( pressbutton );
            return;
        }
        var r = new RegExp("[\<|\>|\"|\'|\%|\;|\(|\)|\&]", "i");
        // do field validation
        if (trim(form.name.value) == "") {
            alert( "<?php echo JText::_('You must provide a name.', true); ?>" );
        } else if (form.username.value == "") {
            alert( "<?php echo JText::_('You must provide a user login name.', true); ?>" );
        } else if (r.exec(form.username.value) || form.username.value.length < 2) {
            alert( "<?php echo JText::_('WARNLOGININVALID', true); ?>" );
        } else if (trim(form.email.value) == "") {
            alert( "<?php echo JText::_('You must provide an email address.', true); ?>" );
        } else if (form.gid.value == "") {
            alert( "<?php echo JText::_('You must assign user to a group.', true); ?>" );
        } else if (((trim(form.password.value) != "") || (trim(form.password2.value) != "")) && (form.password.value != form.password2.value)){
            alert( "<?php echo JText::_('Password do not match.', true); ?>" );
        } else if (form.gid.value == "29") {
            alert( "<?php echo JText::_('WARNSELECTPF', true); ?>" );
        } else if (form.gid.value == "30") {
            alert( "<?php echo JText::_('WARNSELECTPB', true); ?>" );
        } else {
            submitform( pressbutton );
        }
    }

    function gotocontact( id ) {
        var form = document.adminForm;
        form.contact_id.value = id;
        submitform( 'contact' );
    }

    function validateData(date)
    {
        alert(date);
    }

    function numeros(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla==8) return true;
        patron = /\d/;
        te = String.fromCharCode(tecla);
        return patron.test(te);
    }
</script>
<form action="index.php" method="post" name="adminForm" autocomplete="off">
    <div class="col width-45">
        <fieldset class="adminform">
            <legend><?php echo JText::_('User Details'); ?></legend>
            <table class="admintable" cellspacing="1">
                <tr>
                    <td width="150" class="key">
                        <label for="name">
                            <?php echo JText::_('Name'); ?> <label style='color:#FF0000' >*</label>
                        </label>
                    </td>
                    <td>
                        <input type="text" name="name" id="name" class="inputbox" size="40" value="<?php echo $this->user->get('name'); ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="username">
                            <?php echo JText::_('Username'); ?> <label style='color:#FF0000' >*</label>
                        </label>
                    </td>
                    <td>
                        <input type="text" name="username" id="username" class="inputbox" size="40" value="<?php echo $this->user->get('username'); ?>" autocomplete="off" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="email">
                            <?php echo JText::_('Email'); ?> <label style='color:#FF0000' >*</label>
                        </label>
                    </td>
                    <td>
                        <input class="inputbox" type="text" name="email" id="email" size="40" value="<?php echo $this->user->get('email'); ?>" />
                    </td>
                </tr>
                <tr>
                    <td class="key">
                        <label for="password">
                            <?php echo JText::_('New Password'); ?>
                        </label>
                    </td>
                    <td>
                        <?php if (!$this->user->get('password')) : ?>
                                <input class="inputbox disabled" type="password" name="password" id="password" size="40" value="" disabled="disabled" />
                        <?php else : ?>
                                    <input class="inputbox" type="password" name="password" id="password" size="40" value=""/>
                        <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="key">
                                    <label for="password2">
                            <?php echo JText::_('Verify Password'); ?>
                                </label>
                            </td>
                            <td>
                        <?php if (!$this->user->get('password')) : ?>
                                        <input class="inputbox disabled" type="password" name="password2" id="password2" size="40" value="" disabled="disabled" />
                        <?php else : ?>
                                            <input class="inputbox" type="password" name="password2" id="password2" size="40" value=""/>
                        <?php endif; ?>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td class="key">
                                            <label for="price">
                            <?php echo JText::_('Time price'); ?>
                                        </label>
                                    </td>
                                    <td>
                                        <input class="inputbox" type="text" name="price" id="price" size="40" value="<?php echo (float) $this->price; ?>" onKeyPress="return numeros(event)" />
                                    </td>
                                </tr>-->
                                <tr>
                                    <td valign="top" class="key">
                                        <label for="gid">
                            <?php echo JText::_('Group'); ?> <label style='color:#FF0000' >*</label>
                                        </label>
                                    </td>
                                    <td>
                        <?php echo $this->lists['gid']; ?>
                                        </td>
                                    </tr>
                <?php if ($this->me->authorize('com_users', 'block user')) {
                ?>
                                                <tr>
                                                    <td class="key">
                        <?php echo JText::_('Block User'); ?>
                                            </td>
                                            <td>
                        <?php echo $this->lists['block']; ?>
                                            </td>
                                        </tr>
                <?php } if ($this->me->authorize('com_users', 'email_events')) {
                ?>
                                                <tr>
                                                    <td class="key">
                        <?php echo JText::_('Receive System Emails'); ?>
                                            </td>
                                            <td>
                        <?php echo $this->lists['sendEmail']; ?>
                                            </td>
                                        </tr>
                <?php } if ($this->user->get('id')) {
                ?>
                                                <tr>
                                                    <td class="key">
                        <?php echo JText::_('Register Date'); ?>
                                            </td>
                                            <td>
                        <?php echo JHTML::_('date', $this->user->get('registerDate'), '%Y-%m-%d %H:%M:%S'); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="key">
                        <?php echo JText::_('Last Visit Date'); ?>
                                            </td>
                                            <td>
                        <?php echo $lvisit; ?>
                                            </td>
                                        </tr>
                <?php } ?>
                                        </table>
                                    </fieldset>
                                </div>
                                <div class="col width-55">
                                    <fieldset class="adminform">
                                        <legend><?php echo JText::_('Parameters'); ?></legend>
                                        <table class="admintable">
                                            <tr>
                                                <td>
                        <?php
                                            $params = $this->user->getParameters(true);
                                            echo $params->render('params');
                        ?>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <fieldset class="adminform">
                                <legend><?php echo JText::_('Contact Information'); ?></legend>
            <?php if (!$this->contact) {
            ?>
                                                <table class="admintable">
                                                    <tr>
                                                        <td>
                                                            <br />
                                                            <span class="note">
                            <?php echo JText::_('No Contact details linked to this User'); ?>:
                                                <br />
                            <?php echo JText::_('SEECOMPCONTACTFORDETAILS'); ?>.
                                            </span>
                                            <br /><br />
                                        </td>
                                    </tr>
                                </table>
            <?php } else {
            ?>
                                                <table class="admintable">
                                                    <tr>
                                                        <td width="120" class="key">
                        <?php echo JText::_('Name'); ?>
                                            </td>
                                            <td>
                                                <strong>
                            <?php echo $this->contact[0]->name; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="key">
                        <?php echo JText::_('Position'); ?>
                                            </td>
                                            <td >
                                                <strong>
                            <?php echo $this->contact[0]->con_position; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="key">
                        <?php echo JText::_('Telephone'); ?>
                                            </td>
                                            <td >
                                                <strong>
                            <?php echo $this->contact[0]->telephone; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="key">
                        <?php echo JText::_('Fax'); ?>
                                            </td>
                                            <td >
                                                <strong>
                            <?php echo $this->contact[0]->fax; ?>
                                            </strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="key">
                        <?php echo JText::_('Misc'); ?>
                                            </td>
                                            <td >
                                                <strong>
                            <?php echo $this->contact[0]->misc; ?>
                                            </strong>
                                        </td>
                                    </tr>
                <?php if ($this->contact[0]->image) {
 ?>
                                                    <tr>
                                                        <td class="key">
<?php echo JText::_('Image'); ?>
                                                </td>
                                                <td valign="top">
                                                    <img src="<?php echo JURI::root() . $cparams->get('image_path') . '/' . $this->contact[0]->image; ?>" align="middle" alt="<?php echo JText::_('Contact'); ?>" />
                                                </td>
                                            </tr>
<?php } ?>
                                                <tr>
                                                    <td class="key">&nbsp;</td>
                                                    <td>
                                                        <div>
                                                            <br />
                                                            <input class="button" type="button" value="<?php echo JText::_('change Contact Details'); ?>" onclick="gotocontact( '<?php echo $this->contact[0]->id; ?>' )" />
                                                            <i>
                                                                <br /><br />
                                                                                                                                                                                                            								'<?php echo JText::_('Components -> Contact -> Manage Contacts'); ?>'
                                            </i>
                                        </div>
                                    </td>
                                </tr>
                            </table>
<?php } ?>
                                        </fieldset>
                                    </div>
                                    <!-- <div class="col width-45">
                                        <fieldset class="adminform">
                                            <legend><?php echo JText::_('Aditional Info'); ?></legend>
                                            <table class="admintable" cellspacing="1">
                                                <tr>
                                                    <td width="150" class="key">
                                                        <label for="personal_type">
<?php echo JText::_('Personal type'); ?>
                                        </label>
                                    </td>
                                    <td>
<?php echo JHTML::_('select.genericlist', $this->personal_types, 'personal_type_id', 'class="inputbox" size="1"', 'id', 'name', $this->userInfoAdd[0]->personal_type_id); ?>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="key">
                                            <label for="acces_type">
<?php echo JText::_('Category type'); ?>
                                        </label>
                                    </td>
                                    <td>
                                        <table border="1" id="category_table">
                                            <THEAD>
                                                <tr>
                                                    <th>
                                                        <label for="acces_type">
<?php echo JText::_('Effective date'); ?>
                                        </label>
                                    </th>
                                    <th>
                                        <label for="acces_type">
<?php echo JText::_('Category'); ?>
                                        </label>
                                    </th>
                                    <th>
                                        <label for="acces_type">
<?php echo JText::_('Remove'); ?>
                                        </label>
                                    </th>
                                </tr>
                            </THEAD>

                            <tr>
                                <td>
                                    <input class="demo_ranged" onchange="validateDateEffective(this)" readonly id="category_date" name="category_date" size="20" type="text" value="<?php echo $this->userInfoAdd[0]->effective_date; ?>" />
                                    <img class="calendar" onclick="return showCalendar('category_date', '%Y-%m-%d');" src="templates/system/images/calendar.png" alt="calendar" />

                                </td>
                                <td>
<?php echo JHTML::_('select.genericlist', $this->categories, 'category_id', 'class="inputbox" size="1"', 'id', 'name', $this->userInfoAdd[0]->id_category); ?>
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </table>
                                <input type="button" id="add_category" name="add_category" value="<?php echo JText::_('Add Category'); ?>" onclick="addCategory()" />
                            </td>
                        </tr>
                        <tr>
                            <td class="key">
                                <label for="start_date">
<?php echo JText::_('Start date'); ?>
                                        </label>
                                    </td>
                                    <td>
                                        <input readonly id="start_date" name="start_date" size="20" type="text" value="<?php echo $this->userInfoAdd[0]->start_date; ?>" />
                                        <img class="calendar" onclick="return showCalendar('start_date', '%Y-%m-%d');" src="templates/system/images/calendar.png" alt="calendar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="key">
                                        <label for="end_date">
<?php echo JText::_('End date'); ?>
                                        </label>
                                    </td>
                                    <td>
                                        <input readonly id="end_date" name="end_date" size="20" type="text" value="<?php
                                            $endDate = $this->userInfoAdd[0]->end_date;
                                            if ($endDate != "0000-00-00") {
                                                echo $endDate;
                                            }
?>"/>
                                     <img class="calendar" onclick="return showCalendar('end_date', '%Y-%m-%d');" src="templates/system/images/calendar.png" alt="calendar" />
                                 </td>
                             </tr>
                         </table>
                     </fieldset>
                 </div> -->
                 <div class="clr"></div>

                 <input type="hidden" name="id" value="<?php echo $this->user->get('id'); ?>" />
                 <input type="hidden" name="cid[]" value="<?php echo $this->user->get('id'); ?>" />
                 <input type="hidden" name="option" value="com_users" />
                 <input type="hidden" name="task" value="" />
                 <input type="hidden" name="contact_id" value="" />
                 <input id="user_category" type="hidden" name="user_category" value="" />
                 <input type="hidden" name="id_data_user" value="<?php echo $this->userInfoAdd[0]->id ?>" />
<?php if (!$this->me->authorize('com_users', 'email_events')) { ?>
                                                <input type="hidden" name="sendEmail" value="0" />
    <?php } ?>
<?php echo JHTML::_('form.token'); ?>
</form>
