<?php
/**
* @package JCron Scheduler
* @email admin@joomlaplug.com
* @Copyright (C) 2006 JoomlaPlug.com. All rights reserved.
* JCron is a free software released under the GNU license.
* GNU General Access License.
*/

// Restrict direct access
//defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class TOOLBAR_JCRON {

 function ttasks(){

        mosMenuBar::startTable();

        mosMenuBar::publishList();

		mosMenuBar::spacer();

		mosMenuBar::unpublishList();

		mosMenuBar::spacer();
  
        mosMenuBar::addNewX();

		mosMenuBar::spacer();

		mosMenuBar::editListX();

		mosMenuBar::spacer();
  
        mosMenuBar::deleteList();

        mosMenuBar::spacer();

		mosMenuBar::cancel();

		mosMenuBar::spacer();

		mosMenuBar::help( 'screen.users.jcron' );

		mosMenuBar::endTable();

     }
  function ttasks_edit(){

        mosMenuBar::startTable();

		mosMenuBar::save();

		mosMenuBar::spacer();
  
        mosMenuBar::apply();

		mosMenuBar::spacer();

		mosMenuBar::cancel();

		mosMenuBar::spacer();

		mosMenuBar::help( 'screen.users.jcron' );

		mosMenuBar::endTable();

    }
     
 function tconfig(){

        mosMenuBar::startTable();

		mosMenuBar::save('config_save','Save');

		mosMenuBar::spacer();

		mosMenuBar::cancel();

		mosMenuBar::spacer();

		mosMenuBar::help( 'screen.users.jcron' );

		mosMenuBar::endTable();

     }
     
 function tabout(){

     }

 function tdefault(){

     }

}
?>
