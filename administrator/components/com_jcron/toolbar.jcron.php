<?php
/**
* @package JCron Scheduler
* @email admin@joomlaplug.com
* @Copyright (C) 2006 JoomlaPlug.com. All rights reserved.
* JCron is a free software released under the GNU license.
* GNU General Access License.
*/

// Restrict direct access
//defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( $mainframe->getPath( 'toolbar_html' ) );

switch ($task) {
        case 'new':
        case 'edit':
        case 'save':
         TOOLBAR_JCRON::ttasks_edit();
         break;
         
        case 'tasks':
         TOOLBAR_JCRON::ttasks();
         break;
         
         
        case 'config_save':
        case 'config':
         TOOLBAR_JCRON::tconfig();
         break;
         
        case 'about':
         TOOLBAR_JCRON::tabout();
         break;

        default:
         TOOLBAR_JCRON::ttasks();
         break;

}
?>
