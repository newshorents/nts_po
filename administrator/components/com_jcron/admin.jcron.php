<?php
/**
* @package JCron Scheduler
* @email admin@joomlaplug.com
* @Copyright (C) 2006 JoomlaPlug.com. All rights reserved.
* JCron is a free software released under the GNU license.
* GNU General Access License.
*/

// Restrict direct access
//defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

require_once( $mainframe->getPath( 'admin_html' ) );

$task2 =$_REQUEST['task2'];

$query = "SELECT * FROM #__jcron_config";
$database->setQuery( $query );
$data =  $database->loadObjectList();
foreach($data as $key=>$value){
    if (!get_magic_quotes_gpc()){
	    $value->value = stripslashes($value->value);
    }
    $_CONFIG[$value->name] = $value->value;
}
    
$_CONFIG['incl_code'] = "\n<"."?php /*JCron Code*/ $"."from_template = 1;@include('components/com_jcron/jcron.php');/*DO NOT REMOVE ANYTHING*/ ?".">\n";

switch($task){
    case 'about':
     fabout($option, $task, $_CONFIG);
     break;
     
    case 'new':
    case 'save':
    case 'apply':
    case 'edit':
     ftasks_edit($option, $task, $task2, $_CONFIG);
     break;
    case 'remove':
    case 'publish':
    case 'unpublish':
    case 'tasks':
     ftasks($option, $task, $_CONFIG);
     break;
     
    case 'config_save':
    case 'config':
     fconfig($option, $task, $_CONFIG);
     break;
     
    case 'cancel':
     fcancel($option, $task);
     break;

    default:
     ftasks($option, $task, $_CONFIG);
     break;

}

function ftasks_edit($option, $task, $task2, $_CONFIG){
    global $database;

    $query = "SELECT * FROM #__jcron_tasks where id='".$_REQUEST[id]."'";
    $database->setQuery( $query );
    $data =  $database->loadObjectList();
    $TASK = $_REQUEST['TASK'];

    if(($task == 'save') || ($task == 'apply')){

     if($_REQUEST['c_crontab']!=1)
      $TASK['mhdmd'] = $_REQUEST[minute2]." ".$_REQUEST[hour2]." ".$_REQUEST[day2]." ".$_REQUEST[month2]." ".$_REQUEST[weekday2];
     else
      $TASK['mhdmd'] = $_REQUEST['crontab'];


     if($task2 == 'new'){

         $query = "INSERT INTO #__jcron_tasks SET  task='".addslashes($TASK['task'])."',
                                                   type='".addslashes($TASK['type'])."',  
                                                   published='".addslashes($TASK['published'])."',
                                                   mhdmd='".addslashes($TASK['mhdmd'])."',
                                                   file='".addslashes($TASK['file'])."',
                                                   ok='".addslashes($TASK['ok'])."',
                                                   ran_at=now(),
                                                   log_text='".addslashes($TASK['log_text'])."'
                                                   
                                                  
                  ";
         $database->setQuery( $query );
         if (!$database->query()) {

	 	  echo "<script> alert('".$database->getErrorMsg()." on query: ".$query."'); window.history.go(-1); </script>\n";
		  exit();

	     }
        $id = $database->insertid();
        $msg = "Tasks successfully registered!";

        if($task == 'apply')
         mosRedirect( 'index2.php?option='.$option."&task=edit&id=$id" , $msg);
        else
         mosRedirect( 'index2.php?option='.$option."&task=tasks" , $msg);
        }
        
     if($task2 == 'edit'){

         $query = "UPDATE #__jcron_tasks SET       task='".addslashes($TASK['task'])."',
                                                   type='".addslashes($TASK['type'])."',  
                                                   published='".addslashes($TASK['published'])."',
                                                   mhdmd='".addslashes($TASK['mhdmd'])."',
                                                   file='".addslashes($TASK['file'])."',
                                                   ok='".addslashes($TASK['ok'])."',
                                                   ran_at=now(),
                                                   log_text='".addslashes($TASK['log_text'])."'

                  WHERE id='".$_REQUEST[id]."'
                  ";
         $database->setQuery( $query );
         if (!$database->query()) {

	 	  echo "<script> alert('".$database->getErrorMsg()." on query: ".$query."'); window.history.go(-1); </script>\n";
		  exit();

	     }

        $msg = "Tasks successfully registered!";
        $msg = "Tasks successfully registered!";
        if($task == 'apply')
         mosRedirect( 'index2.php?option='.$option."&task=edit&id=$_REQUEST[id]" , $msg);
        else
         mosRedirect( 'index2.php?option='.$option."&task=tasks" , $msg);
        }

    }

    JCRON_HTML::ttasks_edit($option, $task, $_CONFIG, $data[0]);
    }
    
function ftasks($option, $task, $_CONFIG){
    global $database;

    $query = "SELECT * FROM #__jcron_tasks";
    $database->setQuery( $query );
    $data =  $database->loadObjectList();

    $cid = $_REQUEST['cid'];

    if($_REQUEST[id]!='')
     $cid = array(0=>$_REQUEST[id]);

    if($task == 'remove'){

        foreach($cid as $id){

            $query = "DELETE FROM #__jcron_tasks WHERE id='".$id."'";
            $database->setQuery( $query );
            $database->query();
        
            }

        $msg = "Task(s) successfully deleted!";
        mosRedirect( 'index2.php?option='.$option."&task=tasks" , $msg);
        }

    if($task == 'publish'){

        foreach($cid as $id){

            $query = "UPDATE #__jcron_tasks SET published='1' WHERE id='".$id."'";
            $database->setQuery( $query );
            $database->query();

            }

        $msg = "Task(s) successfully published!";
        mosRedirect( 'index2.php?option='.$option."&task=tasks" , $msg);
        }
        
    if($task == 'unpublish'){

        foreach($cid as $id){

            $query = "UPDATE #__jcron_tasks SET published='0' WHERE id='".$id."'";
            $database->setQuery( $query );
            $database->query();

            }

        $msg = "Task(s) successfully un-published!";
        mosRedirect( 'index2.php?option='.$option."&task=tasks" , $msg);
        }
        

    JCRON_HTML::ttasks($option, $task, $_CONFIG, $data);
    }
function fconfig($option, $task, $_CONFIG){
    global $database, $mosConfig_absolute_path;
    
    $incl_code = $_CONFIG['incl_code'];
    
    $query = "SELECT template"
		   . "\n FROM #__templates_menu"
		   . "\n WHERE client_id = 0"
		   . "\n AND menuid = 0"
		   ;
    $database->setQuery( $query );

    $cur_template = $database->loadResult();

    $tfile = $mosConfig_absolute_path."/templates/".$cur_template."/index.php";
          
    $file = file_get_contents($tfile);


    if(strstr($file, $incl_code))
     $_CONFIG['iscron'] = true;
    else
     $_CONFIG['iscron'] = false;

    if(isset($_REQUEST['crond2'])){
        ####### ADD TEMPLATE CRON CODE
        if($fp = fopen($tfile, 'w')){
          fwrite($fp, $file.$incl_code);
          fclose($fp);
          $msg = "File $tfile successfully updated, cron is now running!";
           mosRedirect( 'index2.php?option='.$option."&task=config" , $msg);
          }
        else{
          $msg = "Could not open $tfile for writing, please add this code manually:<br/>
          <b>$incl_code</b>";
          mosRedirect( 'index2.php?option='.$option."&task=config" , $msg);
            }
        }
    elseif(isset($_REQUEST['crond1'])){
        ####### REMOVE TEMPLATE CRON CODE
        if($fp = fopen($tfile, 'w')){
          $file = str_replace($incl_code, "", $file);
          fwrite($fp, $file);
          fclose($fp);
          $msg = "File $tfile successfully updated, cron is now removed!";
          mosRedirect( 'index2.php?option='.$option."&task=config" , $msg);
          }
        else{
          $msg = "Could not open $tfile for writing, please remove this code manually:<br/>
          <b>$incl_code</b>";
          mosRedirect( 'index2.php?option='.$option."&task=config" , $msg);
            }

        }
    if($task == 'config_save'){

        $query = "DELETE FROM #__jcron_config";
        $database->setQuery( $query );
        $database->query();

        #print_r($_REQUEST);exit;
        foreach($_REQUEST[CONFIG] as $key=>$value)
         foreach($value as $k=>$v){

		 if (!get_magic_quotes_gpc()){
		    $k = str_replace("'", "", $k);
		 	$v = addslashes($v);
		 }else{
		    $k = str_replace("\\'", "", $k);		 
		 }
         $query = "INSERT INTO #__jcron_config SET name='".$k."',
                                                   scope='".$k."',
                                                   value='".$v."'";
         $database->setQuery( $query );
         if (!$database->query()) {

	 	  echo "<script> alert('".$database->getErrorMsg()." on query: ".$query."'); window.history.go(-1); </script>\n";
		  exit();

	     }

        }
    $msg = "Config successfully updated!";
    mosRedirect( 'index2.php?option='.$option."&task=config" , $msg);
    }

    JCRON_HTML::tconfig($option, $task, $_CONFIG);

    }

function  fabout($option, $task, $_CONFIG){

    JCRON_HTML::tabout($option, $task, $_CONFIG);

    }

function  fdefault($option, $task, $_CONFIG){

    JCRON_HTML::tdefault($option, $task, $_CONFIG);
    }

function fcancel($option, $task){

    mosRedirect( 'index2.php?option='. $option );
    
    }
?>
