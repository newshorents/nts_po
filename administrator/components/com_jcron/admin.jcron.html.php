<?php
/**
* @package JCron Scheduler
* @email admin@joomlaplug.com
* @Copyright (C) 2006 JoomlaPlug.com. All rights reserved.
* JCron is a free software released under the GNU license.
* GNU General Access License.
*/

// Restrict direct access
//defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class JCRON_HTML{

     function tconfig($option, $task, $_CONFIG){

         ?>
         <script language="javascript" type="text/javascript">
			function submitbutton(pressbutton) {
				var form = document.adminForm;

                if (pressbutton == 'cancel') {
					submitform( pressbutton );
					return;
				}
                else{
                    submitform( pressbutton );
                    }
           }
         </script>

         <form action="index2.php" name="adminForm" method="post">

         <table class="adminheading">
          <tr>
           <th valign='top'  style="background: url(components/com_jcron/images/logo.png) no-repeat left;">
           JCron Scheduler - Joomla CronTask Manager
          </th>
         </table>
         

         <table class="adminform">
          <tr>
           <th>
            JCron Config Manager
          </th>
         </tr>
         </table>
         
          <?php
          $tabs = new mosTabs(1);
          $tabs->startPane("ConfigPane");
          $tabs->startTab("General","config-general-tab");
          ?>

          <table class='adminform'>

          <tr><td>
          <?php
          if($_CONFIG['iscron'])
           echo "<input type=submit name='crond1' style='color:red;' value='Disable CRON RUN'>";
          else
           echo "<input type=submit name='crond2' style='color:green;' value='Enable CRON RUN'>";
          ?>
          </td></tr>
          <tr>
            <td width='200'>
             Send Cron Logs to Email:
            </td>
            <td>
             <input type="text" size = "30" name="CONFIG[]['email']" value="<?php echo $_CONFIG['email']?>" >
            </td>
          </tr>
          <tr>
            <td width='200'>
             Activate Email Logs:
            </td>
            <td>
             <input type="checkbox" name="CONFIG[]['cemail']" <?php  if($_CONFIG['cemail']==1) echo "checked"?> value='1'>
            </td>
          </tr>

          <tr>
            <td width='200'>
             Enable Database Logs:
            </td>
            <td>
             <input type="checkbox" name="CONFIG[]['dlog']" <?php  if($_CONFIG['dlog']==1) echo "checked"?> value='1'>
            </td>
          </tr>

          </table>
    
          <?php
          $tabs->endTab();
          $tabs->endPane();
          ?>
         <input type="hidden" name="option" value="<?php echo $option; ?>"/>
         <input type="hidden" name="task" value="<?php echo $task;?>"/>
         <input type="hidden" name="task2" value=""/>
         </form>
         <?php

         }
         
     function ttasks($option, $task, $_CONFIG, $data){
         ?>

         <script language="javascript" type="text/javascript">
			function submitbutton(pressbutton) {
				var form = document.adminForm;

                if (pressbutton == 'cancel') {
					submitform( pressbutton );
					return;
				}
                else{
                    submitform( pressbutton );
                    }
           }
         </script>

         <form action="index2.php" name="adminForm" method="post">

         <table class="adminheading">
          <tr>
           <th valign='top'  style="background: url(components/com_jcron/images/logo.png) no-repeat left;">
           JCron Scheduler - View Tasks
          </th>
         </table>
         <table class="adminform">
          <tr>
           <th width="5">#</th>
           <th width='5'>
            <input type="checkbox" name="toggle" value="" onClick="checkAll(<?php echo count( $files ); ?>);" />
          </th>
           <th width='100'>Task Name</th>
           <th width="100">Published</th>
           <th width="200">Running At(Unix mode: minute hour day month year)</th>
           <th width='10'>Last Run Time</th>
           <th width='10'>ID#</th>
         </tr>
         <?php
          foreach($data as $key=>$value){
              $i = $key;
              ?>
              <tr>
              <td><?php echo $i;?></td>
              <td align="center">
                <input type="checkbox" id="cb<?php echo $i ?>" name="cid[<?php echo $i?>]" value="<?php echo $value->id ?>" onclick="isChecked(this.checked);" />
             </td>
             <td>
             <a href='index2.php?option=<?echo $option?>&task=edit&hidemainmenu=1&id=<?php echo $value->id?>'>
             <?php echo $value->task;?>
             </a>
             </td>
             <td>
             <?php

             if($value->published == 1)
              echo "<a href=\"index2.php?option=$option&task=unpublish&id=$value->id\">
                 <img border='0' src='images/tick.png'></a>";
             else
              echo "<a href=\"index2.php?option=$option&task=publish&id=$value->id\">
                 <img border='0' src='images/publish_x.png'></a>";

             ?>
             </td>
             <td><b><?php echo str_replace(" ","   ",$value->mhdmd);?></b></td>
             <td><?php echo $value->ran_at;?></td>
             <td><?php echo $value->id;?></td>
             </tr>
              <?php
              }
         ?>
         </table>

         <input type="hidden" name="option" value="<?php echo $option?>" />
         <input type="hidden" name="task" value="" />
         <input type="hidden" name="boxchecked" value="0" />
         <input type="hidden" name="hidemainmenu" value="0" />
         </form>

         <?php
         }
         
     function ttasks_edit($option, $task, $_CONFIG, $data){
         ?>
         
         <script language="javascript" type="text/javascript">
			function submitbutton(pressbutton) {
				var form = document.adminForm;

                if (pressbutton == 'cancel') {
					submitform( pressbutton );
					return;
				}
                else{
                    submitform( pressbutton );
                    }
           }
         </script>

         <form action="index2.php" name="adminForm" method="post">

         <table class="adminheading">
          <tr>
           <th valign='top'  style="background: url(components/com_jcron/images/logo.png) no-repeat left;">
           JCron Scheduler - Edit/Add Task
          </th>
         </table>

         <table class='adminform'>

          <tr>
            <th colspan='2'>
             CronJob Editor:
            </th>
          </tr>
          
          <tr>
            <td width='150'>
             Task Name:
            </td>
            <td>
             <input type='text' size = '30' name='TASK[task]' value="<?php echo $data->task?>" >
            </td>
          </tr>
          
          <tr>
            <td width='150'>
             Task Type:
            </td>
            <td>
             <select name='TASK[type]'>
             <option value='0' <?php if($data->type==0) echo 'selected';?>>SSH Command</option> 
             <option value='1' <?php if($data->type==1) echo 'selected';?>>Web Address</option>
             </select>
            </td>
          </tr>
 
          <tr>
            <td>
             Command to Run:
            </td>
            <td>
             <input type='text' size = '70' name='TASK[file]' value="<?php echo $data->file?>" >
            </td>
          </tr>



          <tr>
            <td>
             Run at:
            </td>
            <td valign='top'>
             <table>
<tr><td valign='top'>Minute(s):<br>
<select multiple name=minute2 size=10 onchange="update_crontab()">
<option value=*> Every Minute
<option value=*/2> Every Other Minute
<option value=*/5> Every Five Minutes
<option value=*/10> Every Ten Minutes

<option value=*/15> Every Fifteen Minutes
<option value=0> 0
<option value=1> 1
<option value=2> 2
<option value=3> 3
<option value=4> 4
<option value=5> 5
<option value=6> 6
<option value=7> 7

<option value=8> 8
<option value=9> 9
<option value=10> 10
<option value=11> 11
<option value=12> 12
<option value=13> 13
<option value=14> 14
<option value=15> 15
<option value=16> 16

<option value=17> 17
<option value=18> 18
<option value=19> 19
<option value=20> 20
<option value=21> 21
<option value=22> 22
<option value=23> 23
<option value=24> 24
<option value=25> 25

<option value=26> 26
<option value=27> 27
<option value=28> 28
<option value=29> 29
<option value=30> 30
<option value=31> 31
<option value=32> 32
<option value=33> 33
<option value=34> 34

<option value=35> 35
<option value=36> 36
<option value=37> 37
<option value=38> 38
<option value=39> 39
<option value=40> 40
<option value=41> 41
<option value=42> 42
<option value=43> 43

<option value=44> 44
<option value=45> 45
<option value=46> 46
<option value=47> 47
<option value=48> 48
<option value=49> 49
<option value=50> 50
<option value=51> 51
<option value=52> 52

<option value=53> 53
<option value=54> 54
<option value=55> 55
<option value=56> 56
<option value=57> 57
<option value=58> 58
<option value=59> 59
</select><br><br>
</td>

<td valign='top'>Hour(s):<br>
<select multiple name=hour2 size=5  onchange="update_crontab()">
<option value=*> Every Hour
<option value=*/2> Every Other Hour
<option value=*/4> Every Four Hours
<option value=*/6> Every Six Hours
<option value=0> 0 = 12 AM/Midnight
<option value=1> 1 = 1 AM
<option value=2> 2 = 2 AM

<option value=3> 3 = 3 AM
<option value=4> 4 = 4 AM
<option value=5> 5 = 5 AM
<option value=6> 6 = 6 AM
<option value=7> 7 = 7 AM
<option value=8> 8 = 8 AM
<option value=9> 9 = 9 AM
<option value=10> 10 = 10 AM
<option value=11> 11 = 11 AM

<option value=12> 12 = 12 PM/Noon
<option value=13> 13 = 1 PM
<option value=14> 14 = 2 PM
<option value=15> 15 = 3 PM
<option value=16> 16 = 4 PM
<option value=17> 17 = 5 PM
<option value=18> 18 = 6 PM
<option value=19> 19 = 7 PM
<option value=20> 20 = 8 PM

<option value=21> 21 = 9 PM
<option value=22> 22 = 10 PM
<option value=23> 23 = 11 PM
</select>
<br><br>Day(s):<br>
<select multiple name=day2 size=5  onchange="update_crontab()">
<option value=*> Every Day
<option value=1> 1
<option value=2> 2
<option value=3> 3

<option value=4> 4
<option value=5> 5
<option value=6> 6
<option value=7> 7
<option value=8> 8
<option value=9> 9
<option value=10> 10
<option value=11> 11
<option value=12> 12

<option value=13> 13
<option value=14> 14
<option value=15> 15
<option value=16> 16
<option value=17> 17
<option value=18> 18
<option value=19> 19
<option value=20> 20
<option value=21> 21

<option value=22> 22
<option value=23> 23
<option value=24> 24
<option value=25> 25
<option value=26> 26
<option value=27> 27
<option value=28> 28
<option value=29> 29
<option value=30> 30

<option value=31> 31
</select><br><br>
</td><td valign='top'>Months(s):<br>
<select multiple name=month2 size=5  onchange="update_crontab()">
<option value=*> Every Month
<option value=1> January
<option value=2> February
<option value=3> March
<option value=4> April
<option value=5> May

<option value=6> June
<option value=7> July
<option value=8> August
<option value=9> September
<option value=10> October
<option value=11> November
<option value=12> December
</select>
<br><br>Weekday(s):<br>

<select multiple name=weekday2 size=5  onchange="update_crontab()">
<option value=*> Every Weekday
<option value=0> Sunday
<option value=1> Monday
<option value=2> Tuesday
<option value=3> Wednesday
<option value=4> Thursday
<option value=5> Friday
<option value=6> Saturday

</select>
</td></tr>
<script>

var croncount=2;

function updateform2() {

var ccount=2;

<?php
$fs = explode(" ",$data->mhdmd);

echo 'fieldvals=new Array("'.$fs[0].'","'.$fs[1].'","'.$fs[2].'","'.$fs[3].'","'.$fs[4].'");





	if ("'.$fs[0].'" == "") {
		document.adminForm.minute2.options[5].selected = true;
	}
	if ("'.$fs[1].'" == "") {
		document.adminForm.hour2.options[7].selected = true;
	}
	if ("'.$fs[2].'" == "") {
		document.adminForm.day2.options[0].selected = true;
	}
	if ("'.$fs[3].'" == "") {
		document.adminForm.month2.options[0].selected = true;
	}
	if ("'.$fs[4].'" == "") {
		document.adminForm.weekday2.options[0].selected = true;
	}
 ';
 
 
?>

	var ft = fieldvals[0];
	var far = ft.split(",");
	for (t=0;t<document.adminForm.minute2.options.length;t++) {
		for (var loop=0; loop < far.length; loop++)
		{
			if ( document.adminForm.minute2.options[t].value == far[loop]) {
				document.adminForm.minute2.options[t].selected = true;
			}
		}
	}
	var ft = fieldvals[1];
	var far = ft.split(",");
	for (t=0;t<document.adminForm.hour2.options.length;t++) {
		for (var loop=0; loop < far.length; loop++)
		{
			if ( document.adminForm.hour2.options[t].value == far[loop]) {
				document.adminForm.hour2.options[t].selected = true;
			}
		}
	}
	var ft = fieldvals[2];
	var far = ft.split(",");
	for (t=0;t<document.adminForm.day2.options.length;t++) {
		for (var loop=0; loop < far.length; loop++)
		{
			if ( document.adminForm.day2.options[t].value == far[loop]) {
				document.adminForm.day2.options[t].selected = true;
			}
		}
	}
	var ft = fieldvals[3];
	var far = ft.split(",");
	for (t=0;t<document.adminForm.month2.options.length;t++) {
		for (var loop=0; loop < far.length; loop++)
		{
			if ( document.adminForm.month2.options[t].value == far[loop]) {
				document.adminForm.month2.options[t].selected = true;
			}
		}
	}
	var ft = fieldvals[4];
	var far = ft.split(",");
	for (t=0;t<document.adminForm.weekday2.options.length;t++) {
		for (var loop=0; loop < far.length; loop++)
		{
			if ( document.adminForm.weekday2.options[t].value == far[loop]) {
				document.adminForm.weekday2.options[t].selected = true;
			}
		}
	}

}


</script>
</table>

<script>
 function update_crontab(){

 document.adminForm.crontab.value =   document.adminForm.minute2.value+" "+
                  document.adminForm.hour2.value+" "+
                  document.adminForm.day2.value+" "+
                  document.adminForm.month2.value+" "+
                  document.adminForm.weekday2.value;
 }
</script>


            </td>
          </tr>
          <tr>
            <td valign='top'>
             UNIX Crontab:
            </td>
            <td valign='top'>
             <input type=text size=30 name=crontab value='<?php echo $data->mhdmd?>'> Use:<input type=checkbox name=c_crontab value='1'>
            </td>
          </tr>
          
          
          <tr>
            <td valign='top'>
             Published:
            </td>
            <td valign='top'>
             <input type='checkbox' value='1' name='TASK[published]' <?php if($data->published) echo "checked";?> >
            </td>
          </tr>
          
          <tr>
            <td valign='top'>
             Enable Logs:
            </td>
            <td valign='top'>
             <input type='checkbox' value='1' name='TASK[ok]' <?php if($data->ok) echo "checked";?> >
            </td>
          </tr>
          
          <tr>
            <td valign='top'>
              Last Run Time:
            </td>
            <td valign='top'>
             <?php echo $data->ran_at; ?>
            </td>
          </tr>
          <tr>
            <td valign='top'>
             Last Run Log Text:
            </td>
            <td valign='top'>
             <textarea type='checkbox' cols=80 rows=10 name='TASK[log_text]'><?php echo htmlentities($data->log_text); ?></textarea>
            </td>
          </tr>
          
         </table>
         
         <input type="hidden" name="option" value="<?php echo $option?>" />
         <input type="hidden" name="task" value="" />
         <input type="hidden" name="task2" value="<?php echo $task;?>" />
         <input type="hidden" name="id" value="<?php echo $data->id;?>">
         </form>
         <script>
      	 eval ("updateform2()");
         </script>
         
         <?php
         }
         
     function tabout($option, $task, $_CONFIG){
         ?>
         <table class='adminform'>

          <tr>
            <th colspan='2'>
             CronJob Editor:
            </th>
          </tr>
          <tr><td>

<h2>JCron - CronTasks Scheduler </h2>
Powered by <a href='http://www.joomlaplug.com'>JoomlaPlug.com</a>
<img style='float:left; align:top;' src='http://www.joomlaplug.com/images/jcron250x250.jpg' border='0'>
<pre>
 JCron is a Joomla component for Cron Jobs Management and Scheduling! It's purpose is to simulate cron jobs through
Joomla front end interface at preset periods of time for users who either don't have access to the server crontab or
don't want to use it for any purpose!

 <b>How it works:</b>
 
 - in your Configuration area you will see a button called "Enable CRON RUN"
 - by clicking  that a code will be inserted into your template so cronjobs
   can be launched at their preset time from within your Joomla site
 - after that, by clicking the "Disable CRON RUN", the code will be removed as
   long as you haven't done any modifications to it

 Note*: the cronjob is launched at the preset time only if your site gets visits in place, each time a user or
        spider accesses your site you will have the cron running, else you might consider a server side solution!
 Note**: to execute the cron jobs, we use the PHP <b>exec()</b> function, so your provider must have it enabled!


 If you want to manually insert the code into your template, copy/paste the following code:
 <span style='background:#000000;color:#ffffff;'><b><?php echo htmlentities($_CONFIG['incl_code']);?></b></span>

</pre>

For support or suggestions please contact us on our <a href='http://www.joomlaplug.com/Forum/'>Forum</a> or by email <a href='mailto:admin@joomlaplug.com'>admin@joomlaplug.com</a>

          </td></tr>
         </table>

         <?php
         }
         
     function tdefault($option, $task, $_CONFIG){

         }

    }
?>
