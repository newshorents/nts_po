<?php
/**
 * No direct access
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Users Component Controller
 *
 * @package		Joomla
 * @subpackage	Adapters
 * @since 1.5
 */
class SectionController extends Adapters
{
	/**
	 * Save Adapter
	 */
	function save($groupName)
	{
        /**
         * Checking post of adapter menu
         */
            if( !empty($_POST['section']) ){
            $post = $_POST['section'];

                /**
                 * Saving site rules
                 */
                if( !empty($post['Site']) ){
                foreach($post['Site'] as $sectionid => $section){
                    $extraParams = array(
                        '$sectionid' => $sectionid
                    );

                        /**
                         * Saving rule
                         */
                        $resultRuleSite = $this->saveRule("section","site",$groupName,$section['tasks'],$extraParams);
                }
            }

            /**
             * Saving admin rules
             */
            if( !empty($post['Admin']) ){
                foreach($post['Admin'] as $sectionid => $section){
                    $extraParams = array(
                        '$sectionid' => $sectionid
                    );

                        /**
                         * Saving Rule
                         */
						 // MIKE Change site in Admin
                        $resultRuleAdmin = $this->saveRule("section","admin",$groupName,$section['tasks'],$extraParams);
                }
            }
            
            /**
             * Verify result of save
             */
			// MIKE change && in ||
            if( $resultRuleAdmin == TRUE || $resultRuleSite == TRUE ){
                return true;
            }

            return false;
        }
	}
	
	/**
	 * Delete Adapter
	 */
	function delete($groupName)
	{
            $this->deleteRules("com_section","access",$groupName);
            $this->deleteRules("com_section","section",$groupName);
	}
	
	/**
	 * Displays a view
	 */
	function display()
	{
		parent::display();
	}
}