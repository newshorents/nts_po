<script type="text/javascript">
function showSectionAdmin()
{
    var showSectionID = "<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>TableSection"+$("<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>TableSection").value;
    var totalOptions = $("<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>TableSection").options.length;
    for(var numOption = 0;numOption < totalOptions;numOption++){
        var mySectionElem = "<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>TableSection"+ $("<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>TableSection").options[numOption].value;
        $(mySectionElem).style.display = 'none';
    }
    $(showSectionID).style.display = 'block';
}
</script>
<?php $styleTable = "display:block;"; ?>
<?php foreach($this->sectionsList as $sections): ?>
<table class="adminlist" cellspacing="1" id="<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>TableSection<?php echo $sections->id; ?>" style="<?php echo $styleTable; ?>">
	<thead>
		<tr>
			<th width="1%">
				<?php echo JText::_( 'ID' ); ?>
			</th>
			<th width="20%" class="title">
				<?php echo JText::_( 'Sections' ); ?>
			</th>
            <th width="9%" nowrap="nowrap">
                <?php echo JText::_( 'Access Level' ); ?>
            </th>
			<th width="70%" nowrap="nowrap">
				<?php echo JText::_( 'Permisions' ); ?>
			</th>
		</tr>
	</thead>
	<tbody>
	<?php if( empty($sections->sectionList) ): ?>
	<tr class="<?php echo "row0"; ?>">
		<td align="center" colspan="100%"><?php echo JText::_('There are no Sections'); ?></td>
	</tr>
	<?php else: ?>
        <?php $k = 0; ?>
		<?php foreach($sections->sectionList as $sectione): ?>
        <?php $access = JHTML::_('grid.access', $sectione, $k ); ?>
        <?php $k = $k % 2; ?>
		<tr class="<?php echo "row$k"; ?>">
			<td align="center"><?php echo $sectione->id; ?></td>
            
			<td align="center">
                    <a href="#" onclick="showAdapterTasks('<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>Section<?php echo $sectione->id; ?>Tasks')">
                    <?php echo $sectione->title; ?>
                    </a>
            </td>
            <td align="center"><?php echo $access; ?></td>
            <td align="center">
                <?php
                    $groupName = JRequest::getVar( 'groupName' );
                    $extraParams = array(
                        '$sectionid' => $sectione->id
                    );
                    $groupTasks = $this->adapterControl->loadGroupTasks($groupName,$this->adapterName,$this->viewName,$extraParams);
                ?>
    			<div id="<?php echo $this->adapterName; ?><?php echo $this->viewName; ?>Section<?php echo $sectione->id; ?>">
                    <?php
                            if( !empty($groupTasks) ){
                                echo trim(implode(',',$groupTasks));
                            }
                            else{
                                echo JText::_( 'none' );
                            }
                    ?>
                </div>
                <?php
                /**
                 * Loading section params
                 */
                $this->adapterControl->renderTasks($this->adapterName,$this->viewName,$this->tasks,"Section{$sectione->id}",$groupTasks,"[{$sectione->id}]");
                ?>
			</td>
		</tr>
        <?php $k++; ?>
		<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>
<?php $styleTable = "display:none;"; ?>
<?php endforeach; ?>