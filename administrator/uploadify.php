<?php

/*
  Uploadify v2.1.4
  Release Date: November 8, 2010

  Copyright (c) 2010 Ronnie Garcia, Travis Nickels

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

function sanearSrtring($cadena) {
    $patron = Array('/ /' => '_',
        '/"|&|<|>| |¡|¢|£|¤/' => '_',
        '/¥|¦|§|¨|©|«|¬|­|®|¯/' => '_',
        '/±|&sup2;|&sup3;|´|µ|¶|·|÷/' => '_',
        '/°|&sup1;|»|&frac14;|&frac12;|&frac34;|¿/' => '_',
// Sustituir vocales con signo por las vocales ordinarias.//
        '/à|á|â|ã|ä|å|æ|ª/' => 'a',
        '/À|Á|Â|Ã|Ä|Å|Æ/' => 'A',
        '/è|é|ê|ë|ð/' => 'e',
        '/È|É|Ê|Ë|Ð/' => 'E',
        '/ì|í|î|ï/' => 'i',
        '/Ì|Í|Î|Ï/' => 'I',
        '/ò|ó|ô|õ|ö|ø|º/' => 'o',
        '/Ò|Ó|Ô|Õ|Ö|Ø/' => 'o',
        '/ù|ú|û|ü/' => 'u',
        '/Ù|Ú|Û|Ü/' => 'U',
// Algunas consonantes especiales como la ç, la y, la ñ y otras.//
        '/ç/' => 'c',
        '/Ç/' => 'C',
        '/ý|ÿ/' => 'y',
        '/Ý|Ÿ/' => 'Y',
        '/ñ/' => 'n',
        '/Ñ/' => 'N',
        '/þ/' => 't',
        '/Þ/' => 'T',
        '/ß/' => 's',
         "/'/" => '',
        '/"/' => '',
        //Personalizadas
        '/\'/' => '_',
    );
    $cadena=str_replace("'","_",$cadena);
    $response = preg_replace(array_keys($patron), array_values($patron), $cadena);
    return $response;
}

if (!empty($_FILES)) {
    $tempFile = sanearSrtring($_FILES['Filedata']['tmp_name']);
    $targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
    $filename = sanearSrtring($_FILES['Filedata']['name']);
    $targetFile = str_replace('//', '/', $targetPath) . $filename;
//    $targetFile = sanearSrtring($targetFile);
    $fileTypes = str_replace('*.', '', $_REQUEST['fileext']);
    $fileTypes = str_replace(';', '|', $fileTypes);
    $typesArray = split('\|', $fileTypes);
    $fileParts = pathinfo($_FILES['Filedata']['name']);

    if (in_array($fileParts['extension'], $typesArray)) {
        // Uncomment the following line if you want to make the directory if it doesn't exist
        mkdir(str_replace('//', '/', $targetPath), 0755, true);

        move_uploaded_file($tempFile, $targetFile);
//        echo str_replace($_SERVER['DOCUMENT_ROOT'], '', $targetFile);
        echo $filename;
    } else {
        echo '0';
    }
}
?>